import { PLATE_SPECS } from '../../src/common/plate/plate.specs';
import { WellUtils } from '../../src/common/plate/well.utils';
import { PATTERNS } from '../../src/common/regex/patterns';
import { CsvUtils } from '../../src/common/csv/tools/csv.utils';

let plate = PLATE_SPECS.P96;
let well = plate.well({ order: 13 });
console.log('P96 横排 well=>', 13, well);
well = plate.well({ order: 13, isHorizontal: false });
console.log('P96 竖排 well=>', 13, well);

plate = PLATE_SPECS.P384;
well = plate.well({ order: 2 });
console.log('P384 横排 well=>', 2, well);
well = plate.well({ order: 2, isHorizontal: false });
console.log('P384 竖排 well=>', 2, well);

well = plate.well({ order: 97 });
console.log('P384 横排 well=>', 97, well);
well = plate.well({ order: 97, isHorizontal: false });
console.log('P384 竖排 well=>', 97, well);

let wellOrder = 192;
well = plate.well({ order: wellOrder });
console.log('P384 横排 well=>', wellOrder, well);
well = plate.well({ order: wellOrder, isHorizontal: false });
console.log('P384 竖排 well=>', wellOrder, well);

wellOrder = 193;
well = plate.well({ order: wellOrder });
console.log('P384 横排 well=>', wellOrder, well);
well = plate.well({ order: wellOrder, isHorizontal: false });
console.log('P384 竖排 well=>', wellOrder, well);

let str = 'A01_WR21P00006P_M60K_032901_WL2101616.CEL_call_code';
let wstr = WellUtils.get(str);
console.log(str, wstr);

str = 'A01.WR21P00006P_M60K_032901_WL2101616.CEL_call_code';
wstr = WellUtils.get(str);
console.log(str, wstr);

str = 'WR21P00006P_M60K_032901_A01_WL2101616.CEL_call_code';
wstr = WellUtils.get(str);
console.log(str, wstr);

str = 'WR21P00006P_M60K_032901.A01_WL2101616.CEL_call_code';
wstr = WellUtils.get(str);
console.log(str, wstr);

str = 'WR21P00006P_M60K_032901_WL2101616_A01.CEL_call_code';
wstr = WellUtils.get(str);
console.log(str, wstr);

str = 'WR21P00006P_M60K_032901_WL2101616.A01_CEL_call_code';
wstr = WellUtils.get(str);
console.log(str, wstr);

str = 'AX-107934621\tA/A\tA/A\t"G/G\'\tA/A\tA/G\tA/A\tA/A\tA/A\tA/A\tA/A\tG/G\tA/A\tA/A\tA/A\tA/A\tA/A\tG/G\tA/A\tG/G\tG/G\tA/A\tA/A\tA/A\tA/A\tA/A\tG/G\tG/G\tG/G\tA/A\tA/A';
let cols = CsvUtils.parse({ row: str });
console.log('COLS=>', cols);

console.log('COLS2=>', str.split(/[\t, ]/));
