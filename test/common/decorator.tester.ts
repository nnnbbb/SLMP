/**
 * 方法装饰器，允许在待装饰函数执行前后调用指定的切面函数列列
 *
 * @param callbacks.befores 前置切面函数
 * @param callbacks.afters 后置切面函数
 * @return
 * @author jiang
 * @date 2021-01-16 18:07:59
 **/
export function aop(callbacks: { befores?: any[]; afters?: any[] }) {
  /**
   * @param _target
   * @param name 被装饰方法的名称
   * @param descriptor <pre>被装饰的方法体描述对象，格式如下:
   * {
   *    value: [Function: actionb],
   *    writable: true,
   *    enumerable: false,
   *    configurable: true
   * }</pre>
   **/
  return (_target, name, descriptor) => {
    // 获取被装饰的函数
    const origin = descriptor.value;

    // 生成一个新函数，在待装饰函数前后分别调用装饰回调方法
    descriptor.value = function (...args) {
      //注：args是待装饰函数传入的参数

      //执行前置的装饰函数
      let result = undefined;
      for (const callback of callbacks.befores) {
        result = callback(this, result, ...args);
      }

      //将前置装饰函数的处理结果合并到待装饰函数中,
      //若前置函数修改了待装饰函数参数值则会被更新到参数表中
      args = { ...args, ...result };

      // 执行待装饰的函数,apply改变this的指向
      result = origin.apply(this, args);

      //执行后置装饰函数
      for (const callback of callbacks.afters) {
        result = callback(this, result, args) || result;
      }
      return result;
    };

    return descriptor;
  };
}

// 不能用箭头函数
function action(target: any, key: string) {
  const origin = target[key];

  // aop
  target[key] = function (...args) {
    // TODO:执行前置切面方法
    console.log('before');

    // 执行待装饰函数
    let result = origin.apply(this, args);

    // TODO:执行后置切面函数
    console.log('after');

    return result;
  };

  return target[key];
}

function log1() {
  // eslint-disable-next-line prefer-rest-params
  console.log('打印器1!', arguments);
}

function log2() {
  // eslint-disable-next-line prefer-rest-params
  console.log('打印器2!', arguments);
}

class NodeTest {
  constructor(public x: number, public y: number) {}

  @action
  action(a?: number, b?: string) {
    console.log('start---------->');
    console.log(this);
    console.log('end---------->');
    return 'hello aop';
  }
}

console.log(new NodeTest(1, 1).action(1, 'xcsd'));
