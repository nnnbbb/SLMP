import * as moment from 'moment';
import { RegexUtils } from '../../src/common/utils/regex.utils';

const cn = 'YYYY年MM月DD日';
const en = 'YYYY-MM-DD';
let dateStr = moment(new Date()).format(cn);
console.log(dateStr);

const str1 = '@BJYJ2100001';
let regex = /^@([0-9a-zA-Z]+)$/g;
console.log('-------------1111--------------');
let array = regex.exec(str1);
console.log(array[0]);
console.log(array[1]);
console.log(array.length);

console.log('-------------2222--------------');
let items = RegexUtils.match(/^@([0-9a-zA-Z]+)$/g, str1);
console.log(items);
