import { SysUser } from '../../src/sys/entity/sys.user.entity';
import { ExcelCreator } from '../../src/common/excel/tools/excel.creator';
import { ExcelUtils } from '../../src/common/excel/tools/excel.utils';

//测试示例代码
let excel = new ExcelCreator();
ExcelUtils.save(
  [
    {
      name: 'sheet1',
      data: [
        [new Date().toDateString(), '2', '3'],
        ['4', 'jiangbin', '6'],
      ],
    },
    {
      name: 'sheet2',
      data: [
        ['11', '22', '33'],
        ['44', '55', '66'],
      ],
    },
  ],
  '/work/test.xlsx',
);

let arr = [];
arr.push([1, 2, 4, 2, 4]);
arr.push([3, 4, 43]);
console.log(arr);
arr.splice(0, 0, ['A', 'B', 'C']);
console.log(arr);

let str = 'xxxx/xadfa/xadf/.xls/aaa.xls';
console.log(str);
let index = str.lastIndexOf('.xls');
console.log(index);
console.log(str.substr(index));

let users = [];

let user = new SysUser();
user.userPassword = '2222';
user.userLoginName = '1';
user.userAddr = '3adads';
user.userEmail = 'fdsagfdsa';
users.push(user);

user = new SysUser();
user.userPassword = '2222';
user.userLoginName = '2';
user.userAddr = '3adads';
user.userEmail = 'fdsagfdsa';
users.push(user);

user = new SysUser();
user.userPassword = '2222';
user.userLoginName = '3';
user.userAddr = '3adads';
user.userEmail = 'fdsagfdsa';
users.push(user);
