import { ArrayUtils } from '../../src/common/utils/array.utils';

let array1 = [1, 2, 3, 4];
let array2 = [10];
let array3 = array2.concat(array1);
console.log(JSON.stringify(array3));

let target = ArrayUtils.addFirst(122, array3);
console.log(JSON.stringify(target));
