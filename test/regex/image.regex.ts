import { ImageUtils } from '../../dist/common/images/image.utils';
import { FileUtils } from '../../dist/common/utils/file.utils';

const base64Img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0';
let results = base64Img.match(/^data:image\/(\w+);base64,(.*)/);
console.log(results[0]);
console.log(results[1]);
console.log(results[2]);
const base64Img2 =
  'data:image/gif;base64,R0lGODdhggCCAIAAAAAAAP///ywAAAAAggCCAAAC/4yPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvHEUDXto3gCc3wgQ+8+QxCYq13/AGMQ6WwOHk+c0kq0oh13qg6naK71HKl1Vn4ukuSy01ksC1WOoLWBnx+XsDB6zr+nccUdndAVxiYRgFouAYl2ETolcY3WAlYB0WIlmnZCLl16Ib42JlFiQmqqdfJmJpnKEgaq+UYt/eKKysXxVqp+5k1y7kIKowIHErry/tbunvrl9z3vCzVjAqm2GuqthwNrZxNTUocTC0uoXp5yGnePlnMXr2daDdqjzdLPj8GKy9pjBvAXWjSeeI3LmErbgwVIlwYbgovTw0hWnyIsWLGiP+1QKzTxxHhmIZ/cKli8THOPogjE5ZsSYLYQGyu1lFkpvJfr5lmVt4TWNPZTYMgm1nb4M9fQIJMl2osmlOptDLa3HmTR7KprVwObyGL5KzqVFFfuqFTZu6bWZMSwVr4em8gpaSuCm79dNTtxKu60rqsVwvvzng0xfEkqzXwMb5TJQn2qTdvWDNgIwc9BW8YW8FwgZ6Eh7is0aBlJXPezLXV4QqqF9tc3LIc1mRW92XV0Lq0UNizZYNE5rmbX4OY7Z496hNq8NXT3pLG1xv16eKAyZTsSFm4TtFou1t+DNohqr576cql9467Y6pbs7b+DAG7e+1Ah3tfCz+kVOc8zV//fS3Ka6v109ZPqww4VnW2RbXbeRflplV8hEEHjkyTCcieYbs5xVo8/pEnlWKmcSUhckh5+B9+qbnGhnUMEicRB2fV92Jj9DwV0YKYzbiXXxiqZcx6Gmm241r2SbgUeOAQuJyLQ6JXIG4bMsWjV9od1CKNRNJ3gW/3EWiiknXpJBtwOWHgZXr6ZRndcQW6CVOXb0o34XoiuqjXNQdOVuJzkLEIJp08WjXoXQZyF42WUYr3wIMsImgXkvXQpONG+XT1KHtEdfhOiFCKeWRmU+qZEowa+vgpW7fZUySTmIoFIG2yxmbkKoRmGGOEk+IUK6qIUTfedqKaGWlPZDpJJW+6/5lIq4WByofTn0LOqutQ4c3VorXOrfqhsIohKitLCu6aX7AVquotiuDW9lewQHK6qJ3oevreorfiCCmr8WY7L2MaTmPmqecc2iiy97ab45mvHuvquaEuW2e/qGqm3oW5qonjphbKG+h8WLJb5mDNGbsxvx3/hSVdwTU5MowHq9nquKdVHHCK+dLaLJcxm4sazSSiPGGfC667J9GeBhkxwLm6nDDRuhntb4NJ47l0weCW7POdYaobHciaRhuuyLyNbO23XSerXIdDh0Spru4a+nS6Zm/bNNs2Pgx0qQxiS2x2OMPdXcNXRt1ygnHyJzHg2FHM6LNf00sw4p952WTb0//yHbnbciJI+cCX16ootE6rndjgDpqsOcelK5qonBPPczqo+v7Ns8onniyutCn+FpeKra8tVqXcHuR130gbXzfTnx9eo5uMbzK414jzfnCcvSbM3JS2l+v5YGhexqaVK89ut8bgcy+42eJ3/1L5YDtvsfoqPr9n7GzSzSF1Wx8/53W4zl0eqr3rSZYjXNTad7P6CTBstVtR7tqzq7glsE+O6tSYsHW1MY3PdlISmNJI5TtyaZB9QKPbtFp3LmfZiHHT4dcEq9O3Jd2tbliDYHIY1oHvoIhZZ8ua9wAXNxn1bm2F41Bhfgis2XzvZajLmA1JGKsRiYl73VKdE+H0JQj/SrFfVDSY3SojkmRd5Fe4ol4A7VcogTFxVUjz2e8ykEJlheeGl9Lbx5SYw7GVsWjHmhzsMrU19PEqhKGhlhpLd702jgBD6tjj32JYq/Ul8I5q+dEM9yMqqFhwdAHy4hQtliSC0Q+EnLQVheLnJ1OiUGxxlODXitVA3L1ydTD0JJ+ASLILXsyR97nJ8uS2wC6C8E4ZxBz9/FOyB9ZRl3q7YvSEpadbARBWzLylM8Wltd318WfUPKR9UjIiRVZyj50znzerhcoi2bFnMguasYwzJ9F1r5EyrBfGwFbM881ShYWU3prw1skt5tOHvrqWO1cpz4DGU47niaYhCQmdk8lgHKIUrahFL4rRjGp0oxztqEc/CtKQinSkJD1AAQAAOw==';
let results2 = base64Img2.match(/^data:image\/(\w+);base64,(.*)/);
console.log(results2[0]);
console.log(results2[1]);
console.log(results2[2]);

const getBase64ImgDetails = (contents: string) => {
  let results = contents.match(/^data:image\/(\w+);base64,(.*)/);
  //匹配后results中第一项为contents内容，第二项为扩展名，第三项为base64解码后的字节流内容
  return results?.length == 3 ? { ext: results[1], buffer: Buffer.from(results[2], 'base64') } : undefined;
};

let results3 = getBase64ImgDetails(base64Img2);
console.log(results3.ext);
console.log(results3.buffer.toString('base64'));

const data = base64Img2.slice('data:image/gif;base64,'.length);
console.log(data);

console.log('------------------liuwenbin-------------');
let buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/liuwenbin.png' });
console.log(buffer.toString('base64'));
console.log('------------------liuyawei-------------');
buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/liuyawei.png' });
console.log(buffer.toString('base64'));
console.log('------------------wangfengge-------------');
buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/wangfengge.png' });
console.log(buffer.toString('base64'));
console.log('------------------yuboyang-------------');
buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/yuboyang.png' });
console.log(buffer.toString('base64'));
console.log('------------------yumizhong-------------');
buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/yumizhong.png' });
console.log(buffer.toString('base64'));
console.log('------------------qianming-------------');
buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/qianming.png' });
console.log(buffer.toString('base64'));
console.log('------------------erweima-------------');
buffer = ImageUtils.getImageData({ imagePath: '/Volumes/work/images/erweima.png' });
console.log(buffer.toString('base64'));
