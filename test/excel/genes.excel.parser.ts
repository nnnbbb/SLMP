import { SampleNameReviser } from '../../src/ssr/reivser/sample.name.reviser';
import { GroupMapper } from '../../src/common/mapper/group.mapper';
import { ExcelParser } from '../../dist/common/excel/tools/excel.parser';
import { ExcelUtils } from '../../dist/common/excel/tools/excel.utils';
import { SheetDataType } from '../../src/common/excel/tools/sheet.data';

const sFilePath1 = '/Volumes/work/gene/gjr/比对结果-0-30-20210219.xlsx';
const sFilePath2 = '/Volumes/work/gene/gjr/比对结果-5-30-20210219.xlsx';

/**

 * 解析比对结果文件
 * @param filePath
 * @return
 * @author: jiangbin
 * @date: 2021-02-20 16:13:25
 **/
const parserResults = (filePath) => {
  //解析数据文件
  const excel = new ExcelParser();
  let sheets = excel.parse(sFilePath1);

  //获取数据行列表
  let rows = ExcelUtils.getAllRows(sheets);
  console.log('rows.length=>', rows.length);

  //转换成json对象
  let colIndex = 0;
  let objects = excel.convert(sheets, (params: { sheet: SheetDataType; index: number; row: any[] }) => {
    let { row } = params;
    return {
      sSamBarcode: row[colIndex++],
      sSamOriginNum: row[colIndex++],
      sSamName: row[colIndex++],
      sSamSource: row[colIndex++],
      tSamBarcode: row[colIndex++],
      tSamOriginNum: row[colIndex++],
      tSamName: row[colIndex++],
      tSamSource: row[colIndex++],
      markerCount: row[colIndex++],
      diffCount: row[colIndex++],
      diffMarkers: row[colIndex++],
      missCount: row[colIndex++],
      missMarkers: row[colIndex++],
    };
  });

  //校正样品名称
  objects.forEach((object) => {
    object['sSamRealName'] = SampleNameReviser.revise(object['sSamName']);
    object['tSamRealName'] = SampleNameReviser.revise(object['tSamName']);
  });

  return objects;
};

/**
 * 比对结果列表的关联分组功能
 * @author: jiangbin
 * @date: 2021-02-20 16:24:35
 **/
const CompareResultGrouper = {
  /**
   * 分组结果映射表
   * @param sources
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 15:58:39
   **/
  groupResults: (sources) => {
    //构建分组映射表
    let sSamNameGroup = new GroupMapper();
    let tSamNameGroup = new GroupMapper();
    let sSamBarcodeGroup = new GroupMapper();
    let tSamBarcodeGroup = new GroupMapper();

    sources.forEach((object) => {
      if (object.sSamRealName != '/' && object.sSamRealName != '') {
        sSamNameGroup.add(object.sSamRealName, [object]);
      }
      if (object.tSamRealName != '/' && object.tSamRealName != '') {
        tSamNameGroup.add(object.tSamRealName, [object]);
      }
      sSamBarcodeGroup.add(object.sSamBarcode, [object]);
      tSamBarcodeGroup.add(object.tSamBarcode, [object]);
    });

    return { sSamNameGroup, tSamNameGroup, sSamBarcodeGroup, tSamBarcodeGroup };
  },

  /**
   * 合并结果数据列表
   * @param sources
   * @param targets
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 15:59:15
   **/
  uniqueResults: (sources: [], targets: []) => {
    if ((!sources || sources.length == 0) && (!targets || targets.length == 0)) {
      return null;
    }
    if (!sources || sources.length == 0) {
      return targets;
    }
    if (!targets || targets.length == 0) {
      return sources;
    }

    let all = sources.concat(targets);
    let results = [];
    all.forEach((item) => {
      let index = results.indexOf(item);
      if (index == -1) {
        results.push(item);
      }
    });
    return results;
  },

  /**
   * 过滤待测样品条码号结果列表
   * @param groups
   * @param results
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 16:01:17
   **/
  filterSourceSamBarcodeResults: (groups, results) => {
    //待测样品条码号
    let samBarcodes = results.map((result) => result.sSamBarcode);
    //待测结果记录
    let values = groups.sSamBarcodeGroup.getAllValueList();
    if (values?.length > 0) {
      groups.sSamBarcodeGroup.delete(samBarcodes);
      results = CompareResultGrouper.uniqueResults(results, values);
      results?.length > 0 &&
        results.forEach((result) => {
          groups.sSamBarcodeGroup.delete(result.sSamBarcode);
        });
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    //对照结果记录
    values = groups.tSamBarcodeGroup.getAllValueList();
    if (values?.length > 0) {
      groups.tSamBarcodeGroup.delete(samBarcodes);
      results = CompareResultGrouper.uniqueResults(results, values);
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    return results;
  },

  /**
   * 过滤对照样品条码号结果列表
   * @param groups
   * @param results
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 16:02:30
   **/
  filterTargetSamBarcodeResults: (groups, results) => {
    //待测样品条码号
    let samBarcodes = results.map((result) => result.tSamBarcode);
    //待测结果记录
    let values = groups.sSamBarcodeGroup.getAllValueList();
    if (values?.length > 0) {
      groups.sSamBarcodeGroup.delete(samBarcodes);
      results = CompareResultGrouper.uniqueResults(results, values);
      results?.length > 0 &&
        results.forEach((result) => {
          groups.sSamBarcodeGroup.delete(result.sSamBarcode);
        });
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    //对照结果记录
    values = groups.tSamBarcodeGroup.getAllValueList();
    if (values?.length > 0) {
      groups.tSamBarcodeGroup.delete(samBarcodes);
      results = CompareResultGrouper.uniqueResults(results, values);
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    return results;
  },

  /**
   * 过滤对照样品名称结果列表
   * @param groups
   * @param results
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 16:02:50
   **/
  filterSourceSamNameResults: (groups, results) => {
    //待测样品名称
    let samNames = results.map((result) => result.sSamName);
    //待测结果记录
    let values = groups.sSamNameGroup.getAllValueList();
    if (values?.length > 0) {
      groups.sSamNameGroup.delete(samNames);
      results = CompareResultGrouper.uniqueResults(results, values);
      results?.length > 0 &&
        results.forEach((result) => {
          groups.sSamBarcodeGroup.delete(result.sSamBarcode);
        });
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    //对照结果记录
    values = groups.tSamNameGroup.getAllValueList();
    if (values?.length > 0) {
      groups.tSamNameGroup.delete(samNames);
      results = CompareResultGrouper.uniqueResults(results, values);
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    return results;
  },

  /**
   * 过滤对照样品名称结果列表
   * @param groups
   * @param results
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 16:02:53
   **/
  filterTargetSamNameResults: (groups, results) => {
    //待测样品名称
    let samNames = results.map((result) => result.tSamName);
    //待测结果记录
    let values = groups.sSamNameGroup.getAllValueList();
    if (values?.length > 0) {
      groups.sSamNameGroup.delete(samNames);
      results = CompareResultGrouper.uniqueResults(results, values);
      results?.length > 0 &&
        results.forEach((result) => {
          groups.sSamBarcodeGroup.delete(result.sSamBarcode);
        });
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    //对照结果记录
    values = groups.tSamNameGroup.getAllValueList();
    if (values?.length > 0) {
      groups.tSamNameGroup.delete(samNames);
      results = CompareResultGrouper.uniqueResults(results, values);
      results = CompareResultGrouper.filterSameGroup(groups, results);
    }
    return results;
  },

  /**
   * 过滤出待测和对照样品条码号及样品名称关联的结果记录
   * @param groups
   * @param results
   * @return
   * @author: jiangbin
   * @date: 2021-02-20 16:03:20
   **/
  filterSameGroup: (groups, results) => {
    results = CompareResultGrouper.filterSourceSamBarcodeResults(groups, results);
    results = CompareResultGrouper.filterTargetSamBarcodeResults(groups, results);
    results = CompareResultGrouper.filterSourceSamNameResults(groups, results);
    results = CompareResultGrouper.filterTargetSamNameResults(groups, results);
    return results;
  },

  /**
   * 差异名称分组
   * @param sources
   * @author: jiangbin
   * @date: 2021-02-20 16:04:10
   **/
  diff: (sources) => {
    if (!sources || sources.length == 0) {
      return null;
    }
    //分组比对结果列表
    let groups = CompareResultGrouper.groupResults(sources);

    let mapper = new GroupMapper();
    sources.forEach((source) => {
      //待比样品条码号
      let sSamBarcode = source.sSamBarcode;

      //获取比对结果记录
      let results = groups.sSamBarcodeGroup.get(sSamBarcode);
      if (!results || results.length == 0) return;

      //移除已有记录
      groups.sSamBarcodeGroup.delete(sSamBarcode);

      //过滤出单个结果关联的分组
      let group = CompareResultGrouper.filterSameGroup(groups, results);
      if (group?.length > 0) {
        mapper.add(sSamBarcode, group);
      }
    });
    return mapper;
  },
};
