import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SysModule } from './sys/sys.module';
import { ErrorsInterceptor } from './common/interceptor/errors.interceptor';
import { ScheduleModule } from '@nestjs/schedule';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AuthModule } from './common/auth/auth.module';
import { UrllogInterceptor } from './common/interceptor/urllog.interceptor';
import { PmsGuard } from './common/auth/guard/pms.guard';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { JwtAuthGuard } from './common/auth/guard/jwt.auth.guard';
import { Log4jsModule } from './common/log4js/log4js.module';
import { FileModule } from './common/file/file.module';
import { SysRequestContext } from './common/context/sys.request.context';
import { RequestContextModule } from '@medibloc/nestjs-request-context';
import { DocxModule } from './common/docx/docx.module';
import { ExcelModule } from './common/excel/excel.module';
import { SystemModule } from './common/system/system.module';
import { MailModule } from './common/mail/mail.module';
import { DbModule } from './common/db/db.module';
import { ApiConfigModule } from './common/config/api.config.module';
import { LocaleInterceptor } from './common/interceptor/locale.interceptor';

@Module({
  imports: [
    //上下文对象
    RequestContextModule.forRoot({
      contextClass: SysRequestContext,
      isGlobal: true,
    }),
    //log4js日志模块
    Log4jsModule.forRoot(),
    //参数模块
    ApiConfigModule,
    //数据库连接
    DbModule,
    //定时任务
    ScheduleModule.forRoot(),
    //事件
    EventEmitterModule.forRoot(),
    //身份认证
    AuthModule,
    //文件模块
    FileModule,
    //Word模块
    DocxModule,
    //Excel模块
    ExcelModule,
    //邮件模块
    MailModule,
    //系统模块
    SystemModule,
    //业务模块
    SysModule,
  ],
  controllers: [AppController],
  providers: [
    {
      //国际化参数
      provide: APP_INTERCEPTOR,
      useClass: LocaleInterceptor,
    },
    {
      //错误消息
      provide: APP_INTERCEPTOR,
      useClass: ErrorsInterceptor,
    },
    {
      //打印URL
      provide: APP_INTERCEPTOR,
      useClass: UrllogInterceptor,
    },
    {
      //序列化,自动过滤敏感信息，插件：class-transformer
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      //身份认证守卫
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      //功能权限验证
      provide: APP_GUARD,
      useClass: PmsGuard,
    },
    AppService,
  ],
  exports: [AppService],
})
export class AppModule {}
