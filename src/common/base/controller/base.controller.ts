import { BaseLogger } from '../logger/base.logger';

/**
 * 基础控制器类，用于提供一些公共方法或组件，例如直接获取日志类
 * @author: jiangbin
 * @date: 2021-01-14 18:09:59
 **/
export class BaseController extends BaseLogger {}
