import { LogUtils } from '../../log4js/log4js.utils';

/**
 * 基础日志类，用于提供获取日志对象类
 * @author: jiangbin
 * @date: 2021-01-14 18:09:59
 **/
export class BaseLogger {
  /**
   * 获取日志对象
   * @author: jiangbin
   * @date: 2021-01-14 18:06:52
   **/
  logger() {
    return LogUtils.getLogger(this.constructor.name);
  }
}
