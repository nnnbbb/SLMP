/**
 * 实体类的SQL接口定义
 * @author: jiangbin
 * @date: 2021-04-09 10:03:13
 **/
export declare type EntitySql = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: any;
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: any;
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: any;
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: any;
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: any;
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: any;
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: any;
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: any;
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: any;
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: any;
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: any;
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: any;
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: any;
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: any;
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: any;
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: any;
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: any;
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: any;
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: any;
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: any;
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: any;
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: any;
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: any;
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: any;
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: any;
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: any;
};
