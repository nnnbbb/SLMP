/**
 * 树节点的类型
 * @author: jiangbin
 * @date: 2021-01-04 10:08:07
 **/
export const TreeNodeType = {
  /**
   * 目录节点
   * @author: jiangbin
   * @date: 2021-01-04 10:06:15
   **/
  DIR: { type: 'DIR', label: '目录' },
  /**
   * 叶子节点即数据节点
   * @author: jiangbin
   * @date: 2021-01-04 10:06:23
   **/
  DATA: { type: 'DATA', label: '数据' },
};
/**
 * 根节点ID
 * @author: jiangbin
 * @date: 2021-01-04 10:10:01
 **/
export const ROOT_NODE_ID = '1';
/**
 * 目录节点类型
 * @author: jiangbin
 * @date: 2021-03-16 15:41:41
 **/
export const DIR_NODE = TreeNodeType.DIR.type;
/**
 * 数据节点类型
 * @author: jiangbin
 * @date: 2021-03-16 15:41:45
 **/
export const DATA_NODE = TreeNodeType.DATA.type;
