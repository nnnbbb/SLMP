import { Body, Controller, Post, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { FileUtils } from '../../utils/file.utils';
import * as fs from 'fs';
import { BaseController } from '../../base/controller/base.controller';
import { PathUtils } from '../../path/path.creator.utils';
import { SysFiles } from '../../../sys/entity/sys.files.entity';
import { SysServices } from '../../../sys/sys.services';
import { UserContext } from '../../context/user.context';
import { SYS_ENABLE_STATE_DICT, SYS_SWITCH_DICT } from '../../dictionary/sys.dict.defined';
import { uuid } from '../../utils/uuid.utils';
import { PathResult } from '../../path/path.creator';

/**
 * @description:
 * @author: jiang
 * @date: 2020-12-06 10:33:47
 * @class:
 **/
@Controller('file')
@ApiTags('文件上传')
export class FileUploadController extends BaseController {
  /**
   * <pre>上传单个文件,file的数据格式如下：
   * <code>
   *  {
   *   fieldname:'file',//文件字段名
   *   originalname:'原始文件名',
   *   encoding:"7bit",
   *   mimetype:"application/octet-stream",
   *   size:1234,//文件长度
   *   buffer:{
   *      type:"Buffer",
   *      data:[],//文件内容
   *     }
   *  }
   *  </code>
   *  </pre>
   * @param file
   * @return
   * @author jiang
   * @date 2020-12-06 10:42:54
   **/
  @ApiOperation({ summary: '上传单个文件' })
  @ApiQuery({ name: 'file', description: '文件参数名' })
  @ApiQuery({ name: 'body', description: '附加参数' })
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file, @Body() body) {
    try {
      //创建文件路径信息
      const pathResult = this.createFilePath(file.originalname, body?.subFolder);

      pathResult.fieldName = file.fieldname;
      pathResult.originalName = file.originalname;
      pathResult.size = file.size;

      this.logger().log('创建文件路径信息', JSON.stringify(pathResult));

      //创建目录路径
      const filePath = pathResult.path;
      FileUtils.createDir(filePath);

      //写磁盘文件
      const writer = fs.createWriteStream(filePath);
      writer.write(file.buffer);
      this.logger().log('写文件成功', filePath);

      //写文件记录
      await this.createFileRecord(pathResult);

      return {
        status: 'OK',
        ...pathResult,
      };
    } catch (err) {
      this.logger().error('上传文件失败==>', err);
      throw err;
    }
  }

  /**
   * 上传多个文件
   * <code>
   *  {
   *   fieldname:'file',//文件字段名
   *   originalname:'原始文件名',
   *   encoding:"7bit",
   *   mimetype:"application/octet-stream",
   *   size:1234,//文件长度
   *   buffer:{
   *      type:"Buffer",
   *      data:[],//文件内容
   *     }
   *  }
   *  </code>
   * @param files 文件数组
   * @param body 附加参数
   */
  @ApiOperation({ summary: '上传多个文件' })
  @ApiQuery({ name: 'files', description: '文件参数名' })
  @ApiQuery({ name: 'body', description: '附加参数' })
  @Post('uploads')
  @UseInterceptors(FilesInterceptor('files'))
  async uploadFiles(@UploadedFiles() files, @Body() body) {
    try {
      const results = [];
      for (const file of files) {
        const pathResult = this.createFilePath(file.originalname, body?.subFolder);

        pathResult.fieldName = file.fieldname;
        pathResult.originalName = file.originalname;
        pathResult.size = file.size;
        pathResult['fileId'] = uuid();

        this.logger().log('创建文件路径信息', JSON.stringify(pathResult));

        const filePath = pathResult.path;
        FileUtils.createDir(filePath);

        const writer = fs.createWriteStream(filePath);
        writer.write(file.buffer);

        //写文件记录
        await this.createFileRecord(pathResult);

        results.push({
          status: 'OK',
          ...pathResult,
        });
      }
      return results;
    } catch (err) {
      this.logger().error('批量上传文件失败=>', err);
      throw err;
    }
  }

  /**
   * 创建文件路径
   *
   * @param fileName 文件名
   * @return 文件路径构建结果
   * <br/><br/>
   * Create by WuHaotian on 2021-01-13
   */
  createFilePath(fileName: string, subFolder?: string) {
    const fileExt = FileUtils.getFileExt(fileName);
    return PathUtils.getPath({ subFolder, fileExt });
  }

  /**
   * 创建文件记录，用于以后可以追溯所有上传的文件信息
   * @param pathResult
   * @author: jiangbin
   * @date: 2021-02-02 08:58:37
   **/
  async createFileRecord(pathResult: PathResult) {
    const file: SysFiles = {
      fileId: pathResult['fileId'],
      fileBarcode: null,
      fileRelateId: null,
      fileName: pathResult.originalName,
      fileNameEn: pathResult.originalName,
      filePath: pathResult.relativePath,
      fileIsUsed: SYS_SWITCH_DICT.YES.value,
      fileOwner: UserContext.loginName,
      fileSize: pathResult.size,
      fileGrants: null,
      fileClasses: null,
      fileState: SYS_ENABLE_STATE_DICT.ENABLE.value,
      fileType: pathResult.fileExt,
      fileComments: null,
      fileSpecies: UserContext.species,
      fileCreateDate: null,
      fileUpdateDate: null,
    };
    await SysServices.sysFilesService.create(file);
  }
}
