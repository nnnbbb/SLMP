import { forwardRef, Global, Module } from '@nestjs/common';
import { SysModule } from '../../sys/sys.module';
import { FileDownloadController } from './controller/file.download.controller';
import { FileUploadController } from './controller/file.upload.controller';

@Global()
@Module({
  imports: [forwardRef(() => SysModule)],
  controllers: [FileDownloadController, FileUploadController],
  providers: [],
  exports: [],
})
export class FileModule {}
