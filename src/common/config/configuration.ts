import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { join } from 'path';
import { Logger } from '@nestjs/common';

/**
 * 获取系统的yaml配置文件路径，会根据给定的NODE_ENV文件自动选择对应的配置文件进行加载
 * @author jiang
 * @date 2020-12-06 12:42:15
 **/
const getYamlConfigFile = () => {
  Logger.log(`项目根目录==>${join(__dirname, '../../../')}`);

  //'development', 'production', 'test'
  const { NODE_ENV } = process.env;
  Logger.log(`运行模式==>${NODE_ENV}`);

  if (!NODE_ENV || NODE_ENV === 'development') {
    Logger.log('development==>开发模式==>config/development.yml!');
    return 'config/development.yml';
  }
  if (NODE_ENV === 'production') {
    Logger.log('production==>生产模式==>config/production.yml!');
    return 'config/production.yml';
  }
  if (NODE_ENV === 'test') {
    Logger.log('test==>测试模式==>config/test.yml!');
    return 'config/test.yml';
  }
  Logger.log('development==>开发模式!');
  return 'config/development.yml';
};

/**
 * 系统yaml配置文件路径
 * @author jiang
 * @date 2020-12-06 12:47:03
 **/
const YAML_CONFIG_FILENAME = getYamlConfigFile();

/**
 * 加载系统基础参数文件
 * @author jiang
 * @date 2020-12-06 12:16:35
 **/
export default () => {
  return yaml.load(fs.readFileSync(join(__dirname, '../../../', YAML_CONFIG_FILENAME), 'utf8'));
};
