import { DynamicModule, Global, Module } from '@nestjs/common';
import { ApiConfig } from './api.config';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './configuration';
import { TypeOrmModule } from '@nestjs/typeorm';

@Global()
@Module({
  imports: [
    //系统基础参数
    // ConfigModule.forRoot({
    //   load: [configuration],
    //   isGlobal: true,
    // }),
    TypeOrmModule.forRoot(),
  ],
  providers: [ConfigService, ApiConfig],
  exports: [ConfigService, ApiConfig],
})
export class ApiConfigModule {
  static forRoot(): DynamicModule {
    return {
      module: ApiConfigModule,
      providers: [ConfigService, ApiConfig],
      exports: [ConfigService, ApiConfig],
    };
  }
}
