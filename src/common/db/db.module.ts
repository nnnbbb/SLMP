import { Global, Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Log4jsModule } from '../log4js/log4js.module';
import { Log4jsTypeorm } from '../log4js/log4js.typeorm';
import { ApiConfig } from '../config/api.config';
import { ApiConfigModule } from '../config/api.config.module';

/**
 * 数据库模块
 *
 * @author jiang
 * @date 2021-04-10 10:16:23
 **/
@Global()
@Module({
  imports: [
    //数据库连接
    TypeOrmModule.forRootAsync({
      imports: [ApiConfigModule, Log4jsModule],
      useFactory: async (config: ApiConfig, logger: Log4jsTypeorm) => {
        Logger.log(`DOCKER DB HOST==>${process.env.MYSQL_HOST || ''}`);
        Logger.log(`DOCKER DB PORT==>${process.env.MYSQL_PORT || ''}`);
        Logger.log(`DOCKER DB DATABASE==>${process.env.MYSQL_DATABASE || ''}`);
        Logger.log(`DOCKER DB USERNAME==>${process.env.MYSQL_USER || ''}`);
        const host = process.env.MYSQL_HOST || config.dbHost;
        const port = parseInt(process.env.MYSQL_PORT) || config.dbPort;
        const database = process.env.MYSQL_DATABASE || config.dbDatabase;
        const username = process.env.MYSQL_USER || config.dbUsername;
        const password = process.env.MYSQL_PASSWORD || config.dbPassword;
        Logger.log(`DB TYPE==>mysql`);
        Logger.log(`DB HOST==>${host}`);
        Logger.log(`DB PORT==>${port}`);
        Logger.log(`DB DATABASE==>${database}`);
        Logger.log(`DB USERNAME==>${username}`);
        return {
          type: 'mysql',
          host: host,
          port: port,
          database: database,
          username: username,
          password: password,
          logging: config.dbLogging,
          logger: logger,
          synchronize: config.dbSynchronize,
          entities: [config.dbEntities],
          timezone: config.dbTimezone,
        };
      },
      inject: [ApiConfig, Log4jsTypeorm],
    }),
  ],
  controllers: [],
  providers: [ApiConfig],
  exports: [],
})
export class DbModule {}
