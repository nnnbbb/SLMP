import { DateUtils } from '../utils/date.utils';
import { uuid } from '../utils/uuid.utils';

/**
 * <pre>文件或目录路径构建器,此构建器生成的文件或目录格式为：
 * 文件路径：主目录/一级子目录/年份/月日/时间/[文件名前缀_]32位UUID[文件名后缀].扩展名
 * 目录路径:主目录/一级子目录/年份/月日/时间/[目录名前缀_]32位UUID
 * 示例如：
 * 文件路径：D://work/upload/CommonFile/2020/12/06/21/29/30/11c2c9d037c711eb93815fb8ade99dd6.xls
 * 目录路径：D://work/upload/CommonFile/2020/12/06/21/30/57/45df8c3037c711ebbabd15eea16ff5e7
 * 并且本接口类提供获取除主目录外的相对路径的方法</pre>
 * @author jiang
 * @date 2020-12-06 17:13:53
 **/
export class PathCreator {
  /**
   * 构建文件路径
   * @param details 构建路径的参数
   * @return
   * @author jiang
   * @date 2020-12-06 17:14:17
   **/
  static file = (details: FilePathDetails | Partial<FilePathDetails>): PathResult => {
    let result = PathCreator.dir(details);
    let fileExt = details.fileExt ? '.' + details.fileExt : '';
    fileExt = fileExt.replace('..', '.');
    result.relativePath = `${result.relativePath}${fileExt}`;
    result.path = `${result.path}${fileExt}`;
    result.fileExt = fileExt;
    result.isFile = true;
    return result;
  };

  /**
   * 构建目录路径
   * @param details
   * @return
   * @author jiang
   * @date 2020-12-06 17:14:42
   **/
  static dir = (details: DirPathDetails | Partial<DirPathDetails>): PathResult => {
    const datePart = DateUtils.format(new Date(), 'yyyy/MM/dd/HHmmss');
    let prefix = details.filePrefix ? `${details.filePrefix}/` : '';
    let suffix = details.fileSuffix ? `/${details.fileSuffix}` : '';
    const fileName = `${prefix}${uuid()}${suffix}`;
    const relativePath = `${details.subFolder}/${datePart}/${fileName}`;
    let path = `${details.mainFolder}/${relativePath}`;
    path = path.replace('//', '/');
    return { path: path, mainFolder: details.mainFolder, relativePath: relativePath, isFile: false };
  };
}

/**
 * 目录路径参数信息
 * @author jiang
 * @date 2020-12-06 17:01:33
 **/
export class DirPathDetails {
  /**
   * 主目录
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  mainFolder: string;
  /**
   * 二级目录
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  subFolder?: string;
  /**
   * 文件前缀，可选字段
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  filePrefix?: string;
  /**
   * 文件后缀，可选字段
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  fileSuffix?: string;
}

/**
 * 文件路径参数信息
 * @author jiang
 * @date 2020-12-06 17:01:33
 **/
export class FilePathDetails extends DirPathDetails {
  /**
   * 文件扩展名，例如：xls，构建时会自动在前面加点
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  fileExt?: string;
}

/**
 * 文件路径构建结果
 * @author jiang
 * @date 2020-12-06 21:53:01
 **/
export class PathResult {
  /**
   * 完整的文件路径
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  path: string;
  /**
   * 存储文件的主目录
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  mainFolder: string;
  /**
   * 文件相对路径
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  relativePath: string;
  /**
   * 文件扩展名
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  fileExt?: string;
  /**
   * 是文件还是目录，true/false--文件/目录
   * @author jiang
   * @date 2020-12-06 17:05:19
   **/
  isFile: boolean;
  /**
   * 文件大小
   * @author: jiangbin
   * @date: 2021-01-29 10:49:37
   **/
  size?: number;
  /**
   * 原文件名称
   * @author: jiangbin
   * @date: 2021-01-29 10:49:40
   **/
  originalName?: string;
  /**
   * 文件绑定的前端字段名
   * @author: jiangbin
   * @date: 2021-01-29 10:49:43
   **/
  fieldName?: string;
}
