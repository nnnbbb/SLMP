import { FilePathDetails, PathCreator } from './path.creator';
import { ConfigMapper } from '../../sys/config/config.mapper';
import { SYS_FILE_PATH } from '../../sys/config/sys.config.defined';
import * as path from 'path';

/**
 * 公共资源目录，即"back-end/public"目录的全路径
 * @author: jiangbin
 * @date: 2021-02-05 00:15:34
 **/
export const PUBLIC_FOLDER = path.join(__dirname, '../../../', 'public');

/**
 * 定义路径的目录信息，如主目录和二级目录，主要是为了方便使用
 * @author: jiangbin
 * @date: 2021-01-14 19:02:18
 **/
export const PathFolders = {
  /**
   * 系统的主存储目录，所有文件都被存储到这个目录中
   * @author: jiangbin
   * @date: 2021-01-14 18:39:50
   **/
  MAIN_FOLDER: () => ConfigMapper.getValue(SYS_FILE_PATH.MAIN_FOLDER),
  /**
   * 通用文件的二级存储目录
   * @author: jiangbin
   * @date: 2021-01-14 18:39:48
   **/
  SUB_COMMON: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_COMMON),
  /**
   * 临时文件的二级存储目录，这个目录内的文件会被定期清理，用作中转或临时存储
   * @author: jiangbin
   * @date: 2021-01-14 18:39:48
   **/
  SUB_TEMP: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_COMMON),
  /**
   * 指纹数据文件的二级存储目录
   * @author: jiangbin
   * @date: 2021-01-14 18:39:48
   **/
  SUB_GENES: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_GENES),
  /**
   * 图片文件的二级存储目录，这个目录内的文件会被定期清理，用作中转或临时存储
   * @author: jiangbin
   * @date: 2021-01-14 18:39:48
   **/
  SUB_IMAGES: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_IMAGES),
  /**
   * Word文件的二级存储目录
   * @author: jiangbin
   * @date: 2021-02-03 19:54:51
   **/
  SUB_WORDS: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_WORDS),
  /**
   * Excel文件的二级存储目录
   * @author: jiangbin
   * @date: 2021-02-03 19:54:51
   **/
  SUB_EXCELS: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_EXCELS),
  /**
   * Word模板文件的二级存储目录,用于存储自定义传的Word模板文件
   * @author: jiangbin
   * @date: 2021-02-03 19:54:51
   **/
  SUB_WORD_TPLS: () => ConfigMapper.getValue(SYS_FILE_PATH.SUB_WORD_TPLS),
};

/**
 * 文件路径参数类型定义
 * @author: jiangbin
 * @date: 2021-02-19 09:07:00
 **/
export declare type FilePathDetailsType = FilePathDetails | Partial<FilePathDetails>;

/**
 * 路径工具类，用于定义一些常用的文件路径构建方法
 * @author: jiangbin
 * @date: 2021-01-14 19:03:03
 **/
export const PathUtils = {
  /**
   * 构建一个文件存储路径
   * @param detail 通用文件构建参数，只需要指定二级目录名、扩展名、文件名前缀和后缀，并且这并不是必需的，二级目录未给定时设置为通用文件二级目录
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getPath: (detail?: FilePathDetailsType) => {
    return PathCreator.file({
      ...detail,
      mainFolder: PathFolders.MAIN_FOLDER(),
      subFolder: detail.subFolder ? detail.subFolder : PathFolders.SUB_COMMON(),
    });
  },
  /**
   * 构建一个通用文件存储路径，一般只需要给定扩展名即可，主目录和二级目录参数被强制指定了
   * @param detail 通用文件构建参数，只需要指定扩展名、文件名前缀和后缀，并且这并不是必需的
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getCommonPath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({ ...detail, subFolder: PathFolders.SUB_COMMON() });
  },
  /**
   * 构建一个临时文件存储路径，一般只需要给定扩展名即可，主目录和二级目录参数被强制指定了
   * @param detail 通用文件构建参数，只需要指定扩展名、文件名前缀和后缀，并且这并不是必需的
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getTempPath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({ ...detail, subFolder: PathFolders.SUB_TEMP() });
  },
  /**
   * 构建一个指纹数据文件存储路径
   * @param detail 指纹数据文件构建参数，只需要指定文件名前缀和后缀，并且这并不是必需的，文件扩展名强制为json
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getGenePath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({
      ...detail,
      subFolder: PathFolders.SUB_GENES(),
      fileExt: 'json',
    });
  },
  /**
   * 构建一个图片文件存储路径
   * @param detail 通用文件构建参数，只需要指定扩展名、文件名前缀和后缀，并且这并不是必需的
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getImagePath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({ ...detail, subFolder: PathFolders.SUB_IMAGES() });
  },
  /**
   * 构建一个Word文件存储路径，默认使用docx文件扩展名
   * @param detail 通用文件构建参数，只需要指定扩展名、文件名前缀和后缀，并且这并不是必需的
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getWordPath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({ fileExt: 'docx', ...detail, subFolder: PathFolders.SUB_WORDS() });
  },
  /**
   * 构建一个Word模板文件存储路径，默认使用docx文件扩展名
   * @param detail 通用文件构建参数，只需要指定扩展名、文件名前缀和后缀，并且这并不是必需的
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getWordTplPath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({ fileExt: 'docx', ...detail, subFolder: PathFolders.SUB_WORD_TPLS() });
  },
  /**
   * 构建一个Excel文件存储路径，默认使用xlsx文件扩展名
   * @param detail 通用文件构建参数，只需要指定扩展名、文件名前缀和后缀，并且这并不是必需的
   * @author: jiangbin
   * @date: 2021-01-14 18:39:45
   **/
  getExcelPath: (detail?: FilePathDetailsType) => {
    return PathUtils.getPath({ fileExt: 'xlsx', ...detail, subFolder: PathFolders.SUB_EXCELS() });
  },
  /**
   * 获取给定相对路径的全路径
   * @param params.mainFolder 文件主目录路径，若未给定则直接从数据库参数表中读取
   * @param params.relativePath 文件相对路径
   * @return {string} 文件的全路径，可以用来读写文件
   * @author: jiangbin
   * @date: 2021-01-14 18:39:33
   **/
  getFullPath: (params: { mainFolder?: string; relativePath: string }) => {
    let path = params.mainFolder ? params.mainFolder + '/' + params.relativePath : PathFolders.MAIN_FOLDER() + '/' + params.relativePath;
    return path.replace('//', '/');
  },
};
