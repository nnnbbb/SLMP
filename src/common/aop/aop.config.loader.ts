import { aop } from './aop.decorator';
import { ConfigMapper } from '../../sys/config/config.mapper';

/**
 * 参数加载装饰器，刷新后端的参数映射表数据
 *
 * @author jiang
 * @date 2021-01-16 18:58:50
 **/
export function AopConfigLoader(sync = true) {
  return aop(sync, { afters: [ConfigMapper.reload] });
}
