import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from '../service/auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable } from '@nestjs/common';
import { ApiConfig } from '../../config/api.config';

/**
 * 用户身份认证JWT认证策略，在用户登录后每次访问后端功能时均需要在请求头部增加一个'token'名的身份认证信息字符串，通过本策略模块进行该字符串的认证
 * @Date 12/2/2020, 1:54:58 PM
 * @author jiangbin
 * @export
 * @class JwtStrategy
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@Inject(AuthService) private readonly authService: AuthService, @Inject(ApiConfig) config: ApiConfig) {
    super({
      jwtFromRequest: ExtractJwt.fromHeader('token'),
      ignoreExpiration: false,
      secretOrKey: config.jwtSeceret,
      signOptions: {
        expiresIn: config.jwtExpiresIn,
      },
    });
  }

  /**
   * 用户身份认证成功后会自动调用这个方法
   * @param payload 格式如下：
   *  <pre>
   *  {
   *    id: 'd5dc9fd036d211eba8d8fdf0ea6d6940',
   *    name: 'renjie',
   *    iat: 1607156472,
   *    exp: 1607160072
   *  }
   *  </pre>
   */
  async validate(payload: any): Promise<any> {
    return await this.authService.check(payload.name);
  }
}
