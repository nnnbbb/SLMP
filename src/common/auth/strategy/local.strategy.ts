import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '../service/auth.service';

/**
 * 本地验证策略，用户登录时会通过本功能验证帐号和密码
 */
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'username',
      passwordField: 'password',
    });
  }

  /**
   * 验证帐号和密码是否正确
   * @param username
   * @param password
   */
  async validate(username: string, password: string): Promise<any> {
    return await this.authService.login(username, password);
  }
}
