import { HttpStatus, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { uuid } from '../../utils/uuid.utils';
import { SysUserService } from '../../../sys/service/sys.user.service';
import { SysUserDetailQueryer } from '../../../sys/queryer/sys.user.detail.queryer';
import { ApiConfig } from '../../config/api.config';
import { ConfigMapper } from '../../../sys/config/config.mapper';
import { SYS_USER } from '../../../sys/config/sys.config.defined';
import { UserContext } from '../../context/user.context';
import { LogUtils } from '../../log4js/log4js.utils';
import { UserMapper } from '../../../sys/user/user.mapper';

/**
 * 用户身份认证服务
 * @Date 12/2/2020, 1:54:58 PM
 * @author jiangbin
 * @export
 * @class AuthService
 */
@Injectable()
export class AuthService {
  constructor(
    @Inject(ApiConfig) private readonly apiConfig: ApiConfig,
    @Inject(SysUserService) private readonly userService: SysUserService,
    @Inject(SysUserDetailQueryer) private readonly userDetailQueryer: SysUserDetailQueryer,
  ) {}

  /**
   * 创建授权Token字符串
   * @param loginName
   * @param password
   */
  async createToken(loginName: string, password: string): Promise<any> {
    const id = uuid(); //创建一个UUID标识Token
    const user = { id: id, name: loginName };
    return jwt.sign(user, this.apiConfig.jwtSeceret, { expiresIn: this.apiConfig.jwtExpiresIn });
  }

  /**
   * 验证Token字符串
   * @param token
   */
  verfiyToken(token: string) {
    return jwt.verify(token, this.apiConfig.jwtSeceret);
  }

  /**
   * 检测用户名和密码是否非法
   * @param username 帐号
   * @param password 密码
   */
  async check(username: string) {
    //查找用户
    const user = UserMapper.getUser(username);
    if (!user || !user.userPassword) {
      LogUtils.error(AuthService.name, `用户[${username}]不存在!`);
      throw new UnauthorizedException(`用户[${username}]不存在!`);
    }

    //去除密码
    const { userPassword, userPasswordPrivate, ...result } = user;

    //设置到上下文对象中
    UserContext.user = user;

    return { statusCode: HttpStatus.OK, message: result };
  }

  /**
   * 验证用户登录帐号和密码是否正确，否则返回null
   * @param username 登录帐号
   * @param password 登录密码，数据库中存储的为md5加密的密码，所以前端也需要加密后传输到本方法进行验证
   * @return 如果登录成功则返回用户信息对象，将生成令牌、查询角色、菜单、权限等信息
   */
  async login(username: string, password: string): Promise<any> {
    //查找用户
    const user = UserMapper.getUser(username);

    if (!user || !user.userPassword) {
      LogUtils.error(AuthService.name, `用户[${username}]不存在!`);
      throw new UnauthorizedException(`用户[${username}]不存在!`);
    }

    if (user.userPassword != password) {
      LogUtils.error(AuthService.name, `用户[${username}]密码错误!`);
      throw new UnauthorizedException(`用户[${username}]密码错误!`);
    }

    //去除密码
    const { userPassword, userPasswordPrivate, ...result } = user;

    //添加令牌
    result['token'] = await this.createToken(username, password);

    //添加语言
    result['locale'] = ConfigMapper.getValue(SYS_USER.USER_LANGUAGE);

    //填充详细信息
    await this.fillUser(result);

    //设置到上下文对象中
    UserContext.user = user;

    return { statusCode: HttpStatus.OK, message: result };
  }

  /**
   * 填充用户信息，如：角色、菜单、授权等信息
   * @param user
   */
  async fillUser(user: any) {
    //查询角色列表
    user['role'] = await this.userDetailQueryer.queryRole(user.userLoginName);

    //过滤角色ID列表
    let roleIdList = this.userDetailQueryer.filterRoleIds(user['role']);
    if (!roleIdList || roleIdList.length == 0) return user;

    //查询菜单列表
    user['menu'] = await this.userDetailQueryer.queryMenu(roleIdList);

    //查询授权列表
    user['pms'] = await this.userDetailQueryer.queryPermission(roleIdList);

    //查询用户首页面URL
    user['index'] = ConfigMapper.getValue(SYS_USER.USER_INDEX_URL);

    return user;
  }
}
