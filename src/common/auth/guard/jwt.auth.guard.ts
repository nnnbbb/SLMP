import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC_AUTH_KEY } from './skip.auth';
import { ApiConfig } from '../../config/api.config';
import { LogUtils } from '../../log4js/log4js.utils';
import { AuthService } from '../service/auth.service';
import { getReqUrl } from '../../utils/req.utils';

/**
 * JWT授权认证守卫，这个装饰器将被注册到全局中，使所有URL均需要认证，
 * 同时定义了一个@SkipAuth()装饰器，这个装饰器装饰的方法将不需要身份令牌认证
 */
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector, private apiConfig: ApiConfig, private authService: AuthService) {
    super();
  }

  canActivate(context: ExecutionContext) {
    let url = getReqUrl(context.switchToHttp().getRequest());

    //是否关闭用户身份认证，关闭后请求的header中不需要包含token参数即可访问任意功能，用于开发模式
    if (!this.apiConfig.isAuthEnabled) {
      LogUtils.info(JwtAuthGuard.name, '关闭了身份认证模块!');
      return true;
    }

    //反射方式获取装饰器@SkipAuth()
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_AUTH_KEY, [context.getHandler(), context.getClass()]);
    if (isPublic) {
      LogUtils.info(JwtAuthGuard.name, `${url}==>公共模块，允许直接访问!`);
      return true;
    }

    return super.canActivate(context);
  }
}
