import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { SysPermission } from '../../../sys/entity/sys.permission.entity';
import { PMS_KEY } from './pms.auth';
import { ApiConfig } from '../../config/api.config';
import { LogUtils } from '../../log4js/log4js.utils';
import { IS_PUBLIC_PMS_KEY } from './skip.pms';
import { getReqUrl } from '../../utils/req.utils';
import { PmsItem } from '../../pms/pms.info';
import { PmsMapper } from '../../pms/pms.mapper';

/**
 * 权限守卫对象，用于验证用户是否拥有权限信息，以便判定是否允许访问相关方法，
 * 若守卫中给定多个权限则表示需要用户同时具备这些权限才允许访问，否则会被拒绝访问
 */
@Injectable()
export class PmsGuard implements CanActivate {
  constructor(private reflector: Reflector, private apiConfig: ApiConfig) {}

  canActivate(context: ExecutionContext): boolean {
    //是否关闭用户功能权限认证，用于开发模式
    if (!this.apiConfig.isPmsEnabled) {
      return true;
    }

    const targets = [context.getHandler(), context.getClass()];

    //反射方式获取装饰器@SkipPms()
    const url = getReqUrl(context.switchToHttp().getRequest());
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_PMS_KEY, targets);
    if (isPublic) {
      LogUtils.info(PmsGuard.name, `${url}==>公共的功能，允许直接访问!`);
      return true;
    }

    //获取访问方法要求的权限列表
    let limits = this.reflector.getAllAndOverride<(PmsItem | Partial<PmsItem>)[]>(PMS_KEY, targets);
    if (!limits || limits.length == 0) {
      LogUtils.info(PmsGuard.name, `${url}==>未定义权限，允许直接访问!`);
      return true;
    }

    //过滤掉未启用的权限
    limits = limits.filter((limit) => {
      return PmsMapper.getPms(limit.root.group, limit.id) ? true : false;
    });
    if (!limits || limits.length == 0) {
      LogUtils.info(PmsGuard.name, `${url}==>权限未启用，允许直接访问!`);
      return true;
    }

    //用户的权限
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) {
      LogUtils.info(PmsGuard.name, `${url}==>用户未登录，不允许访问!`);
      return false;
    }

    //用户权限列表
    const userPms = user?.pms;
    if (!userPms || userPms.length == 0) {
      LogUtils.info(PmsGuard.name, `${url}==>${user.userLoginName}用户未被权限，不允许访问!`);
      return false;
    }

    //判定用户是否包含相关要求的权限
    return matchPms(limits, userPms);
  }
}

/**
 * 权限匹配，判定当前用户是否具有权限
 * @param limits 访问功能所需的权限列表
 * @param userPms 用户拥有的权限列表
 * @return true/false 允许用户访问/不允许用户访问
 * @author: jiangbin
 * @date: 2021-01-25 11:45:49
 **/
const matchPms = (limits: (PmsItem | Partial<PmsItem>)[], userPms: SysPermission[]) => {
  //过滤出当前用户拥有的授权项列表
  const hasList = limits.filter((limit) => {
    const spmsId = limit.id;
    const ssystem = limit.root.system;
    const sgroup = limit.root.group;
    const exists = userPms.filter((item) => {
      const tpmsId = item.perUrl;
      const tsystem = item.perSystem;
      const tgroup = item.perModule;
      return spmsId === tpmsId && ssystem === tsystem && sgroup === tgroup;
    });
    return exists && exists.length > 0;
  });

  //用户包含给定全部的权限列表
  return hasList && hasList.length === limits.length;
};
