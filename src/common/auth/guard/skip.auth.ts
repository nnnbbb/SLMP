import { SetMetadata } from '@nestjs/common';

export const IS_PUBLIC_AUTH_KEY = 'isPublicAuthKey';

/**
 * 装饰到那些不需要身份认证的控制器方法上，这样避免某些URL被拦截，如用户登录URL
 * @constructor
 */
export const SkipAuth = () => SetMetadata(IS_PUBLIC_AUTH_KEY, true);
