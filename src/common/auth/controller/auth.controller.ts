import { Controller, Get, Inject, Post, Request, UseGuards } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { ApiHeader, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { LocalAuthGuard } from '../guard/local.auth.guard';
import { SkipAuth } from '../guard/skip.auth';

/**
 * 用户身份认证控制器类
 * @Date 12/2/2020, 1:54:58 PM
 * @author jiangbin
 * @export
 * @class AuthController
 */
@Controller('auth')
@ApiTags('用户身份认证授权控制器类')
export class AuthController {
  constructor(@Inject(AuthService) private readonly authService: AuthService) {}

  /**
   * 用户登录并返回用户详细信息和令牌
   * @param req
   */
  @SkipAuth()
  @Post('login')
  @UseGuards(LocalAuthGuard)
  @ApiOperation({ summary: '用户登录并返回Token' })
  @ApiQuery({ name: 'password', description: '用户登录密码，需要Md5加密' })
  @ApiQuery({ name: 'username', description: '用户登录帐号' })
  async login(@Request() req) {
    return req.user;
  }

  /**
   * 用户登录状态验证并返回用户基本信息
   * @param req
   */
  @Get('valid')
  @ApiOperation({ summary: '根据Token检查用户登录状态并返回用户基本信息' })
  @ApiHeader({ name: 'token', description: '用户身份token字符串，放到header中' })
  public valid(@Request() req) {
    return req.user;
  }
}
