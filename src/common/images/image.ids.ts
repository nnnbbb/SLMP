/**
 * 公共图片目录中图片定义，方便使用文件
 * @author: jiangbin
 * @date: 2021-02-05 00:24:24
 **/
export const ImageIds = {
  YMS_LOGO_BIG: 'logo/yms-logo-big.png',
  YMS_LOGO_SMALL: 'logo/yms-logo-small.png',
};
