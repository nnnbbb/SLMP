import { Well } from './well';
import { Plate } from './plate';
import { PATTERNS } from '../regex/patterns';

/**
 * 孔位号工具类，用于解析孔位号的行和列信息
 * @author: jiangbin
 * @date: 2021-03-18 14:07:33
 **/
export const WellUtils = {
  /**
   * 解析孔位号
   * @param well 孔位号字符串，如:A1或A01
   * @return 孔位信息
   * @author: jiangbin
   * @date: 2021-03-18 14:07:56
   **/
  detail(well: string): Well | null {
    return PATTERNS.well_detail.match(well);
  },
  /**
   * 从给定的字符串中提取出孔位号
   * @param str 源字符串，例如：A01_WR21P00006P_M60K_032901_WL2101616或A01_WL2101616
   * @return 孔位号，例如：A01
   * @author: jiangbin
   * @date: 2021-04-21 13:20:37
   **/
  get(str: string): string | null {
    return PATTERNS.well.match(str);
  },
  /**
   * 获取行号名称,例如给定行顺序号为2，则得到行号名称为B
   * @param row
   * @return
   * @author: jiangbin
   * @date: 2021-03-18 15:09:43
   **/
  getRowName(row: number): string {
    return String.fromCharCode(row + 'A'.charCodeAt(0) - 1);
  },
  /**
   * 获取行顺序号，例如给定:B行号，得到行顺序号为2
   * @param rowName
   * @return
   * @author: jiangbin
   * @date: 2021-03-18 15:09:48
   **/
  getRowNum(rowName: string): number {
    return rowName.charCodeAt(0) - 'A'.charCodeAt(0) + 1;
  },
  /**
   * 格式化成孔位号字符串
   * @param well 孔位信息
   * @param doubleCol true/false-是否为双列列号，例如：A01，默认为双列的
   * @return 格式化后的孔位号字符串，例如:A1或A01
   * @author: jiangbin
   * @date: 2021-03-18 14:16:59
   **/
  format(well: { row: number; col: number }, doubleCol = true): string {
    return doubleCol ? `${this.getRowName(well.row) || ''}${well?.col?.toString().padStart(2, '0') || ''}` : `${this.getRowName(well.row) || ''}${well?.col || ''}`;
  },

  /**
   * 获取指定上样顺序对应的上样孔位名称，例如：上样顺序为2对应孔位号为A02
   * @param plate 板规格
   * @param order 板序号
   * @param isHorizontal 是否横向，默认是横向上样
   * @return
   * @author: jiangbin
   * @date: 2021-04-07 16:18:09
   **/
  getWellName(params: { plate: Plate | Required<Plate>; order: number }, isHorizontal = true): Well {
    let { plate, order } = params;
    return plate.well({ order, isHorizontal });
  },
};
