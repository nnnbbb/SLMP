import { Well } from './well';

/**
 * 上样板规格定义
 * @author: jiangbin
 * @date: 2021-04-07 16:19:27
 **/
export interface Plate {
  /**
   * 总行数，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 14:59:59
   **/
  rowCount: number;
  /**
   * 总列数，由1开始
   * @author: jiangbin
   * @date: 2021-03-18 15:00:01
   **/
  colCount: number;
  /**
   * 孔位总数
   * @author: jiangbin
   * @date: 2021-04-16 14:00:33
   **/
  count: number;
  /**
   * 获取横向孔位名
   * @param order 上样顺序
   * @return
   * @author: jiangbin
   * @date: 2021-04-16 14:00:36
   **/
  horizontal: (order: number) => Well;
  /**
   * 获取纵向孔位名
   * @param order 上样顺序
   * @return
   * @author: jiangbin
   * @date: 2021-04-16 14:00:38
   **/
  vertical: (order: number) => Well;
  /**
   * 根据给定的填充方式获取上样顺序对应的孔位号信息
   *
   * @param params.order 上样顺序
   * @param params.isHorizontal 是否为横向填充方式，默认为true
   * @return
   * @author jiang
   * @date 2021-04-16 22:28:53
   **/
  well: (params: { order: number; isHorizontal?: boolean }) => Well;
}
