import { WellUtils } from './well.utils';
import { Plate } from './plate';

/**
 * 上样板规格
 * @author: jiangbin
 * @date: 2021-04-07 16:32:18
 **/
export const PLATE_SPECS = {
  /**
   * 96孔上样板
   *
   * @author jiang
   * @date 2021-04-16 23:08:52
   **/
  P96: {
    rowCount: 8,
    colCount: 12,
    count: 8 * 12,
    horizontal(order: number) {
      let row = Math.floor((order - 1) / this.colCount) + 1;
      let col = ((order - 1) % this.colCount) + 1;
      return { row, col, rowName: WellUtils.getRowName(row), well: WellUtils.format({ row, col }), order };
    },
    vertical(order: number) {
      let col = Math.floor((order - 1) / this.rowCount) + 1;
      let row = ((order - 1) % this.rowCount) + 1;
      return { row, col, rowName: WellUtils.getRowName(row), well: WellUtils.format({ row, col }), order };
    },
    well(params: { order: number; isHorizontal?: boolean }) {
      return params.isHorizontal ? this.horizontal(params.order) : this.vertical(params.order);
    },
  } as Plate,
  /**
   * 384孔上样板
   *
   * @author jiang
   * @date 2021-04-16 23:09:09
   **/
  P384: {
    rowCount: 8 * 2,
    colCount: 12 * 2,
    count: 8 * 12 * 4,
    /**
     * 获取给定上样顺序号所在的孔位号信息
     *
     * @param order 上样顺序
     * @param startWells 384板的4块96孔子板起始孔位号，用作偏移基准孔位号
     * @return
     * @author jiang
     * @date 2021-04-16 23:13:25
     **/
    getWell(order: number, startWells: [{ row: number; col: number }]) {
      //当前上样序号所处的子板索引号
      let plateIndex = Math.floor((order - 1) / 96);
      //模除获取子板上样顺序号
      let subOrder = Math.floor((order - 1) % 96) + 1;
      //获取子板的起始孔位号，作为子板的孔位位置进行偏移参考基准
      let startWell = startWells[plateIndex];
      //计算子板上的孔位信息，子板均采用横向上样方式
      let well = WellUtils.getWellName({ plate: PLATE_SPECS.P96, order: subOrder });
      //偏移子板孔位号获取384板上的实际孔位号
      let row = startWell.row + (well.row - 1) * 2;
      let col = startWell.col + (well.col - 1) * 2;
      return { row, col, rowName: WellUtils.getRowName(row), order, well: WellUtils.format({ row, col }), sub: well };
    },
    horizontal(order: number) {
      //4块子板的起始孔位号
      let startWells = [
        { row: 1, col: 1 },
        { row: 1, col: 2 },
        { row: 2, col: 1 },
        { row: 2, col: 2 },
      ];
      return this.getWell(order, startWells);
    },
    vertical(order: number) {
      //4块子板的起始孔位号
      let startWells = [
        { row: 1, col: 1 },
        { row: 2, col: 1 },
        { row: 1, col: 2 },
        { row: 2, col: 2 },
      ];
      return this.getWell(order, startWells);
    },
    well(params: { order: number; isHorizontal?: boolean }) {
      return params.isHorizontal ? this.horizontal(params.order) : this.vertical(params.order);
    },
  } as Plate,
  /**
   * 1538孔位上样板
   *
   * @author jiang
   * @date 2021-04-16 23:09:29
   **/
  P1536: {
    rowCount: 8 * 4,
    colCount: 12 * 4,
    count: 8 * 12 * 16,
    horizontal(order: number) {
      let row = Math.floor((order - 1) / this.colCount) + 1;
      let col = ((order - 1) % this.colCount) + 1;
      return { row, col, rowName: WellUtils.getRowName(row), well: WellUtils.format({ row, col }) };
    },
    vertical(order: number) {
      let col = Math.floor((order - 1) / this.rowCount) + 1;
      let row = ((order - 1) % this.rowCount) + 1;
      return { row, col, rowName: WellUtils.getRowName(row), well: WellUtils.format({ row, col }) };
    },
    well(params: { order: number; isHorizontal?: boolean }) {
      return params.isHorizontal ? this.horizontal(params.order) : this.vertical(params.order);
    },
  } as Plate,
};

/**
 * 板规格工具方法
 *
 * @author jiang
 * @date 2021-04-16 22:27:32
 **/
export const PlateUtils = {
  /**
   * 获取电泳板规格详细定义信息
   * @param plateSpecs 板规格，例如:96/384/1536
   * @return
   * @author: jiangbin
   * @date: 2021-04-16 10:40:13
   **/
  get(plateSpecs: number) {
    switch (plateSpecs) {
      case PLATE_SPECS.P96.count:
        return PLATE_SPECS.P96;
      case PLATE_SPECS.P384.count:
        return PLATE_SPECS.P384;
      case PLATE_SPECS.P1536.count:
        return PLATE_SPECS.P1536;
      default:
        return PLATE_SPECS.P96;
    }
    return PLATE_SPECS.P96;
  },
};
