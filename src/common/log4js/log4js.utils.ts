import { NestExpressApplication } from '@nestjs/platform-express';
import { Log4jsService } from './log4js.service';

/**
 * Log4js日志模块服务类的静态引用
 * @date 1/7/2021, 9:25:29 PM
 * @author jiangbin
 **/
export class LogUtils {
  private static log4jsService: Log4jsService;

  /**
   * 初始化日志模块服务类的静态引用
   * @date 1/7/2021, 9:25:29 PM
   * @author jiangbin
   **/
  static init(app: NestExpressApplication) {
    LogUtils.log4jsService = app.get(Log4jsService);
  }

  /**
   * 获取日志对象
   * @param category 日志名称，例如类名称
   * @author: jiangbin
   * @date: 2021-01-14 13:24:57
   **/
  static getLogger(category = 'APP') {
    return LogUtils.log4jsService.getLogger(category);
  }

  static trace(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).trace(message, args);
    } catch (err) {
      console.log(err);
    }
  }

  static debug(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).debug(message, args);
    } catch (err) {
      console.log(err);
    }
  }

  static info(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).info(message, args);
    } catch (err) {
      console.log(err);
    }
  }

  static warn(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).warn(message, args);
    } catch (err) {
      console.log(err);
    }
  }

  static error(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).error(message, args);
    } catch (err) {
      console.log(err);
    }
  }

  static fatal(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).fatal(message, args);
    } catch (err) {
      console.log(err);
    }
  }

  static mark(category: string, message: any, ...args: any[]) {
    try {
      LogUtils.getLogger(category).mark(message, args);
    } catch (err) {
      console.log(err);
    }
  }
}
