export const LOG4JS_OPTION = Symbol('LOG4JS_OPTION');

/**
 * 日志名称定义，用于获取指定日志对象输出日志到指定日志文件中
 * @author jiang
 * @date 2021-01-16 11:52:08
 **/
export const LOG_NAME = {
  sql: 'sql',
  error: 'error',
};
