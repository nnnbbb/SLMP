import { Logger, QueryRunner } from 'typeorm';
import { Inject, Injectable } from '@nestjs/common';
import { Log4jsService } from './log4js.service';
import { LOG_NAME } from './log4js.constants';

/**
 * 用于TypeOrm框架的日志输出到Log4js框架中
 * @author: jiangbin
 * @date: 2021-01-15 23:14:25
 **/
@Injectable()
export class Log4jsTypeorm implements Logger {
  constructor(@Inject(Log4jsService) private readonly log4jsService: Log4jsService) {}

  /**
   * 获取日志对象
   * @author: jiangbin
   * @date: 2021-01-15 23:34:52
   **/
  logger() {
    return this.log4jsService.getLogger(LOG_NAME.sql);
  }

  log(level: 'log' | 'info' | 'warn', message: any, queryRunner?: QueryRunner): any {
    switch (level) {
      case 'info':
        this.logger().info(message);
      case 'log':
        this.logger().log(message);
      case 'warn':
        this.logger().warn(message);
    }
  }

  logMigration(message: string, queryRunner?: QueryRunner): any {
    this.logger().info(message);
  }

  logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner): any {
    this.logger().info(query, parameters || '');
  }

  logQueryError(error: string | Error, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
    this.logger().error(error, query, parameters || '');
  }

  logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
    this.logger().log(time, query, parameters || '');
  }

  logSchemaBuild(message: string, queryRunner?: QueryRunner): any {
    this.logger().log(message);
  }
}
