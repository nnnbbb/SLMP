import { DynamicModule, Global, Module } from '@nestjs/common';
import { Configuration } from 'log4js';
import { Log4jsService } from './log4js.service';
import { createOptionProvider } from './log4js.provider';
import { Log4jsTypeorm } from './log4js.typeorm';

@Global()
@Module({
  providers: [Log4jsService, Log4jsTypeorm, createOptionProvider()],
  exports: [Log4jsService, Log4jsTypeorm],
})
export class Log4jsModule {
  static forRoot(options?: Configuration | string): DynamicModule {
    const optionProvider = createOptionProvider(options);
    return {
      module: Log4jsModule,
      providers: [Log4jsService, Log4jsTypeorm, optionProvider],
      exports: [Log4jsService, Log4jsTypeorm],
    };
  }
}
