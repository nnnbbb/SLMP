import { forwardRef, Global, Module } from '@nestjs/common';
import { SysModule } from '../../sys/sys.module';
import { SystemController } from './controller/system.controller';

@Global()
@Module({
  imports: [forwardRef(() => SysModule)],
  controllers: [SystemController],
  providers: [],
  exports: [],
})
export class SystemModule {}
