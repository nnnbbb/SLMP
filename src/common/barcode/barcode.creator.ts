const bwipjs = require('bwip-js');

/**
 * 生成条形码的图片，参看详细：
 * {@link http://bwip-js.metafloor.com/}
 * {@link https://github.com/metafloor/bwip-js}
 * @author: jiangbin
 * @date: 2021-02-04 15:39:22
 **/
export const BarcodeCreator = {
  /**
   * 创建条形码图片(异步函数)
   * @param barcodeStr 条形码字符串
   * @param showText 显示文本内容
   * @return {@link Buffer}
   * @author: jiangbin
   * @date: 2021-02-04 15:39:42
   **/
  create: async (barcodeStr: string, showText = true) => {
    const pngBuffer = await bwipjs.toBuffer({
      bcid: 'code128', // 条码类型
      text: barcodeStr, // 条码内容
      scale: 3, // 3x scaling factor
      height: 100, // Bar height, in millimeters
      includetext: showText, // 显示条码文本
      textxalign: 'center', // Always good to set this
    });

    return pngBuffer;
  },
};

/**
 * 生成二维码的图片
 * @author: jiangbin
 * @date: 2021-02-04 23:23:00
 **/
export const QrcodeCreator = {
  /**
   * 创建条形码图片(异步函数)
   * @param barcodeStr 条形码字符串
   * @return {@link Buffer}
   * @author: jiangbin
   * @date: 2021-02-04 15:39:42
   **/
  create: async (barcodeStr: string) => {
    let pngBuffer = await bwipjs.toBuffer({
      bcid: 'qrcode', // Barcode type
      text: barcodeStr, // Text to encode
      scale: 3, // 3x scaling factor
      height: 100, // Bar height, in millimeters
    });

    return pngBuffer;
  },
};
