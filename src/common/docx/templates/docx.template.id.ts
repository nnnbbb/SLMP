/**
 * 文件模板列表
 * @author: jiangbin
 * @date: 2021-02-03 12:21:40
 **/
export const DOCX_TEMPLATES = {
  test_result: {
    cn: '检验结果单.docx',
    en: '检验结果单.docx',
  },
  test_tpl: {
    cn: '测试.docx',
    en: '测试.docx',
  },
};
