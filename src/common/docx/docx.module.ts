import { forwardRef, Global, Module } from '@nestjs/common';
import { SysModule } from '../../sys/sys.module';
import { DocxController } from './controller/docx.controller';

@Global()
@Module({
  imports: [forwardRef(() => SysModule)],
  controllers: [DocxController],
  providers: [],
  exports: [],
})
export class DocxModule {}
