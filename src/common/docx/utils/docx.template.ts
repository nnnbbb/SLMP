import createDocx from 'docx-templates';
import * as fs from 'fs';
import * as path from 'path';
import { PathUtils } from '../../path/path.creator.utils';
import { FileUtils } from '../../utils/file.utils';
import { LogUtils } from '../../log4js/log4js.utils';
import { BarcodeCreator, QrcodeCreator } from '../../barcode/barcode.creator';
import { ImageIds } from '../../images/image.ids';
import { UserContext } from '../../context/user.context';
import { DictMapper } from '../../dictionary/dict.mapper';
import { SYS_SAM_SPECIES_DICT_GROUP } from '../../dictionary/sys.dict.defined';
import { ImageUtils } from '../../images/image.utils';
import * as moment from 'moment';
import { LocaleContext, MLOCALES } from '../../locales/locale';

/**
 * 自定义的一些方法，在Word模板中可以直接调用，示例:{@code barcode('1235678')}
 * @author: jiangbin
 * @date: 2021-02-05 10:45:19
 **/
export const DocxJsContext = {
  /**
   * 生成条码号图片
   * @param barcodeStr 条码号字符串
   * @param width 图片宽度,单位：cm
   * @param height 图片高度,单位：cm
   * @author: jiangbin
   * @date: 2021-02-04 15:37:20
   **/
  barcode: async (barcodeStr: string, width = 5, height = 2) => {
    const data = await BarcodeCreator.create(barcodeStr);
    return {
      width: width,
      height: height,
      data,
      extension: '.png',
    };
  },
  /**
   * 生成二维码图片
   * @param barcodeStr 条码号字符串
   * @param width 图片宽度,单位：cm
   * @param height 图片高度,单位：cm
   * @author: jiangbin
   * @date: 2021-02-04 23:33:32
   **/
  qrcode: async (barcodeStr: string, width = 3, height = 3) => {
    const data = await QrcodeCreator.create(barcodeStr);
    return {
      width: width,
      height: height,
      data,
      extension: '.png',
    };
  },
  /**
   * 加载指定路径的图片到Word中
   * @param imgPath 图片文件路径
   * @param width 图片宽度,单位：cm
   * @param height 图片高度,单位：cm
   * @author: jiangbin
   * @date: 2021-02-05 00:04:16
   **/
  image: async (imgPath: string, width = 5, height = 5) => {
    const data = await fs.readFileSync(imgPath);
    return {
      width: width,
      height: height,
      data,
      extension: path.extname(imgPath),
    };
  },
  /**
   * 加载指定Base64编码的图片到Word中
   * @param contents 图片文件内容，采用Base64编码的图片内容
   * @param width 图片宽度,单位：cm
   * @param height 图片高度,单位：cm
   * @author: jiangbin
   * @date: 2021-02-05 00:04:16
   **/
  bimage: async (contents: string, width = 5, height = 5) => {
    const detail = ImageUtils.getBase64ImgDetails(contents);
    return {
      width: width,
      height: height,
      data: detail.data,
      extension: detail.ext,
    };
  },
  /**
   * 格式化日期对象或日期字符串
   * @param date 日期
   * @param format 请查看moment组件库的格式化pattern语法，默认中文环境为：'YYYY年MM月DD日'，英文环境为：'YYYY-MM-DD'
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:13:00
   **/
  date: (date: Date | string, format = MLOCALES.pattern) => {
    return moment(date || new Date()).format(format);
  },
  /**
   * 4位年份，例如:2021
   * @param date 日期
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:31:13
   **/
  year: (date?: Date | string) => {
    return moment(date || new Date()).format('YYYY');
  },
  /**
   * 月份
   * @param date 日期
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:31:27
   **/
  month: (date?: Date | string) => {
    return moment(new Date()).format('M');
  },
  /**
   * 天
   * @param date 日期
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:31:48
   **/
  day: (date?: Date | string) => {
    return moment(date || new Date()).format('D');
  },
  /**
   * 小时
   * @param date 日期
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:31:59
   **/
  hour: (date?: Date | string) => {
    return moment(date || new Date()).format('H');
  },
  /**
   * 分钟
   * @param date 日期
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:31:59
   **/
  minute: (date?: Date | string) => {
    return moment(date || new Date()).format('m');
  },
  /**
   * 秒
   * @param date 日期
   * @return
   * @author: jiangbin
   * @date: 2021-02-05 10:31:59
   **/
  second: (date?: Date | string) => {
    return moment(date || new Date()).format('s');
  },
};

/**
 * 默认数据信息，包含Logo、用户信息等
 * @author: jiangbin
 * @date: 2021-02-05 08:40:22
 **/
export const DocxDefaultData = {
  /**
   * 默认数据信息，包含Logo、用户信息等
   * @author: jiangbin
   * @date: 2021-02-05 08:40:22
   **/
  get data() {
    return {
      YMS_LOGO_BIG: ImageUtils.getImagePath(ImageIds.YMS_LOGO_BIG),
      YMS_LOGO_SMALL: ImageUtils.getImagePath(ImageIds.YMS_LOGO_SMALL),
      CURR_DATE: moment(new Date()).format(LocaleContext.message({ cn: 'YYYY年M月D日', en: 'YYYY/M/D' })),
      CURR_USER: {
        ...UserContext.user,
        speciesEn: UserContext.species,
        speciesCn: DictMapper.getName({
          dicGroup: SYS_SAM_SPECIES_DICT_GROUP.group,
          dicValue: UserContext.species,
        }),
      },
    };
  },
};

/**
 * 基于docx-templates的Word模板处理类，将指定模板文件和数据合并生成Word文件，可以用于报告生成等场景
 * @author: jiangbin
 * @date: 2021-02-04 09:36:11
 **/
export const DocxTemplate = {
  /**
   * 合并数据到模板中
   * @param tplPath Word模板路径
   * @param json 合并用的数据，数据为json格式
   * @param cmdDelimiter 占位参数的分隔符，默认为'['${', '}']'，若为string类型则表示前后相同，若为数组，则可分别指定前后分隔符,
   *  注意：在docx中使用了HTML命令时，必需使用"+++"分隔符，否则这个命令将不能正确生效，这个命令使用时需要像如下方式：
   *  <body><span style="color:red;">${xxx}</span></body>
   *  来使用，即至少需要被body标签包含着
   * @return {@link PathResult}
   * @author: jiangbin
   * @date: 2021-02-04 09:37:08
   **/
  create: async (tplPath: string, json: any, cmdDelimiter?: string | [string, string]) => {
    try {
      cmdDelimiter = cmdDelimiter || json['cmdDelimiter'] || ['${', '}'];
      LogUtils.info('DocxTemplate', `使用参数前后分隔符=>${JSON.stringify(cmdDelimiter)}`);
      //读取模板内容
      const template = fs.readFileSync(tplPath);

      //合并Word模板和数据
      const buffer = await createDocx({
        template,
        /**
         * 参数数据
         * @author: jiangbin
         * @date: 2021-02-05 08:46:55
         **/
        data: {
          ...DocxDefaultData.data,
          ...json,
        },
        /**
         * 占位符命令的前后分隔符
         * @author: jiangbin
         * @date: 2021-02-05 08:46:30
         **/
        cmdDelimiter: cmdDelimiter || json['cmdDelimiter'] || ['${', '}'],
        /**
         * 错误消息日志
         * @author: jiangbin
         * @date: 2021-02-05 08:45:43
         **/
        errorHandler: (e: Error, raw_code?: string) => {
          LogUtils.error('DocxTemplate', e.message);
        },
        /**
         * 额外的JS上下文内容，用于生成图片等
         * @author: jiangbin
         * @date: 2021-02-05 08:45:59
         **/
        additionalJsContext: DocxJsContext,
      });

      //创建文件路径
      const pathResult = PathUtils.getWordPath();
      FileUtils.createDir(pathResult.path);

      console.log(`Word PATH==>${JSON.stringify(pathResult)}`);

      //写文件
      fs.writeFileSync(pathResult.path, buffer);

      return pathResult;
    } catch (err) {
      LogUtils.error('DocxTemplate', '处理Word出现异常=>' + err);
      throw err;
    }
  },
};
