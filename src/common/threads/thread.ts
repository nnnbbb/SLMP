//加载多线程模块
const { isMainThread, Worker, parentPort, threadId, workerData, MessagePort, MessageChannel } = require('worker_threads');

/**
 * 多线程支持
 * @author: jiangbin
 * @date: 2020-12-17 08:49:29
 **/
export const Thread = {};
