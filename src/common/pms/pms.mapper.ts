import { LogUtils } from '../../common/log4js/log4js.utils';
import { SysPermission } from '../../sys/entity/sys.permission.entity';
import { SysServices } from '../../sys/sys.services';
import { SYS_NODE_TYPE_DICT, SYS_SUB_SYSTEM_DICT, SYS_SWITCH_ENUM_DICT } from '../dictionary/sys.dict.defined';

/**
 * 权限信息映射表，加载系统数据库中定义的所有已启用的后端权限
 *
 * @author jiang
 * @date 2021-01-06 20:15:40
 **/
export class PmsMapper {
  private static pmsMap: Map<string, SysPermission> = new Map<string, SysPermission>();
  private static pmsList: SysPermission[] = [];

  /**
   * 重新加载全部后端权限
   * @author: jiangbin
   * @date: 2021-01-07 08:46:52
   **/
  static reload() {
    LogUtils.info(PmsMapper.name, '开始加载全部后端权限信息...');
    SysServices.sysPermissionService
      .findList({
        perSystem: SYS_SUB_SYSTEM_DICT.BACK_END.value,
        perType: SYS_NODE_TYPE_DICT.DATA.value,
        perState: SYS_SWITCH_ENUM_DICT.ON.value,
      })
      .then((pmsList: SysPermission[]) => {
        PmsMapper.pmsMap = new Map<string, SysPermission>();
        PmsMapper.pmsList = [];
        if (!pmsList || pmsList.length == 0) return;
        pmsList.forEach((pms) => {
          PmsMapper.pmsMap.set(`${pms.perModule}-${pms.perUrl}`, pms);
        });
        PmsMapper.pmsList = pmsList;
        LogUtils.info(PmsMapper.name, '成功加载全部后端权限数据!');
      })
      .catch((err) => {
        LogUtils.error(PmsMapper.name, '加载全部后端权限数据失败!', err);
      });
  }

  /**
   * 获取指定权限信息记录
   * @param module 模块名
   * @param url 权限URL
   * @return SysPermission
   * @author: jiangbin
   * @date: 2021-01-07 12:00:54
   **/
  public static getPms(module: string, url: string): SysPermission {
    return PmsMapper.pmsMap.get(`${module}-${url}`);
  }

  /**
   * 获取所有权限信息列表
   * @author: jiangbin
   * @date: 2021-01-18 18:56:20
   **/
  public static getAll(): SysPermission[] {
    return PmsMapper.pmsList;
  }
}
