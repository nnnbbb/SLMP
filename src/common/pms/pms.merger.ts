import { DATA_NODE, DIR_NODE, ROOT_NODE_ID } from '../tree/tree.node.type';
import { uuid } from '../utils/uuid.utils';
import { SysPermission } from '../../sys/entity/sys.permission.entity';
import { SysPermissionCDto } from '../../sys/dto/create/sys.permission.cdto';

export declare type SysPermissionMergerDtos = SysPermission[] | Partial<SysPermission>[] | SysPermissionCDto[] | Partial<SysPermissionCDto>[];

/**
 * <pre>合并新权限到老权限列表中，用于提供权限的增量更新，即已有权限不动，
 * 只对照新老权限表将新权限进行初始化，合并逻辑如下：
 * 1、若新权限表和老权限表均不存在，则直接返回null
 * 2、若新权限表不存在，则直接返回老权限表
 * 3、若老权限表不存在，则直接返回新权限表
 * 4、从根结点开始合并权限：
 *    a、若新权限表中存在且老权限表中不存在则添加到老权限表中并构建关联关系
 *    b、若新权限表中存在且老权限表中也存在，则跳过节点
 * </pre>
 * @author: jiangbin
 * @date: 2021-03-16 14:42:06
 **/
export const SysPmsMerger = {
  /**
   * 合并新数据到已有权限中
   * @param perSystem 所属系统
   * @param news 原权限列表
   * @param olds 已有权限列表
   * @return 合并后的权限列表
   * @author: jiangbin
   * @date: 2021-03-16 15:14:03
   **/
  merger(news: SysPermissionMergerDtos, olds: SysPermissionMergerDtos) {
    if ((!olds || olds.length == 0) && (!news || news.length == 0)) {
      return null;
    }
    if (!olds || olds.length == 0) {
      return news;
    }
    if (!news || news.length == 0) {
      return olds;
    }

    let targets = [];

    //处理目录节点
    let dirs = this.mergerDir(news, olds);

    //合并数据节点
    return targets.concat(dirs);
  },

  /**
   * 合并DIR节点
   * @param news 原权限列表
   * @param olds 已有权限列表
   * @return 合并后的权限列表
   * @author: jiangbin
   * @date: 2021-03-16 15:53:12
   **/
  mergerDir(news: SysPermissionMergerDtos, olds: SysPermissionMergerDtos) {
    let odirs = olds.filter((oitem) => oitem.perParentId === ROOT_NODE_ID);
    let ndirs = news.filter((nitem) => nitem.perParentId === ROOT_NODE_ID);
    if ((!odirs || odirs.length == 0) && (!ndirs || ndirs.length == 0)) {
      return null;
    }
    if (!odirs || odirs.length == 0) {
      return ndirs;
    }
    if (!ndirs || ndirs.length == 0) {
      return odirs;
    }

    //构建分组映射表
    let odmap = new Map();
    odirs.forEach((odir) => {
      odmap.set(odir.perModule, odir);
    });

    let targets = [];

    //老权限不作变动
    targets = targets.concat(odirs);

    ndirs.forEach((ndir) => {
      if (!ndir.perId) {
        ndir.perId = uuid();
      }
      let odir = odmap.get(ndir.perModule);
      if (odir) {
        //查找新老数据中的叶子节点列表
        let oleafs = this.findLeafByGroup(olds, ndir.perModule);
        let nleafs = this.findLeafByGroup(news, ndir.perModule);

        //合并叶子结点列表
        let leafs = this.mergerLeafs(nleafs, oleafs, odir.perId);
        if (leafs?.length > 0) {
          targets = targets.concat(leafs);
        }
      } else {
        //老数据中不存在该新分组号
        targets.push(ndir);

        //查找指定分组关联的叶子节点列表
        let leafs = this.findLeafByGroup(news, ndir.perModule);
        if (leafs?.length > 0) {
          targets = targets.concat(leafs);
        }
      }
    });

    //填充目录节点序号
    let index = 1;
    targets.forEach((target) => {
      if (target.perId != ROOT_NODE_ID && target.perType === DIR_NODE) {
        target.perOrder = index++;
      }
    });

    return targets;
  },

  /**
   * 查找指定分组的叶子节点
   * @param items 节点列表
   * @param perModule 分组
   * @return
   * @author: jiangbin
   * @date: 2021-03-16 16:47:25
   **/
  findLeafByGroup(items: SysPermissionMergerDtos, perModule: string) {
    return items.filter((item) => item.perModule === perModule && item.perType === DATA_NODE);
  },

  /**
   * 合并叶子节点列表
   * @param nleafs 新叶子节点列表
   * @param oleafs 老叶子节点列表
   * @param perParentId 父节点ID
   * @return
   * @author: jiangbin
   * @date: 2021-03-16 16:52:36
   **/
  mergerLeafs(nleafs: SysPermissionMergerDtos, oleafs: SysPermissionMergerDtos, perParentId: string) {
    if ((!nleafs || nleafs.length == 0) && (!oleafs || oleafs.length == 0)) {
      return null;
    }
    if (!nleafs || nleafs.length == 0) {
      return oleafs;
    }
    if (!oleafs || oleafs.length == 0) {
      return nleafs;
    }

    //构建分组映射表
    let olmap = new Map();
    oleafs.forEach((oleaf) => {
      olmap.set(oleaf.perUrl, oleaf);
    });

    let targets = [];

    //原权限不作变动
    targets = targets.concat(oleafs);

    //新权限不在老权限列表中则添加之
    nleafs.forEach((nleaf) => {
      let oleaf = olmap.get(nleaf.perUrl);
      if (!oleaf) {
        nleaf.perParentId = perParentId;
        targets.push(nleaf);
      }
    });

    //填充叶子节点顺序号
    targets.forEach((item, index) => {
      item.perOrder = index + 1;
    });

    return targets;
  },
};
