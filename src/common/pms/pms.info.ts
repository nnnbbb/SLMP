/**
 * 权限父结点信息
 * @author: jiangbin
 * @date: 2021-01-12 19:18:01
 **/
export interface PmsRoot {
  system: string;
  group: string;
  name: string;
  nameEn: string;
  type: string;
}

/**
 * 权限项节点信息
 * @author: jiangbin
 * @date: 2021-01-12 19:17:49
 **/
export interface PmsItem {
  id: string;
  name: string;
  nameEn: string;
  enable: boolean;
  root: PmsRoot;
}

/**
 * 权限信息
 * @author: jiangbin
 * @date: 2021-01-12 17:21:01
 **/
export interface PmsInfo {
  [key: string]: PmsItem;
}
