/**
 * 消息代码，用于区分不同类型的消息
 * @author: jiangbin
 * @date: 2021-02-07 12:04:19
 **/
export enum MESSAGE_CODE {
  /**
   * 正常消息
   * @author: jiangbin
   * @date: 2021-02-07 12:05:08
   **/
  INFO = 0,
  /**
   * 警告消息
   * @author: jiangbin
   * @date: 2021-02-07 12:05:11
   **/
  WARN = 1,
  /**
   * 错误消息
   * @author: jiangbin
   * @date: 2021-02-07 12:05:13
   **/
  ERROR = 2,
}
