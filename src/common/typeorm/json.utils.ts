/**
 * JSON工具类
 * @Date 12/5/2020, 1:54:58 PM
 * @author jiangbin
 * @export
 * @class JsonUtils
 */
export const JsonUtils = {
  /**
   * 格式化JSON字符串，用于将mysql查询结果格式化，去掉RowDataPacket等影响使用的信息
   * @param results
   */
  format: (results) => {
    if (!results || results.length == 0) return '';
    //格式化成字符串
    let target = JSON.stringify(results);
    //重新解析成json
    return JSON.parse(target);
  },
  /**
   * 格式化并获取第一个数据
   * @param results
   */
  first: (results) => {
    let json = JsonUtils.format(results);
    return json && json.length > 0 ? json[0] : '';
  },
};
