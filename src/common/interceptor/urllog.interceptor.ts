import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LogUtils } from '../log4js/log4js.utils';
import { getReqUrl } from '../utils/req.utils';

/**
 * 打印用户访问的URL到日志文件
 */
@Injectable()
export class UrllogInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req = context.switchToHttp().getRequest();

    //打印URL日志
    let url = getReqUrl(req);
    LogUtils.info(UrllogInterceptor.name, `${url}`);
    const now = Date.now();
    return next.handle().pipe(tap(() => LogUtils.info(UrllogInterceptor.name, `${url} 耗时:${Date.now() - now}ms`)));
  }
}
