import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { PARAM_NAMES } from '../constants/constants';
import { UserContext } from '../context/user.context';
import { SYS_USER } from '../../sys/config/sys.config.defined';

/**
 * 前端语言拦截器，用于将前端语言参数设置到当前用户上下文环境中，以便于支持国际化语言
 */
@Injectable()
export class LocaleInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req = context.switchToHttp().getRequest();

    //设置语言
    UserContext.language = req.query[PARAM_NAMES.LANGUAGE] || SYS_USER.USER_LANGUAGE.value;

    return next.handle();
  }
}
