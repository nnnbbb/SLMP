import { Controller, Get, NotFoundException, Query, Res } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from '../../base/controller/base.controller';
import { EXCEL_TEMPLATES } from '../templates/excel.template.ids';
import { ExcelContext } from '../templates/excel.context';
import * as fs from 'fs';

/**
 * Excel文件处理控制器
 * @author jiang
 * @date 2020-12-06 10:45:35
 **/
@Controller('excel')
@ApiTags('文件下载')
export class ExcelController extends BaseController {
  /**
   * 下载指定ID的模板文件
   *
   * @param templateId 定义于{@FILE_TEMPLATES}中
   * @return
   * @author jiang
   * @date 2021-01-25 00:42:18
   **/
  @ApiOperation({ summary: '下载指定ID的模板文件' })
  @ApiQuery({ name: 'templateId', description: '模板ID' })
  @Get('get/template/path')
  async getTemplatePath(@Res() res: Response, @Query('templateId') templateId: string) {
    return ExcelContext.getTemplateRelativePath(EXCEL_TEMPLATES[templateId]);
  }

  /**
   * 下载指定ID的模板文件
   *
   * @param templateId 定义于{@EXCEL_TEMPLATES}中
   * @return
   * @author jiang
   * @date 2021-01-25 00:42:18
   **/
  @ApiOperation({ summary: '下载指定ID的模板文件' })
  @ApiQuery({ name: 'templateId', description: '模板ID' })
  @Get('download/template')
  async downloadTemplate(@Res() res: Response, @Query('templateId') templateId: string) {
    const filePath = ExcelContext.getTemplatePath(EXCEL_TEMPLATES[templateId]);
    if (!filePath || filePath.length == 0) {
      this.logger().error(`模板未定义==>${templateId}`);
      throw new NotFoundException(`模板(${templateId})未定义!`);
    }
    if (fs.existsSync(filePath)) {
      this.logger().info(`开始下载模板文件==>${filePath}`);
      return res.sendFile(filePath);
    } else {
      this.logger().error(`模板文件不存在==>${filePath}`);
      throw new NotFoundException('模板文件不存在!');
    }
  }
}
