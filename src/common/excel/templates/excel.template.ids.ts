/**
 * Excel文件模板列表
 * @author: jiangbin
 * @date: 2021-02-03 12:21:40
 **/
export const EXCEL_TEMPLATES = {
  ssr_sample: {
    cn: 'sample/样品信息模板-20201015.xlsx',
    en: 'sample/SampleTemplate-20201015.xlsx',
  },
  ssr_sample_relations: {
    cn: 'dna/DNA信息模板-20210328.xlsx',
    en: 'dna/DnaTemplate-20210328.xlsx',
  },
  ssr_sample_plate: {
    cn: 'plate/上样板模板-20210406.xlsx',
    en: 'plate/SamPlateTemplate-20210406.xlsx',
  },
  ssr_sample_download: {
    cn: 'sample/样品信息模板-20201015.xlsx',
    en: 'sample/SampleTemplate-20201015.xlsx',
  },
  snp_locus_upload_template: {
    cn: 'locus/位点信息上传模板-20201015.xlsx',
    en: 'locus/LocusUploadTemplate-20201015.xlsx',
  },
  snp_locus_download_template: {
    cn: 'locus/位点信息下载模板-20201015.xlsx',
    en: 'locus/LocusDownloadTemplate-20201015.xlsx',
  },
};
