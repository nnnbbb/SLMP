import { SheetData, SheetDataType } from './sheet.data';
import { ArrayUtils } from '../../utils/array.utils';
import { ExcelUtils } from './excel.utils';

const xlsx = require('node-xlsx');

/**
 * 行数据转换器
 * @param params.index 行索引号，由0开始，0表示第一行数据
 * @param params.row 行数据
 * @return 行数据转换成的数据对象{T}
 * @author: jiangbin
 * @date: 2021-03-25 17:38:23
 **/
export declare type ExcelRowConvertor<T> = (params: { sheet: SheetDataType; index: number; row: any[] }) => T;

/**
 * Excel解析类，主要是包装了node-xlsx模块的功能
 * @author jiang
 * @date 2020-12-07 22:09:52
 **/
export interface IExcelParser {
  /**
   * 解析Excel文件所有Sheet的数据
   * @param path
   * @return { SheetData[] }
   * @author jiang
   * @date 2020-12-07 22:06:18
   **/
  parse(path: string): SheetDataType[];
}

/**
 * Excel类，主要是包装了node-xlsx模块的功能，以适应大多数常规操作环境，
 * 如果本类功能不适应需要也可以参考类中的代码自行操作Excel完成更加复杂的效果
 * @author jiang
 * @date 2020-12-07 22:09:52
 **/
export class ExcelParser implements IExcelParser {
  /**
   * 解析Excel文件
   * @param path
   * @return { SheetData[] }
   * @author jiang
   * @date 2020-12-07 22:06:18
   **/
  parse(path: string): SheetDataType[] {
    let sheets = xlsx.parse(path);
    if (ArrayUtils.isEmpty(sheets)) {
      return null;
    }
    let targets = new Array<SheetDataType>();
    for (let sheetIndex = 0; sheetIndex < sheets.length; sheetIndex++) {
      let sheet = sheets[sheetIndex];

      let { data } = sheet;

      //统计行数和列数
      let colCount = 0;
      let rows = new Array<string[]>();
      for (let rowIndex = 0; rowIndex < data.length; rowIndex++) {
        let cols = data[rowIndex];
        if (ArrayUtils.isEmpty(cols)) {
          continue;
        }
        rows.push(cols);
        if (cols.length > colCount) {
          colCount = cols.length;
        }
      }

      let target: SheetDataType = {
        index: sheetIndex,
        name: sheet['name'],
        data: rows,
        rowCount: rows?.length || 0,
        colCount,
      };
      targets.push(target);
    }
    return targets;
  }

  /**
   * 将sheet数据转换成对象列表
   * @param sheets sheet数组列表
   * @param render 行数据转换器
   * @return 转换后的数据对象列表{T}，若未给定sheet数组则返回null值
   * @author: jiangbin
   * @date: 2021-03-25 17:41:12
   **/
  convert<T>(sheets: SheetDataType[], render: ExcelRowConvertor<T>): T[] {
    if (ArrayUtils.isEmpty(sheets)) {
      return null;
    }
    let targets = new Array<T>();
    for (let index = 0; index < sheets.length; index++) {
      let sheet = sheets[index];
      if (!sheet) continue;
      let rows = ExcelUtils.getRows(sheet);
      rows?.forEach((row, index) => {
        let target = render({ sheet, index, row: row as any[] });
        target && targets.push(target);
      });
    }
    return targets;
  }
}
