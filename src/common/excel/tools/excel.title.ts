import { LocaleData, LocaleDataArray } from '../../locales/locale';

/**
 * Excel标题信息定义接口，支持国际化
 *
 * @author jiang
 * @date 2021-03-27 12:44:04
 **/
export interface ExcelTitle {
  /**
   * Sheet名称
   *
   * @author jiang
   * @date 2021-03-27 12:44:38
   **/
  sheetName: LocaleData<string>;
  /**
   * 默认标题行
   *
   * @author jiang
   * @date 2021-03-27 12:44:42
   **/
  title: LocaleDataArray<string>;
  /**
   * Excel列参数定义，例如：
   * <code> {'!cols': [{ wch: 6 }, { wch: 7 }, { wch: 10 }, { wch: 20 } ]}</code>
   * 以定义参数表示定义列宽，可以参看：{@link https://www.npmjs.com/package/node-xlsx}
   * @author: jiangbin
   * @date: 2021-04-20 10:24:29
   **/
  options?: any;
}
