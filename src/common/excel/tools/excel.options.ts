/**
 * Excel参数名
 * @author: jiangbin
 * @date: 2021-04-20 10:45:15
 **/
export const ExcelOptions = {
  /**
   * 合并列参数名
   * @author: jiangbin
   * @date: 2021-04-20 10:45:30
   **/
  merges: '!merges',
  /**
   * 列宽定义参数名
   * @author: jiangbin
   * @date: 2021-04-20 10:45:39
   **/
  cols: '!cols',
};
