import { Global, Logger, Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { MailController } from './controller/mail.controller';
import { PUBLIC_FOLDER } from '../path/path.creator.utils';
import { CONFIG } from '../domain/constants';
import { ApiConfigModule } from '../config/api.config.module';
import { ApiConfig } from '../config/api.config';

/**
 * 邮件模块
 *
 * @author jiang
 * @date 2021-04-10 10:16:23
 **/
@Global()
@Module({
  imports: [
    //邮件服务
    MailerModule.forRootAsync({
      imports: [ApiConfigModule],
      useFactory: async (config: ApiConfig) => {
        Logger.log(`DOCKER MAIL HOST==>${process.env.MAIL_HOST || ''}`);
        Logger.log(`DOCKER MAIL PORT==>${process.env.MAIL_PORT || ''}`);
        Logger.log(`DOCKER MAIL USER==>${process.env.MAIL_USER || ''}`);
        const host = process.env.MAIL_HOST || config.mailHost;
        const port = parseInt(process.env.MAIL_PORT) || config.mailPort;
        const user = process.env.MAIL_USER || config.mailUser;
        const pass = process.env.MAIL_PASSWORD || config.mailPassword;
        Logger.log(`MAIL HOST==>${host}`);
        Logger.log(`MAIL PORT==>${port}`);
        Logger.log(`MAIL USER==>${user}`);
        return {
          transport: {
            host,
            port,
            secure: true,
            auth: {
              user,
              pass,
            },
            preview: true,
            template: {
              dir: `${PUBLIC_FOLDER}/template/mail`,
              adapter: new PugAdapter(),
              options: {
                strict: true,
              },
            },
          },
        };
      },
      inject: [ApiConfig],
    }),
  ],
  controllers: [MailController],
  providers: [],
  exports: [],
})
export class MailModule {}
