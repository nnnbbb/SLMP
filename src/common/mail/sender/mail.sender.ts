import { NestExpressApplication } from '@nestjs/platform-express';
import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { LogUtils } from '../../log4js/log4js.utils';

/**
 * 邮件参数信息对象
 * @author: jiangbin
 * @date: 2021-04-13 09:03:42
 **/
export type MailParams = ISendMailOptions & {
  /**
   * 邮件模板ID
   *
   * @author jiang
   * @date 2021-04-10 11:44:10
   **/
  templateId?: string;
};

/**
 * 邮件服务
 *
 * @author jiang
 * @date 2021-04-10 10:26:50
 **/
export class MailSender {
  private static category = 'MailSender';
  public static mailerService: MailerService;

  /**
   * 初始化邮件模块服务类的静态引用
   * @date 1/7/2021, 9:25:29 PM
   * @author jiangbin
   **/
  static init(app: NestExpressApplication) {
    MailSender.mailerService = app.get(MailerService);
  }

  /**
   * 发送邮件
   * @param params 邮件发送参数
   * @return
   * @author: jiangbin
   * @date: 2021-04-13 08:55:29
   **/
  static async send(params: MailParams) {
    try {
      return await this.mailerService.sendMail(params);
    } catch (err) {
      LogUtils.error(this.category, `邮件发送失败=>${JSON.stringify(err)}`);
    }
  }
}
