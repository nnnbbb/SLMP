import { Body, Controller, Get, NotFoundException, Post, Query, Res } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from '../../base/controller/base.controller';
import * as fs from 'fs';
import { MailContext } from '../templates/mail.context';
import { MAIL_TEMPLATES } from '../templates/mail.template.ids';
import { MailParams, MailSender } from '../sender/mail.sender';
import { SkipAuth } from '../../auth/guard/skip.auth';
import { DOCX_TEMPLATES } from '../../docx/templates/docx.template.id';
import { DocxContext } from '../../docx/templates/docx.context';
import { uuid } from '../../utils/uuid.utils';

/**
 * Mail处理控制器
 * @author jiang
 * @date 2020-12-06 10:45:35
 **/
@SkipAuth()
@Controller('mail')
@ApiTags('邮件')
export class MailController extends BaseController {
  /**
   * 下载指定ID的邮件模板文件
   *
   * @param templateId 定义于{@MAIL_TEMPLATES}中
   * @return
   * @author jiang
   * @date 2021-01-25 00:42:18
   **/
  @ApiOperation({ summary: '下载指定ID的模板文件' })
  @ApiQuery({ name: 'templateId', description: '模板ID' })
  @Get('get/template/path')
  async getTemplatePath(@Res() res: Response, @Query('templateId') templateId: string) {
    return MailContext.getTemplateRelativePath(MAIL_TEMPLATES[templateId]);
  }

  /**
   * 发送指定ID的模板文件的邮件
   *
   * @param templateId 定义于{@MAIL_TEMPLATES}中
   * @return
   * @author jiang
   * @date 2021-01-25 00:42:18
   **/
  @ApiOperation({ summary: '发送指定ID的模板文件的邮件' })
  @ApiQuery({ name: 'params', description: '邮件参数' })
  @Post('send')
  async send(@Res() res: Response, @Body('params') params: MailParams) {
    let { templateId } = params;
    let template = MAIL_TEMPLATES[templateId];
    params.template = template;
    const filePath = MailContext.getTemplatePath(template);
    if (!filePath || filePath.length == 0) {
      this.logger().error(`邮件模板未定义==>${templateId}`);
      throw new NotFoundException(`邮件模板(${templateId})未定义!`);
    }

    if (fs.existsSync(filePath)) {
      this.logger().info(`使用的邮件模板==>${filePath}`);
      await MailSender.send(params);
      return true;
    } else {
      this.logger().error(`邮件模板文件不存在==>${filePath}`);
      throw new NotFoundException('邮件模板文件不存在!');
    }
  }

  /**
   * 测试发送邮件
   * @author: jiangbin
   * @date: 2021-04-13 09:05:26
   **/
  @Get('test')
  @ApiOperation({ summary: '发送测试邮件' })
  async test() {
    //经测试：预读取附件内容为Buffer再发送的方式会提高发送邮件的速度
    const filePath = DocxContext.getTemplatePath(DOCX_TEMPLATES['test_result']);
    const buffer = fs.readFileSync(filePath);
    /**
     * CC 英文全称是 Carbon Copy(抄送);
     * BCC英文全称是 Blind CarbonCopy(密送)。
     * 两者的区别在于在BCC栏中的收件人可以看到所有的收件人名(TO,CC,BCC)，而在TO 和CC栏中的收件人看不到BBC的收件人名。
     */
    return await MailSender.send({
      from: { name: '江彬', address: 'ximele@qq.com' },
      to: 'jiangbinboy@126.com',
      cc: 'ximele@126.com', //抄送
      bcc: '404093781@qq.com', //暗抄送
      text: '测试邮件',
      subject: '测试邮件',
      attachments: [
        {
          filename: '测试附件.docx',
          content: buffer,
          cid: uuid(),
        },
      ],
    });
  }
}
