/**
 * 邮件内容模板列表
 * @author: jiangbin
 * @date: 2021-02-03 12:21:40
 **/
export const MAIL_TEMPLATES = {
  result: {
    cn: 'result/结果邮件-20201015.pug',
    en: 'result/ResultMail-20201015.pug',
  },
};
