import { BOMUtils } from '../../utils/bom.utils';
import { ArrayUtils } from '../../utils/array.utils';
import { StringUtils } from '../../utils/string.utils';

/**
 * CSV行数据渲染器
 * @author: jiangbin
 * @date: 2021-03-31 11:49:19
 **/
export declare type CsvRowRender<T> = (item: T, index: number) => string;

/**
 * CSV记录创建器
 * @author: jiangbin
 * @date: 2021-03-31 10:47:22
 **/
export class CsvCreator<T> {
  /**
   * 创建CSV文件
   * @param params.data
   * @param params.render
   * @return string
   * @author: jiangbin
   * @date: 2021-03-31 10:47:39
   **/
  create(params: { data: T[]; render: CsvRowRender<T> }): string {
    let { data, render } = params;
    let contents = new Array<string>();
    if (ArrayUtils.isEmpty(data)) {
      return null;
    }
    data.forEach((item, index) => {
      let content = render(item, index);
      if (StringUtils.isNotBlank(content)) {
        contents.push(content);
      }
    });
    if (ArrayUtils.isEmpty(contents)) {
      return null;
    }
    let str = contents.join('\n');
    return BOMUtils.addBOM(str, BOMUtils.UTF_8);
  }
}
