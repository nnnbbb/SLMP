import { LocaleDataArray } from '../../locales/locale';

/**
 * Csv标题信息定义接口，支持国际化
 *
 * @author jiang
 * @date 2021-03-27 12:44:04
 **/
export interface CsvTitle {
  /**
   * 默认标题行
   *
   * @author jiang
   * @date 2021-03-27 12:44:42
   **/
  title: LocaleDataArray<string>;
}
