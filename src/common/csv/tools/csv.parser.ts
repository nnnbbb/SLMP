import * as fs from 'fs';
import * as readline from 'readline';
import { LogUtils } from '../../log4js/log4js.utils';
import { StringUtils } from '../../utils/string.utils';
import { ArrayUtils } from '../../utils/array.utils';

/**
 * CSV行数据转换器，将行数据转换成对象
 * @author: jiangbin
 * @date: 2021-03-31 11:49:19
 **/
export declare type CsvRowConvertor<T> = (row: string, index: number) => T;

/**
 * CSV回调接口，用于保存解析后的记录列表
 * @author: jiangbin
 * @date: 2021-03-31 14:12:50
 **/
export declare type CsvCallback<T> = (records: T[]) => void;

/**
 * CSV记录解析器
 * @author: jiangbin
 * @date: 2021-03-31 10:47:22
 **/
export class CsvParser<T> {
  //日志分类名
  category = 'CsvParser';

  /**
   * 解析CSV文件
   * @param params.csvPath csv文件路径
   * @param params.convertor 转换行数据为对象类型
   * @return
   * @author: jiangbin
   * @date: 2021-03-31 10:47:39
   **/
  parser(params: { csvPath: string; convertor: CsvRowConvertor<T>; callback: CsvCallback<T> }) {
    let { csvPath, convertor, callback } = params;

    const start = Date.now();

    //创建读取流对象
    let stream = fs.createReadStream(csvPath, { autoClose: true, encoding: 'utf-8' });

    //创建行读取接口
    let reader = readline.createInterface({ input: stream });

    //结果列表
    let records = new Array<T>();

    //行索引号
    let rowIndex = 0;

    //按行读取数据事件
    reader.on('line', (line: string) => {
      if (StringUtils.isBlank(line)) {
        return;
      }

      //转换行数据成对象
      let record = convertor(line, rowIndex++);
      if (record) {
        records.push(record);
      }
    });

    //关闭事件
    reader.on('close', (err) => {
      if (err) {
        LogUtils.error(this.category, `关闭读取流失败=>${csvPath}`, err);
      } else {
        LogUtils.info(this.category, `读取文件成功(${csvPath})，共耗时==>${(Date.now() - start) / 1000} s`);
      }

      //回调处理记录列表
      if (ArrayUtils.isNotEmpty(records)) {
        callback(records);
      }
    });
  }
}
