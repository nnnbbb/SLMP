import { ArrayUtils } from '../../utils/array.utils';
import { StringUtils } from '../../utils/string.utils';
import { PATTERNS } from '../../regex/patterns';

/**
 * CSV工具类
 * @author: jiangbin
 * @date: 2021-04-21 15:51:59
 **/
export const CsvUtils = {
  /**
   * 解析行数据，若列数据以单引号或双引号包括则会自动剔除掉，例如:
   * 'G/G',G/A=>[G/G,G/A]
   * @param params.row 行数据
   * @param params.separator 分隔符，默认支持的正则式为：/[\t, ]/
   * @param revise 校正列数据
   * @return
   * @author: jiangbin
   * @date: 2021-04-21 15:52:07
   **/
  parse(params: { row: string; separator?: string | RegExp }, revise?: (col: string, index?: number) => string) {
    let { row, separator = /[\t, ]/ } = params;
    let cols = row.split(separator);
    if (ArrayUtils.isEmpty(cols)) {
      return void 0;
    }
    return cols.map((col, index) => {
      if (StringUtils.isBlank(col)) {
        return void 0;
      }
      let target = PATTERNS.csv.match(col);
      if (revise !== void 0) {
        target = revise(target, index);
      }
      return target;
    });
  },
};
