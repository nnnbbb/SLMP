import { SysDictionary } from '../../sys/entity/sys.dictionary.entity';
import { DATA_NODE, DIR_NODE, ROOT_NODE_ID } from '../tree/tree.node.type';
import { SysDictionaryCDto } from '../../sys/dto/create/sys.dictionary.cdto';
import { ArrayUtils } from '../utils/array.utils';
import { Mapper } from '../mapper/mapper';

export declare type SysDictMergerDto = SysDictionary | Partial<SysDictionary> | SysDictionaryCDto | Partial<SysDictionaryCDto>;

export declare type SysDictMergerDtos = SysDictMergerDto[];

/**
 * <pre>合并新字典到老字典列表中，用于提供字典的增量更新，即已有字典不动，
 * 只对照新老字典表将新字典进行初始化，合并逻辑如下：
 * 1、若新字典表和老字典表均不存在，则直接返回null
 * 2、若新字典表不存在，则直接返回老字典表
 * 3、若老字典表不存在，则直接返回新字典表
 * 4、从根结点开始合并字典：
 *    a、若新字典表中存在且老字典表中不存在则添加到老字典表中并构建关联关系
 *    b、若新字典表中存在且老字典表中也存在，则跳过节点
 * </pre>
 * @author: jiangbin
 * @date: 2021-03-16 14:42:06
 **/
export const SysDictMerger = {
  /**
   * 合并新数据到已有字典中
   * @param news 原字典列表
   * @param olds 已有字典列表
   * @return 合并后的字典列表
   * @author: jiangbin
   * @date: 2021-03-16 15:14:03
   **/
  merger(news: SysDictMergerDtos, olds: SysDictMergerDtos) {
    if ((!olds || olds.length == 0) && (!news || news.length == 0)) {
      return null;
    }
    if (ArrayUtils.isEmpty(olds)) {
      return this.addRoot(news);
    }
    if (ArrayUtils.isEmpty(news)) {
      return this.addRoot(olds);
    }

    let targets = [];
    //处理根节点
    let root = this.mergerRoot(news, olds);
    targets.push(root);

    //处理目录节点
    let dirs = this.mergerDir(news, olds);

    dirs?.length > 0 && (targets = targets.concat(dirs));

    //合并数据节点
    return targets;
  },
  /**
   * 合并ROOT节点
   * @param news 原字典列表
   * @param olds 已有字典列表
   * @return 合并后的字典列表
   * @author: jiangbin
   * @date: 2021-03-16 15:52:51
   **/
  mergerRoot(news: SysDictMergerDtos, olds: SysDictMergerDtos) {
    let oroot = olds.filter((oitem) => oitem.dicId === ROOT_NODE_ID);
    if (oroot && oroot['dicId']) return oroot;
    let nroot = news.filter((nitem) => nitem.dicId === ROOT_NODE_ID);
    return nroot && nroot['dicId'] ? nroot : this.createRoot();
  },
  /**
   * 合并DIR节点
   * @param news 原字典列表
   * @param olds 已有字典列表
   * @return 合并后的字典列表
   * @author: jiangbin
   * @date: 2021-03-16 15:53:12
   **/
  mergerDir(news: SysDictMergerDtos, olds: SysDictMergerDtos) {
    let odirs = olds.filter((item) => item.dicParentId === ROOT_NODE_ID);
    let ndirs = news.filter((item) => item.dicParentId === ROOT_NODE_ID);
    if (ArrayUtils.isEmpty(odirs) && ArrayUtils.isEmpty(ndirs)) {
      return null;
    }
    if (ArrayUtils.isEmpty(odirs)) {
      return ndirs;
    }
    if (ArrayUtils.isEmpty(ndirs)) {
      return odirs;
    }

    //构建分组映射表
    let odmap = new Mapper<string, SysDictMergerDto>();
    odmap.addList(odirs, (record) => record.dicGroup);

    let ndmap = new Mapper<string, SysDictMergerDto>();
    ndmap.addList(ndirs, (record) => record.dicGroup);

    //移除已有目录节点列表
    ndmap.deleteList(odmap.getKeys());

    //得到新插入的目录节点列表
    ndirs = ndmap.getAllValues();

    let targets = [];

    //合并目录节点列表
    if (odirs?.length > 0) {
      targets = targets.concat(odirs);
    }
    if (ndirs?.length > 0) {
      targets = targets.concat(ndirs);
    }

    //目录节点排序
    targets = this.sortNodes(targets);

    //设置目录节点序号
    this.fillNodeOrder(targets);

    //添加新插入目录节点列表
    let nleafs = this.mergerNewDirLeafs(ndirs, news);
    if (nleafs?.length > 0) {
      targets = targets.concat(nleafs);
    }

    //老字典不作变动
    let oleafs = this.mergerOldDirLeafs(odirs, olds, news);
    if (oleafs?.length > 0) {
      targets = targets.concat(oleafs);
    }

    return targets;
  },

  /**
   * 合并新目录节点的所有子节点列表
   *
   * @param ndirs 新目录节点列表
   * @param news 新节点列表
   * @return
   * @author jiang
   * @date 2021-03-27 17:04:46
   **/
  mergerNewDirLeafs(ndirs: SysDictMergerDtos, news: SysDictMergerDtos) {
    let targets = [];
    ndirs?.length > 0 &&
      ndirs.forEach((ndir) => {
        let leafs = this.findLeafByGroup(news, ndir.dicGroup);
        if (leafs?.length > 0) {
          leafs = this.sortNodes(leafs);
          this.fillNodeOrder(leafs);
          targets = targets.concat(leafs);
        }
      });
    return targets;
  },

  /**
   * 合并老目录节点下的叶子节点，若存在新的叶子节点则合并之
   *
   * @param odirs 已存在的老目录节点列表
   * @param olds 老字典节点列表
   * @param news 新字典节点列表
   * @return
   * @author jiang
   * @date 2021-03-27 17:08:02
   **/
  mergerOldDirLeafs(odirs: SysDictMergerDtos, olds: SysDictMergerDtos, news: SysDictMergerDtos) {
    let targets = [];
    odirs?.length > 0 &&
      odirs.forEach((odir) => {
        //获取老叶子节点列表
        let oleafs = this.findLeafByGroup(olds, odir.dicGroup);
        let omap = new Mapper<string, SysDictMergerDto>();
        omap.addList(oleafs, (record) => record.dicName);

        //获取新叶子节点列表
        let nleafs = this.findLeafByGroup(news, odir.dicGroup);
        let nmap = new Mapper<string, SysDictMergerDto>();
        nmap.addList(nleafs, (record) => record.dicName);

        //得到新插入的叶子节点
        nmap.deleteList(omap.getKeys());
        nleafs = nmap.getAllValues();

        //设置父节点ID为老节点目录ID
        nleafs?.length > 0 && nleafs.forEach((leaf) => (leaf.dicParentId = odir.dicId));

        let nodes = [];

        //合并数据
        if (oleafs?.length > 0) {
          nodes = nodes.concat(oleafs);
        }
        if (nleafs?.length > 0) {
          nodes = nodes.concat(nleafs);
        }

        //节点排序
        nodes = this.sortNodes(nodes);

        //设置节点序号
        this.fillNodeOrder(nodes);

        nodes?.length > 0 && (targets = targets.concat(nodes));
      });

    return targets;
  },

  /**
   * 查找指定分组的叶子节点
   * @param configs 字典列表
   * @param dicGroup 字典分组
   * @return
   * @author: jiangbin
   * @date: 2021-03-16 16:47:25
   **/
  findLeafByGroup(items: SysDictMergerDtos, dicGroup: string) {
    return items.filter((item) => item.dicGroup === dicGroup && item.dicType === DATA_NODE);
  },

  /**
   * 创建根节点
   * @author: jiangbin
   * @date: 2021-03-16 18:30:17
   **/
  createRoot() {
    return {
      dicId: ROOT_NODE_ID,
      dicName: '根结点',
      dicNameEn: 'ROOT',
      dicModule: 'ROOT',
      dicType: DIR_NODE,
      dicOrder: 1,
      dicParentId: null,
      dicState: 'ON',
    } as SysDictionary;
  },

  /**
   * 添加根结点
   * @author: jiangbin
   * @date: 2021-03-17 09:57:43
   **/
  addRoot(dtos: SysDictMergerDtos) {
    if (dtos?.length > 0) {
      let sroot = dtos.filter((dto) => dto.dicId === ROOT_NODE_ID);

      let targets = [];
      if (!sroot || sroot.length == 0) {
        targets.push(this.createRoot());
      }

      return targets.concat(dtos);
    }
    return dtos;
  },

  /**
   * 节点列表排序
   *
   * @param nodes
   * @return
   * @author jiang
   * @date 2021-03-27 17:25:20
   **/
  sortNodes(nodes: SysDictMergerDtos) {
    return nodes?.length > 0
      ? nodes.sort((source, target) => {
          if (source.dicOrder > target.dicOrder) {
            return 1;
          } else if (source.dicOrder == target.dicOrder) {
            return 0;
          } else {
            return -1;
          }
        })
      : [];
  },

  /**
   * 设置节点序号
   *
   * @param nodes
   * @return
   * @author jiang
   * @date 2021-03-27 17:29:27
   **/
  fillNodeOrder(nodes: SysDictMergerDtos) {
    nodes?.length > 0 && nodes.forEach((value, index) => (value.dicOrder = index + 1));
  },
};
