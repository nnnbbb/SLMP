/**
 * 一些在字典中可能用到的图标名称代码的定义，前端通过{AntdIconsEnum[iconName]}方式可以直接使用该图标，
 * 主要用于字典种属初始化时能自动绑定图标
 * @author: jiangbin
 * @date: 2021-03-26 19:02:42
 **/
export const SysIconNames = {
  YUMI1: 'ICON_YUMI1',

  YUMI2: 'ICON_YUMI2',

  HUANGGUA: 'ICON_HUANGGUA',

  PUTAO: 'ICON_PUTAO',

  XIGUA: 'ICON_XIGUA',

  BOCAIYE: 'ICON_BOCAIYE',

  QIEZI: 'ICON_QIEZI',

  GAOLIANG: 'ICON_GAOLIANG',

  DADOU: 'ICON_DADOU',

  GANZHE: 'ICON_GANZHE',

  LUOBU: 'ICON_LUOBU',

  NANGUA: 'ICON_NANGUA',

  XIHULU: 'ICON_XIHULU',

  FANQIE: 'ICON_FANQIE',

  LAJIAO: 'ICON_LAJIAO',

  XIAOMAI: 'ICON_XIAOMAI',

  QINCAI: 'ICON_QINCAI',

  SIGUA: 'ICON_SIGUA',

  TIANJIAO: 'ICON_TIANJIAO',

  ZIGANLAN: 'ICON_ZIGANLAN',

  SILIAO: 'ICON_SILIAO',

  LVDOU: 'ICON_LVDOU',

  HUASHENG: 'ICON_HUASHENG',

  HULUOBU: 'ICON_HULUOBU',

  WANDOU: 'ICON_WANDOU',

  CANDOU: 'ICON_CANDOU',

  TIANGUA: 'ICON_TIANGUA',

  BAICAI: 'ICON_BAICAI',

  GANJU: 'ICON_GANJU',

  CAIDOU: 'ICON_CAIDOU',

  SHUIDAO: 'ICON_SHUIDAO',

  KUGUA: 'ICON_KUGUA',

  YOUCAI: 'ICON_YOUCAI',

  ZHIMA: 'ICON_ZHIMA',

  MALINGSHU: 'ICON_MALINGSHU',

  MIANHUA: 'ICON_MIANHUA',

  XIANGRIKUI: 'ICON_XIANGRIKUI',

  DONGGUA: 'ICON_DONGGUA',

  SHENGCAI: 'ICON_SHENGCAI',

  MUGUA: 'ICON_MUGUA',

  DNA_JIANCE: 'ICON_DNA_JIANCE',

  DNA: 'ICON_DNA',

  DNA_ANALYSIS: 'ICON_DNA_ANALYSIS',
};
