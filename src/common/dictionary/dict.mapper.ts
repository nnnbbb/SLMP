import { SysServices } from '../../sys/sys.services';
import { SysDictionary } from '../../sys/entity/sys.dictionary.entity';
import { LogUtils } from '../log4js/log4js.utils';
import { LocaleContext } from '../locales/locale';

/**
 * 参数容器对象
 *
 * @author jiang
 * @date 2021-01-06 20:15:40
 **/
export class DictMapper {
  private static dictMap: Map<string, SysDictionary[]> = new Map<string, SysDictionary[]>();
  private static items: SysDictionary[] = new Array<SysDictionary>();

  /**
   * 加载全部字典项数据
   * @author: jiangbin
   * @date: 2021-01-15 21:23:09
   **/
  static reload() {
    LogUtils.info(DictMapper.name, '开始加载全部字典项...');
    DictMapper.dictMap = new Map<string, SysDictionary[]>();
    DictMapper.items = new Array<SysDictionary>();
    SysServices.sysDictionaryService
      .findList({ dicType: 'DATA', dicState: 'ON' })
      .then((res) => {
        if (!res || res.length == 0) return;
        DictMapper.items = res;
        res.forEach((dict) => {
          const items = DictMapper.dictMap.get(dict.dicGroup) || new Array<SysDictionary>();
          items.push(dict);
          DictMapper.dictMap.set(dict.dicGroup, items);
        });
        LogUtils.info(DictMapper.name, '成功加载全部字典数据!');
      })
      .catch((err) => {
        LogUtils.error(DictMapper.name, '加载全部字典数据失败!', err);
      });
  }

  /**
   * 获取字典项名
   * @param params
   * @return 字典项名
   * @author: jiangbin
   * @date: 2021-01-15 21:50:55
   **/
  static getName(params: { dicGroup: string; dicValue: string }): string {
    let items = DictMapper.dictMap.get(params.dicGroup);
    items = items.filter((item) => item.dicValue === params.dicValue);
    return items?.length > 0 ? LocaleContext.message({ cn: items[0].dicName, en: items[0].dicNameEn }) : '';
  }

  /**
   * 获取字典项值
   * @param params
   * @return 字典项值
   * @author: jiangbin
   * @date: 2021-01-15 21:50:52
   **/
  static getValue(params: { dicGroup: string; dicName: string }): string {
    let items = DictMapper.dictMap.get(params.dicGroup);
    items = items.filter((item) => {
      return LocaleContext.message({ cn: item.dicName, en: item.dicNameEn }) === params.dicName;
    });
    return items?.length > 0 ? items[0].dicValue : '';
  }

  /**
   * 获取字典项名列表
   * @param params
   * @return 字典项名列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:49
   **/
  static getNames(params: { dicGroup: string; dicValues: string[] }): string[] {
    let items =
      DictMapper.getItems(params.dicGroup).filter((item) => {
        return item.dicValue ? params.dicValues.includes(item.dicValue) : false;
      }) || [];
    return (
      items.map((item) => {
        return LocaleContext.message({ cn: item.dicName, en: item.dicNameEn });
      }) || []
    );
  }

  /**
   * 获取字典项名列表
   * @param params
   * @return 字典项名列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:49
   **/
  static getValues(params: { dicGroup: string; dicNames: string[] }): string[] {
    let items =
      DictMapper.getItems(params.dicGroup).filter((item) => {
        let itemName = LocaleContext.message({ cn: item.dicName, en: item.dicNameEn });
        return item.dicValue ? params.dicNames.includes(itemName) : false;
      }) || [];
    return items.map((item) => item.dicValue) || [];
  }

  /**
   * 获取字典项值列表
   * @param dicGroup 字典分组
   * @return 字典项值列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:46
   **/
  static getGroupValues(dicGroup: string): string[] {
    return DictMapper.getItems(dicGroup).map((item) => item.dicValue) || [];
  }

  /**
   * 获取字典项名列表
   * @param dicGroup 字典分组
   * @return 字典项名列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:49
   **/
  static getGroupNames(dicGroup: string): string[] {
    return (
      DictMapper.getItems(dicGroup).map((item) => {
        return LocaleContext.message({ cn: item.dicName, en: item.dicNameEn });
      }) || []
    );
  }

  /**
   * 获取字典项列表
   * @param dicGroup 字典分组
   * @return 字典项列表
   * @author: jiangbin
   * @date: 2021-01-15 21:50:20
   **/
  static getItems(dicGroup: string): SysDictionary[] {
    return DictMapper.dictMap.get(dicGroup) || [];
  }

  /**
   * 获取缓存的所有字典项
   *
   * @return
   * @author jiang
   * @date 2021-04-11 21:57:21
   **/
  static getAll(): SysDictionary[] {
    return DictMapper.items;
  }
}
