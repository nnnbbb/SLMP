/**
 * 字典分组信息
 * @author jiang
 * @date 2021-01-06 22:35:45
 **/
export interface DictGroup {
  nameCn: string;
  nameEn: string;
  group: string;
  enable: 'ON' | 'OFF';
}

/**
 * 字典项信息
 * @author jiang
 * @date 2021-01-06 22:35:57
 **/
export interface DictItem {
  nameCn: string;
  nameEn: string;
  value: string;
  order: number;
  type: 'string' | 'number' | 'boolean' | '';
  species: string;
  group: DictGroup;
  icon?: string;
}

export interface DictItemType {
  [key: string]: DictItem;
}
