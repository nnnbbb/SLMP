import { classToPlain } from 'class-transformer';

/**
 * 对象操作工具类
 * @author: jiangbin
 * @date: 2020-12-07 14:02:16
 **/
export const ObjUtils = {
  /**
   * 移除没有值的属性，例如字符串属性为undifined或未包含字符，Date或number为undifined
   * @param obj
   * @author: jiangbin
   * @date: 2020-12-07 14:02:13
   **/
  delEmptyProp: (obj) => {
    if (!obj) return obj;
    for (const key in obj) {
      if (obj[key] == undefined || obj[key] == '') {
        delete obj[key];
      }
    }
    return obj;
  },
  /**
   * 采用class-transformer将对象转换成纯json对象，调用本方法时class-transformer所允许使用的装饰器将起作用，
   * 这对于定义了屏蔽属性@Exclude()或@Expose()的方法等将非常方便，所以在一些需要将方法暴露成json属性的应用场景
   * 中非常有用
   * @author: jiangbin
   * @date: 2020-12-07 14:06:12
   **/
  classToPlain: (obj) => {
    return classToPlain(obj);
  },
};
