/**
 * 数字大小写处理工具类
 * @author: jiangbin
 * @date: 2021-02-19 14:57:39
 **/
export const NumberUtils = {
  /**
   * 获取大写数字(key)与阿拉伯数字(value)映射关系Mapper，例如：零-->0
   * @author Jiangbin
   * @return
   * @date 2013-8-26下午11:10:53
   */
  get mapper(): Map<string, string> {
    let mapper = new Map<string, string>();
    mapper.set('零', '0');
    mapper.set('一', '1');
    mapper.set('壹', '1');
    mapper.set('二', '2');
    mapper.set('贰', '2');
    mapper.set('三', '3');
    mapper.set('叁', '3');
    mapper.set('四', '4');
    mapper.set('肆', '4');
    mapper.set('五', '5');
    mapper.set('伍', '5');
    mapper.set('六', '6');
    mapper.set('陆', '6');
    mapper.set('七', '7');
    mapper.set('柒', '7');
    mapper.set('八', '8');
    mapper.set('捌', '8');
    mapper.set('九', '9');
    mapper.set('玖', '9');
    mapper.set('十', '9');
    mapper.set('拾', '9');
    mapper.set('百', '100');
    mapper.set('佰', '100');
    mapper.set('千', '1000');
    mapper.set('仟', '1000');
    return mapper;
  },

  /**
   * 罗马数字更改为阿拉伯数字
   * @param source
   * @return
   * @author wuhaotian
   * @date 2018年5月11日下午5:33:04
   */
  roman2int: (source: string) => {
    // 罗马数字转阿拉伯数字：
    // 从前往后遍历罗马数字，如果某个数比前一个数小，则把该数加入到结果中；
    // 反之，则在结果中两次减去前一个数并加上当前这个数；
    // I、V、X、   L、   C、     D、     M
    // 1．5、10、50、100、500、1000
    let map = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000 };
    let index = 0;
    let result = 0;
    let len = source.length;
    while (index < len) {
      let current = map[source[index]];
      result += current;
      if (index > 0) {
        let before = map[source[index - 1]];
        if ((current === map.V || current === map.X) && before === map.I) {
          result -= 2;
        }
        if ((current === map.L || current === map.C) && before === map.X) {
          result -= 20;
        }
        if ((current === map.D || current === map.M) && before === map.C) {
          result -= 200;
        }
      }
      index++;
    }
    return result;
  },

  /**
   * 数字转换成罗马数字
   * @param num
   * @return
   * @author: jiangbin
   * @date: 2021-02-19 15:48:21
   **/
  int2roman: function (num: number) {
    let map = { 1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M' };
    // 标识位数
    let digits = 1;
    // 结果
    let result = '';
    while (num) {
      let current = num % 10;
      if (current < 4) {
        result = map[digits].repeat(current) + result;
      } else if (current === 4) {
        result = map[digits] + map[digits * 5] + result;
      } else if (current > 4 && current < 9) {
        result = map[digits * 5] + map[digits].repeat(current - 5) + result;
      } else {
        result = map[digits] + map[digits * 10] + result;
      }
      digits *= 10;
      num = Math.trunc(num / 10);
    }
    return result;
  },
};
