import { format } from 'date-fns';

/**
 * 字符串处理
 * @author: jiangbin
 * @date: 2020-12-28 21:13:40
 **/
export const StringUtils = {
  /**
   * 是空字符串
   *
   * @param str
   * @return
   * @author jiang
   * @date 2021-03-27 13:40:03
   **/
  isBlank(str: string | undefined) {
    return !str || str.length === 0;
  },

  /**
   * 不是空字符串
   *
   * @param str
   * @return
   * @author jiang
   * @date 2021-03-27 13:40:03
   **/
  isNotBlank(str: string | undefined) {
    return !this.isBlank(str);
  },

  /**
   * 将下划线命名转换为驼峰命名
   * @description 'SAM_NAME' => 'samName'
   * @param {string} string
   */
  camelCase(str: string) {
    return str.toLowerCase().replace(/_([a-z])/g, function (all: string, letter: string) {
      return letter.toUpperCase();
    });
  },

  /**
   * 第一个字母大写
   * @param str
   */
  firstUpperCase(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },

  /**
   * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
   * @param value
   */
  toSqlValue: (value: string | number | Date | string[] | number[]) => {
    //null值需要特殊处理，特别针对某些树形级联构建的表数据，在针对根结点插入时必需设置上级结点ID为null值才行
    if (value === undefined || value === null || value === '') return 'null';
    if ('string' === typeof value) {
      let str = StringUtils.dealSingleQuotesSql(value); //处理单引号问题
      return `'${str}'`;
    }
    if ('number' === typeof value) return value;
    if ('boolean' === typeof value) return value ? value : 'null';
    if (value instanceof Date) return value ? `'${format(value, 'yyyy-MM-dd HH:mm:ss')}'` : 'null';
    if (Array.isArray(value)) {
      if (value.length === 0) return `''`;
      let items = [];
      value.forEach((item) => {
        if ('string' === typeof item) {
          items.push(StringUtils.dealSingleQuotesSql(item)); //处理单引号问题
        } else {
          items.push(item);
        }
      });
      return `'${items.join(',')}'`;
    }
    return value;
  },

  /**
   * 处理给定字符串值中的单引号问题，用于生成SQL字符串类型值的时候，避免单引号导致SQL语法错误
   * @param value
   * @return
   * @author: jiangbin
   * @date: 2021-04-27 09:58:04
   **/
  dealSingleQuotesSql: (value: string) => {
    return value.replace(/[\']/, "\\'");
  },

  /**
   * 移除日期时间列名
   * @param cols
   */
  delDateCols: (cols: string[]) => {
    return cols?.length > 0
      ? cols.filter((col) => {
          return col.search(/(CREATE_DATE)|(UPDATE_DATE)|(CREATE_TIME)|(UPDATE_TIME)/) == -1;
        })
      : [];
  },

  /**
   * <pre>将给定的列名或列属性名校正成数据库表的列名，用于查询语句构建，
   * 因为前端给定的列名可能是属性名，若给定列名不能被转换则会过滤掉,
   * 例如：
   *  前端限定只查询DIC_ID列数据时，可能会给定的是列属性名dicId，
   * 所以在此转换成DIC_ID列名才能正确的进行SQL拼装和查询</pre>
   * @param sources 给定的列名或列属性名
   * @param allCols 表格的列名列表
   * @return [string[]]
   * @author jiang
   * @date 2020-12-27 10:49:34
   **/
  toColName: (sources: string[], allCols = [], allProps = []): string[] => {
    if (!sources || sources.length == 0) return [];
    if (!allCols || allCols.length == 0) return [];

    //生成属性名:SAM_NAME==>samName
    if (!allProps || allProps.length == 0) {
      allProps = allCols.map((col) => {
        return StringUtils.camelCase(col);
      });
    }

    //避免某些时候前端只给定单列时被解析成字符串导致后续遍历报错
    if (!Array.isArray(sources)) {
      sources = [sources];
    }

    //将列名或列属性名转换成列名，避免查询报错问题
    return sources
      .map((source) => {
        //检测列名
        let index = allCols.indexOf(source.toUpperCase());
        if (index > -1) return allCols[index];

        //检测属性名
        index = allProps.indexOf(source);
        if (index > -1) return allCols[index];

        return null;
      })
      .filter((source) => source != null);
  },
};
