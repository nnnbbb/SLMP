import { format } from 'date-fns';

/**
 * 日期工具类
 * @author jiang
 * @date 2020-12-06 16:50:50
 **/
export const DateUtils = {
  /**
   * 采用date-fns库格式化日期对象成字符串
   * @param date 日期对象
   * @param pattern 参看：https://date-fns.org/
   * @return
   * @author jiang
   * @date 2020-12-06 16:50:03
   **/
  format: (date: Date, pattern: string) => {
    return format(date, pattern);
  },
};
