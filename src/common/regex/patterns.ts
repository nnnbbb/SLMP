import { WellUtils } from '../plate/well.utils';
import { StringUtils } from '../utils/string.utils';

/**
 * 正则式匹配模式接口定义
 * @author: jiangbin
 * @date: 2021-04-17 15:08:30
 **/
export interface Pattern {
  /**
   * @description:
   * @author: jiangbin
   * @date: 2021-04-17 15:08:43
   **/
  pattern: RegExp | RegExp[];
  /**
   * 解析字符串中匹配的内容
   * @author: jiangbin
   * @date: 2021-04-17 15:08:46
   **/
  match: (source: string) => any;
}

/**
 * 系统常用的一些正则式匹配模式
 * @author: jiangbin
 * @date: 2021-04-17 15:01:49
 **/
export const PATTERNS = {
  /**
   * 孔位号正则式
   * @author: jiangbin
   * @date: 2021-03-18 14:07:53
   **/
  well_detail: {
    /**
     * 匹配孔位号，用于解析出其中的行号和列号部分，例如:A01 => (A,01)
     * @author: jiangbin
     * @date: 2021-04-17 15:15:26
     **/
    pattern: /^([a-zA-Z])([0-9]{1,2})$/,
    /**
     * 解析孔位字符串的行列号信息
     * @param well 孔位号字符串，如:A1或A01
     * @return
     * @author: jiangbin
     * @date: 2021-04-17 15:11:03
     **/
    match(source: string) {
      if (StringUtils.isBlank(source)) {
        return null;
      }
      let match = source.match(this.pattern);
      if (match) {
        let rowName = match[1];
        let row = WellUtils.getRowNum(rowName);
        return { rowName, row, col: parseInt(match[2], 10), well: source };
      }
      return null;
    },
  } as Pattern,
  /**
   * 提取芯片指纹数据标题行中孔位号正则式
   * @author: jiangbin
   * @date: 2021-03-18 14:07:53
   **/
  well: {
    /**
     * 匹配孔位号，用于解析出其中的行号和列号部分，例如:
     * A01_WR21P00006P_M60K_032901_WL2101616.CEL_call_code==>A01
     * A01.WR21P00006P_M60K_032901_WL2101616.CEL_call_code==>A01
     * A01_WL2101616.CEL_call_code==>A01
     * A01.WL2101616.CEL_call_code==>A01
     * @author: jiangbin
     * @date: 2021-04-17 15:15:26
     **/
    pattern: [/^([a-zA-Z][0-9]{1,2})[_\\.].+$/, /^.+[_\\.]([a-zA-Z][0-9]{1,2})$/, /^.+[_\\.]([a-zA-Z][0-9]{1,2})[_\\.].+$/],
    /**
     * 解析出芯片指纹中标题行里的孔位号
     * @param source 标题列字符串,例如：A01_WR21P00006P_M60K_032901_WL2101616或A01_WL2101616
     * @return
     * @author: jiangbin
     * @date: 2021-04-17 15:11:03
     **/
    match(source: string) {
      if (StringUtils.isBlank(source)) {
        return null;
      }
      for (let index = 0; index < this.pattern.length; index++) {
        let item = this.pattern[index];
        let match = source.match(item);
        if (match) {
          return { well: match[1], pattern: item };
        }
      }
      return null;
    },
  } as Pattern,

  /**
   * 校正CSV的列数据正则式
   * @author: jiangbin
   * @date: 2021-04-21 15:59:31
   **/
  csv: {
    /**
     * CSV列数据提取正则式
     * @author: jiangbin
     * @date: 2021-04-21 15:59:53
     **/
    pattern: /^["']([^"']*)["']$/,
    /**
     * 解析出CSV列数据
     * @param source CSV列数据
     * @return
     * @author: jiangbin
     * @date: 2021-04-17 15:11:03
     **/
    match(source: string) {
      if (StringUtils.isBlank(source)) {
        return void 0;
      }
      let match = source.match(this.pattern);
      return match ? match[1] : source;
    },
  } as Pattern,
};
