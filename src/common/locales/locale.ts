import { UserContext } from '../context/user.context';

const en_mapper = require('./en-US');
const zh_mapper = require('./zh-CN');

/**
 * 支持国际化的泛型数据类型
 * @author: jiangbin
 * @date: 2021-03-25 16:53:13
 **/
export declare type LocaleData<S> = { cn: S; en: S };

/**
 * 支持国际化的泛型数据类型数组
 * @author: jiangbin
 * @date: 2021-03-26 09:31:01
 **/
export declare type LocaleDataArray<S> = LocaleData<S[]>;

/**
 * 语言类型定义
 * @author jiang
 * @date 2021-01-25 00:26:33
 **/
export const LOCALES = {
  zh_cn: 'zh-CN',
  en_us: 'en-US',
};

/**
 * moment日期对象语言定义
 * @author: jiangbin
 * @date: 2021-01-18 09:28:02
 **/
export const MLOCALES = {
  zh_cn: 'zh-cn',
  en: 'en',
  /**
   * 获取moment日期格式化pattern
   * @author: jiangbin
   * @date: 2021-01-18 09:42:07
   **/
  get pattern(): string {
    return LocaleContext.isZhCn ? 'YYYY年MM月DD日' : 'YYYY-MM-DD';
  },
};

/**
 * 语言上下文对象，可以获取系统参数，提供判定语言类型的方法
 * @author: jiangbin
 * @date: 2021-01-20 14:02:42
 **/
export const LocaleContext = {
  /**
   * 用户当前是否为英文语言
   * @author: jiangbin
   * @date: 2021-01-17 20:27:09
   **/
  get isEnUs() {
    return UserContext.language === LOCALES.en_us;
  },

  /**
   * 用户当前是否为中文语言
   * @author: jiangbin
   * @date: 2021-01-17 20:27:09
   **/
  get isZhCn() {
    return UserContext.language === LOCALES.zh_cn;
  },

  /**
   * 根据ID号获取国际化消息
   * @param params.id
   * @param params.defaultMessage
   * @return
   * @author: jiangbin
   * @date: 2021-02-03 13:07:36
   **/
  locale: (params: { id: string; defaultMessage: string }) => {
    if (LocaleContext.isZhCn) {
      return zh_mapper[params.id] || params.defaultMessage;
    } else {
      return en_mapper[params.id] || params.defaultMessage;
    }
  },

  /**
   * 获取国际化的消息内容
   * @param params.cn
   * @param params.en
   * @return
   * @author: jiangbin
   * @date: 2021-02-03 13:06:11
   **/
  message: (params: LocaleData<string>) => {
    return LocaleContext.isZhCn ? params.cn : params.en || params.cn;
  },

  /**
   * 根据当前语言上下文环境获取对应的数据
   * @param params.cn
   * @param params.en
   * @return
   * @author: jiangbin
   * @date: 2021-03-25 09:21:28
   **/
  data: <S>(params: LocaleData<S>) => {
    return LocaleContext.isZhCn ? params.cn : params.en;
  },
};
