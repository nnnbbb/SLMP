import { createParamDecorator, ExecutionContext } from '@nestjs/common';

/**
 * 自定义分页查询装饰器，会自处理分页参数offset和pageSize
 * @author: jiangbin
 * @date: 2021-05-12 09:41:12
 **/
export const Pager = createParamDecorator((data: string, ctx: ExecutionContext) => {
  const req = ctx.switchToHttp().getRequest();
  let page = {};
  if (req.path && req.path.endsWith('find/page')) {
    Object.keys(req.query).map((prop) => {
      if (prop === 'offset') {
        page['offset'] = +req.query.offset || 0;
      } else if (prop === 'pageSize') {
        page['pageSize'] = +req.query.pageSize || 20;
      }
    });
  }
  return data ? req.query[data] : { ...req.query, ...page };
});
