import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import * as cookieParser from 'cookie-parser';
import { SysModule } from './sys/sys.module';
import { AuthModule } from './common/auth/auth.module';
import { FileModule } from './common/file/file.module';
import { ApiConfig } from './common/config/api.config';
import { ConfigMapper } from './sys/config/config.mapper';
import { SysServices } from './sys/sys.services';
import { LogUtils } from './common/log4js/log4js.utils';
import { DictMapper } from './common/dictionary/dict.mapper';
import { Log4jsService } from './common/log4js/log4js.service';
import { UserMapper } from './sys/user/user.mapper';
import { PmsMapper } from './common/pms/pms.mapper';
import { MailModule } from './common/mail/mail.module';
import { MailSender } from './common/mail/sender/mail.sender';

declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  //初始化服务或其它参数
  init(app);

  //系统参数
  const apiConfig = app.get(ApiConfig);

  //nestjs使用log4js日志
  const logger = app.get(Log4jsService);
  app.useLogger(logger);

  //启用cookie功能
  app.use(cookieParser());

  //启用安全保护功能
  app.use(helmet());

  //设置请求实体限制值
  const bodyParser = require('body-parser');
  app.use(bodyParser.json({ limit: apiConfig.reqJsonLimit }));
  app.use(bodyParser.urlencoded({ limit: apiConfig.reqUrlencodedLimit, extended: true }));

  //限速
  app.use(
    rateLimit({
      windowMs: apiConfig.limitRateWindowMs, // 15 minutes
      max: apiConfig.limitRateMax, // limit each IP to 100 requests per windowMs
    }),
  );

  //启用cors，允许跨域访问
  app.enableCors();

  //配置虚拟目录，示例:http://localhost:3000/static/a.png
  app.useStaticAssets('public', {
    prefix: '/static/',
  });

  //集成swagger插件
  swagger(app);

  let port = apiConfig.serverPort;
  await app.listen(port);

  let host = apiConfig.serverHost;
  logger.log(`Application is running on=>http://${host}:${port}/`);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}

//启动服务
bootstrap();

/**
 * 初始化静态服务引用
 * @param app
 * @author: jiangbin
 * @date: 2021-01-07 21:05:06
 **/
const init = (app: NestExpressApplication) => {
  //设置服务的静态引用
  SysServices.init(app);
  LogUtils.init(app);
  MailSender.init(app);

  //加载数据库参数表
  ConfigMapper.reload();
  DictMapper.reload();
  UserMapper.reload();
  PmsMapper.reload();
};

/**
 * 集成swagger插件
 * @param app
 */
const swagger = (app) => {
  //APP主程序模板API
  const options = new DocumentBuilder().setTitle('BCMP').setDescription('SNP芯片指纹数据库管理系统后端接口描述').setVersion('1.0').addTag('bcmp').build();
  const document = SwaggerModule.createDocument(app, options, {
    include: [AppModule],
  });
  SwaggerModule.setup('api', app, document);

  //Auth模块API
  const authOptions = new DocumentBuilder().setTitle('Auth模块信息文档').setDescription('用于用户身份授权和认证').setVersion('1.0').addTag('auth').build();

  const authDocument = SwaggerModule.createDocument(app, authOptions, {
    include: [AuthModule],
  });
  SwaggerModule.setup('api/auth', app, authDocument);

  //File模块API
  const fileOptions = new DocumentBuilder().setTitle('File模块信息文档').setDescription('用于文件上传和下载').setVersion('1.0').addTag('file').build();

  const fileDocument = SwaggerModule.createDocument(app, fileOptions, {
    include: [FileModule],
  });
  SwaggerModule.setup('api/file', app, fileDocument);

  //Mail模块API
  const mailOptions = new DocumentBuilder().setTitle('Mail模块信息文档').setDescription('用于邮件发送').setVersion('1.0').addTag('mail').build();

  const mailDocument = SwaggerModule.createDocument(app, mailOptions, {
    include: [MailModule],
  });
  SwaggerModule.setup('api/mail', app, mailDocument);

  //系统模块API
  const sysOptions = new DocumentBuilder().setTitle('系统模块信息文档').setDescription('用于系统基础信息CRUD').setVersion('1.0').addTag('system').build();

  const sysDocument = SwaggerModule.createDocument(app, sysOptions, {
    include: [SysModule],
  });
  SwaggerModule.setup('api/sys', app, sysDocument);
};
