import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysDictionary } from '../entity/sys.dictionary.entity';
import { SysDictionaryUDto } from '../dto/update/sys.dictionary.udto';
import { SysDictionaryCDto } from '../dto/create/sys.dictionary.cdto';
import { SysDictionaryQDto } from '../dto/query/sys.dictionary.qdto';
import { SysDictionarySQL } from '../entity/sql/sys.dictionary.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import { SysDictMerger } from '../../common/dictionary/sys.dict.merger';
import { SysDictParser } from '../../common/dictionary/sys.dict.parser';
import { SpliterUtils } from '../../common/utils/spliter.utils';

/**
 * SYS_DICTIONARY表对应服务层类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysDictionaryService
 */
@Injectable()
export class SysDictionaryService {
  constructor(@InjectRepository(SysDictionary) private readonly entityRepo: Repository<SysDictionary>) {}

  /**
   * 获取实体类(SysDictionary)的Repository对象
   */
  get repository(): Repository<SysDictionary> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysDictionary[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): Promise<SysDictionary[]> {
    let querySql = SysDictionarySQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysDictionary> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysDictionary[]> {
    let querySql = SysDictionarySQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysDictionaryQDto | Partial<SysDictionaryQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysDictionarySQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysDictionaryQDto | Partial<SysDictionaryQDto>): Promise<SysDictionary> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysDictionarySQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysDictionaryQDto | Partial<SysDictionaryQDto>): Promise<number> {
    let countSql = SysDictionarySQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ dicId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysDictionaryQDto | Partial<SysDictionaryQDto>): Promise<any> {
    let deleteSql = SysDictionarySQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 删除所有记录
   */
  async deleteAll(): Promise<any> {
    const deleteSql = SysDictionarySQL.DELETE_ALL_SQL();
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysDictionaryUDto | Partial<SysDictionaryUDto>): Promise<any> {
    if (!dto.dicId || dto.dicId.length == 0) return {};
    let sql = SysDictionarySQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysDictionaryUDto[] | Partial<SysDictionaryUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        if (!dto.dicId || dto.dicId.length == 0) continue;
        let sql = SysDictionarySQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
        let result = JsonUtils.first(await manager.query(sql));
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysDictionaryUDto | Partial<SysDictionaryUDto>): Promise<any> {
    if (!dto.dicId || dto.dicId.length == 0) return {};
    let sql = SysDictionarySQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysDictionaryUDto | Partial<SysDictionaryUDto>): Promise<any> {
    if (!dto.dicIdList || dto.dicIdList.length == 0) return {};
    let sql = SysDictionarySQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysDictionaryUDto[] | Partial<SysDictionaryUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        if (!dto.dicId || dto.dicId.length == 0) continue;
        let sql = SysDictionarySQL.UPDATE_BY_ID_SQL(dto);
        let result = JsonUtils.first(await manager.query(sql));
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysDictionaryCDto | Partial<SysDictionaryCDto>): Promise<SysDictionaryCDto> {
    if (!dto.dicId) {
      dto.dicId = uuid();
    }
    let sql = SysDictionarySQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysDictionaryCDto[] | Partial<SysDictionaryCDto>[]): Promise<SysDictionaryCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.dicId) dto.dicId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let groups = SpliterUtils.group(dtos, 3000);
      for (let index = 0; index < groups.length; index++) {
        let sql = SysDictionarySQL.BATCH_INSERT_SQL(groups[index]);
        let result = JsonUtils.first(await manager.query(sql));
        result?.length > 0 && (results = results.concat(result));
      }
    });
    return results;
  }

  /***************新增的方法***************/

  /**
   * 分页查询记录
   * @param query
   */
  async findPageDetail(query: SysDictionaryQDto | Partial<SysDictionaryQDto>): Promise<any> {
    let page = await this.findPage(query);
    let data = page.data;
    if (!data || data.length == 0) {
      return page;
    }

    //查询字典的字典项列表
    let idList = data.map((row) => row.dicId);
    let subRows = await this.findList({ dicParentIdList: idList, order: 'DIC_ORDER ASC' });
    subRows?.length > 0 &&
      (data = data.map((row) => {
        row.children = subRows.filter((sub) => sub.dicParentId === row.dicId);
        //若字典记录未设置分组，则用子节点的分组进行设置
        !row.dicGroup && row.children.length > 0 && (row.dicGroup = row.children[0].dicGroup);
        return row;
      }));

    page.data = data;
    return page;
  }

  /**
   * 初始化字典
   * @param news 记录列表
   * @return true/false--初始化成功/失败
   * @author: jiangbin
   * @date: 2020-12-28 20:38:30
   **/
  async init(): Promise<boolean> {
    //解析字典定义
    const news: SysDictionary[] = SysDictParser.parserAll();
    if (!news || news.length == 0) return false;

    //查询所有结点
    let olds = await this.findAll();

    //合并结点
    let records = SysDictMerger.merger(news, olds);
    if (!records || records.length == 0) return false;

    await this.entityRepo.manager.transaction(async (manager) => {
      //删除模块关联的字典
      const deleteSql = SysDictionarySQL.DELETE_ALL_SQL();
      await manager.query(deleteSql);

      //批量插入模块关联的字典
      let sql = SysDictionarySQL.BATCH_INSERT_SQL(records);
      await manager.query(sql);
    });
    return true;
  }
}
