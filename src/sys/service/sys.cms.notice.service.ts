import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysCmsNotice } from '../entity/sys.cms.notice.entity';
import { SysCmsNoticeUDto } from '../dto/update/sys.cms.notice.udto';
import { SysCmsNoticeCDto } from '../dto/create/sys.cms.notice.cdto';
import { SysCmsNoticeQDto } from '../dto/query/sys.cms.notice.qdto';
import { SysCmsNoticeSQL } from '../entity/sql/sys.cms.notice.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_CMS_NOTICE表对应服务层类
 * @date 12/30/2020, 3:45:15 PM
 * @author jiangbin
 * @export
 * @class SysCmsNoticeService
 */
@Injectable()
export class SysCmsNoticeService {
  constructor(@InjectRepository(SysCmsNotice) private readonly entityRepo: Repository<SysCmsNotice>) {}

  /**
   * 获取实体类(SysCmsNotice)的Repository对象
   */
  get repository(): Repository<SysCmsNotice> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysCmsNotice[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): Promise<SysCmsNotice[]> {
    let querySql = SysCmsNoticeSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysCmsNotice> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysCmsNotice[]> {
    let querySql = SysCmsNoticeSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysCmsNoticeSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): Promise<SysCmsNotice> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysCmsNoticeSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): Promise<number> {
    let countSql = SysCmsNoticeSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ scNoticeId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysCmsNoticeQDto | Partial<SysCmsNoticeQDto>): Promise<any> {
    let deleteSql = SysCmsNoticeSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysCmsNoticeUDto | Partial<SysCmsNoticeUDto>): Promise<any> {
    if (!dto.scNoticeId || dto.scNoticeId.length == 0) return {};
    let sql = SysCmsNoticeSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysCmsNoticeUDto[] | Partial<SysCmsNoticeUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.scNoticeId) dto.scNoticeId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysCmsNoticeUDto | Partial<SysCmsNoticeUDto>): Promise<any> {
    if (!dto.scNoticeId || dto.scNoticeId.length == 0) return {};
    let sql = SysCmsNoticeSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysCmsNoticeUDto | Partial<SysCmsNoticeUDto>): Promise<any> {
    if (!dto.scNoticeIdList || dto.scNoticeIdList.length == 0) return {};
    let sql = SysCmsNoticeSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysCmsNoticeUDto[] | Partial<SysCmsNoticeUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.scNoticeId) dto.scNoticeId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysCmsNoticeCDto | Partial<SysCmsNoticeCDto>): Promise<SysCmsNoticeCDto> {
    if (!dto.scNoticeId) {
      dto.scNoticeId = uuid();
    }
    let sql = SysCmsNoticeSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysCmsNoticeCDto[] | Partial<SysCmsNoticeCDto>[]): Promise<SysCmsNoticeCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.scNoticeId) dto.scNoticeId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysCmsNoticeSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
