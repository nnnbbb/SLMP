import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysUserParams } from '../entity/sys.user.params.entity';
import { SysUserParamsUDto } from '../dto/update/sys.user.params.udto';
import { SysUserParamsCDto } from '../dto/create/sys.user.params.cdto';
import { SysUserParamsQDto } from '../dto/query/sys.user.params.qdto';
import { SysUserParamsSQL } from '../entity/sql/sys.user.params.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import { SpliterUtils } from '../../common/utils/spliter.utils';
/**
 * SYS_USER_PARAMS表对应服务层类
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export
 * @class SysUserParamsService
 */
@Injectable()
export class SysUserParamsService {
  constructor(@InjectRepository(SysUserParams) private readonly entityRepo: Repository<SysUserParams>) {}
  /**
   * 获取实体类(SysUserParams)的Repository对象
   */
  get repository(): Repository<SysUserParams> {
    return this.entityRepo;
  }
  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysUserParams[]> {
    return await this.entityRepo.find();
  }
  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): Promise<SysUserParams[]> {
    let querySql = SysUserParamsSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }
  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysUserParams> {
    return await this.entityRepo.findOne(id);
  }
  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysUserParams[]> {
    let querySql = SysUserParamsSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }
  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysUserParamsQDto | Partial<SysUserParamsQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysUserParamsSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }
  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): Promise<SysUserParams> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysUserParamsSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysUserParamsQDto | Partial<SysUserParamsQDto>): Promise<number> {
    let countSql = SysUserParamsSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }
  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ upId: id });
  }
  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }
  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysUserParamsQDto | Partial<SysUserParamsQDto>): Promise<any> {
    let deleteSql = SysUserParamsSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }
  /**
   * 删除所有记录
   */
  async deleteAll(): Promise<any> {
    let deleteSql = SysUserParamsSQL.DELETE_ALL_SQL();
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }
  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysUserParamsUDto | Partial<SysUserParamsUDto>): Promise<any> {
    if (!dto.upId || dto.upId.length == 0) return {};
    let sql = SysUserParamsSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysUserParamsUDto[] | Partial<SysUserParamsUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        if (!dto.upId || dto.upId.length == 0) return {};
        let sql = SysUserParamsSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
        let result = JsonUtils.first(await manager.query(sql));
        results.push(result);
      }
    });
    return results;
  }
  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysUserParamsUDto | Partial<SysUserParamsUDto>): Promise<any> {
    if (!dto.upIdList || dto.upIdList.length == 0) return {};
    let sql = SysUserParamsSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysUserParamsUDto | Partial<SysUserParamsUDto>): Promise<any> {
    if (!dto.upId || dto.upId.length == 0) return {};
    let sql = SysUserParamsSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysUserParamsUDto[] | Partial<SysUserParamsUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        if (!dto.upId || dto.upId.length == 0) return {};
        let sql = SysUserParamsSQL.UPDATE_BY_ID_SQL(dto);
        let result = JsonUtils.first(await manager.query(sql));
        results.push(result);
      }
    });
    return results;
  }
  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysUserParamsCDto | Partial<SysUserParamsCDto>): Promise<SysUserParamsCDto> {
    if (!dto.upId) {
      dto.upId = uuid();
    }
    let sql = SysUserParamsSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }
  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysUserParamsCDto[] | Partial<SysUserParamsCDto>[]): Promise<SysUserParamsCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.upId) dto.upId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let groups = SpliterUtils.group(dtos, 3000);
      for (let index = 0; index < groups.length; index++) {
        let sql = SysUserParamsSQL.BATCH_INSERT_SQL(groups[index]);
        let result = JsonUtils.first(await manager.query(sql));
        result?.length > 0 && (results = results.concat(result));
      }
    });
    return results;
  }
}
