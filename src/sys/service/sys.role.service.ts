import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysRole } from '../entity/sys.role.entity';
import { SysRoleUDto } from '../dto/update/sys.role.udto';
import { SysRoleCDto } from '../dto/create/sys.role.cdto';
import { SysRoleQDto } from '../dto/query/sys.role.qdto';
import { SysRoleSQL } from '../entity/sql/sys.role.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import { SysRoleMenuService } from './sys.role.menu.service';
import { SysRoleDictionaryService } from './sys.role.dictionary.service';
import { SysRolePermissionService } from './sys.role.permission.service';

/**
 * SYS_ROLE表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysRoleService
 */
@Injectable()
export class SysRoleService {
  constructor(
    @InjectRepository(SysRole) private readonly entityRepo: Repository<SysRole>,
    @Inject(SysRoleMenuService) private readonly roleMenuService: SysRoleMenuService,
    @Inject(SysRoleDictionaryService) private readonly roleDictionaryService: SysRoleDictionaryService,
    @Inject(SysRolePermissionService) private readonly rolePermissionService: SysRolePermissionService,
  ) {}

  /**
   * 获取实体类(SysRole)的Repository对象
   */
  get repository(): Repository<SysRole> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysRole[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysRoleQDto | Partial<SysRoleQDto>): Promise<SysRole[]> {
    let querySql = SysRoleSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysRole> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysRole[]> {
    let querySql = SysRoleSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysRoleQDto | Partial<SysRoleQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysRoleSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysRoleQDto | Partial<SysRoleQDto>): Promise<SysRole> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysRoleSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysRoleQDto | Partial<SysRoleQDto>): Promise<number> {
    let countSql = SysRoleSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    let result = {};
    await this.entityRepo.manager.transaction(async (manager) => {
      await this.roleDictionaryService.delete({ rdRoleId: id });
      await this.roleMenuService.delete({ rmRoleId: id });
      await this.rolePermissionService.delete({ rpRoleId: id });
      result = await this.entityRepo.delete({ roleId: id });
    });
    return result;
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    let result = {};
    await this.entityRepo.manager.transaction(async (manager) => {
      await this.roleDictionaryService.delete({ rdRoleIdList: ids });
      await this.roleMenuService.delete({ rmRoleIdList: ids });
      await this.rolePermissionService.delete({ rpRoleIdList: ids });
      result = await this.entityRepo.delete(ids);
    });
    return result;
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysRoleQDto | Partial<SysRoleQDto>): Promise<any> {
    let sql = SysRoleSQL.SELECT_ID_LIST_SQL(query);
    let idList = JsonUtils.format(await this.entityRepo.query(sql));
    return await this.deleteByIdList(idList);
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysRoleUDto | Partial<SysRoleUDto>): Promise<any> {
    if (!dto.roleId || dto.roleId.length == 0) return {};
    let sql = SysRoleSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysRoleUDto[] | Partial<SysRoleUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.roleId) dto.roleId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysRoleUDto | Partial<SysRoleUDto>): Promise<any> {
    if (!dto.roleId || dto.roleId.length == 0) return {};
    let sql = SysRoleSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysRoleUDto | Partial<SysRoleUDto>): Promise<any> {
    if (!dto.roleIdList || dto.roleIdList.length == 0) return {};
    let sql = SysRoleSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysRoleUDto[] | Partial<SysRoleUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.roleId) dto.roleId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysRoleCDto | Partial<SysRoleCDto>): Promise<SysRoleCDto> {
    if (!dto.roleId) {
      dto.roleId = uuid();
    }
    let sql = SysRoleSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysRoleCDto[] | Partial<SysRoleCDto>[]): Promise<SysRoleCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.roleId) dto.roleId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysRoleSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
