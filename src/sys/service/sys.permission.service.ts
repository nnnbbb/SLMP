import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysPermission } from '../entity/sys.permission.entity';
import { SysPermissionUDto } from '../dto/update/sys.permission.udto';
import { SysPermissionCDto } from '../dto/create/sys.permission.cdto';
import { SysPermissionQDto } from '../dto/query/sys.permission.qdto';
import { SysPermissionSQL } from '../entity/sql/sys.permission.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import { SysPmsMerger } from '../../common/pms/pms.merger';
import { DIR_NODE, ROOT_NODE_ID } from '../../common/tree/tree.node.type';

/**
 * SYS_PERMISSION表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysPermissionService
 */
@Injectable()
export class SysPermissionService {
  constructor(@InjectRepository(SysPermission) private readonly entityRepo: Repository<SysPermission>) {}

  /**
   * 获取实体类(SysPermission)的Repository对象
   */
  get repository(): Repository<SysPermission> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysPermission[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysPermissionQDto | Partial<SysPermissionQDto>): Promise<SysPermission[]> {
    let querySql = SysPermissionSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysPermission> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysPermission[]> {
    let querySql = SysPermissionSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysPermissionQDto | Partial<SysPermissionQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysPermissionSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysPermissionQDto | Partial<SysPermissionQDto>): Promise<SysPermission> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysPermissionSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysPermissionQDto | Partial<SysPermissionQDto>): Promise<number> {
    let countSql = SysPermissionSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ perId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysPermissionQDto | Partial<SysPermissionQDto>): Promise<any> {
    let deleteSql = SysPermissionSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysPermissionUDto | Partial<SysPermissionUDto>): Promise<any> {
    if (!dto.perId || dto.perId.length == 0) return {};
    let sql = SysPermissionSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysPermissionUDto[] | Partial<SysPermissionUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.perId) dto.perId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysPermissionUDto | Partial<SysPermissionUDto>): Promise<any> {
    if (!dto.perId || dto.perId.length == 0) return {};
    let sql = SysPermissionSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysPermissionUDto | Partial<SysPermissionUDto>): Promise<any> {
    if (!dto.perIdList || dto.perIdList.length == 0) return {};
    let sql = SysPermissionSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysPermissionUDto[] | Partial<SysPermissionUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.perId) dto.perId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysPermissionCDto | Partial<SysPermissionCDto>): Promise<SysPermissionCDto> {
    if (!dto.perId) {
      dto.perId = uuid();
    }
    let sql = SysPermissionSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysPermissionCDto[] | Partial<SysPermissionCDto>[]): Promise<SysPermissionCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.perId) dto.perId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysPermissionSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }

  /***************新增的方法***************/

  /**
   * 分页查询记录
   * @param query
   */
  async findPageDetail(query: SysPermissionCDto | Partial<SysPermissionCDto>): Promise<any> {
    let page = await this.findPage(query);
    let data = page.data;
    if (!data || data.length == 0) {
      return page;
    }

    //查询字典的字典项列表
    let idList = data.map((row) => row.perId);
    let subRows = await this.findList({ perParentIdList: idList, order: 'PER_ORDER ASC' });
    subRows?.length > 0 &&
      (data = data.map((row) => {
        row.children = subRows.filter((sub) => sub.perParentId === row.perId);
        return row;
      }));

    page.data = data;
    return page;
  }

  /**
   * 初始化权限列表
   * @param subSystem
   * @param
   * @author: jiangbin
   * @date: 2021-01-02 14:04:19
   **/
  async init(subSystem: string, news: SysPermissionCDto[] | Partial<SysPermissionCDto>[]): Promise<boolean> {
    if (!news || news.length == 0 || !subSystem) return false;

    //查询指定子系统的权限列表
    let olds = await this.findList({ perSystem: subSystem });

    //增量合并节点列表
    let records = SysPmsMerger.merger(news, olds);
    if (!records || records.length == 0) return false;

    //创建根结点
    await this.createRoot();

    await this.entityRepo.manager.transaction(async (manager) => {
      //删除子系统关联的权限
      await this.delete({ perSystem: subSystem });

      //批量插入模块关联的权限
      let sql = SysPermissionSQL.BATCH_INSERT_SQL(records);
      await this.entityRepo.query(sql);
    });
    return true;
  }

  /**
   * 创建根节点
   * @author: jiangbin
   * @date: 2020-12-28 20:02:28
   **/
  public async createRoot(): Promise<any> {
    let root = await this.entityRepo.findOne({ perId: ROOT_NODE_ID });
    if (!root) {
      let record = {
        perId: ROOT_NODE_ID,
        perName: '根结点',
        perNameEn: 'ROOT',
        perModule: 'ROOT',
        perType: DIR_NODE,
        perOrder: 1,
        perParentId: null,
        perState: 'ON',
        perSystem: 'ROOT',
        perUrl: null,
        perMethod: null,
        perRemark: null,
      };
      return await this.create(record);
    }
    return root;
  }
}
