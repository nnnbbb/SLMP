import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysNotice } from '../entity/sys.notice.entity';
import { SysNoticeUDto } from '../dto/update/sys.notice.udto';
import { SysNoticeCDto } from '../dto/create/sys.notice.cdto';
import { SysNoticeQDto } from '../dto/query/sys.notice.qdto';
import { SysNoticeSQL } from '../entity/sql/sys.notice.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';

/**
 * SYS_NOTICE表对应服务层类
 * @date 12/30/2020, 3:45:16 PM
 * @author jiangbin
 * @export
 * @class SysNoticeService
 */
@Injectable()
export class SysNoticeService {
  constructor(@InjectRepository(SysNotice) private readonly entityRepo: Repository<SysNotice>) {}

  /**
   * 获取实体类(SysNotice)的Repository对象
   */
  get repository(): Repository<SysNotice> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysNotice[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysNoticeQDto | Partial<SysNoticeQDto>): Promise<SysNotice[]> {
    let querySql = SysNoticeSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysNotice> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysNotice[]> {
    let querySql = SysNoticeSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysNoticeQDto | Partial<SysNoticeQDto>): Promise<any> {
    let count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    let querySql = SysNoticeSQL.SELECT_SQL(query);
    let data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysNoticeQDto | Partial<SysNoticeQDto>): Promise<SysNotice> {
    dto.offset = 0;
    dto.pageSize = 1;
    let sql = SysNoticeSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysNoticeQDto | Partial<SysNoticeQDto>): Promise<number> {
    let countSql = SysNoticeSQL.SELECT_COUNT_SQL(query);
    let result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ noticeId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysNoticeQDto | Partial<SysNoticeQDto>): Promise<any> {
    let deleteSql = SysNoticeSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysNoticeUDto | Partial<SysNoticeUDto>): Promise<any> {
    if (!dto.noticeId || dto.noticeId.length == 0) return {};
    let sql = SysNoticeSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysNoticeUDto[] | Partial<SysNoticeUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.noticeId) dto.noticeId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.updateSelective(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysNoticeUDto | Partial<SysNoticeUDto>): Promise<any> {
    if (!dto.noticeId || dto.noticeId.length == 0) return {};
    let sql = SysNoticeSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysNoticeUDto | Partial<SysNoticeUDto>): Promise<any> {
    if (!dto.noticeIdList || dto.noticeIdList.length == 0) return {};
    let sql = SysNoticeSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysNoticeUDto[] | Partial<SysNoticeUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.noticeId) dto.noticeId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        let result = await this.update(dto);
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysNoticeCDto | Partial<SysNoticeCDto>): Promise<SysNoticeCDto> {
    if (!dto.noticeId) {
      dto.noticeId = uuid();
    }
    let sql = SysNoticeSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysNoticeCDto[] | Partial<SysNoticeCDto>[]): Promise<SysNoticeCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.noticeId) dto.noticeId = uuid();
    });
    let result = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let sql = SysNoticeSQL.BATCH_INSERT_SQL(dtos);
      result = JsonUtils.first(await this.entityRepo.query(sql));
    });
    return result;
  }
}
