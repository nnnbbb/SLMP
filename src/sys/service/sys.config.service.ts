import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SysConfig } from '../entity/sys.config.entity';
import { SysConfigUDto } from '../dto/update/sys.config.udto';
import { SysConfigCDto } from '../dto/create/sys.config.cdto';
import { SysConfigQDto } from '../dto/query/sys.config.qdto';
import { SysConfigSQL } from '../entity/sql/sys.config.sql';
import { uuid } from '../../common/utils/uuid.utils';
import { JsonUtils } from '../../common/typeorm/json.utils';
import { SysConfigParser } from '../config/sys.config.parser';
import { ConfigMapper } from '../config/config.mapper';
import { SYS_USER } from '../config/sys.config.defined';
import { SysConfigMerger } from '../config/sys.config.merger';
import { SpliterUtils } from '../../common/utils/spliter.utils';

/**
 * SYS_CONFIG表对应服务层类
 * @date 12/30/2020, 3:45:15 PM
 * @author jiangbin
 * @export
 * @class SysConfigService
 */
@Injectable()
export class SysConfigService {
  constructor(@InjectRepository(SysConfig) private readonly entityRepo: Repository<SysConfig>) {}

  /**
   * 获取实体类(SysConfig)的Repository对象
   */
  get repository(): Repository<SysConfig> {
    return this.entityRepo;
  }

  /**
   * 查询所有记录
   */
  async findAll(): Promise<SysConfig[]> {
    return await this.entityRepo.find();
  }

  /**
   * 条件查询记录
   * @param query
   */
  async findList(dto: SysConfigQDto | Partial<SysConfigQDto>): Promise<SysConfig[]> {
    const querySql = SysConfigSQL.SELECT_SQL(dto);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 查询ID对应记录
   * @param id
   */
  async findById(id: string): Promise<SysConfig> {
    return await this.entityRepo.findOne(id);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  async findByIdList(ids: string[], cols = []): Promise<SysConfig[]> {
    const querySql = SysConfigSQL.SELECT_BY_ID_LIST_SQL(ids, cols);
    return JsonUtils.format(await this.entityRepo.query(querySql));
  }

  /**
   * 分页查询记录
   * @param query
   */
  async findPage(query: SysConfigQDto | Partial<SysConfigQDto>): Promise<any> {
    const count: number = await this.count(query);
    query.offset = +query.offset || 0;
    query.pageSize = +query.pageSize || 20;
    query.offset = count < query.offset ? 0 : query.offset;
    if (count == 0) return { data: [], page: { count: 0, offset: query.offset, pageSize: query.pageSize } };
    const querySql = SysConfigSQL.SELECT_SQL(query);
    const data = JsonUtils.format(await this.entityRepo.query(querySql));
    return { data, page: { count: +count, offset: query.offset, pageSize: query.pageSize } };
  }

  /**
   * 查询单条记录
   * @param dto
   */
  async findOne(dto: SysConfigQDto | Partial<SysConfigQDto>): Promise<SysConfig> {
    dto.offset = 0;
    dto.pageSize = 1;
    const sql = SysConfigSQL.SELECT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 查询记录数
   * @param query
   */
  async count(query: SysConfigQDto | Partial<SysConfigQDto>): Promise<number> {
    const countSql = SysConfigSQL.SELECT_COUNT_SQL(query);
    const result = JsonUtils.first(await this.entityRepo.query(countSql));
    return +result.count || 0;
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  async deleteById(id: string): Promise<any> {
    return await this.entityRepo.delete({ conId: id });
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  async deleteByIdList(ids: string[]): Promise<any> {
    return await this.entityRepo.delete(ids);
  }

  /**
   * 条件查询记录
   * @param query
   */
  async delete(query: SysConfigQDto | Partial<SysConfigQDto>): Promise<any> {
    const deleteSql = SysConfigSQL.DELETE_SELECTIVE_SQL(query);
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 删除所有记录
   */
  async deleteAll(): Promise<any> {
    const deleteSql = SysConfigSQL.DELETE_ALL_SQL();
    return JsonUtils.first(await this.entityRepo.query(deleteSql));
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async updateSelective(dto: SysConfigUDto | Partial<SysConfigUDto>): Promise<any> {
    if (!dto.conId || dto.conId.length == 0) return {};
    const sql = SysConfigSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，只更新给定对象中有值的属性
   * @param dtos
   */
  async updateListSelective(dtos: SysConfigUDto[] | Partial<SysConfigUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    const results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (let dto of dtos) {
        if (!dto.conId || dto.conId.length == 0) return {};
        let sql = SysConfigSQL.UPDATE_BY_ID_SELECTIVE_SQL(dto);
        let result = JsonUtils.first(await manager.query(sql));
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 更新指定ID记录
   * @param dto
   */
  async update(dto: SysConfigUDto | Partial<SysConfigUDto>): Promise<any> {
    if (!dto.conId || dto.conId.length == 0) return {};
    const sql = SysConfigSQL.UPDATE_BY_ID_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 更新指定ID列表记录
   * @param dto
   */
  async updateByIds(dto: SysConfigUDto | Partial<SysConfigUDto>): Promise<any> {
    if (!dto.conIdList || dto.conIdList.length == 0) return {};
    const sql = SysConfigSQL.UPDATE_BY_IDS_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量更新指定ID记录，更新全部字段的值，若参数中未给值将会被清空
   * @param dtos
   */
  async updateList(dtos: SysConfigUDto[] | Partial<SysConfigUDto>[]): Promise<any> {
    if (!dtos || dtos.length == 0) return [];
    const results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      for (const dto of dtos) {
        if (!dto.conId || dto.conId.length == 0) return {};
        let sql = SysConfigSQL.UPDATE_BY_ID_SQL(dto);
        let result = JsonUtils.first(await manager.query(sql));
        results.push(result);
      }
    });
    return results;
  }

  /**
   * 创建新记录
   * @param dto
   */
  async create(dto: SysConfigCDto | Partial<SysConfigCDto>): Promise<SysConfigCDto> {
    if (!dto.conId) {
      dto.conId = uuid();
    }
    const sql = SysConfigSQL.INSERT_SQL(dto);
    return JsonUtils.first(await this.entityRepo.query(sql));
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  async createList(dtos: SysConfigCDto[] | Partial<SysConfigCDto>[]): Promise<SysConfigCDto[]> {
    if (!dtos || dtos.length == 0) return [];
    dtos.forEach((dto) => {
      if (!dto.conId) dto.conId = uuid();
    });
    let results = [];
    await this.entityRepo.manager.transaction(async (manager) => {
      let groups = SpliterUtils.group(dtos, 3000);
      for (let index = 0; index < groups.length; index++) {
        let sql = SysConfigSQL.BATCH_INSERT_SQL(groups[index]);
        let result = JsonUtils.first(await manager.query(sql));
        result?.length > 0 && (results = results.concat(result));
      }
    });
    return results;
  }

  /***************新增的方法***************/

  /**
   * 分页查询记录
   * @param query
   */
  async findPageDetail(query: SysConfigCDto | Partial<SysConfigCDto>): Promise<any> {
    const page = await this.findPage(query);
    let data = page.data;
    if (!data || data.length == 0) {
      return page;
    }

    //查询字典的字典项列表
    const idList = data.map((row) => row.conId);
    const subRows = await this.findList({ conParentIdList: idList, order: 'CON_ORDER ASC' });
    subRows?.length > 0 &&
      (data = data.map((row) => {
        row.children = subRows.filter((sub) => sub.conParentId === row.conId);
        return row;
      }));

    page.data = data;
    return page;
  }

  /**
   * 初始化参数
   * @author: jiangbin
   * @date: 2021-01-07 18:57:20
   **/
  async init(): Promise<boolean> {
    //查询所有结点
    let olds = await this.findAll();

    //解析出参数
    let news = SysConfigParser.parser();
    if (!news || news.length == 0) return false;

    //合并结点
    let records = SysConfigMerger.merger(news, olds);
    if (!records || records.length == 0) return false;

    await this.entityRepo.manager.transaction(async (manager) => {
      //删除模块关联的字典
      const deleteSql = SysConfigSQL.DELETE_ALL_SQL();
      await manager.query(deleteSql);

      //批量插入模块关联的字典
      let sql = SysConfigSQL.BATCH_INSERT_SQL(records);
      await manager.query(sql);
    });

    return true;
  }

  /**
   * 更新当前用户的语言参数
   * @param locale 语言参数
   */
  async updateLocale(locale: string): Promise<any> {
    if (!locale || locale.length == 0) return false;
    const config = ConfigMapper.getConfig(SYS_USER.USER_LANGUAGE);
    if (config) {
      await this.update({ cols: ['conValue'], conValue: locale, conId: config.conId });
    }
    return true;
  }
}
