import { NestExpressApplication } from '@nestjs/platform-express';
import { SysCmsFilesService } from './service/sys.cms.files.service';
import { SysCmsNoticeService } from './service/sys.cms.notice.service';
import { SysConfigService } from './service/sys.config.service';
import { SysContactService } from './service/sys.contact.service';
import { SysDictionaryService } from './service/sys.dictionary.service';
import { SysFilesService } from './service/sys.files.service';
import { SysLogService } from './service/sys.log.service';
import { SysMemoryService } from './service/sys.memory.service';
import { SysMenuService } from './service/sys.menu.service';
import { SysNoticeService } from './service/sys.notice.service';
import { SysPermissionService } from './service/sys.permission.service';
import { SysRoleService } from './service/sys.role.service';
import { SysRoleDictionaryService } from './service/sys.role.dictionary.service';
import { SysRoleMenuService } from './service/sys.role.menu.service';
import { SysRolePermissionService } from './service/sys.role.permission.service';
import { SysTipService } from './service/sys.tip.service';
import { SysUpdateService } from './service/sys.update.service';
import { SysUserService } from './service/sys.user.service';
import { SysUserRoleService } from './service/sys.user.role.service';
import { SysUserParamsService } from './service/sys.user.params.service';
/**
 * SYS模块服务类的静态引用
 * @date 1/7/2021, 9:25:29 PM
 * @author jiangbin
 **/
export class SysServices {
  public static sysCmsFilesService: SysCmsFilesService;
  public static sysCmsNoticeService: SysCmsNoticeService;
  public static sysConfigService: SysConfigService;
  public static sysContactService: SysContactService;
  public static sysDictionaryService: SysDictionaryService;
  public static sysFilesService: SysFilesService;
  public static sysLogService: SysLogService;
  public static sysMemoryService: SysMemoryService;
  public static sysMenuService: SysMenuService;
  public static sysNoticeService: SysNoticeService;
  public static sysPermissionService: SysPermissionService;
  public static sysRoleService: SysRoleService;
  public static sysRoleDictionaryService: SysRoleDictionaryService;
  public static sysRoleMenuService: SysRoleMenuService;
  public static sysRolePermissionService: SysRolePermissionService;
  public static sysTipService: SysTipService;
  public static sysUpdateService: SysUpdateService;
  public static sysUserService: SysUserService;
  public static sysUserParamsService: SysUserParamsService;
  public static sysUserRoleService: SysUserRoleService;

  /**
   * 初始化SYS模块服务类的静态引用
   * @date 1/7/2021, 9:25:29 PM
   * @author jiangbin
   **/
  static init(app: NestExpressApplication) {
    this.sysCmsFilesService = app.get(SysCmsFilesService);
    this.sysCmsNoticeService = app.get(SysCmsNoticeService);
    this.sysConfigService = app.get(SysConfigService);
    this.sysContactService = app.get(SysContactService);
    this.sysDictionaryService = app.get(SysDictionaryService);
    this.sysFilesService = app.get(SysFilesService);
    this.sysLogService = app.get(SysLogService);
    this.sysMemoryService = app.get(SysMemoryService);
    this.sysMenuService = app.get(SysMenuService);
    this.sysNoticeService = app.get(SysNoticeService);
    this.sysPermissionService = app.get(SysPermissionService);
    this.sysRoleService = app.get(SysRoleService);
    this.sysRoleDictionaryService = app.get(SysRoleDictionaryService);
    this.sysRoleMenuService = app.get(SysRoleMenuService);
    this.sysRolePermissionService = app.get(SysRolePermissionService);
    this.sysTipService = app.get(SysTipService);
    this.sysUpdateService = app.get(SysUpdateService);
    this.sysUserService = app.get(SysUserService);
    this.sysUserParamsService = app.get(SysUserParamsService);
    this.sysUserRoleService = app.get(SysUserRoleService);
  }
}
