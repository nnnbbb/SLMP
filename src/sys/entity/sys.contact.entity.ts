import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_CONTACT
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysContact
 */
@Entity({
  name: 'SYS_CONTACT',
})
export class SysContact {
  /**
   * 反馈编号-主键
   *
   * @type { string }
   * @memberof SysContact
   */
  @Column({ name: 'CONTACT_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【反馈编号】不能为空' })
  @Max(64, { message: '【反馈编号】长度不能超过64' })
  contactId: string;
  /**
   * 反馈用户
   *
   * @type { string }
   * @memberof SysContact
   */
  @Column({ name: 'CONTACT_AUTHOR', type: 'varchar', length: '128' })
  @Max(128, { message: '【反馈用户】长度不能超过128' })
  contactAuthor: string;
  /**
   * 反馈邮箱
   *
   * @type { string }
   * @memberof SysContact
   */
  @Column({ name: 'CONTACT_EMAIL', type: 'varchar', length: '128' })
  @Max(128, { message: '【反馈邮箱】长度不能超过128' })
  contactEmail: string;
  /**
   * 反馈主题
   *
   * @type { string }
   * @memberof SysContact
   */
  @Column({ name: 'CONTACT_SUBJECT', type: 'varchar', length: '128' })
  @Max(128, { message: '【反馈主题】长度不能超过128' })
  contactSubject: string;
  /**
   * 反馈内容
   *
   * @type { string }
   * @memberof SysContact
   */
  @Column({ name: 'CONTACT_TEXT', type: 'varchar', length: '256' })
  @IsNotEmpty({ message: '【反馈内容】不能为空' })
  @Max(256, { message: '【反馈内容】长度不能超过256' })
  contactText: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysContact
   */
  @Type(() => Date)
  @Column({ name: 'CONTACT_CREATE_DATE', type: 'datetime' })
  @IsNotEmpty({ message: '【创建日期】不能为空' })
  contactCreateDate: Date;
}
