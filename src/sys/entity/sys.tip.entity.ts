import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_TIP
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysTip
 */
@Entity({
  name: 'SYS_TIP',
})
export class SysTip {
  /**
   * 提示信息ID-主键
   *
   * @type { string }
   * @memberof SysTip
   */
  @Column({ name: 'TIP_ID', type: 'varchar', length: '32', primary: true })
  @IsNotEmpty({ message: '【提示信息ID】不能为空' })
  @Max(32, { message: '【提示信息ID】长度不能超过32' })
  tipId: string;
  /**
   * 提示次数
   *
   * @type { number }
   * @memberof SysTip
   */
  @Type(() => Number)
  @Column({ name: 'TIP_COUNT', type: 'int' })
  tipCount: number;
  /**
   * 提示信息内容
   *
   * @type { string }
   * @memberof SysTip
   */
  @Column({ name: 'TIP_CONTENTS', type: 'varchar', length: '512' })
  @Max(512, { message: '【提示信息内容】长度不能超过512' })
  tipContents: string;
  /**
   * 所属者
   *
   * @type { string }
   * @memberof SysTip
   */
  @Column({ name: 'TIP_MANAGER', type: 'varchar', length: '255' })
  @Max(255, { message: '【所属者】长度不能超过255' })
  tipManager: string;
  /**
   * 消息受众
   *
   * @type { string }
   * @memberof SysTip
   */
  @Column({ name: 'TIP_AUDIENCE', type: 'varchar', length: '255' })
  @Max(255, { message: '【消息受众】长度不能超过255' })
  tipAudience: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysTip
   */
  @Type(() => Date)
  @Column({ name: 'TIP_CREATE_DATE', type: 'datetime' })
  tipCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysTip
   */
  @Type(() => Date)
  @Column({ name: 'TIP_UPDATE_DATE', type: 'datetime' })
  tipUpdateDate: Date;
}
