import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
/**
 * SYS_ROLE
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysRole
 */
@Entity({
  name: 'SYS_ROLE',
})
export class SysRole {
  /**
   * 角色ID-主键
   *
   * @type { string }
   * @memberof SysRole
   */
  @Column({ name: 'ROLE_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【角色ID】不能为空' })
  @Max(64, { message: '【角色ID】长度不能超过64' })
  roleId: string;
  /**
   * 角色名称
   *
   * @type { string }
   * @memberof SysRole
   */
  @Column({ name: 'ROLE_NAME', type: 'varchar', length: '64' })
  @Max(64, { message: '【角色名称】长度不能超过64' })
  roleName: string;
  /**
   * roleNameEn
   *
   * @type { string }
   * @memberof SysRole
   */
  @Column({ name: 'ROLE_NAME_EN', type: 'varchar', length: '64' })
  @Max(64, { message: '【roleNameEn】长度不能超过64' })
  roleNameEn: string;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysRole
   */
  @Column({ name: 'ROLE_STATE', type: 'varchar', length: '64' })
  @Max(64, { message: '【状态】长度不能超过64' })
  roleState: string;
  /**
   * 角色所属系统
   *
   * @type { string }
   * @memberof SysRole
   */
  @Column({ name: 'ROLE_SYS', type: 'varchar', length: '64' })
  @Max(64, { message: '【角色所属系统】长度不能超过64' })
  roleSys: string;
}
