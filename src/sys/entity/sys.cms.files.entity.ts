import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_CMS_FILES
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysCmsFiles
 */
@Entity({
  name: 'SYS_CMS_FILES',
})
export class SysCmsFiles {
  /**
   * 文件编号-主键
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【文件编号】不能为空' })
  @Max(64, { message: '【文件编号】长度不能超过64' })
  scFileId: string;
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_NAME', type: 'varchar', length: '128' })
  @Max(128, { message: '【文件名称】长度不能超过128' })
  scFileName: string;
  /**
   * scFileNameCn
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_NAME_CN', type: 'varchar', length: '128' })
  @Max(128, { message: '【scFileNameCn】长度不能超过128' })
  scFileNameCn: string;
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_TYPE', type: 'varchar', length: '128' })
  @IsNotEmpty({ message: '【文件类型】不能为空' })
  @Max(128, { message: '【文件类型】长度不能超过128' })
  scFileType: string;
  /**
   * 文件路径
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_PATH', type: 'varchar', length: '1024' })
  @Max(1024, { message: '【文件路径】长度不能超过1024' })
  scFilePath: string;
  /**
   * 是否是最新
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_IS_NEW', type: 'varchar', length: '4' })
  @Max(4, { message: '【是否是最新】长度不能超过4' })
  scFileIsNew: string;
  /**
   * 文件上传者
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_MANAGER', type: 'varchar', length: '128' })
  @Max(128, { message: '【文件上传者】长度不能超过128' })
  scFileManager: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsFiles
   */
  @Type(() => Date)
  @Column({ name: 'SC_FILE_CREATE_DATE', type: 'datetime' })
  scFileCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsFiles
   */
  @Type(() => Date)
  @Column({ name: 'SC_FIEL_UPDATE_DATE', type: 'datetime' })
  scFielUpdateDate: Date;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysCmsFiles
   */
  @Column({ name: 'SC_FILE_SPECIES', type: 'varchar', length: '32' })
  @Max(32, { message: '【种属】长度不能超过32' })
  scFileSpecies: string;
}
