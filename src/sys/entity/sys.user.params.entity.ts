import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * 用户参数表
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export
 * @class SysUserParams
 */
@Entity({
  name: 'SYS_USER_PARAMS',
})
export class SysUserParams {
  /**
   * 主键-主键
   *
   * @type { string }
   * @memberof SysUserParams
   */
  @Column({ name: 'UP_ID', type: 'varchar', length: '32', primary: true })
  @IsNotEmpty({ message: '【主键】不能为空' })
  @Max(32, { message: '【主键】长度不能超过32' })
  upId: string;
  /**
   * 帐号
   *
   * @type { string }
   * @memberof SysUserParams
   */
  @Column({ name: 'UP_LOGIN_NAME', type: 'varchar', length: '64' })
  @Max(64, { message: '【帐号】长度不能超过64' })
  upLoginName: string;
  /**
   * 参数名
   *
   * @type { string }
   * @memberof SysUserParams
   */
  @Column({ name: 'UP_PARAM_NAME', type: 'varchar', length: '128' })
  @Max(128, { message: '【参数名】长度不能超过128' })
  upParamName: string;
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysUserParams
   */
  @Column({ name: 'UP_PARAM_VALUE', type: 'longtext' })
  upParamValue: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserParams
   */
  @Type(() => Date)
  @Column({ name: 'UP_CREATE_DATE', type: 'datetime' })
  upCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUserParams
   */
  @Type(() => Date)
  @Column({ name: 'UP_UPDATE_DATE', type: 'datetime' })
  upUpdateDate: Date;
}
//声明类型定义
export declare type SysUserParamsType = SysUserParams | Partial<SysUserParams>;
