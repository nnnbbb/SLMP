import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
/**
 * SYS_ROLE_PERMISSION
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysRolePermission
 */
@Entity({
  name: 'SYS_ROLE_PERMISSION',
})
export class SysRolePermission {
  /**
   * 角色权限表Id-主键
   *
   * @type { string }
   * @memberof SysRolePermission
   */
  @Column({ name: 'RP_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【角色权限表Id】不能为空' })
  @Max(64, { message: '【角色权限表Id】长度不能超过64' })
  rpId: string;
  /**
   * 角色ID号
   *
   * @type { string }
   * @memberof SysRolePermission
   */
  @Column({ name: 'RP_ROLE_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【角色ID号】不能为空' })
  @Max(64, { message: '【角色ID号】长度不能超过64' })
  rpRoleId: string;
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysRolePermission
   */
  @Column({ name: 'RP_PERMISSION_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【权限ID】不能为空' })
  @Max(64, { message: '【权限ID】长度不能超过64' })
  rpPermissionId: string;
}
