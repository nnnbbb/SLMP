import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * 用户提示消息
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysNotice
 */
@Entity({
  name: 'SYS_NOTICE',
})
export class SysNotice {
  /**
   * 消息编号-主键
   *
   * @type { string }
   * @memberof SysNotice
   */
  @Column({ name: 'NOTICE_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【消息编号】不能为空' })
  @Max(64, { message: '【消息编号】长度不能超过64' })
  noticeId: string;
  /**
   * 消息标题
   *
   * @type { string }
   * @memberof SysNotice
   */
  @Column({ name: 'NOTICE_TITLE', type: 'varchar', length: '128' })
  @Max(128, { message: '【消息标题】长度不能超过128' })
  noticeTitle: string;
  /**
   * 消息内容
   *
   * @type { string }
   * @memberof SysNotice
   */
  @Column({ name: 'NOTICE_CONTENT', type: 'varchar', length: '20000' })
  @Max(20000, { message: '【消息内容】长度不能超过20000' })
  noticeContent: string;
  /**
   * 消息发布者
   *
   * @type { string }
   * @memberof SysNotice
   */
  @Column({ name: 'NOTICE_MANAGER', type: 'varchar', length: '128' })
  @Max(128, { message: '【消息发布者】长度不能超过128' })
  noticeManager: string;
  /**
   * 是否是新消息
   *
   * @type { string }
   * @memberof SysNotice
   */
  @Column({ name: 'NOTICE_IS_NEW', type: 'varchar', length: '64' })
  @Max(64, { message: '【是否是新消息】长度不能超过64' })
  noticeIsNew: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysNotice
   */
  @Type(() => Date)
  @Column({ name: 'NOTICE_CREATE_DATE', type: 'datetime' })
  noticeCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysNotice
   */
  @Type(() => Date)
  @Column({ name: 'NOTICE_UPDATE_DATE', type: 'datetime' })
  noticeUpdateDate: Date;
  /**
   * 消息接收者
   *
   * @type { string }
   * @memberof SysNotice
   */
  @Column({ name: 'NOTICE_RECEIVER', type: 'varchar', length: '128' })
  @Max(128, { message: '【消息接收者】长度不能超过128' })
  noticeReceiver: string;
}
