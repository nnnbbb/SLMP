import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
/**
 * SYS_USER_ROLE
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysUserRole
 */
@Entity({
  name: 'SYS_USER_ROLE',
})
export class SysUserRole {
  /**
   * 用户角色ID-主键
   *
   * @type { string }
   * @memberof SysUserRole
   */
  @Column({ name: 'UR_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【用户角色ID】不能为空' })
  @Max(64, { message: '【用户角色ID】长度不能超过64' })
  urId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysUserRole
   */
  @Column({ name: 'UR_ROLE_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【角色ID】不能为空' })
  @Max(64, { message: '【角色ID】长度不能超过64' })
  urRoleId: string;
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUserRole
   */
  @Column({ name: 'UR_USER_LOGIN_NAME', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【用户帐号】不能为空' })
  @Max(64, { message: '【用户帐号】长度不能超过64' })
  urUserLoginName: string;
}
