import { SysFilesQDto } from '../../dto/query/sys.files.qdto';
import { SysFilesCDto } from '../../dto/create/sys.files.cdto';
import { SysFilesUDto } from '../../dto/update/sys.files.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysFilesColNameEnum, SysFilesColPropEnum, SysFilesTable, SysFilesColNames, SysFilesColProps } from '../ddl/sys.files.cols';
/**
 * SYS_FILES--文件管理表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysFilesSQL
 */
export const SysFilesSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let cols = SysFilesSQL.COL_MAPPER_SQL(dto);
    let where = SysFilesSQL.WHERE_SQL(dto);
    let group = SysFilesSQL.GROUP_BY_SQL(dto);
    let order = SysFilesSQL.ORDER_BY_SQL(dto);
    let limit = SysFilesSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let where = SysFilesSQL.WHERE_SQL(dto);
    let group = SysFilesSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let cols = SysFilesSQL.COL_MAPPER_SQL(dto);
    let group = SysFilesSQL.GROUP_BY_SQL(dto);
    let order = SysFilesSQL.ORDER_BY_SQL(dto);
    let limit = SysFilesSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysFilesSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysFilesSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysFilesSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let where = SysFilesSQL.WHERE_SQL(dto);
    let group = SysFilesSQL.GROUP_BY_SQL(dto);
    let order = SysFilesSQL.ORDER_BY_SQL(dto);
    let limit = SysFilesSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysFilesColNames, SysFilesColProps);
    if (!cols || cols.length == 0) cols = SysFilesColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[fileId]?.length > 0) items.push(` ${FILE_ID} = '${dto[fileId]}'`);
    if (dto['nfileId']?.length > 0) items.push(` ${FILE_ID} != '${dto['nfileId']}'`);
    if (dto[fileBarcode]?.length > 0) items.push(` ${FILE_BARCODE} = '${dto[fileBarcode]}'`);
    if (dto['nfileBarcode']?.length > 0) items.push(` ${FILE_BARCODE} != '${dto['nfileBarcode']}'`);
    if (dto[fileRelateId]?.length > 0) items.push(` ${FILE_RELATE_ID} = '${dto[fileRelateId]}'`);
    if (dto['nfileRelateId']?.length > 0) items.push(` ${FILE_RELATE_ID} != '${dto['nfileRelateId']}'`);
    if (dto[fileName]?.length > 0) items.push(` ${FILE_NAME} = '${dto[fileName]}'`);
    if (dto['nfileName']?.length > 0) items.push(` ${FILE_NAME} != '${dto['nfileName']}'`);
    if (dto[fileNameEn]?.length > 0) items.push(` ${FILE_NAME_EN} = '${dto[fileNameEn]}'`);
    if (dto['nfileNameEn']?.length > 0) items.push(` ${FILE_NAME_EN} != '${dto['nfileNameEn']}'`);
    if (dto[filePath]?.length > 0) items.push(` ${FILE_PATH} = '${dto[filePath]}'`);
    if (dto['nfilePath']?.length > 0) items.push(` ${FILE_PATH} != '${dto['nfilePath']}'`);
    if (dto[fileCreateDate]) items.push(`${FILE_CREATE_DATE} is not null and date_format(${FILE_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[fileCreateDate])}`);
    if (dto['nfileCreateDate']) items.push(`${FILE_CREATE_DATE} is not null and date_format(${FILE_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nfileCreateDate'])}`);
    if (dto[fileUpdateDate]) items.push(`${FILE_UPDATE_DATE} is not null and date_format(${FILE_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[fileUpdateDate])}`);
    if (dto['nfileUpdateDate']) items.push(`${FILE_UPDATE_DATE} is not null and date_format(${FILE_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nfileUpdateDate'])}`);
    if (dto[fileIsUsed]?.length > 0) items.push(` ${FILE_IS_USED} = '${dto[fileIsUsed]}'`);
    if (dto['nfileIsUsed']?.length > 0) items.push(` ${FILE_IS_USED} != '${dto['nfileIsUsed']}'`);
    if (dto[fileOwner]?.length > 0) items.push(` ${FILE_OWNER} = '${dto[fileOwner]}'`);
    if (dto['nfileOwner']?.length > 0) items.push(` ${FILE_OWNER} != '${dto['nfileOwner']}'`);
    if (dto[fileSize]) items.push(` ${FILE_SIZE} = '${dto[fileSize]}'`);
    if (dto['nfileSize']) items.push(` ${FILE_SIZE} != '${dto['nfileSize']}'`);
    if (dto[fileGrants]?.length > 0) items.push(` ${FILE_GRANTS} = '${dto[fileGrants]}'`);
    if (dto['nfileGrants']?.length > 0) items.push(` ${FILE_GRANTS} != '${dto['nfileGrants']}'`);
    if (dto[fileClasses]?.length > 0) items.push(` ${FILE_CLASSES} = '${dto[fileClasses]}'`);
    if (dto['nfileClasses']?.length > 0) items.push(` ${FILE_CLASSES} != '${dto['nfileClasses']}'`);
    if (dto[fileState]?.length > 0) items.push(` ${FILE_STATE} = '${dto[fileState]}'`);
    if (dto['nfileState']?.length > 0) items.push(` ${FILE_STATE} != '${dto['nfileState']}'`);
    if (dto[fileType]?.length > 0) items.push(` ${FILE_TYPE} = '${dto[fileType]}'`);
    if (dto['nfileType']?.length > 0) items.push(` ${FILE_TYPE} != '${dto['nfileType']}'`);
    if (dto[fileComments]?.length > 0) items.push(` ${FILE_COMMENTS} = '${dto[fileComments]}'`);
    if (dto['nfileComments']?.length > 0) items.push(` ${FILE_COMMENTS} != '${dto['nfileComments']}'`);
    if (dto[fileSpecies]?.length > 0) items.push(` ${FILE_SPECIES} = '${dto[fileSpecies]}'`);
    if (dto['nfileSpecies']?.length > 0) items.push(` ${FILE_SPECIES} != '${dto['nfileSpecies']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //FILE_ID--字符串字段
    if (dto['fileIdList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_ID, <string[]>dto['fileIdList']));
    if (dto['nfileIdList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_ID, <string[]>dto['nfileIdList'], true));
    if (dto['fileIdLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_ID, dto['fileIdLike'])); //For MysSQL
    if (dto['nfileIdLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_ID, dto['nfileIdLike'], true)); //For MysSQL
    if (dto['fileIdLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_ID, <string[]>dto['fileIdLikeList'])); //For MysSQL
    //FILE_BARCODE--字符串字段
    if (dto['fileBarcodeList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_BARCODE, <string[]>dto['fileBarcodeList']));
    if (dto['nfileBarcodeList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_BARCODE, <string[]>dto['nfileBarcodeList'], true));
    if (dto['fileBarcodeLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_BARCODE, dto['fileBarcodeLike'])); //For MysSQL
    if (dto['nfileBarcodeLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_BARCODE, dto['nfileBarcodeLike'], true)); //For MysSQL
    if (dto['fileBarcodeLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_BARCODE, <string[]>dto['fileBarcodeLikeList'])); //For MysSQL
    //FILE_RELATE_ID--字符串字段
    if (dto['fileRelateIdList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_RELATE_ID, <string[]>dto['fileRelateIdList']));
    if (dto['nfileRelateIdList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_RELATE_ID, <string[]>dto['nfileRelateIdList'], true));
    if (dto['fileRelateIdLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_RELATE_ID, dto['fileRelateIdLike'])); //For MysSQL
    if (dto['nfileRelateIdLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_RELATE_ID, dto['nfileRelateIdLike'], true)); //For MysSQL
    if (dto['fileRelateIdLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_RELATE_ID, <string[]>dto['fileRelateIdLikeList'])); //For MysSQL
    //FILE_NAME--字符串字段
    if (dto['fileNameList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_NAME, <string[]>dto['fileNameList']));
    if (dto['nfileNameList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_NAME, <string[]>dto['nfileNameList'], true));
    if (dto['fileNameLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_NAME, dto['fileNameLike'])); //For MysSQL
    if (dto['nfileNameLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_NAME, dto['nfileNameLike'], true)); //For MysSQL
    if (dto['fileNameLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_NAME, <string[]>dto['fileNameLikeList'])); //For MysSQL
    //FILE_NAME_EN--字符串字段
    if (dto['fileNameEnList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_NAME_EN, <string[]>dto['fileNameEnList']));
    if (dto['nfileNameEnList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_NAME_EN, <string[]>dto['nfileNameEnList'], true));
    if (dto['fileNameEnLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_NAME_EN, dto['fileNameEnLike'])); //For MysSQL
    if (dto['nfileNameEnLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_NAME_EN, dto['nfileNameEnLike'], true)); //For MysSQL
    if (dto['fileNameEnLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_NAME_EN, <string[]>dto['fileNameEnLikeList'])); //For MysSQL
    //FILE_PATH--字符串字段
    if (dto['filePathList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_PATH, <string[]>dto['filePathList']));
    if (dto['nfilePathList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_PATH, <string[]>dto['nfilePathList'], true));
    if (dto['filePathLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_PATH, dto['filePathLike'])); //For MysSQL
    if (dto['nfilePathLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_PATH, dto['nfilePathLike'], true)); //For MysSQL
    if (dto['filePathLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_PATH, <string[]>dto['filePathLikeList'])); //For MysSQL
    //FILE_CREATE_DATE--日期字段
    if (dto[fileCreateDate]) items.push(`${FILE_CREATE_DATE} is not null and date_format(${FILE_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sFileCreateDate'])}`);
    if (dto[fileCreateDate]) items.push(`${FILE_CREATE_DATE} is not null and date_format(${FILE_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eFileCreateDate'])}`);
    //FILE_UPDATE_DATE--日期字段
    if (dto[fileUpdateDate]) items.push(`${FILE_UPDATE_DATE} is not null and date_format(${FILE_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sFileUpdateDate'])}`);
    if (dto[fileUpdateDate]) items.push(`${FILE_UPDATE_DATE} is not null and date_format(${FILE_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eFileUpdateDate'])}`);
    //FILE_IS_USED--字符串字段
    if (dto['fileIsUsedList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_IS_USED, <string[]>dto['fileIsUsedList']));
    if (dto['nfileIsUsedList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_IS_USED, <string[]>dto['nfileIsUsedList'], true));
    if (dto['fileIsUsedLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_IS_USED, dto['fileIsUsedLike'])); //For MysSQL
    if (dto['nfileIsUsedLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_IS_USED, dto['nfileIsUsedLike'], true)); //For MysSQL
    if (dto['fileIsUsedLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_IS_USED, <string[]>dto['fileIsUsedLikeList'])); //For MysSQL
    //FILE_OWNER--字符串字段
    if (dto['fileOwnerList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_OWNER, <string[]>dto['fileOwnerList']));
    if (dto['nfileOwnerList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_OWNER, <string[]>dto['nfileOwnerList'], true));
    if (dto['fileOwnerLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_OWNER, dto['fileOwnerLike'])); //For MysSQL
    if (dto['nfileOwnerLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_OWNER, dto['nfileOwnerLike'], true)); //For MysSQL
    if (dto['fileOwnerLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_OWNER, <string[]>dto['fileOwnerLikeList'])); //For MysSQL
    //FILE_SIZE--数字字段
    if (dto['fileSizeList']) items.push(SysFilesSQL.IN_SQL(FILE_SIZE, dto['fileSizeList']));
    if (dto['nfileSizeList']) items.push(SysFilesSQL.IN_SQL(FILE_SIZE, dto['nfileSizeList'], true));
    //FILE_GRANTS--字符串字段
    if (dto['fileGrantsList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_GRANTS, <string[]>dto['fileGrantsList']));
    if (dto['nfileGrantsList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_GRANTS, <string[]>dto['nfileGrantsList'], true));
    if (dto['fileGrantsLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_GRANTS, dto['fileGrantsLike'])); //For MysSQL
    if (dto['nfileGrantsLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_GRANTS, dto['nfileGrantsLike'], true)); //For MysSQL
    if (dto['fileGrantsLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_GRANTS, <string[]>dto['fileGrantsLikeList'])); //For MysSQL
    //FILE_CLASSES--字符串字段
    if (dto['fileClassesList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_CLASSES, <string[]>dto['fileClassesList']));
    if (dto['nfileClassesList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_CLASSES, <string[]>dto['nfileClassesList'], true));
    if (dto['fileClassesLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_CLASSES, dto['fileClassesLike'])); //For MysSQL
    if (dto['nfileClassesLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_CLASSES, dto['nfileClassesLike'], true)); //For MysSQL
    if (dto['fileClassesLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_CLASSES, <string[]>dto['fileClassesLikeList'])); //For MysSQL
    //FILE_STATE--字符串字段
    if (dto['fileStateList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_STATE, <string[]>dto['fileStateList']));
    if (dto['nfileStateList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_STATE, <string[]>dto['nfileStateList'], true));
    if (dto['fileStateLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_STATE, dto['fileStateLike'])); //For MysSQL
    if (dto['nfileStateLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_STATE, dto['nfileStateLike'], true)); //For MysSQL
    if (dto['fileStateLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_STATE, <string[]>dto['fileStateLikeList'])); //For MysSQL
    //FILE_TYPE--字符串字段
    if (dto['fileTypeList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_TYPE, <string[]>dto['fileTypeList']));
    if (dto['nfileTypeList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_TYPE, <string[]>dto['nfileTypeList'], true));
    if (dto['fileTypeLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_TYPE, dto['fileTypeLike'])); //For MysSQL
    if (dto['nfileTypeLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_TYPE, dto['nfileTypeLike'], true)); //For MysSQL
    if (dto['fileTypeLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_TYPE, <string[]>dto['fileTypeLikeList'])); //For MysSQL
    //FILE_COMMENTS--字符串字段
    if (dto['fileCommentsList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_COMMENTS, <string[]>dto['fileCommentsList']));
    if (dto['nfileCommentsList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_COMMENTS, <string[]>dto['nfileCommentsList'], true));
    if (dto['fileCommentsLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_COMMENTS, dto['fileCommentsLike'])); //For MysSQL
    if (dto['nfileCommentsLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_COMMENTS, dto['nfileCommentsLike'], true)); //For MysSQL
    if (dto['fileCommentsLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_COMMENTS, <string[]>dto['fileCommentsLikeList'])); //For MysSQL
    //FILE_SPECIES--字符串字段
    if (dto['fileSpeciesList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_SPECIES, <string[]>dto['fileSpeciesList']));
    if (dto['nfileSpeciesList']?.length > 0) items.push(SysFilesSQL.IN_SQL(FILE_SPECIES, <string[]>dto['nfileSpeciesList'], true));
    if (dto['fileSpeciesLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_SPECIES, dto['fileSpeciesLike'])); //For MysSQL
    if (dto['nfileSpeciesLike']?.length > 0) items.push(SysFilesSQL.LIKE_SQL(FILE_SPECIES, dto['nfileSpeciesLike'], true)); //For MysSQL
    if (dto['fileSpeciesLikeList']?.length > 0) items.push(SysFilesSQL.LIKE_LIST_SQL(FILE_SPECIES, <string[]>dto['fileSpeciesLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysFilesSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysFilesCDto | Partial<SysFilesCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(FILE_ID);
    cols.push(FILE_BARCODE);
    cols.push(FILE_RELATE_ID);
    cols.push(FILE_NAME);
    cols.push(FILE_NAME_EN);
    cols.push(FILE_PATH);
    cols.push(FILE_CREATE_DATE);
    cols.push(FILE_UPDATE_DATE);
    cols.push(FILE_IS_USED);
    cols.push(FILE_OWNER);
    cols.push(FILE_SIZE);
    cols.push(FILE_GRANTS);
    cols.push(FILE_CLASSES);
    cols.push(FILE_STATE);
    cols.push(FILE_TYPE);
    cols.push(FILE_COMMENTS);
    cols.push(FILE_SPECIES);
    values.push(toSqlValue(dto[fileId]));
    values.push(toSqlValue(dto[fileBarcode]));
    values.push(toSqlValue(dto[fileRelateId]));
    values.push(toSqlValue(dto[fileName]));
    values.push(toSqlValue(dto[fileNameEn]));
    values.push(toSqlValue(dto[filePath]));
    values.push('NOW()');
    values.push('NOW()');
    values.push(toSqlValue(dto[fileIsUsed]));
    values.push(toSqlValue(dto[fileOwner]));
    values.push(toSqlValue(dto[fileSize]));
    values.push(toSqlValue(dto[fileGrants]));
    values.push(toSqlValue(dto[fileClasses]));
    values.push(toSqlValue(dto[fileState]));
    values.push(toSqlValue(dto[fileType]));
    values.push(toSqlValue(dto[fileComments]));
    values.push(toSqlValue(dto[fileSpecies]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysFilesCDto[] | Partial<SysFilesCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(FILE_ID);
    cols.push(FILE_BARCODE);
    cols.push(FILE_RELATE_ID);
    cols.push(FILE_NAME);
    cols.push(FILE_NAME_EN);
    cols.push(FILE_PATH);
    cols.push(FILE_CREATE_DATE);
    cols.push(FILE_UPDATE_DATE);
    cols.push(FILE_IS_USED);
    cols.push(FILE_OWNER);
    cols.push(FILE_SIZE);
    cols.push(FILE_GRANTS);
    cols.push(FILE_CLASSES);
    cols.push(FILE_STATE);
    cols.push(FILE_TYPE);
    cols.push(FILE_COMMENTS);
    cols.push(FILE_SPECIES);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[fileId]));
      values.push(toSqlValue(dto[fileBarcode]));
      values.push(toSqlValue(dto[fileRelateId]));
      values.push(toSqlValue(dto[fileName]));
      values.push(toSqlValue(dto[fileNameEn]));
      values.push(toSqlValue(dto[filePath]));
      values.push('NOW()');
      values.push('NOW()');
      values.push(toSqlValue(dto[fileIsUsed]));
      values.push(toSqlValue(dto[fileOwner]));
      values.push(toSqlValue(dto[fileSize]));
      values.push(toSqlValue(dto[fileGrants]));
      values.push(toSqlValue(dto[fileClasses]));
      values.push(toSqlValue(dto[fileState]));
      values.push(toSqlValue(dto[fileType]));
      values.push(toSqlValue(dto[fileComments]));
      values.push(toSqlValue(dto[fileSpecies]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysFilesUDto | Partial<SysFilesUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysFilesColNames, SysFilesColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${FILE_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${FILE_BARCODE} = ${toSqlValue(dto[fileBarcode])}`);
    items.push(`${FILE_RELATE_ID} = ${toSqlValue(dto[fileRelateId])}`);
    items.push(`${FILE_NAME} = ${toSqlValue(dto[fileName])}`);
    items.push(`${FILE_NAME_EN} = ${toSqlValue(dto[fileNameEn])}`);
    items.push(`${FILE_PATH} = ${toSqlValue(dto[filePath])}`);
    items.push(`${FILE_UPDATE_DATE} = NOW()`);
    items.push(`${FILE_IS_USED} = ${toSqlValue(dto[fileIsUsed])}`);
    items.push(`${FILE_OWNER} = ${toSqlValue(dto[fileOwner])}`);
    items.push(`${FILE_SIZE} = ${toSqlValue(dto[fileSize])}`);
    items.push(`${FILE_GRANTS} = ${toSqlValue(dto[fileGrants])}`);
    items.push(`${FILE_CLASSES} = ${toSqlValue(dto[fileClasses])}`);
    items.push(`${FILE_STATE} = ${toSqlValue(dto[fileState])}`);
    items.push(`${FILE_TYPE} = ${toSqlValue(dto[fileType])}`);
    items.push(`${FILE_COMMENTS} = ${toSqlValue(dto[fileComments])}`);
    items.push(`${FILE_SPECIES} = ${toSqlValue(dto[fileSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysFilesUDto | Partial<SysFilesUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysFilesColNames, SysFilesColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${FILE_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysFilesSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${FILE_BARCODE} = ${toSqlValue(dto[fileBarcode])}`);
    items.push(`${FILE_RELATE_ID} = ${toSqlValue(dto[fileRelateId])}`);
    items.push(`${FILE_NAME} = ${toSqlValue(dto[fileName])}`);
    items.push(`${FILE_NAME_EN} = ${toSqlValue(dto[fileNameEn])}`);
    items.push(`${FILE_PATH} = ${toSqlValue(dto[filePath])}`);
    items.push(`${FILE_UPDATE_DATE} = NOW()`);
    items.push(`${FILE_IS_USED} = ${toSqlValue(dto[fileIsUsed])}`);
    items.push(`${FILE_OWNER} = ${toSqlValue(dto[fileOwner])}`);
    items.push(`${FILE_SIZE} = ${toSqlValue(dto[fileSize])}`);
    items.push(`${FILE_GRANTS} = ${toSqlValue(dto[fileGrants])}`);
    items.push(`${FILE_CLASSES} = ${toSqlValue(dto[fileClasses])}`);
    items.push(`${FILE_STATE} = ${toSqlValue(dto[fileState])}`);
    items.push(`${FILE_TYPE} = ${toSqlValue(dto[fileType])}`);
    items.push(`${FILE_COMMENTS} = ${toSqlValue(dto[fileComments])}`);
    items.push(`${FILE_SPECIES} = ${toSqlValue(dto[fileSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysFilesSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysFilesUDto[] | Partial<SysFilesUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysFilesSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysFilesUDto | Partial<SysFilesUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[fileBarcode]?.length > 0) items.push(`${FILE_BARCODE} = ${toSqlValue(dto[fileBarcode])}`);
    if (dto[fileRelateId]?.length > 0) items.push(`${FILE_RELATE_ID} = ${toSqlValue(dto[fileRelateId])}`);
    if (dto[fileName]?.length > 0) items.push(`${FILE_NAME} = ${toSqlValue(dto[fileName])}`);
    if (dto[fileNameEn]?.length > 0) items.push(`${FILE_NAME_EN} = ${toSqlValue(dto[fileNameEn])}`);
    if (dto[filePath]?.length > 0) items.push(`${FILE_PATH} = ${toSqlValue(dto[filePath])}`);
    if (dto[fileUpdateDate]) items.push(`${FILE_UPDATE_DATE}= NOW()`);
    if (dto[fileIsUsed]?.length > 0) items.push(`${FILE_IS_USED} = ${toSqlValue(dto[fileIsUsed])}`);
    if (dto[fileOwner]?.length > 0) items.push(`${FILE_OWNER} = ${toSqlValue(dto[fileOwner])}`);
    if (dto[fileSize]) items.push(`${FILE_SIZE} = ${toSqlValue(dto[fileSize])}`);
    if (dto[fileGrants]?.length > 0) items.push(`${FILE_GRANTS} = ${toSqlValue(dto[fileGrants])}`);
    if (dto[fileClasses]?.length > 0) items.push(`${FILE_CLASSES} = ${toSqlValue(dto[fileClasses])}`);
    if (dto[fileState]?.length > 0) items.push(`${FILE_STATE} = ${toSqlValue(dto[fileState])}`);
    if (dto[fileType]?.length > 0) items.push(`${FILE_TYPE} = ${toSqlValue(dto[fileType])}`);
    if (dto[fileComments]?.length > 0) items.push(`${FILE_COMMENTS} = ${toSqlValue(dto[fileComments])}`);
    if (dto[fileSpecies]?.length > 0) items.push(`${FILE_SPECIES} = ${toSqlValue(dto[fileSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysFilesUDto[] | Partial<SysFilesUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysFilesSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysFilesQDto | Partial<SysFilesQDto>): string => {
    if (!dto) return '';
    let where = SysFilesSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysFilesSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_FILES';
/**
 * 列名常量字段
 */
const FILE_ID = SysFilesColNameEnum.FILE_ID;
const FILE_BARCODE = SysFilesColNameEnum.FILE_BARCODE;
const FILE_RELATE_ID = SysFilesColNameEnum.FILE_RELATE_ID;
const FILE_NAME = SysFilesColNameEnum.FILE_NAME;
const FILE_NAME_EN = SysFilesColNameEnum.FILE_NAME_EN;
const FILE_PATH = SysFilesColNameEnum.FILE_PATH;
const FILE_CREATE_DATE = SysFilesColNameEnum.FILE_CREATE_DATE;
const FILE_UPDATE_DATE = SysFilesColNameEnum.FILE_UPDATE_DATE;
const FILE_IS_USED = SysFilesColNameEnum.FILE_IS_USED;
const FILE_OWNER = SysFilesColNameEnum.FILE_OWNER;
const FILE_SIZE = SysFilesColNameEnum.FILE_SIZE;
const FILE_GRANTS = SysFilesColNameEnum.FILE_GRANTS;
const FILE_CLASSES = SysFilesColNameEnum.FILE_CLASSES;
const FILE_STATE = SysFilesColNameEnum.FILE_STATE;
const FILE_TYPE = SysFilesColNameEnum.FILE_TYPE;
const FILE_COMMENTS = SysFilesColNameEnum.FILE_COMMENTS;
const FILE_SPECIES = SysFilesColNameEnum.FILE_SPECIES;
/**
 * 实体类属性名
 */
const fileId = SysFilesColPropEnum.fileId;
const fileBarcode = SysFilesColPropEnum.fileBarcode;
const fileRelateId = SysFilesColPropEnum.fileRelateId;
const fileName = SysFilesColPropEnum.fileName;
const fileNameEn = SysFilesColPropEnum.fileNameEn;
const filePath = SysFilesColPropEnum.filePath;
const fileCreateDate = SysFilesColPropEnum.fileCreateDate;
const fileUpdateDate = SysFilesColPropEnum.fileUpdateDate;
const fileIsUsed = SysFilesColPropEnum.fileIsUsed;
const fileOwner = SysFilesColPropEnum.fileOwner;
const fileSize = SysFilesColPropEnum.fileSize;
const fileGrants = SysFilesColPropEnum.fileGrants;
const fileClasses = SysFilesColPropEnum.fileClasses;
const fileState = SysFilesColPropEnum.fileState;
const fileType = SysFilesColPropEnum.fileType;
const fileComments = SysFilesColPropEnum.fileComments;
const fileSpecies = SysFilesColPropEnum.fileSpecies;
/**
 * 主键信息
 */
const PRIMER_KEY = SysFilesTable.PRIMER_KEY;
const primerKey = SysFilesTable.primerKey;
