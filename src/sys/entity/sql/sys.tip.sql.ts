import { SysTipQDto } from '../../dto/query/sys.tip.qdto';
import { SysTipCDto } from '../../dto/create/sys.tip.cdto';
import { SysTipUDto } from '../../dto/update/sys.tip.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysTipColNameEnum, SysTipColPropEnum, SysTipTable, SysTipColNames, SysTipColProps } from '../ddl/sys.tip.cols';
/**
 * SYS_TIP--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysTipSQL
 */
export const SysTipSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let cols = SysTipSQL.COL_MAPPER_SQL(dto);
    let where = SysTipSQL.WHERE_SQL(dto);
    let group = SysTipSQL.GROUP_BY_SQL(dto);
    let order = SysTipSQL.ORDER_BY_SQL(dto);
    let limit = SysTipSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let where = SysTipSQL.WHERE_SQL(dto);
    let group = SysTipSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let cols = SysTipSQL.COL_MAPPER_SQL(dto);
    let group = SysTipSQL.GROUP_BY_SQL(dto);
    let order = SysTipSQL.ORDER_BY_SQL(dto);
    let limit = SysTipSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysTipSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysTipSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysTipSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let where = SysTipSQL.WHERE_SQL(dto);
    let group = SysTipSQL.GROUP_BY_SQL(dto);
    let order = SysTipSQL.ORDER_BY_SQL(dto);
    let limit = SysTipSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysTipColNames, SysTipColProps);
    if (!cols || cols.length == 0) cols = SysTipColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[tipId]?.length > 0) items.push(` ${TIP_ID} = '${dto[tipId]}'`);
    if (dto['ntipId']?.length > 0) items.push(` ${TIP_ID} != '${dto['ntipId']}'`);
    if (dto[tipCount]) items.push(` ${TIP_COUNT} = '${dto[tipCount]}'`);
    if (dto['ntipCount']) items.push(` ${TIP_COUNT} != '${dto['ntipCount']}'`);
    if (dto[tipContents]?.length > 0) items.push(` ${TIP_CONTENTS} = '${dto[tipContents]}'`);
    if (dto['ntipContents']?.length > 0) items.push(` ${TIP_CONTENTS} != '${dto['ntipContents']}'`);
    if (dto[tipManager]?.length > 0) items.push(` ${TIP_MANAGER} = '${dto[tipManager]}'`);
    if (dto['ntipManager']?.length > 0) items.push(` ${TIP_MANAGER} != '${dto['ntipManager']}'`);
    if (dto[tipAudience]?.length > 0) items.push(` ${TIP_AUDIENCE} = '${dto[tipAudience]}'`);
    if (dto['ntipAudience']?.length > 0) items.push(` ${TIP_AUDIENCE} != '${dto['ntipAudience']}'`);
    if (dto[tipCreateDate]) items.push(`${TIP_CREATE_DATE} is not null and date_format(${TIP_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[tipCreateDate])}`);
    if (dto['ntipCreateDate']) items.push(`${TIP_CREATE_DATE} is not null and date_format(${TIP_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ntipCreateDate'])}`);
    if (dto[tipUpdateDate]) items.push(`${TIP_UPDATE_DATE} is not null and date_format(${TIP_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[tipUpdateDate])}`);
    if (dto['ntipUpdateDate']) items.push(`${TIP_UPDATE_DATE} is not null and date_format(${TIP_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ntipUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //TIP_ID--字符串字段
    if (dto['tipIdList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_ID, <string[]>dto['tipIdList']));
    if (dto['ntipIdList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_ID, <string[]>dto['ntipIdList'], true));
    if (dto['tipIdLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_ID, dto['tipIdLike'])); //For MysSQL
    if (dto['ntipIdLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_ID, dto['ntipIdLike'], true)); //For MysSQL
    if (dto['tipIdLikeList']?.length > 0) items.push(SysTipSQL.LIKE_LIST_SQL(TIP_ID, <string[]>dto['tipIdLikeList'])); //For MysSQL
    //TIP_COUNT--数字字段
    if (dto['tipCountList']) items.push(SysTipSQL.IN_SQL(TIP_COUNT, dto['tipCountList']));
    if (dto['ntipCountList']) items.push(SysTipSQL.IN_SQL(TIP_COUNT, dto['ntipCountList'], true));
    //TIP_CONTENTS--字符串字段
    if (dto['tipContentsList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_CONTENTS, <string[]>dto['tipContentsList']));
    if (dto['ntipContentsList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_CONTENTS, <string[]>dto['ntipContentsList'], true));
    if (dto['tipContentsLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_CONTENTS, dto['tipContentsLike'])); //For MysSQL
    if (dto['ntipContentsLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_CONTENTS, dto['ntipContentsLike'], true)); //For MysSQL
    if (dto['tipContentsLikeList']?.length > 0) items.push(SysTipSQL.LIKE_LIST_SQL(TIP_CONTENTS, <string[]>dto['tipContentsLikeList'])); //For MysSQL
    //TIP_MANAGER--字符串字段
    if (dto['tipManagerList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_MANAGER, <string[]>dto['tipManagerList']));
    if (dto['ntipManagerList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_MANAGER, <string[]>dto['ntipManagerList'], true));
    if (dto['tipManagerLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_MANAGER, dto['tipManagerLike'])); //For MysSQL
    if (dto['ntipManagerLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_MANAGER, dto['ntipManagerLike'], true)); //For MysSQL
    if (dto['tipManagerLikeList']?.length > 0) items.push(SysTipSQL.LIKE_LIST_SQL(TIP_MANAGER, <string[]>dto['tipManagerLikeList'])); //For MysSQL
    //TIP_AUDIENCE--字符串字段
    if (dto['tipAudienceList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_AUDIENCE, <string[]>dto['tipAudienceList']));
    if (dto['ntipAudienceList']?.length > 0) items.push(SysTipSQL.IN_SQL(TIP_AUDIENCE, <string[]>dto['ntipAudienceList'], true));
    if (dto['tipAudienceLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_AUDIENCE, dto['tipAudienceLike'])); //For MysSQL
    if (dto['ntipAudienceLike']?.length > 0) items.push(SysTipSQL.LIKE_SQL(TIP_AUDIENCE, dto['ntipAudienceLike'], true)); //For MysSQL
    if (dto['tipAudienceLikeList']?.length > 0) items.push(SysTipSQL.LIKE_LIST_SQL(TIP_AUDIENCE, <string[]>dto['tipAudienceLikeList'])); //For MysSQL
    //TIP_CREATE_DATE--日期字段
    if (dto[tipCreateDate]) items.push(`${TIP_CREATE_DATE} is not null and date_format(${TIP_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sTipCreateDate'])}`);
    if (dto[tipCreateDate]) items.push(`${TIP_CREATE_DATE} is not null and date_format(${TIP_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eTipCreateDate'])}`);
    //TIP_UPDATE_DATE--日期字段
    if (dto[tipUpdateDate]) items.push(`${TIP_UPDATE_DATE} is not null and date_format(${TIP_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sTipUpdateDate'])}`);
    if (dto[tipUpdateDate]) items.push(`${TIP_UPDATE_DATE} is not null and date_format(${TIP_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eTipUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysTipSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysTipCDto | Partial<SysTipCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(TIP_ID);
    cols.push(TIP_COUNT);
    cols.push(TIP_CONTENTS);
    cols.push(TIP_MANAGER);
    cols.push(TIP_AUDIENCE);
    cols.push(TIP_CREATE_DATE);
    cols.push(TIP_UPDATE_DATE);
    values.push(toSqlValue(dto[tipId]));
    values.push(toSqlValue(dto[tipCount]));
    values.push(toSqlValue(dto[tipContents]));
    values.push(toSqlValue(dto[tipManager]));
    values.push(toSqlValue(dto[tipAudience]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysTipCDto[] | Partial<SysTipCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(TIP_ID);
    cols.push(TIP_COUNT);
    cols.push(TIP_CONTENTS);
    cols.push(TIP_MANAGER);
    cols.push(TIP_AUDIENCE);
    cols.push(TIP_CREATE_DATE);
    cols.push(TIP_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[tipId]));
      values.push(toSqlValue(dto[tipCount]));
      values.push(toSqlValue(dto[tipContents]));
      values.push(toSqlValue(dto[tipManager]));
      values.push(toSqlValue(dto[tipAudience]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysTipUDto | Partial<SysTipUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysTipColNames, SysTipColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${TIP_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${TIP_COUNT} = ${toSqlValue(dto[tipCount])}`);
    items.push(`${TIP_CONTENTS} = ${toSqlValue(dto[tipContents])}`);
    items.push(`${TIP_MANAGER} = ${toSqlValue(dto[tipManager])}`);
    items.push(`${TIP_AUDIENCE} = ${toSqlValue(dto[tipAudience])}`);
    items.push(`${TIP_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysTipUDto | Partial<SysTipUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysTipColNames, SysTipColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${TIP_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysTipSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${TIP_COUNT} = ${toSqlValue(dto[tipCount])}`);
    items.push(`${TIP_CONTENTS} = ${toSqlValue(dto[tipContents])}`);
    items.push(`${TIP_MANAGER} = ${toSqlValue(dto[tipManager])}`);
    items.push(`${TIP_AUDIENCE} = ${toSqlValue(dto[tipAudience])}`);
    items.push(`${TIP_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysTipSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysTipUDto[] | Partial<SysTipUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysTipSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysTipUDto | Partial<SysTipUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[tipCount]) items.push(`${TIP_COUNT} = ${toSqlValue(dto[tipCount])}`);
    if (dto[tipContents]?.length > 0) items.push(`${TIP_CONTENTS} = ${toSqlValue(dto[tipContents])}`);
    if (dto[tipManager]?.length > 0) items.push(`${TIP_MANAGER} = ${toSqlValue(dto[tipManager])}`);
    if (dto[tipAudience]?.length > 0) items.push(`${TIP_AUDIENCE} = ${toSqlValue(dto[tipAudience])}`);
    if (dto[tipUpdateDate]) items.push(`${TIP_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysTipUDto[] | Partial<SysTipUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysTipSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysTipQDto | Partial<SysTipQDto>): string => {
    if (!dto) return '';
    let where = SysTipSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysTipSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_TIP';
/**
 * 列名常量字段
 */
const TIP_ID = SysTipColNameEnum.TIP_ID;
const TIP_COUNT = SysTipColNameEnum.TIP_COUNT;
const TIP_CONTENTS = SysTipColNameEnum.TIP_CONTENTS;
const TIP_MANAGER = SysTipColNameEnum.TIP_MANAGER;
const TIP_AUDIENCE = SysTipColNameEnum.TIP_AUDIENCE;
const TIP_CREATE_DATE = SysTipColNameEnum.TIP_CREATE_DATE;
const TIP_UPDATE_DATE = SysTipColNameEnum.TIP_UPDATE_DATE;
/**
 * 实体类属性名
 */
const tipId = SysTipColPropEnum.tipId;
const tipCount = SysTipColPropEnum.tipCount;
const tipContents = SysTipColPropEnum.tipContents;
const tipManager = SysTipColPropEnum.tipManager;
const tipAudience = SysTipColPropEnum.tipAudience;
const tipCreateDate = SysTipColPropEnum.tipCreateDate;
const tipUpdateDate = SysTipColPropEnum.tipUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysTipTable.PRIMER_KEY;
const primerKey = SysTipTable.primerKey;
