import { SysUserQDto } from '../../dto/query/sys.user.qdto';
import { SysUserCDto } from '../../dto/create/sys.user.cdto';
import { SysUserUDto } from '../../dto/update/sys.user.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysUserColNameEnum, SysUserColPropEnum, SysUserTable, SysUserColNames, SysUserColProps } from '../ddl/sys.user.cols';
/**
 * SYS_USER--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysUserSQL
 */
export const SysUserSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let cols = SysUserSQL.COL_MAPPER_SQL(dto);
    let where = SysUserSQL.WHERE_SQL(dto);
    let group = SysUserSQL.GROUP_BY_SQL(dto);
    let order = SysUserSQL.ORDER_BY_SQL(dto);
    let limit = SysUserSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let where = SysUserSQL.WHERE_SQL(dto);
    let group = SysUserSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let cols = SysUserSQL.COL_MAPPER_SQL(dto);
    let group = SysUserSQL.GROUP_BY_SQL(dto);
    let order = SysUserSQL.ORDER_BY_SQL(dto);
    let limit = SysUserSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysUserSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysUserSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysUserSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let where = SysUserSQL.WHERE_SQL(dto);
    let group = SysUserSQL.GROUP_BY_SQL(dto);
    let order = SysUserSQL.ORDER_BY_SQL(dto);
    let limit = SysUserSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysUserColNames, SysUserColProps);
    if (!cols || cols.length == 0) cols = SysUserColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[userId]?.length > 0) items.push(` ${USER_ID} = '${dto[userId]}'`);
    if (dto['nuserId']?.length > 0) items.push(` ${USER_ID} != '${dto['nuserId']}'`);
    if (dto[userLoginName]?.length > 0) items.push(` ${USER_LOGIN_NAME} = '${dto[userLoginName]}'`);
    if (dto['nuserLoginName']?.length > 0) items.push(` ${USER_LOGIN_NAME} != '${dto['nuserLoginName']}'`);
    if (dto[userPassword]?.length > 0) items.push(` ${USER_PASSWORD} = '${dto[userPassword]}'`);
    if (dto['nuserPassword']?.length > 0) items.push(` ${USER_PASSWORD} != '${dto['nuserPassword']}'`);
    if (dto[userPasswordPrivate]?.length > 0) items.push(` ${USER_PASSWORD_PRIVATE} = '${dto[userPasswordPrivate]}'`);
    if (dto['nuserPasswordPrivate']?.length > 0) items.push(` ${USER_PASSWORD_PRIVATE} != '${dto['nuserPasswordPrivate']}'`);
    if (dto[userNameAbbr]?.length > 0) items.push(` ${USER_NAME_ABBR} = '${dto[userNameAbbr]}'`);
    if (dto['nuserNameAbbr']?.length > 0) items.push(` ${USER_NAME_ABBR} != '${dto['nuserNameAbbr']}'`);
    if (dto[userName]?.length > 0) items.push(` ${USER_NAME} = '${dto[userName]}'`);
    if (dto['nuserName']?.length > 0) items.push(` ${USER_NAME} != '${dto['nuserName']}'`);
    if (dto[userNameEn]?.length > 0) items.push(` ${USER_NAME_EN} = '${dto[userNameEn]}'`);
    if (dto['nuserNameEn']?.length > 0) items.push(` ${USER_NAME_EN} != '${dto['nuserNameEn']}'`);
    if (dto[userSex]?.length > 0) items.push(` ${USER_SEX} = '${dto[userSex]}'`);
    if (dto['nuserSex']?.length > 0) items.push(` ${USER_SEX} != '${dto['nuserSex']}'`);
    if (dto[userPhone]?.length > 0) items.push(` ${USER_PHONE} = '${dto[userPhone]}'`);
    if (dto['nuserPhone']?.length > 0) items.push(` ${USER_PHONE} != '${dto['nuserPhone']}'`);
    if (dto[userEmail]?.length > 0) items.push(` ${USER_EMAIL} = '${dto[userEmail]}'`);
    if (dto['nuserEmail']?.length > 0) items.push(` ${USER_EMAIL} != '${dto['nuserEmail']}'`);
    if (dto[userImageUrl]?.length > 0) items.push(` ${USER_IMAGE_URL} = '${dto[userImageUrl]}'`);
    if (dto['nuserImageUrl']?.length > 0) items.push(` ${USER_IMAGE_URL} != '${dto['nuserImageUrl']}'`);
    if (dto[userOrder]) items.push(` ${USER_ORDER} = '${dto[userOrder]}'`);
    if (dto['nuserOrder']) items.push(` ${USER_ORDER} != '${dto['nuserOrder']}'`);
    if (dto[userState]?.length > 0) items.push(` ${USER_STATE} = '${dto[userState]}'`);
    if (dto['nuserState']?.length > 0) items.push(` ${USER_STATE} != '${dto['nuserState']}'`);
    if (dto[userCode]?.length > 0) items.push(` ${USER_CODE} = '${dto[userCode]}'`);
    if (dto['nuserCode']?.length > 0) items.push(` ${USER_CODE} != '${dto['nuserCode']}'`);
    if (dto[userCreateDate]) items.push(`${USER_CREATE_DATE} is not null and date_format(${USER_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[userCreateDate])}`);
    if (dto['nuserCreateDate']) items.push(`${USER_CREATE_DATE} is not null and date_format(${USER_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nuserCreateDate'])}`);
    if (dto[userUpdateDate]) items.push(`${USER_UPDATE_DATE} is not null and date_format(${USER_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[userUpdateDate])}`);
    if (dto['nuserUpdateDate']) items.push(`${USER_UPDATE_DATE} is not null and date_format(${USER_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nuserUpdateDate'])}`);
    if (dto[userLastLoginTime]) items.push(`${USER_LAST_LOGIN_TIME} is not null and date_format(${USER_LAST_LOGIN_TIME},'%Y-%m-%d') = ${toSqlValue(dto[userLastLoginTime])}`);
    if (dto['nuserLastLoginTime']) items.push(`${USER_LAST_LOGIN_TIME} is not null and date_format(${USER_LAST_LOGIN_TIME},'%Y-%m-%d') != ${toSqlValue(dto['nuserLastLoginTime'])}`);
    if (dto[userLastLoginIp]?.length > 0) items.push(` ${USER_LAST_LOGIN_IP} = '${dto[userLastLoginIp]}'`);
    if (dto['nuserLastLoginIp']?.length > 0) items.push(` ${USER_LAST_LOGIN_IP} != '${dto['nuserLastLoginIp']}'`);
    if (dto[userUnit]?.length > 0) items.push(` ${USER_UNIT} = '${dto[userUnit]}'`);
    if (dto['nuserUnit']?.length > 0) items.push(` ${USER_UNIT} != '${dto['nuserUnit']}'`);
    if (dto[userJobTitle]?.length > 0) items.push(` ${USER_JOB_TITLE} = '${dto[userJobTitle]}'`);
    if (dto['nuserJobTitle']?.length > 0) items.push(` ${USER_JOB_TITLE} != '${dto['nuserJobTitle']}'`);
    if (dto[userAddr]?.length > 0) items.push(` ${USER_ADDR} = '${dto[userAddr]}'`);
    if (dto['nuserAddr']?.length > 0) items.push(` ${USER_ADDR} != '${dto['nuserAddr']}'`);
    if (dto[userPostcode]?.length > 0) items.push(` ${USER_POSTCODE} = '${dto[userPostcode]}'`);
    if (dto['nuserPostcode']?.length > 0) items.push(` ${USER_POSTCODE} != '${dto['nuserPostcode']}'`);
    if (dto[userSamSpecies]?.length > 0) items.push(` ${USER_SAM_SPECIES} = '${dto[userSamSpecies]}'`);
    if (dto['nuserSamSpecies']?.length > 0) items.push(` ${USER_SAM_SPECIES} != '${dto['nuserSamSpecies']}'`);
    if (dto[userCurrSpecies]?.length > 0) items.push(` ${USER_CURR_SPECIES} = '${dto[userCurrSpecies]}'`);
    if (dto['nuserCurrSpecies']?.length > 0) items.push(` ${USER_CURR_SPECIES} != '${dto['nuserCurrSpecies']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //USER_ID--字符串字段
    if (dto['userIdList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_ID, <string[]>dto['userIdList']));
    if (dto['nuserIdList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_ID, <string[]>dto['nuserIdList'], true));
    if (dto['userIdLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_ID, dto['userIdLike'])); //For MysSQL
    if (dto['nuserIdLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_ID, dto['nuserIdLike'], true)); //For MysSQL
    if (dto['userIdLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_ID, <string[]>dto['userIdLikeList'])); //For MysSQL
    //USER_LOGIN_NAME--字符串字段
    if (dto['userLoginNameList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_LOGIN_NAME, <string[]>dto['userLoginNameList']));
    if (dto['nuserLoginNameList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_LOGIN_NAME, <string[]>dto['nuserLoginNameList'], true));
    if (dto['userLoginNameLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_LOGIN_NAME, dto['userLoginNameLike'])); //For MysSQL
    if (dto['nuserLoginNameLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_LOGIN_NAME, dto['nuserLoginNameLike'], true)); //For MysSQL
    if (dto['userLoginNameLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_LOGIN_NAME, <string[]>dto['userLoginNameLikeList'])); //For MysSQL
    //USER_PASSWORD--字符串字段
    if (dto['userPasswordList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_PASSWORD, <string[]>dto['userPasswordList']));
    if (dto['nuserPasswordList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_PASSWORD, <string[]>dto['nuserPasswordList'], true));
    if (dto['userPasswordLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_PASSWORD, dto['userPasswordLike'])); //For MysSQL
    if (dto['nuserPasswordLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_PASSWORD, dto['nuserPasswordLike'], true)); //For MysSQL
    if (dto['userPasswordLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_PASSWORD, <string[]>dto['userPasswordLikeList'])); //For MysSQL
    //USER_PASSWORD_PRIVATE--字符串字段
    if (dto['userPasswordPrivateList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_PASSWORD_PRIVATE, <string[]>dto['userPasswordPrivateList']));
    if (dto['nuserPasswordPrivateList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_PASSWORD_PRIVATE, <string[]>dto['nuserPasswordPrivateList'], true));
    if (dto['userPasswordPrivateLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_PASSWORD_PRIVATE, dto['userPasswordPrivateLike'])); //For MysSQL
    if (dto['nuserPasswordPrivateLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_PASSWORD_PRIVATE, dto['nuserPasswordPrivateLike'], true)); //For MysSQL
    if (dto['userPasswordPrivateLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_PASSWORD_PRIVATE, <string[]>dto['userPasswordPrivateLikeList'])); //For MysSQL
    //USER_NAME_ABBR--字符串字段
    if (dto['userNameAbbrList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_NAME_ABBR, <string[]>dto['userNameAbbrList']));
    if (dto['nuserNameAbbrList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_NAME_ABBR, <string[]>dto['nuserNameAbbrList'], true));
    if (dto['userNameAbbrLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_NAME_ABBR, dto['userNameAbbrLike'])); //For MysSQL
    if (dto['nuserNameAbbrLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_NAME_ABBR, dto['nuserNameAbbrLike'], true)); //For MysSQL
    if (dto['userNameAbbrLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_NAME_ABBR, <string[]>dto['userNameAbbrLikeList'])); //For MysSQL
    //USER_NAME--字符串字段
    if (dto['userNameList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_NAME, <string[]>dto['userNameList']));
    if (dto['nuserNameList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_NAME, <string[]>dto['nuserNameList'], true));
    if (dto['userNameLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_NAME, dto['userNameLike'])); //For MysSQL
    if (dto['nuserNameLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_NAME, dto['nuserNameLike'], true)); //For MysSQL
    if (dto['userNameLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_NAME, <string[]>dto['userNameLikeList'])); //For MysSQL
    //USER_NAME_EN--字符串字段
    if (dto['userNameEnList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_NAME_EN, <string[]>dto['userNameEnList']));
    if (dto['nuserNameEnList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_NAME_EN, <string[]>dto['nuserNameEnList'], true));
    if (dto['userNameEnLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_NAME_EN, dto['userNameEnLike'])); //For MysSQL
    if (dto['nuserNameEnLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_NAME_EN, dto['nuserNameEnLike'], true)); //For MysSQL
    if (dto['userNameEnLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_NAME_EN, <string[]>dto['userNameEnLikeList'])); //For MysSQL
    //USER_SEX--字符串字段
    if (dto['userSexList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_SEX, <string[]>dto['userSexList']));
    if (dto['nuserSexList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_SEX, <string[]>dto['nuserSexList'], true));
    if (dto['userSexLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_SEX, dto['userSexLike'])); //For MysSQL
    if (dto['nuserSexLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_SEX, dto['nuserSexLike'], true)); //For MysSQL
    if (dto['userSexLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_SEX, <string[]>dto['userSexLikeList'])); //For MysSQL
    //USER_PHONE--字符串字段
    if (dto['userPhoneList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_PHONE, <string[]>dto['userPhoneList']));
    if (dto['nuserPhoneList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_PHONE, <string[]>dto['nuserPhoneList'], true));
    if (dto['userPhoneLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_PHONE, dto['userPhoneLike'])); //For MysSQL
    if (dto['nuserPhoneLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_PHONE, dto['nuserPhoneLike'], true)); //For MysSQL
    if (dto['userPhoneLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_PHONE, <string[]>dto['userPhoneLikeList'])); //For MysSQL
    //USER_EMAIL--字符串字段
    if (dto['userEmailList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_EMAIL, <string[]>dto['userEmailList']));
    if (dto['nuserEmailList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_EMAIL, <string[]>dto['nuserEmailList'], true));
    if (dto['userEmailLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_EMAIL, dto['userEmailLike'])); //For MysSQL
    if (dto['nuserEmailLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_EMAIL, dto['nuserEmailLike'], true)); //For MysSQL
    if (dto['userEmailLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_EMAIL, <string[]>dto['userEmailLikeList'])); //For MysSQL
    //USER_IMAGE_URL--字符串字段
    if (dto['userImageUrlList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_IMAGE_URL, <string[]>dto['userImageUrlList']));
    if (dto['nuserImageUrlList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_IMAGE_URL, <string[]>dto['nuserImageUrlList'], true));
    if (dto['userImageUrlLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_IMAGE_URL, dto['userImageUrlLike'])); //For MysSQL
    if (dto['nuserImageUrlLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_IMAGE_URL, dto['nuserImageUrlLike'], true)); //For MysSQL
    if (dto['userImageUrlLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_IMAGE_URL, <string[]>dto['userImageUrlLikeList'])); //For MysSQL
    //USER_ORDER--数字字段
    if (dto['userOrderList']) items.push(SysUserSQL.IN_SQL(USER_ORDER, dto['userOrderList']));
    if (dto['nuserOrderList']) items.push(SysUserSQL.IN_SQL(USER_ORDER, dto['nuserOrderList'], true));
    //USER_STATE--字符串字段
    if (dto['userStateList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_STATE, <string[]>dto['userStateList']));
    if (dto['nuserStateList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_STATE, <string[]>dto['nuserStateList'], true));
    if (dto['userStateLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_STATE, dto['userStateLike'])); //For MysSQL
    if (dto['nuserStateLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_STATE, dto['nuserStateLike'], true)); //For MysSQL
    if (dto['userStateLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_STATE, <string[]>dto['userStateLikeList'])); //For MysSQL
    //USER_CODE--字符串字段
    if (dto['userCodeList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_CODE, <string[]>dto['userCodeList']));
    if (dto['nuserCodeList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_CODE, <string[]>dto['nuserCodeList'], true));
    if (dto['userCodeLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_CODE, dto['userCodeLike'])); //For MysSQL
    if (dto['nuserCodeLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_CODE, dto['nuserCodeLike'], true)); //For MysSQL
    if (dto['userCodeLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_CODE, <string[]>dto['userCodeLikeList'])); //For MysSQL
    //USER_CREATE_DATE--日期字段
    if (dto[userCreateDate]) items.push(`${USER_CREATE_DATE} is not null and date_format(${USER_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUserCreateDate'])}`);
    if (dto[userCreateDate]) items.push(`${USER_CREATE_DATE} is not null and date_format(${USER_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUserCreateDate'])}`);
    //USER_UPDATE_DATE--日期字段
    if (dto[userUpdateDate]) items.push(`${USER_UPDATE_DATE} is not null and date_format(${USER_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUserUpdateDate'])}`);
    if (dto[userUpdateDate]) items.push(`${USER_UPDATE_DATE} is not null and date_format(${USER_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUserUpdateDate'])}`);
    //USER_LAST_LOGIN_TIME--日期字段
    if (dto[userLastLoginTime]) items.push(`${USER_LAST_LOGIN_TIME} is not null and date_format(${USER_LAST_LOGIN_TIME},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUserLastLoginTime'])}`);
    if (dto[userLastLoginTime]) items.push(`${USER_LAST_LOGIN_TIME} is not null and date_format(${USER_LAST_LOGIN_TIME},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUserLastLoginTime'])}`);
    //USER_LAST_LOGIN_IP--字符串字段
    if (dto['userLastLoginIpList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_LAST_LOGIN_IP, <string[]>dto['userLastLoginIpList']));
    if (dto['nuserLastLoginIpList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_LAST_LOGIN_IP, <string[]>dto['nuserLastLoginIpList'], true));
    if (dto['userLastLoginIpLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_LAST_LOGIN_IP, dto['userLastLoginIpLike'])); //For MysSQL
    if (dto['nuserLastLoginIpLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_LAST_LOGIN_IP, dto['nuserLastLoginIpLike'], true)); //For MysSQL
    if (dto['userLastLoginIpLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_LAST_LOGIN_IP, <string[]>dto['userLastLoginIpLikeList'])); //For MysSQL
    //USER_UNIT--字符串字段
    if (dto['userUnitList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_UNIT, <string[]>dto['userUnitList']));
    if (dto['nuserUnitList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_UNIT, <string[]>dto['nuserUnitList'], true));
    if (dto['userUnitLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_UNIT, dto['userUnitLike'])); //For MysSQL
    if (dto['nuserUnitLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_UNIT, dto['nuserUnitLike'], true)); //For MysSQL
    if (dto['userUnitLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_UNIT, <string[]>dto['userUnitLikeList'])); //For MysSQL
    //USER_JOB_TITLE--字符串字段
    if (dto['userJobTitleList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_JOB_TITLE, <string[]>dto['userJobTitleList']));
    if (dto['nuserJobTitleList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_JOB_TITLE, <string[]>dto['nuserJobTitleList'], true));
    if (dto['userJobTitleLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_JOB_TITLE, dto['userJobTitleLike'])); //For MysSQL
    if (dto['nuserJobTitleLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_JOB_TITLE, dto['nuserJobTitleLike'], true)); //For MysSQL
    if (dto['userJobTitleLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_JOB_TITLE, <string[]>dto['userJobTitleLikeList'])); //For MysSQL
    //USER_ADDR--字符串字段
    if (dto['userAddrList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_ADDR, <string[]>dto['userAddrList']));
    if (dto['nuserAddrList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_ADDR, <string[]>dto['nuserAddrList'], true));
    if (dto['userAddrLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_ADDR, dto['userAddrLike'])); //For MysSQL
    if (dto['nuserAddrLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_ADDR, dto['nuserAddrLike'], true)); //For MysSQL
    if (dto['userAddrLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_ADDR, <string[]>dto['userAddrLikeList'])); //For MysSQL
    //USER_POSTCODE--字符串字段
    if (dto['userPostcodeList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_POSTCODE, <string[]>dto['userPostcodeList']));
    if (dto['nuserPostcodeList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_POSTCODE, <string[]>dto['nuserPostcodeList'], true));
    if (dto['userPostcodeLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_POSTCODE, dto['userPostcodeLike'])); //For MysSQL
    if (dto['nuserPostcodeLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_POSTCODE, dto['nuserPostcodeLike'], true)); //For MysSQL
    if (dto['userPostcodeLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_POSTCODE, <string[]>dto['userPostcodeLikeList'])); //For MysSQL
    //USER_SAM_SPECIES--字符串字段
    if (dto['userSamSpeciesList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_SAM_SPECIES, <string[]>dto['userSamSpeciesList']));
    if (dto['nuserSamSpeciesList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_SAM_SPECIES, <string[]>dto['nuserSamSpeciesList'], true));
    if (dto['userSamSpeciesLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_SAM_SPECIES, dto['userSamSpeciesLike'])); //For MysSQL
    if (dto['nuserSamSpeciesLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_SAM_SPECIES, dto['nuserSamSpeciesLike'], true)); //For MysSQL
    if (dto['userSamSpeciesLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_SAM_SPECIES, <string[]>dto['userSamSpeciesLikeList'])); //For MysSQL
    //USER_CURR_SPECIES--字符串字段
    if (dto['userCurrSpeciesList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_CURR_SPECIES, <string[]>dto['userCurrSpeciesList']));
    if (dto['nuserCurrSpeciesList']?.length > 0) items.push(SysUserSQL.IN_SQL(USER_CURR_SPECIES, <string[]>dto['nuserCurrSpeciesList'], true));
    if (dto['userCurrSpeciesLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_CURR_SPECIES, dto['userCurrSpeciesLike'])); //For MysSQL
    if (dto['nuserCurrSpeciesLike']?.length > 0) items.push(SysUserSQL.LIKE_SQL(USER_CURR_SPECIES, dto['nuserCurrSpeciesLike'], true)); //For MysSQL
    if (dto['userCurrSpeciesLikeList']?.length > 0) items.push(SysUserSQL.LIKE_LIST_SQL(USER_CURR_SPECIES, <string[]>dto['userCurrSpeciesLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysUserSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysUserCDto | Partial<SysUserCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(USER_ID);
    cols.push(USER_LOGIN_NAME);
    cols.push(USER_PASSWORD);
    cols.push(USER_PASSWORD_PRIVATE);
    cols.push(USER_NAME_ABBR);
    cols.push(USER_NAME);
    cols.push(USER_NAME_EN);
    cols.push(USER_SEX);
    cols.push(USER_PHONE);
    cols.push(USER_EMAIL);
    cols.push(USER_IMAGE_URL);
    cols.push(USER_ORDER);
    cols.push(USER_STATE);
    cols.push(USER_CODE);
    cols.push(USER_CREATE_DATE);
    cols.push(USER_UPDATE_DATE);
    cols.push(USER_LAST_LOGIN_TIME);
    cols.push(USER_LAST_LOGIN_IP);
    cols.push(USER_UNIT);
    cols.push(USER_JOB_TITLE);
    cols.push(USER_ADDR);
    cols.push(USER_POSTCODE);
    cols.push(USER_SAM_SPECIES);
    cols.push(USER_CURR_SPECIES);
    values.push(toSqlValue(dto[userId]));
    values.push(toSqlValue(dto[userLoginName]));
    values.push(toSqlValue(dto[userPassword]));
    values.push(toSqlValue(dto[userPasswordPrivate]));
    values.push(toSqlValue(dto[userNameAbbr]));
    values.push(toSqlValue(dto[userName]));
    values.push(toSqlValue(dto[userNameEn]));
    values.push(toSqlValue(dto[userSex]));
    values.push(toSqlValue(dto[userPhone]));
    values.push(toSqlValue(dto[userEmail]));
    values.push(toSqlValue(dto[userImageUrl]));
    values.push(toSqlValue(dto[userOrder]));
    values.push(toSqlValue(dto[userState]));
    values.push(toSqlValue(dto[userCode]));
    values.push('NOW()');
    values.push('NOW()');
    values.push('NOW()');
    values.push(toSqlValue(dto[userLastLoginIp]));
    values.push(toSqlValue(dto[userUnit]));
    values.push(toSqlValue(dto[userJobTitle]));
    values.push(toSqlValue(dto[userAddr]));
    values.push(toSqlValue(dto[userPostcode]));
    values.push(toSqlValue(dto[userSamSpecies]));
    values.push(toSqlValue(dto[userCurrSpecies]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysUserCDto[] | Partial<SysUserCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(USER_ID);
    cols.push(USER_LOGIN_NAME);
    cols.push(USER_PASSWORD);
    cols.push(USER_PASSWORD_PRIVATE);
    cols.push(USER_NAME_ABBR);
    cols.push(USER_NAME);
    cols.push(USER_NAME_EN);
    cols.push(USER_SEX);
    cols.push(USER_PHONE);
    cols.push(USER_EMAIL);
    cols.push(USER_IMAGE_URL);
    cols.push(USER_ORDER);
    cols.push(USER_STATE);
    cols.push(USER_CODE);
    cols.push(USER_CREATE_DATE);
    cols.push(USER_UPDATE_DATE);
    cols.push(USER_LAST_LOGIN_TIME);
    cols.push(USER_LAST_LOGIN_IP);
    cols.push(USER_UNIT);
    cols.push(USER_JOB_TITLE);
    cols.push(USER_ADDR);
    cols.push(USER_POSTCODE);
    cols.push(USER_SAM_SPECIES);
    cols.push(USER_CURR_SPECIES);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[userId]));
      values.push(toSqlValue(dto[userLoginName]));
      values.push(toSqlValue(dto[userPassword]));
      values.push(toSqlValue(dto[userPasswordPrivate]));
      values.push(toSqlValue(dto[userNameAbbr]));
      values.push(toSqlValue(dto[userName]));
      values.push(toSqlValue(dto[userNameEn]));
      values.push(toSqlValue(dto[userSex]));
      values.push(toSqlValue(dto[userPhone]));
      values.push(toSqlValue(dto[userEmail]));
      values.push(toSqlValue(dto[userImageUrl]));
      values.push(toSqlValue(dto[userOrder]));
      values.push(toSqlValue(dto[userState]));
      values.push(toSqlValue(dto[userCode]));
      values.push('NOW()');
      values.push('NOW()');
      values.push('NOW()');
      values.push(toSqlValue(dto[userLastLoginIp]));
      values.push(toSqlValue(dto[userUnit]));
      values.push(toSqlValue(dto[userJobTitle]));
      values.push(toSqlValue(dto[userAddr]));
      values.push(toSqlValue(dto[userPostcode]));
      values.push(toSqlValue(dto[userSamSpecies]));
      values.push(toSqlValue(dto[userCurrSpecies]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysUserUDto | Partial<SysUserUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserColNames, SysUserColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${USER_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${USER_LOGIN_NAME} = ${toSqlValue(dto[userLoginName])}`);
    items.push(`${USER_PASSWORD} = ${toSqlValue(dto[userPassword])}`);
    items.push(`${USER_PASSWORD_PRIVATE} = ${toSqlValue(dto[userPasswordPrivate])}`);
    items.push(`${USER_NAME_ABBR} = ${toSqlValue(dto[userNameAbbr])}`);
    items.push(`${USER_NAME} = ${toSqlValue(dto[userName])}`);
    items.push(`${USER_NAME_EN} = ${toSqlValue(dto[userNameEn])}`);
    items.push(`${USER_SEX} = ${toSqlValue(dto[userSex])}`);
    items.push(`${USER_PHONE} = ${toSqlValue(dto[userPhone])}`);
    items.push(`${USER_EMAIL} = ${toSqlValue(dto[userEmail])}`);
    items.push(`${USER_IMAGE_URL} = ${toSqlValue(dto[userImageUrl])}`);
    items.push(`${USER_ORDER} = ${toSqlValue(dto[userOrder])}`);
    items.push(`${USER_STATE} = ${toSqlValue(dto[userState])}`);
    items.push(`${USER_CODE} = ${toSqlValue(dto[userCode])}`);
    items.push(`${USER_UPDATE_DATE} = NOW()`);
    items.push(`${USER_LAST_LOGIN_IP} = ${toSqlValue(dto[userLastLoginIp])}`);
    items.push(`${USER_UNIT} = ${toSqlValue(dto[userUnit])}`);
    items.push(`${USER_JOB_TITLE} = ${toSqlValue(dto[userJobTitle])}`);
    items.push(`${USER_ADDR} = ${toSqlValue(dto[userAddr])}`);
    items.push(`${USER_POSTCODE} = ${toSqlValue(dto[userPostcode])}`);
    items.push(`${USER_SAM_SPECIES} = ${toSqlValue(dto[userSamSpecies])}`);
    items.push(`${USER_CURR_SPECIES} = ${toSqlValue(dto[userCurrSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysUserUDto | Partial<SysUserUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserColNames, SysUserColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${USER_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${USER_LOGIN_NAME} = ${toSqlValue(dto[userLoginName])}`);
    items.push(`${USER_PASSWORD} = ${toSqlValue(dto[userPassword])}`);
    items.push(`${USER_PASSWORD_PRIVATE} = ${toSqlValue(dto[userPasswordPrivate])}`);
    items.push(`${USER_NAME_ABBR} = ${toSqlValue(dto[userNameAbbr])}`);
    items.push(`${USER_NAME} = ${toSqlValue(dto[userName])}`);
    items.push(`${USER_NAME_EN} = ${toSqlValue(dto[userNameEn])}`);
    items.push(`${USER_SEX} = ${toSqlValue(dto[userSex])}`);
    items.push(`${USER_PHONE} = ${toSqlValue(dto[userPhone])}`);
    items.push(`${USER_EMAIL} = ${toSqlValue(dto[userEmail])}`);
    items.push(`${USER_IMAGE_URL} = ${toSqlValue(dto[userImageUrl])}`);
    items.push(`${USER_ORDER} = ${toSqlValue(dto[userOrder])}`);
    items.push(`${USER_STATE} = ${toSqlValue(dto[userState])}`);
    items.push(`${USER_CODE} = ${toSqlValue(dto[userCode])}`);
    items.push(`${USER_UPDATE_DATE} = NOW()`);
    items.push(`${USER_LAST_LOGIN_IP} = ${toSqlValue(dto[userLastLoginIp])}`);
    items.push(`${USER_UNIT} = ${toSqlValue(dto[userUnit])}`);
    items.push(`${USER_JOB_TITLE} = ${toSqlValue(dto[userJobTitle])}`);
    items.push(`${USER_ADDR} = ${toSqlValue(dto[userAddr])}`);
    items.push(`${USER_POSTCODE} = ${toSqlValue(dto[userPostcode])}`);
    items.push(`${USER_SAM_SPECIES} = ${toSqlValue(dto[userSamSpecies])}`);
    items.push(`${USER_CURR_SPECIES} = ${toSqlValue(dto[userCurrSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysUserUDto[] | Partial<SysUserUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysUserSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysUserUDto | Partial<SysUserUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[userLoginName]?.length > 0) items.push(`${USER_LOGIN_NAME} = ${toSqlValue(dto[userLoginName])}`);
    if (dto[userPassword]?.length > 0) items.push(`${USER_PASSWORD} = ${toSqlValue(dto[userPassword])}`);
    if (dto[userPasswordPrivate]?.length > 0) items.push(`${USER_PASSWORD_PRIVATE} = ${toSqlValue(dto[userPasswordPrivate])}`);
    if (dto[userNameAbbr]?.length > 0) items.push(`${USER_NAME_ABBR} = ${toSqlValue(dto[userNameAbbr])}`);
    if (dto[userName]?.length > 0) items.push(`${USER_NAME} = ${toSqlValue(dto[userName])}`);
    if (dto[userNameEn]?.length > 0) items.push(`${USER_NAME_EN} = ${toSqlValue(dto[userNameEn])}`);
    if (dto[userSex]?.length > 0) items.push(`${USER_SEX} = ${toSqlValue(dto[userSex])}`);
    if (dto[userPhone]?.length > 0) items.push(`${USER_PHONE} = ${toSqlValue(dto[userPhone])}`);
    if (dto[userEmail]?.length > 0) items.push(`${USER_EMAIL} = ${toSqlValue(dto[userEmail])}`);
    if (dto[userImageUrl]?.length > 0) items.push(`${USER_IMAGE_URL} = ${toSqlValue(dto[userImageUrl])}`);
    if (dto[userOrder]) items.push(`${USER_ORDER} = ${toSqlValue(dto[userOrder])}`);
    if (dto[userState]?.length > 0) items.push(`${USER_STATE} = ${toSqlValue(dto[userState])}`);
    if (dto[userCode]?.length > 0) items.push(`${USER_CODE} = ${toSqlValue(dto[userCode])}`);
    if (dto[userUpdateDate]) items.push(`${USER_UPDATE_DATE}= NOW()`);
    if (dto[userLastLoginIp]?.length > 0) items.push(`${USER_LAST_LOGIN_IP} = ${toSqlValue(dto[userLastLoginIp])}`);
    if (dto[userUnit]?.length > 0) items.push(`${USER_UNIT} = ${toSqlValue(dto[userUnit])}`);
    if (dto[userJobTitle]?.length > 0) items.push(`${USER_JOB_TITLE} = ${toSqlValue(dto[userJobTitle])}`);
    if (dto[userAddr]?.length > 0) items.push(`${USER_ADDR} = ${toSqlValue(dto[userAddr])}`);
    if (dto[userPostcode]?.length > 0) items.push(`${USER_POSTCODE} = ${toSqlValue(dto[userPostcode])}`);
    if (dto[userSamSpecies]?.length > 0) items.push(`${USER_SAM_SPECIES} = ${toSqlValue(dto[userSamSpecies])}`);
    if (dto[userCurrSpecies]?.length > 0) items.push(`${USER_CURR_SPECIES} = ${toSqlValue(dto[userCurrSpecies])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysUserUDto[] | Partial<SysUserUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysUserSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysUserQDto | Partial<SysUserQDto>): string => {
    if (!dto) return '';
    let where = SysUserSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysUserSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_USER';
/**
 * 列名常量字段
 */
const USER_ID = SysUserColNameEnum.USER_ID;
const USER_LOGIN_NAME = SysUserColNameEnum.USER_LOGIN_NAME;
const USER_PASSWORD = SysUserColNameEnum.USER_PASSWORD;
const USER_PASSWORD_PRIVATE = SysUserColNameEnum.USER_PASSWORD_PRIVATE;
const USER_NAME_ABBR = SysUserColNameEnum.USER_NAME_ABBR;
const USER_NAME = SysUserColNameEnum.USER_NAME;
const USER_NAME_EN = SysUserColNameEnum.USER_NAME_EN;
const USER_SEX = SysUserColNameEnum.USER_SEX;
const USER_PHONE = SysUserColNameEnum.USER_PHONE;
const USER_EMAIL = SysUserColNameEnum.USER_EMAIL;
const USER_IMAGE_URL = SysUserColNameEnum.USER_IMAGE_URL;
const USER_ORDER = SysUserColNameEnum.USER_ORDER;
const USER_STATE = SysUserColNameEnum.USER_STATE;
const USER_CODE = SysUserColNameEnum.USER_CODE;
const USER_CREATE_DATE = SysUserColNameEnum.USER_CREATE_DATE;
const USER_UPDATE_DATE = SysUserColNameEnum.USER_UPDATE_DATE;
const USER_LAST_LOGIN_TIME = SysUserColNameEnum.USER_LAST_LOGIN_TIME;
const USER_LAST_LOGIN_IP = SysUserColNameEnum.USER_LAST_LOGIN_IP;
const USER_UNIT = SysUserColNameEnum.USER_UNIT;
const USER_JOB_TITLE = SysUserColNameEnum.USER_JOB_TITLE;
const USER_ADDR = SysUserColNameEnum.USER_ADDR;
const USER_POSTCODE = SysUserColNameEnum.USER_POSTCODE;
const USER_SAM_SPECIES = SysUserColNameEnum.USER_SAM_SPECIES;
const USER_CURR_SPECIES = SysUserColNameEnum.USER_CURR_SPECIES;
/**
 * 实体类属性名
 */
const userId = SysUserColPropEnum.userId;
const userLoginName = SysUserColPropEnum.userLoginName;
const userPassword = SysUserColPropEnum.userPassword;
const userPasswordPrivate = SysUserColPropEnum.userPasswordPrivate;
const userNameAbbr = SysUserColPropEnum.userNameAbbr;
const userName = SysUserColPropEnum.userName;
const userNameEn = SysUserColPropEnum.userNameEn;
const userSex = SysUserColPropEnum.userSex;
const userPhone = SysUserColPropEnum.userPhone;
const userEmail = SysUserColPropEnum.userEmail;
const userImageUrl = SysUserColPropEnum.userImageUrl;
const userOrder = SysUserColPropEnum.userOrder;
const userState = SysUserColPropEnum.userState;
const userCode = SysUserColPropEnum.userCode;
const userCreateDate = SysUserColPropEnum.userCreateDate;
const userUpdateDate = SysUserColPropEnum.userUpdateDate;
const userLastLoginTime = SysUserColPropEnum.userLastLoginTime;
const userLastLoginIp = SysUserColPropEnum.userLastLoginIp;
const userUnit = SysUserColPropEnum.userUnit;
const userJobTitle = SysUserColPropEnum.userJobTitle;
const userAddr = SysUserColPropEnum.userAddr;
const userPostcode = SysUserColPropEnum.userPostcode;
const userSamSpecies = SysUserColPropEnum.userSamSpecies;
const userCurrSpecies = SysUserColPropEnum.userCurrSpecies;
/**
 * 主键信息
 */
const PRIMER_KEY = SysUserTable.PRIMER_KEY;
const primerKey = SysUserTable.primerKey;
