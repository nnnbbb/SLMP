import { SysContactQDto } from '../../dto/query/sys.contact.qdto';
import { SysContactCDto } from '../../dto/create/sys.contact.cdto';
import { SysContactUDto } from '../../dto/update/sys.contact.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysContactColNameEnum, SysContactColPropEnum, SysContactTable, SysContactColNames, SysContactColProps } from '../ddl/sys.contact.cols';
/**
 * SYS_CONTACT--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysContactSQL
 */
export const SysContactSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let cols = SysContactSQL.COL_MAPPER_SQL(dto);
    let where = SysContactSQL.WHERE_SQL(dto);
    let group = SysContactSQL.GROUP_BY_SQL(dto);
    let order = SysContactSQL.ORDER_BY_SQL(dto);
    let limit = SysContactSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let where = SysContactSQL.WHERE_SQL(dto);
    let group = SysContactSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let cols = SysContactSQL.COL_MAPPER_SQL(dto);
    let group = SysContactSQL.GROUP_BY_SQL(dto);
    let order = SysContactSQL.ORDER_BY_SQL(dto);
    let limit = SysContactSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysContactSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysContactSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysContactSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let where = SysContactSQL.WHERE_SQL(dto);
    let group = SysContactSQL.GROUP_BY_SQL(dto);
    let order = SysContactSQL.ORDER_BY_SQL(dto);
    let limit = SysContactSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysContactColNames, SysContactColProps);
    if (!cols || cols.length == 0) cols = SysContactColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[contactId]?.length > 0) items.push(` ${CONTACT_ID} = '${dto[contactId]}'`);
    if (dto['ncontactId']?.length > 0) items.push(` ${CONTACT_ID} != '${dto['ncontactId']}'`);
    if (dto[contactAuthor]?.length > 0) items.push(` ${CONTACT_AUTHOR} = '${dto[contactAuthor]}'`);
    if (dto['ncontactAuthor']?.length > 0) items.push(` ${CONTACT_AUTHOR} != '${dto['ncontactAuthor']}'`);
    if (dto[contactEmail]?.length > 0) items.push(` ${CONTACT_EMAIL} = '${dto[contactEmail]}'`);
    if (dto['ncontactEmail']?.length > 0) items.push(` ${CONTACT_EMAIL} != '${dto['ncontactEmail']}'`);
    if (dto[contactSubject]?.length > 0) items.push(` ${CONTACT_SUBJECT} = '${dto[contactSubject]}'`);
    if (dto['ncontactSubject']?.length > 0) items.push(` ${CONTACT_SUBJECT} != '${dto['ncontactSubject']}'`);
    if (dto[contactText]?.length > 0) items.push(` ${CONTACT_TEXT} = '${dto[contactText]}'`);
    if (dto['ncontactText']?.length > 0) items.push(` ${CONTACT_TEXT} != '${dto['ncontactText']}'`);
    if (dto[contactCreateDate]) items.push(`${CONTACT_CREATE_DATE} is not null and date_format(${CONTACT_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[contactCreateDate])}`);
    if (dto['ncontactCreateDate']) items.push(`${CONTACT_CREATE_DATE} is not null and date_format(${CONTACT_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['ncontactCreateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //CONTACT_ID--字符串字段
    if (dto['contactIdList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_ID, <string[]>dto['contactIdList']));
    if (dto['ncontactIdList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_ID, <string[]>dto['ncontactIdList'], true));
    if (dto['contactIdLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_ID, dto['contactIdLike'])); //For MysSQL
    if (dto['ncontactIdLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_ID, dto['ncontactIdLike'], true)); //For MysSQL
    if (dto['contactIdLikeList']?.length > 0) items.push(SysContactSQL.LIKE_LIST_SQL(CONTACT_ID, <string[]>dto['contactIdLikeList'])); //For MysSQL
    //CONTACT_AUTHOR--字符串字段
    if (dto['contactAuthorList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_AUTHOR, <string[]>dto['contactAuthorList']));
    if (dto['ncontactAuthorList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_AUTHOR, <string[]>dto['ncontactAuthorList'], true));
    if (dto['contactAuthorLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_AUTHOR, dto['contactAuthorLike'])); //For MysSQL
    if (dto['ncontactAuthorLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_AUTHOR, dto['ncontactAuthorLike'], true)); //For MysSQL
    if (dto['contactAuthorLikeList']?.length > 0) items.push(SysContactSQL.LIKE_LIST_SQL(CONTACT_AUTHOR, <string[]>dto['contactAuthorLikeList'])); //For MysSQL
    //CONTACT_EMAIL--字符串字段
    if (dto['contactEmailList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_EMAIL, <string[]>dto['contactEmailList']));
    if (dto['ncontactEmailList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_EMAIL, <string[]>dto['ncontactEmailList'], true));
    if (dto['contactEmailLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_EMAIL, dto['contactEmailLike'])); //For MysSQL
    if (dto['ncontactEmailLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_EMAIL, dto['ncontactEmailLike'], true)); //For MysSQL
    if (dto['contactEmailLikeList']?.length > 0) items.push(SysContactSQL.LIKE_LIST_SQL(CONTACT_EMAIL, <string[]>dto['contactEmailLikeList'])); //For MysSQL
    //CONTACT_SUBJECT--字符串字段
    if (dto['contactSubjectList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_SUBJECT, <string[]>dto['contactSubjectList']));
    if (dto['ncontactSubjectList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_SUBJECT, <string[]>dto['ncontactSubjectList'], true));
    if (dto['contactSubjectLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_SUBJECT, dto['contactSubjectLike'])); //For MysSQL
    if (dto['ncontactSubjectLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_SUBJECT, dto['ncontactSubjectLike'], true)); //For MysSQL
    if (dto['contactSubjectLikeList']?.length > 0) items.push(SysContactSQL.LIKE_LIST_SQL(CONTACT_SUBJECT, <string[]>dto['contactSubjectLikeList'])); //For MysSQL
    //CONTACT_TEXT--字符串字段
    if (dto['contactTextList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_TEXT, <string[]>dto['contactTextList']));
    if (dto['ncontactTextList']?.length > 0) items.push(SysContactSQL.IN_SQL(CONTACT_TEXT, <string[]>dto['ncontactTextList'], true));
    if (dto['contactTextLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_TEXT, dto['contactTextLike'])); //For MysSQL
    if (dto['ncontactTextLike']?.length > 0) items.push(SysContactSQL.LIKE_SQL(CONTACT_TEXT, dto['ncontactTextLike'], true)); //For MysSQL
    if (dto['contactTextLikeList']?.length > 0) items.push(SysContactSQL.LIKE_LIST_SQL(CONTACT_TEXT, <string[]>dto['contactTextLikeList'])); //For MysSQL
    //CONTACT_CREATE_DATE--日期字段
    if (dto[contactCreateDate]) items.push(`${CONTACT_CREATE_DATE} is not null and date_format(${CONTACT_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sContactCreateDate'])}`);
    if (dto[contactCreateDate]) items.push(`${CONTACT_CREATE_DATE} is not null and date_format(${CONTACT_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eContactCreateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysContactSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysContactCDto | Partial<SysContactCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(CONTACT_ID);
    cols.push(CONTACT_AUTHOR);
    cols.push(CONTACT_EMAIL);
    cols.push(CONTACT_SUBJECT);
    cols.push(CONTACT_TEXT);
    cols.push(CONTACT_CREATE_DATE);
    values.push(toSqlValue(dto[contactId]));
    values.push(toSqlValue(dto[contactAuthor]));
    values.push(toSqlValue(dto[contactEmail]));
    values.push(toSqlValue(dto[contactSubject]));
    values.push(toSqlValue(dto[contactText]));
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysContactCDto[] | Partial<SysContactCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(CONTACT_ID);
    cols.push(CONTACT_AUTHOR);
    cols.push(CONTACT_EMAIL);
    cols.push(CONTACT_SUBJECT);
    cols.push(CONTACT_TEXT);
    cols.push(CONTACT_CREATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[contactId]));
      values.push(toSqlValue(dto[contactAuthor]));
      values.push(toSqlValue(dto[contactEmail]));
      values.push(toSqlValue(dto[contactSubject]));
      values.push(toSqlValue(dto[contactText]));
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysContactUDto | Partial<SysContactUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysContactColNames, SysContactColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CONTACT_AUTHOR} = ${toSqlValue(dto[contactAuthor])}`);
    items.push(`${CONTACT_EMAIL} = ${toSqlValue(dto[contactEmail])}`);
    items.push(`${CONTACT_SUBJECT} = ${toSqlValue(dto[contactSubject])}`);
    items.push(`${CONTACT_TEXT} = ${toSqlValue(dto[contactText])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysContactUDto | Partial<SysContactUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysContactColNames, SysContactColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysContactSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${CONTACT_AUTHOR} = ${toSqlValue(dto[contactAuthor])}`);
    items.push(`${CONTACT_EMAIL} = ${toSqlValue(dto[contactEmail])}`);
    items.push(`${CONTACT_SUBJECT} = ${toSqlValue(dto[contactSubject])}`);
    items.push(`${CONTACT_TEXT} = ${toSqlValue(dto[contactText])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysContactSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysContactUDto[] | Partial<SysContactUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysContactSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysContactUDto | Partial<SysContactUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[contactAuthor]?.length > 0) items.push(`${CONTACT_AUTHOR} = ${toSqlValue(dto[contactAuthor])}`);
    if (dto[contactEmail]?.length > 0) items.push(`${CONTACT_EMAIL} = ${toSqlValue(dto[contactEmail])}`);
    if (dto[contactSubject]?.length > 0) items.push(`${CONTACT_SUBJECT} = ${toSqlValue(dto[contactSubject])}`);
    if (dto[contactText]?.length > 0) items.push(`${CONTACT_TEXT} = ${toSqlValue(dto[contactText])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysContactUDto[] | Partial<SysContactUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysContactSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysContactQDto | Partial<SysContactQDto>): string => {
    if (!dto) return '';
    let where = SysContactSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysContactSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_CONTACT';
/**
 * 列名常量字段
 */
const CONTACT_ID = SysContactColNameEnum.CONTACT_ID;
const CONTACT_AUTHOR = SysContactColNameEnum.CONTACT_AUTHOR;
const CONTACT_EMAIL = SysContactColNameEnum.CONTACT_EMAIL;
const CONTACT_SUBJECT = SysContactColNameEnum.CONTACT_SUBJECT;
const CONTACT_TEXT = SysContactColNameEnum.CONTACT_TEXT;
const CONTACT_CREATE_DATE = SysContactColNameEnum.CONTACT_CREATE_DATE;
/**
 * 实体类属性名
 */
const contactId = SysContactColPropEnum.contactId;
const contactAuthor = SysContactColPropEnum.contactAuthor;
const contactEmail = SysContactColPropEnum.contactEmail;
const contactSubject = SysContactColPropEnum.contactSubject;
const contactText = SysContactColPropEnum.contactText;
const contactCreateDate = SysContactColPropEnum.contactCreateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysContactTable.PRIMER_KEY;
const primerKey = SysContactTable.primerKey;
