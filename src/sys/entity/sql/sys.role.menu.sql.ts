import { SysRoleMenuQDto } from '../../dto/query/sys.role.menu.qdto';
import { SysRoleMenuCDto } from '../../dto/create/sys.role.menu.cdto';
import { SysRoleMenuUDto } from '../../dto/update/sys.role.menu.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysRoleMenuColNameEnum, SysRoleMenuColPropEnum, SysRoleMenuTable, SysRoleMenuColNames, SysRoleMenuColProps } from '../ddl/sys.role.menu.cols';
/**
 * SYS_ROLE_MENU--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysRoleMenuSQL
 */
export const SysRoleMenuSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let cols = SysRoleMenuSQL.COL_MAPPER_SQL(dto);
    let where = SysRoleMenuSQL.WHERE_SQL(dto);
    let group = SysRoleMenuSQL.GROUP_BY_SQL(dto);
    let order = SysRoleMenuSQL.ORDER_BY_SQL(dto);
    let limit = SysRoleMenuSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let where = SysRoleMenuSQL.WHERE_SQL(dto);
    let group = SysRoleMenuSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let cols = SysRoleMenuSQL.COL_MAPPER_SQL(dto);
    let group = SysRoleMenuSQL.GROUP_BY_SQL(dto);
    let order = SysRoleMenuSQL.ORDER_BY_SQL(dto);
    let limit = SysRoleMenuSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysRoleMenuSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysRoleMenuSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysRoleMenuSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let where = SysRoleMenuSQL.WHERE_SQL(dto);
    let group = SysRoleMenuSQL.GROUP_BY_SQL(dto);
    let order = SysRoleMenuSQL.ORDER_BY_SQL(dto);
    let limit = SysRoleMenuSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysRoleMenuColNames, SysRoleMenuColProps);
    if (!cols || cols.length == 0) cols = SysRoleMenuColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[rmId]?.length > 0) items.push(` ${RM_ID} = '${dto[rmId]}'`);
    if (dto['nrmId']?.length > 0) items.push(` ${RM_ID} != '${dto['nrmId']}'`);
    if (dto[rmRoleId]?.length > 0) items.push(` ${RM_ROLE_ID} = '${dto[rmRoleId]}'`);
    if (dto['nrmRoleId']?.length > 0) items.push(` ${RM_ROLE_ID} != '${dto['nrmRoleId']}'`);
    if (dto[rmMenuId]?.length > 0) items.push(` ${RM_MENU_ID} = '${dto[rmMenuId]}'`);
    if (dto['nrmMenuId']?.length > 0) items.push(` ${RM_MENU_ID} != '${dto['nrmMenuId']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //RM_ID--字符串字段
    if (dto['rmIdList']?.length > 0) items.push(SysRoleMenuSQL.IN_SQL(RM_ID, <string[]>dto['rmIdList']));
    if (dto['nrmIdList']?.length > 0) items.push(SysRoleMenuSQL.IN_SQL(RM_ID, <string[]>dto['nrmIdList'], true));
    if (dto['rmIdLike']?.length > 0) items.push(SysRoleMenuSQL.LIKE_SQL(RM_ID, dto['rmIdLike'])); //For MysSQL
    if (dto['nrmIdLike']?.length > 0) items.push(SysRoleMenuSQL.LIKE_SQL(RM_ID, dto['nrmIdLike'], true)); //For MysSQL
    if (dto['rmIdLikeList']?.length > 0) items.push(SysRoleMenuSQL.LIKE_LIST_SQL(RM_ID, <string[]>dto['rmIdLikeList'])); //For MysSQL
    //RM_ROLE_ID--字符串字段
    if (dto['rmRoleIdList']?.length > 0) items.push(SysRoleMenuSQL.IN_SQL(RM_ROLE_ID, <string[]>dto['rmRoleIdList']));
    if (dto['nrmRoleIdList']?.length > 0) items.push(SysRoleMenuSQL.IN_SQL(RM_ROLE_ID, <string[]>dto['nrmRoleIdList'], true));
    if (dto['rmRoleIdLike']?.length > 0) items.push(SysRoleMenuSQL.LIKE_SQL(RM_ROLE_ID, dto['rmRoleIdLike'])); //For MysSQL
    if (dto['nrmRoleIdLike']?.length > 0) items.push(SysRoleMenuSQL.LIKE_SQL(RM_ROLE_ID, dto['nrmRoleIdLike'], true)); //For MysSQL
    if (dto['rmRoleIdLikeList']?.length > 0) items.push(SysRoleMenuSQL.LIKE_LIST_SQL(RM_ROLE_ID, <string[]>dto['rmRoleIdLikeList'])); //For MysSQL
    //RM_MENU_ID--字符串字段
    if (dto['rmMenuIdList']?.length > 0) items.push(SysRoleMenuSQL.IN_SQL(RM_MENU_ID, <string[]>dto['rmMenuIdList']));
    if (dto['nrmMenuIdList']?.length > 0) items.push(SysRoleMenuSQL.IN_SQL(RM_MENU_ID, <string[]>dto['nrmMenuIdList'], true));
    if (dto['rmMenuIdLike']?.length > 0) items.push(SysRoleMenuSQL.LIKE_SQL(RM_MENU_ID, dto['rmMenuIdLike'])); //For MysSQL
    if (dto['nrmMenuIdLike']?.length > 0) items.push(SysRoleMenuSQL.LIKE_SQL(RM_MENU_ID, dto['nrmMenuIdLike'], true)); //For MysSQL
    if (dto['rmMenuIdLikeList']?.length > 0) items.push(SysRoleMenuSQL.LIKE_LIST_SQL(RM_MENU_ID, <string[]>dto['rmMenuIdLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysRoleMenuSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysRoleMenuCDto | Partial<SysRoleMenuCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(RM_ID);
    cols.push(RM_ROLE_ID);
    cols.push(RM_MENU_ID);
    values.push(toSqlValue(dto[rmId]));
    values.push(toSqlValue(dto[rmRoleId]));
    values.push(toSqlValue(dto[rmMenuId]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysRoleMenuCDto[] | Partial<SysRoleMenuCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(RM_ID);
    cols.push(RM_ROLE_ID);
    cols.push(RM_MENU_ID);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[rmId]));
      values.push(toSqlValue(dto[rmRoleId]));
      values.push(toSqlValue(dto[rmMenuId]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysRoleMenuUDto | Partial<SysRoleMenuUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysRoleMenuColNames, SysRoleMenuColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${RM_ROLE_ID} = ${toSqlValue(dto[rmRoleId])}`);
    items.push(`${RM_MENU_ID} = ${toSqlValue(dto[rmMenuId])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysRoleMenuUDto | Partial<SysRoleMenuUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysRoleMenuColNames, SysRoleMenuColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysRoleMenuSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${RM_ROLE_ID} = ${toSqlValue(dto[rmRoleId])}`);
    items.push(`${RM_MENU_ID} = ${toSqlValue(dto[rmMenuId])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysRoleMenuSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysRoleMenuUDto[] | Partial<SysRoleMenuUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysRoleMenuSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysRoleMenuUDto | Partial<SysRoleMenuUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[rmRoleId]?.length > 0) items.push(`${RM_ROLE_ID} = ${toSqlValue(dto[rmRoleId])}`);
    if (dto[rmMenuId]?.length > 0) items.push(`${RM_MENU_ID} = ${toSqlValue(dto[rmMenuId])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysRoleMenuUDto[] | Partial<SysRoleMenuUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysRoleMenuSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysRoleMenuQDto | Partial<SysRoleMenuQDto>): string => {
    if (!dto) return '';
    let where = SysRoleMenuSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysRoleMenuSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_ROLE_MENU';
/**
 * 列名常量字段
 */
const RM_ID = SysRoleMenuColNameEnum.RM_ID;
const RM_ROLE_ID = SysRoleMenuColNameEnum.RM_ROLE_ID;
const RM_MENU_ID = SysRoleMenuColNameEnum.RM_MENU_ID;
/**
 * 实体类属性名
 */
const rmId = SysRoleMenuColPropEnum.rmId;
const rmRoleId = SysRoleMenuColPropEnum.rmRoleId;
const rmMenuId = SysRoleMenuColPropEnum.rmMenuId;
/**
 * 主键信息
 */
const PRIMER_KEY = SysRoleMenuTable.PRIMER_KEY;
const primerKey = SysRoleMenuTable.primerKey;
