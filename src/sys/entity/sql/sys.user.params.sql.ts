import { SysUserParamsQDto } from '../../dto/query/sys.user.params.qdto';
import { SysUserParamsCDto } from '../../dto/create/sys.user.params.cdto';
import { SysUserParamsUDto } from '../../dto/update/sys.user.params.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysUserParamsColNameEnum, SysUserParamsColPropEnum, SysUserParamsTable, SysUserParamsColNames, SysUserParamsColProps } from '../ddl/sys.user.params.cols';
/**
 * SYS_USER_PARAMS--用户参数表表相关SQL定义
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export
 * @class SysUserParamsSQL
 */
export const SysUserParamsSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let cols = SysUserParamsSQL.COL_MAPPER_SQL(dto);
    let where = SysUserParamsSQL.WHERE_SQL(dto);
    let group = SysUserParamsSQL.GROUP_BY_SQL(dto);
    let order = SysUserParamsSQL.ORDER_BY_SQL(dto);
    let limit = SysUserParamsSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let where = SysUserParamsSQL.WHERE_SQL(dto);
    let group = SysUserParamsSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let cols = SysUserParamsSQL.COL_MAPPER_SQL(dto);
    let group = SysUserParamsSQL.GROUP_BY_SQL(dto);
    let order = SysUserParamsSQL.ORDER_BY_SQL(dto);
    let limit = SysUserParamsSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysUserParamsSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysUserParamsSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysUserParamsSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let where = SysUserParamsSQL.WHERE_SQL(dto);
    let group = SysUserParamsSQL.GROUP_BY_SQL(dto);
    let order = SysUserParamsSQL.ORDER_BY_SQL(dto);
    let limit = SysUserParamsSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysUserParamsColNames, SysUserParamsColProps);
    if (!cols || cols.length == 0) cols = SysUserParamsColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[upId]?.length > 0) items.push(` ${UP_ID} = '${dto[upId]}'`);
    if (dto['nupId']?.length > 0) items.push(` ${UP_ID} != '${dto['nupId']}'`);
    if (dto[upLoginName]?.length > 0) items.push(` ${UP_LOGIN_NAME} = '${dto[upLoginName]}'`);
    if (dto['nupLoginName']?.length > 0) items.push(` ${UP_LOGIN_NAME} != '${dto['nupLoginName']}'`);
    if (dto[upParamName]?.length > 0) items.push(` ${UP_PARAM_NAME} = '${dto[upParamName]}'`);
    if (dto['nupParamName']?.length > 0) items.push(` ${UP_PARAM_NAME} != '${dto['nupParamName']}'`);
    if (dto[upParamValue]?.length > 0) items.push(` ${UP_PARAM_VALUE} = '${dto[upParamValue]}'`);
    if (dto['nupParamValue']?.length > 0) items.push(` ${UP_PARAM_VALUE} != '${dto['nupParamValue']}'`);
    if (dto[upCreateDate]) items.push(`${UP_CREATE_DATE} is not null and date_format(${UP_CREATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[upCreateDate])}`);
    if (dto['nupCreateDate']) items.push(`${UP_CREATE_DATE} is not null and date_format(${UP_CREATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nupCreateDate'])}`);
    if (dto[upUpdateDate]) items.push(`${UP_UPDATE_DATE} is not null and date_format(${UP_UPDATE_DATE},'%Y-%m-%d') = ${toSqlValue(dto[upUpdateDate])}`);
    if (dto['nupUpdateDate']) items.push(`${UP_UPDATE_DATE} is not null and date_format(${UP_UPDATE_DATE},'%Y-%m-%d') != ${toSqlValue(dto['nupUpdateDate'])}`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //UP_ID--字符串字段
    if (dto['upIdList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_ID, <string[]>dto['upIdList']));
    if (dto['nupIdList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_ID, <string[]>dto['nupIdList'], true));
    if (dto['upIdLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_ID, dto['upIdLike'])); //For MysSQL
    if (dto['nupIdLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_ID, dto['nupIdLike'], true)); //For MysSQL
    if (dto['upIdLikeList']?.length > 0) items.push(SysUserParamsSQL.LIKE_LIST_SQL(UP_ID, <string[]>dto['upIdLikeList'])); //For MysSQL
    //UP_LOGIN_NAME--字符串字段
    if (dto['upLoginNameList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_LOGIN_NAME, <string[]>dto['upLoginNameList']));
    if (dto['nupLoginNameList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_LOGIN_NAME, <string[]>dto['nupLoginNameList'], true));
    if (dto['upLoginNameLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_LOGIN_NAME, dto['upLoginNameLike'])); //For MysSQL
    if (dto['nupLoginNameLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_LOGIN_NAME, dto['nupLoginNameLike'], true)); //For MysSQL
    if (dto['upLoginNameLikeList']?.length > 0) items.push(SysUserParamsSQL.LIKE_LIST_SQL(UP_LOGIN_NAME, <string[]>dto['upLoginNameLikeList'])); //For MysSQL
    //UP_PARAM_NAME--字符串字段
    if (dto['upParamNameList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_PARAM_NAME, <string[]>dto['upParamNameList']));
    if (dto['nupParamNameList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_PARAM_NAME, <string[]>dto['nupParamNameList'], true));
    if (dto['upParamNameLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_PARAM_NAME, dto['upParamNameLike'])); //For MysSQL
    if (dto['nupParamNameLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_PARAM_NAME, dto['nupParamNameLike'], true)); //For MysSQL
    if (dto['upParamNameLikeList']?.length > 0) items.push(SysUserParamsSQL.LIKE_LIST_SQL(UP_PARAM_NAME, <string[]>dto['upParamNameLikeList'])); //For MysSQL
    //UP_PARAM_VALUE--字符串字段
    if (dto['upParamValueList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_PARAM_VALUE, <string[]>dto['upParamValueList']));
    if (dto['nupParamValueList']?.length > 0) items.push(SysUserParamsSQL.IN_SQL(UP_PARAM_VALUE, <string[]>dto['nupParamValueList'], true));
    if (dto['upParamValueLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_PARAM_VALUE, dto['upParamValueLike'])); //For MysSQL
    if (dto['nupParamValueLike']?.length > 0) items.push(SysUserParamsSQL.LIKE_SQL(UP_PARAM_VALUE, dto['nupParamValueLike'], true)); //For MysSQL
    if (dto['upParamValueLikeList']?.length > 0) items.push(SysUserParamsSQL.LIKE_LIST_SQL(UP_PARAM_VALUE, <string[]>dto['upParamValueLikeList'])); //For MysSQL
    //UP_CREATE_DATE--日期字段
    if (dto[upCreateDate]) items.push(`${UP_CREATE_DATE} is not null and date_format(${UP_CREATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUpCreateDate'])}`);
    if (dto[upCreateDate]) items.push(`${UP_CREATE_DATE} is not null and date_format(${UP_CREATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUpCreateDate'])}`);
    //UP_UPDATE_DATE--日期字段
    if (dto[upUpdateDate]) items.push(`${UP_UPDATE_DATE} is not null and date_format(${UP_UPDATE_DATE},'%Y-%m-%d %T') >= ${toSqlValue(dto['sUpUpdateDate'])}`);
    if (dto[upUpdateDate]) items.push(`${UP_UPDATE_DATE} is not null and date_format(${UP_UPDATE_DATE},'%Y-%m-%d %T') <= ${toSqlValue(dto['eUpUpdateDate'])}`);
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysUserParamsSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysUserParamsCDto | Partial<SysUserParamsCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(UP_ID);
    cols.push(UP_LOGIN_NAME);
    cols.push(UP_PARAM_NAME);
    cols.push(UP_PARAM_VALUE);
    cols.push(UP_CREATE_DATE);
    cols.push(UP_UPDATE_DATE);
    values.push(toSqlValue(dto[upId]));
    values.push(toSqlValue(dto[upLoginName]));
    values.push(toSqlValue(dto[upParamName]));
    values.push(toSqlValue(dto[upParamValue]));
    values.push('NOW()');
    values.push('NOW()');
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysUserParamsCDto[] | Partial<SysUserParamsCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(UP_ID);
    cols.push(UP_LOGIN_NAME);
    cols.push(UP_PARAM_NAME);
    cols.push(UP_PARAM_VALUE);
    cols.push(UP_CREATE_DATE);
    cols.push(UP_UPDATE_DATE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[upId]));
      values.push(toSqlValue(dto[upLoginName]));
      values.push(toSqlValue(dto[upParamName]));
      values.push(toSqlValue(dto[upParamValue]));
      values.push('NOW()');
      values.push('NOW()');
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysUserParamsUDto | Partial<SysUserParamsUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserParamsColNames, SysUserParamsColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${UP_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UP_LOGIN_NAME} = ${toSqlValue(dto[upLoginName])}`);
    items.push(`${UP_PARAM_NAME} = ${toSqlValue(dto[upParamName])}`);
    items.push(`${UP_PARAM_VALUE} = ${toSqlValue(dto[upParamValue])}`);
    items.push(`${UP_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysUserParamsUDto | Partial<SysUserParamsUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysUserParamsColNames, SysUserParamsColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      items.push(`${UP_UPDATE_DATE} = NOW()`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserParamsSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${UP_LOGIN_NAME} = ${toSqlValue(dto[upLoginName])}`);
    items.push(`${UP_PARAM_NAME} = ${toSqlValue(dto[upParamName])}`);
    items.push(`${UP_PARAM_VALUE} = ${toSqlValue(dto[upParamValue])}`);
    items.push(`${UP_UPDATE_DATE} = NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysUserParamsSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysUserParamsUDto[] | Partial<SysUserParamsUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysUserParamsSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysUserParamsUDto | Partial<SysUserParamsUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[upLoginName]?.length > 0) items.push(`${UP_LOGIN_NAME} = ${toSqlValue(dto[upLoginName])}`);
    if (dto[upParamName]?.length > 0) items.push(`${UP_PARAM_NAME} = ${toSqlValue(dto[upParamName])}`);
    if (dto[upParamValue]?.length > 0) items.push(`${UP_PARAM_VALUE} = ${toSqlValue(dto[upParamValue])}`);
    if (dto[upUpdateDate]) items.push(`${UP_UPDATE_DATE}= NOW()`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysUserParamsUDto[] | Partial<SysUserParamsUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysUserParamsSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysUserParamsQDto | Partial<SysUserParamsQDto>): string => {
    if (!dto) return '';
    let where = SysUserParamsSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysUserParamsSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_USER_PARAMS';
/**
 * 列名常量字段
 */
const UP_ID = SysUserParamsColNameEnum.UP_ID;
const UP_LOGIN_NAME = SysUserParamsColNameEnum.UP_LOGIN_NAME;
const UP_PARAM_NAME = SysUserParamsColNameEnum.UP_PARAM_NAME;
const UP_PARAM_VALUE = SysUserParamsColNameEnum.UP_PARAM_VALUE;
const UP_CREATE_DATE = SysUserParamsColNameEnum.UP_CREATE_DATE;
const UP_UPDATE_DATE = SysUserParamsColNameEnum.UP_UPDATE_DATE;
/**
 * 实体类属性名
 */
const upId = SysUserParamsColPropEnum.upId;
const upLoginName = SysUserParamsColPropEnum.upLoginName;
const upParamName = SysUserParamsColPropEnum.upParamName;
const upParamValue = SysUserParamsColPropEnum.upParamValue;
const upCreateDate = SysUserParamsColPropEnum.upCreateDate;
const upUpdateDate = SysUserParamsColPropEnum.upUpdateDate;
/**
 * 主键信息
 */
const PRIMER_KEY = SysUserParamsTable.PRIMER_KEY;
const primerKey = SysUserParamsTable.primerKey;
