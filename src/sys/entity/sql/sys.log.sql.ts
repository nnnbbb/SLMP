import { SysLogQDto } from '../../dto/query/sys.log.qdto';
import { SysLogCDto } from '../../dto/create/sys.log.cdto';
import { SysLogUDto } from '../../dto/update/sys.log.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysLogColNameEnum, SysLogColPropEnum, SysLogTable, SysLogColNames, SysLogColProps } from '../ddl/sys.log.cols';
/**
 * SYS_LOG--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysLogSQL
 */
export const SysLogSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let cols = SysLogSQL.COL_MAPPER_SQL(dto);
    let where = SysLogSQL.WHERE_SQL(dto);
    let group = SysLogSQL.GROUP_BY_SQL(dto);
    let order = SysLogSQL.ORDER_BY_SQL(dto);
    let limit = SysLogSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let where = SysLogSQL.WHERE_SQL(dto);
    let group = SysLogSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let cols = SysLogSQL.COL_MAPPER_SQL(dto);
    let group = SysLogSQL.GROUP_BY_SQL(dto);
    let order = SysLogSQL.ORDER_BY_SQL(dto);
    let limit = SysLogSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysLogSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysLogSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysLogSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let where = SysLogSQL.WHERE_SQL(dto);
    let group = SysLogSQL.GROUP_BY_SQL(dto);
    let order = SysLogSQL.ORDER_BY_SQL(dto);
    let limit = SysLogSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysLogColNames, SysLogColProps);
    if (!cols || cols.length == 0) cols = SysLogColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[logId]?.length > 0) items.push(` ${LOG_ID} = '${dto[logId]}'`);
    if (dto['nlogId']?.length > 0) items.push(` ${LOG_ID} != '${dto['nlogId']}'`);
    if (dto[logUser]?.length > 0) items.push(` ${LOG_USER} = '${dto[logUser]}'`);
    if (dto['nlogUser']?.length > 0) items.push(` ${LOG_USER} != '${dto['nlogUser']}'`);
    if (dto[logTime]) items.push(`${LOG_TIME} is not null and date_format(${LOG_TIME},'%Y-%m-%d') = ${toSqlValue(dto[logTime])}`);
    if (dto['nlogTime']) items.push(`${LOG_TIME} is not null and date_format(${LOG_TIME},'%Y-%m-%d') != ${toSqlValue(dto['nlogTime'])}`);
    if (dto[logIp]?.length > 0) items.push(` ${LOG_IP} = '${dto[logIp]}'`);
    if (dto['nlogIp']?.length > 0) items.push(` ${LOG_IP} != '${dto['nlogIp']}'`);
    if (dto[logUrl]?.length > 0) items.push(` ${LOG_URL} = '${dto[logUrl]}'`);
    if (dto['nlogUrl']?.length > 0) items.push(` ${LOG_URL} != '${dto['nlogUrl']}'`);
    if (dto[logTitle]?.length > 0) items.push(` ${LOG_TITLE} = '${dto[logTitle]}'`);
    if (dto['nlogTitle']?.length > 0) items.push(` ${LOG_TITLE} != '${dto['nlogTitle']}'`);
    if (dto[logContent]?.length > 0) items.push(` ${LOG_CONTENT} = '${dto[logContent]}'`);
    if (dto['nlogContent']?.length > 0) items.push(` ${LOG_CONTENT} != '${dto['nlogContent']}'`);
    if (dto[logType]) items.push(` ${LOG_TYPE} = '${dto[logType]}'`);
    if (dto['nlogType']) items.push(` ${LOG_TYPE} != '${dto['nlogType']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //LOG_ID--字符串字段
    if (dto['logIdList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_ID, <string[]>dto['logIdList']));
    if (dto['nlogIdList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_ID, <string[]>dto['nlogIdList'], true));
    if (dto['logIdLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_ID, dto['logIdLike'])); //For MysSQL
    if (dto['nlogIdLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_ID, dto['nlogIdLike'], true)); //For MysSQL
    if (dto['logIdLikeList']?.length > 0) items.push(SysLogSQL.LIKE_LIST_SQL(LOG_ID, <string[]>dto['logIdLikeList'])); //For MysSQL
    //LOG_USER--字符串字段
    if (dto['logUserList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_USER, <string[]>dto['logUserList']));
    if (dto['nlogUserList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_USER, <string[]>dto['nlogUserList'], true));
    if (dto['logUserLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_USER, dto['logUserLike'])); //For MysSQL
    if (dto['nlogUserLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_USER, dto['nlogUserLike'], true)); //For MysSQL
    if (dto['logUserLikeList']?.length > 0) items.push(SysLogSQL.LIKE_LIST_SQL(LOG_USER, <string[]>dto['logUserLikeList'])); //For MysSQL
    //LOG_TIME--日期字段
    if (dto[logTime]) items.push(`${LOG_TIME} is not null and date_format(${LOG_TIME},'%Y-%m-%d %T') >= ${toSqlValue(dto['sLogTime'])}`);
    if (dto[logTime]) items.push(`${LOG_TIME} is not null and date_format(${LOG_TIME},'%Y-%m-%d %T') <= ${toSqlValue(dto['eLogTime'])}`);
    //LOG_IP--字符串字段
    if (dto['logIpList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_IP, <string[]>dto['logIpList']));
    if (dto['nlogIpList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_IP, <string[]>dto['nlogIpList'], true));
    if (dto['logIpLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_IP, dto['logIpLike'])); //For MysSQL
    if (dto['nlogIpLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_IP, dto['nlogIpLike'], true)); //For MysSQL
    if (dto['logIpLikeList']?.length > 0) items.push(SysLogSQL.LIKE_LIST_SQL(LOG_IP, <string[]>dto['logIpLikeList'])); //For MysSQL
    //LOG_URL--字符串字段
    if (dto['logUrlList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_URL, <string[]>dto['logUrlList']));
    if (dto['nlogUrlList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_URL, <string[]>dto['nlogUrlList'], true));
    if (dto['logUrlLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_URL, dto['logUrlLike'])); //For MysSQL
    if (dto['nlogUrlLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_URL, dto['nlogUrlLike'], true)); //For MysSQL
    if (dto['logUrlLikeList']?.length > 0) items.push(SysLogSQL.LIKE_LIST_SQL(LOG_URL, <string[]>dto['logUrlLikeList'])); //For MysSQL
    //LOG_TITLE--字符串字段
    if (dto['logTitleList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_TITLE, <string[]>dto['logTitleList']));
    if (dto['nlogTitleList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_TITLE, <string[]>dto['nlogTitleList'], true));
    if (dto['logTitleLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_TITLE, dto['logTitleLike'])); //For MysSQL
    if (dto['nlogTitleLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_TITLE, dto['nlogTitleLike'], true)); //For MysSQL
    if (dto['logTitleLikeList']?.length > 0) items.push(SysLogSQL.LIKE_LIST_SQL(LOG_TITLE, <string[]>dto['logTitleLikeList'])); //For MysSQL
    //LOG_CONTENT--字符串字段
    if (dto['logContentList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_CONTENT, <string[]>dto['logContentList']));
    if (dto['nlogContentList']?.length > 0) items.push(SysLogSQL.IN_SQL(LOG_CONTENT, <string[]>dto['nlogContentList'], true));
    if (dto['logContentLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_CONTENT, dto['logContentLike'])); //For MysSQL
    if (dto['nlogContentLike']?.length > 0) items.push(SysLogSQL.LIKE_SQL(LOG_CONTENT, dto['nlogContentLike'], true)); //For MysSQL
    if (dto['logContentLikeList']?.length > 0) items.push(SysLogSQL.LIKE_LIST_SQL(LOG_CONTENT, <string[]>dto['logContentLikeList'])); //For MysSQL
    //LOG_TYPE--数字字段
    if (dto['logTypeList']) items.push(SysLogSQL.IN_SQL(LOG_TYPE, dto['logTypeList']));
    if (dto['nlogTypeList']) items.push(SysLogSQL.IN_SQL(LOG_TYPE, dto['nlogTypeList'], true));
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysLogSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysLogCDto | Partial<SysLogCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(LOG_ID);
    cols.push(LOG_USER);
    cols.push(LOG_TIME);
    cols.push(LOG_IP);
    cols.push(LOG_URL);
    cols.push(LOG_TITLE);
    cols.push(LOG_CONTENT);
    cols.push(LOG_TYPE);
    values.push(toSqlValue(dto[logId]));
    values.push(toSqlValue(dto[logUser]));
    values.push('NOW()');
    values.push(toSqlValue(dto[logIp]));
    values.push(toSqlValue(dto[logUrl]));
    values.push(toSqlValue(dto[logTitle]));
    values.push(toSqlValue(dto[logContent]));
    values.push(toSqlValue(dto[logType]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysLogCDto[] | Partial<SysLogCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(LOG_ID);
    cols.push(LOG_USER);
    cols.push(LOG_TIME);
    cols.push(LOG_IP);
    cols.push(LOG_URL);
    cols.push(LOG_TITLE);
    cols.push(LOG_CONTENT);
    cols.push(LOG_TYPE);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[logId]));
      values.push(toSqlValue(dto[logUser]));
      values.push('NOW()');
      values.push(toSqlValue(dto[logIp]));
      values.push(toSqlValue(dto[logUrl]));
      values.push(toSqlValue(dto[logTitle]));
      values.push(toSqlValue(dto[logContent]));
      values.push(toSqlValue(dto[logType]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysLogUDto | Partial<SysLogUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysLogColNames, SysLogColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${LOG_USER} = ${toSqlValue(dto[logUser])}`);
    items.push(`${LOG_IP} = ${toSqlValue(dto[logIp])}`);
    items.push(`${LOG_URL} = ${toSqlValue(dto[logUrl])}`);
    items.push(`${LOG_TITLE} = ${toSqlValue(dto[logTitle])}`);
    items.push(`${LOG_CONTENT} = ${toSqlValue(dto[logContent])}`);
    items.push(`${LOG_TYPE} = ${toSqlValue(dto[logType])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysLogUDto | Partial<SysLogUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysLogColNames, SysLogColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysLogSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${LOG_USER} = ${toSqlValue(dto[logUser])}`);
    items.push(`${LOG_IP} = ${toSqlValue(dto[logIp])}`);
    items.push(`${LOG_URL} = ${toSqlValue(dto[logUrl])}`);
    items.push(`${LOG_TITLE} = ${toSqlValue(dto[logTitle])}`);
    items.push(`${LOG_CONTENT} = ${toSqlValue(dto[logContent])}`);
    items.push(`${LOG_TYPE} = ${toSqlValue(dto[logType])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysLogSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysLogUDto[] | Partial<SysLogUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysLogSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysLogUDto | Partial<SysLogUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[logUser]?.length > 0) items.push(`${LOG_USER} = ${toSqlValue(dto[logUser])}`);
    if (dto[logIp]?.length > 0) items.push(`${LOG_IP} = ${toSqlValue(dto[logIp])}`);
    if (dto[logUrl]?.length > 0) items.push(`${LOG_URL} = ${toSqlValue(dto[logUrl])}`);
    if (dto[logTitle]?.length > 0) items.push(`${LOG_TITLE} = ${toSqlValue(dto[logTitle])}`);
    if (dto[logContent]?.length > 0) items.push(`${LOG_CONTENT} = ${toSqlValue(dto[logContent])}`);
    if (dto[logType]) items.push(`${LOG_TYPE} = ${toSqlValue(dto[logType])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysLogUDto[] | Partial<SysLogUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysLogSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysLogQDto | Partial<SysLogQDto>): string => {
    if (!dto) return '';
    let where = SysLogSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysLogSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_LOG';
/**
 * 列名常量字段
 */
const LOG_ID = SysLogColNameEnum.LOG_ID;
const LOG_USER = SysLogColNameEnum.LOG_USER;
const LOG_TIME = SysLogColNameEnum.LOG_TIME;
const LOG_IP = SysLogColNameEnum.LOG_IP;
const LOG_URL = SysLogColNameEnum.LOG_URL;
const LOG_TITLE = SysLogColNameEnum.LOG_TITLE;
const LOG_CONTENT = SysLogColNameEnum.LOG_CONTENT;
const LOG_TYPE = SysLogColNameEnum.LOG_TYPE;
/**
 * 实体类属性名
 */
const logId = SysLogColPropEnum.logId;
const logUser = SysLogColPropEnum.logUser;
const logTime = SysLogColPropEnum.logTime;
const logIp = SysLogColPropEnum.logIp;
const logUrl = SysLogColPropEnum.logUrl;
const logTitle = SysLogColPropEnum.logTitle;
const logContent = SysLogColPropEnum.logContent;
const logType = SysLogColPropEnum.logType;
/**
 * 主键信息
 */
const PRIMER_KEY = SysLogTable.PRIMER_KEY;
const primerKey = SysLogTable.primerKey;
