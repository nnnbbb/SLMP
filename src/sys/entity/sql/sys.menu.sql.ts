import { SysMenuQDto } from '../../dto/query/sys.menu.qdto';
import { SysMenuCDto } from '../../dto/create/sys.menu.cdto';
import { SysMenuUDto } from '../../dto/update/sys.menu.udto';
import { StringUtils } from '../../../common/utils/string.utils';
import { SysMenuColNameEnum, SysMenuColPropEnum, SysMenuTable, SysMenuColNames, SysMenuColProps } from '../ddl/sys.menu.cols';
/**
 * SYS_MENU--表相关SQL定义
 * @date 1/13/2021, 2:31:20 PM
 * @author jiangbin
 * @export
 * @class SysMenuSQL
 */
export const SysMenuSQL = {
  /**
   * 构建根据有值属性查询SQL
   * @param dto
   * @return {string}
   */
  SELECT_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let cols = SysMenuSQL.COL_MAPPER_SQL(dto);
    let where = SysMenuSQL.WHERE_SQL(dto);
    let group = SysMenuSQL.GROUP_BY_SQL(dto);
    let order = SysMenuSQL.ORDER_BY_SQL(dto);
    let limit = SysMenuSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询记录数SQL
   * @param dto
   * @return {string}
   */
  SELECT_COUNT_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let where = SysMenuSQL.WHERE_SQL(dto);
    let group = SysMenuSQL.GROUP_BY_SQL(dto);
    return `SELECT count(*) as count FROM ${TABLE} ${where} ${group}`;
  },
  /**
   * 根据ID查询记录SQL
   * @param dto
   * @return {string}
   */
  SELECT_BY_ID_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let cols = SysMenuSQL.COL_MAPPER_SQL(dto);
    let group = SysMenuSQL.GROUP_BY_SQL(dto);
    let order = SysMenuSQL.ORDER_BY_SQL(dto);
    let limit = SysMenuSQL.LIMIT_SQL(dto);
    return `SELECT ${cols} FROM ${TABLE} WHERE ${PRIMER_KEY} = ${toSqlValue(dto[primerKey])} ${group} ${order} ${limit}`;
  },
  /**
   * 根据ID列表查询记录SQL
   * @param idList
   * @return {string}
   */
  SELECT_BY_ID_LIST_SQL: (idList: string[], cols = []): string => {
    if (!idList || idList.length == 0) return '';
    let sql = SysMenuSQL.COL_MAPPER_SQL({ cols });
    return `SELECT ${sql} FROM ${TABLE} WHERE ${SysMenuSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询数据库中已存在ID的SQL语句
   * @param idList
   * @return {string}
   */
  SELECT_EXIST_ID_LIST_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    return `SELECT ${PRIMER_KEY} FROM ${TABLE} WHERE ${SysMenuSQL.IN_SQL(PRIMER_KEY, idList)}`;
  },
  /**
   * 查询所有ID列表SQL语句
   * @return {string}
   */
  SELECT_ALL_ID_LIST_SQL: (): string => {
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}`;
  },
  /**
   * 条件查询ID列表SQL语句
   * @param dto
   * @return {string}
   */
  SELECT_ID_LIST_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let where = SysMenuSQL.WHERE_SQL(dto);
    let group = SysMenuSQL.GROUP_BY_SQL(dto);
    let order = SysMenuSQL.ORDER_BY_SQL(dto);
    let limit = SysMenuSQL.LIMIT_SQL(dto);
    return `SELECT ${PRIMER_KEY} FROM ${TABLE}  ${where} ${group} ${order} ${limit}`;
  },
  /**
   * 查询结果列映射SQL
   * @param dto
   * @return {string}
   */
  COL_MAPPER_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let cols = StringUtils.toColName(dto.cols, SysMenuColNames, SysMenuColProps);
    if (!cols || cols.length == 0) cols = SysMenuColNames;
    let sql = cols.map((col) => `${col} as ${StringUtils.camelCase(col)}`).join(',');
    return dto.distinct ? `DISTINCT ${sql}` : sql;
  },
  /**
   * Where语句，根据有值属性查询
   * @param dto
   * @return {string}
   */
  WHERE_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[menuId]?.length > 0) items.push(` ${MENU_ID} = '${dto[menuId]}'`);
    if (dto['nmenuId']?.length > 0) items.push(` ${MENU_ID} != '${dto['nmenuId']}'`);
    if (dto[menuName]?.length > 0) items.push(` ${MENU_NAME} = '${dto[menuName]}'`);
    if (dto['nmenuName']?.length > 0) items.push(` ${MENU_NAME} != '${dto['nmenuName']}'`);
    if (dto[menuNameEn]?.length > 0) items.push(` ${MENU_NAME_EN} = '${dto[menuNameEn]}'`);
    if (dto['nmenuNameEn']?.length > 0) items.push(` ${MENU_NAME_EN} != '${dto['nmenuNameEn']}'`);
    if (dto[menuUrl]?.length > 0) items.push(` ${MENU_URL} = '${dto[menuUrl]}'`);
    if (dto['nmenuUrl']?.length > 0) items.push(` ${MENU_URL} != '${dto['nmenuUrl']}'`);
    if (dto[menuParentId]?.length > 0) items.push(` ${MENU_PARENT_ID} = '${dto[menuParentId]}'`);
    if (dto['nmenuParentId']?.length > 0) items.push(` ${MENU_PARENT_ID} != '${dto['nmenuParentId']}'`);
    if (dto[menuOrder]) items.push(` ${MENU_ORDER} = '${dto[menuOrder]}'`);
    if (dto['nmenuOrder']) items.push(` ${MENU_ORDER} != '${dto['nmenuOrder']}'`);
    if (dto[menuRemark]?.length > 0) items.push(` ${MENU_REMARK} = '${dto[menuRemark]}'`);
    if (dto['nmenuRemark']?.length > 0) items.push(` ${MENU_REMARK} != '${dto['nmenuRemark']}'`);
    if (dto[menuState]?.length > 0) items.push(` ${MENU_STATE} = '${dto[menuState]}'`);
    if (dto['nmenuState']?.length > 0) items.push(` ${MENU_STATE} != '${dto['nmenuState']}'`);
    if (dto[menuType]?.length > 0) items.push(` ${MENU_TYPE} = '${dto[menuType]}'`);
    if (dto['nmenuType']?.length > 0) items.push(` ${MENU_TYPE} != '${dto['nmenuType']}'`);
    if (dto[menuIconClass]?.length > 0) items.push(` ${MENU_ICON_CLASS} = '${dto[menuIconClass]}'`);
    if (dto['nmenuIconClass']?.length > 0) items.push(` ${MENU_ICON_CLASS} != '${dto['nmenuIconClass']}'`);
    //数组、模糊条件、年份日期范围等额外查询条件
    //MENU_ID--字符串字段
    if (dto['menuIdList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_ID, <string[]>dto['menuIdList']));
    if (dto['nmenuIdList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_ID, <string[]>dto['nmenuIdList'], true));
    if (dto['menuIdLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_ID, dto['menuIdLike'])); //For MysSQL
    if (dto['nmenuIdLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_ID, dto['nmenuIdLike'], true)); //For MysSQL
    if (dto['menuIdLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_ID, <string[]>dto['menuIdLikeList'])); //For MysSQL
    //MENU_NAME--字符串字段
    if (dto['menuNameList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_NAME, <string[]>dto['menuNameList']));
    if (dto['nmenuNameList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_NAME, <string[]>dto['nmenuNameList'], true));
    if (dto['menuNameLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_NAME, dto['menuNameLike'])); //For MysSQL
    if (dto['nmenuNameLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_NAME, dto['nmenuNameLike'], true)); //For MysSQL
    if (dto['menuNameLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_NAME, <string[]>dto['menuNameLikeList'])); //For MysSQL
    //MENU_NAME_EN--字符串字段
    if (dto['menuNameEnList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_NAME_EN, <string[]>dto['menuNameEnList']));
    if (dto['nmenuNameEnList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_NAME_EN, <string[]>dto['nmenuNameEnList'], true));
    if (dto['menuNameEnLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_NAME_EN, dto['menuNameEnLike'])); //For MysSQL
    if (dto['nmenuNameEnLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_NAME_EN, dto['nmenuNameEnLike'], true)); //For MysSQL
    if (dto['menuNameEnLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_NAME_EN, <string[]>dto['menuNameEnLikeList'])); //For MysSQL
    //MENU_URL--字符串字段
    if (dto['menuUrlList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_URL, <string[]>dto['menuUrlList']));
    if (dto['nmenuUrlList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_URL, <string[]>dto['nmenuUrlList'], true));
    if (dto['menuUrlLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_URL, dto['menuUrlLike'])); //For MysSQL
    if (dto['nmenuUrlLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_URL, dto['nmenuUrlLike'], true)); //For MysSQL
    if (dto['menuUrlLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_URL, <string[]>dto['menuUrlLikeList'])); //For MysSQL
    //MENU_PARENT_ID--字符串字段
    if (dto['menuParentIdList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_PARENT_ID, <string[]>dto['menuParentIdList']));
    if (dto['nmenuParentIdList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_PARENT_ID, <string[]>dto['nmenuParentIdList'], true));
    if (dto['menuParentIdLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_PARENT_ID, dto['menuParentIdLike'])); //For MysSQL
    if (dto['nmenuParentIdLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_PARENT_ID, dto['nmenuParentIdLike'], true)); //For MysSQL
    if (dto['menuParentIdLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_PARENT_ID, <string[]>dto['menuParentIdLikeList'])); //For MysSQL
    //MENU_ORDER--数字字段
    if (dto['menuOrderList']) items.push(SysMenuSQL.IN_SQL(MENU_ORDER, dto['menuOrderList']));
    if (dto['nmenuOrderList']) items.push(SysMenuSQL.IN_SQL(MENU_ORDER, dto['nmenuOrderList'], true));
    //MENU_REMARK--字符串字段
    if (dto['menuRemarkList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_REMARK, <string[]>dto['menuRemarkList']));
    if (dto['nmenuRemarkList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_REMARK, <string[]>dto['nmenuRemarkList'], true));
    if (dto['menuRemarkLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_REMARK, dto['menuRemarkLike'])); //For MysSQL
    if (dto['nmenuRemarkLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_REMARK, dto['nmenuRemarkLike'], true)); //For MysSQL
    if (dto['menuRemarkLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_REMARK, <string[]>dto['menuRemarkLikeList'])); //For MysSQL
    //MENU_STATE--字符串字段
    if (dto['menuStateList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_STATE, <string[]>dto['menuStateList']));
    if (dto['nmenuStateList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_STATE, <string[]>dto['nmenuStateList'], true));
    if (dto['menuStateLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_STATE, dto['menuStateLike'])); //For MysSQL
    if (dto['nmenuStateLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_STATE, dto['nmenuStateLike'], true)); //For MysSQL
    if (dto['menuStateLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_STATE, <string[]>dto['menuStateLikeList'])); //For MysSQL
    //MENU_TYPE--字符串字段
    if (dto['menuTypeList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_TYPE, <string[]>dto['menuTypeList']));
    if (dto['nmenuTypeList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_TYPE, <string[]>dto['nmenuTypeList'], true));
    if (dto['menuTypeLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_TYPE, dto['menuTypeLike'])); //For MysSQL
    if (dto['nmenuTypeLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_TYPE, dto['nmenuTypeLike'], true)); //For MysSQL
    if (dto['menuTypeLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_TYPE, <string[]>dto['menuTypeLikeList'])); //For MysSQL
    //MENU_ICON_CLASS--字符串字段
    if (dto['menuIconClassList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_ICON_CLASS, <string[]>dto['menuIconClassList']));
    if (dto['nmenuIconClassList']?.length > 0) items.push(SysMenuSQL.IN_SQL(MENU_ICON_CLASS, <string[]>dto['nmenuIconClassList'], true));
    if (dto['menuIconClassLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_ICON_CLASS, dto['menuIconClassLike'])); //For MysSQL
    if (dto['nmenuIconClassLike']?.length > 0) items.push(SysMenuSQL.LIKE_SQL(MENU_ICON_CLASS, dto['nmenuIconClassLike'], true)); //For MysSQL
    if (dto['menuIconClassLikeList']?.length > 0) items.push(SysMenuSQL.LIKE_LIST_SQL(MENU_ICON_CLASS, <string[]>dto['menuIconClassLikeList'])); //For MysSQL
    return items.length > 0 ? ` WHERE ${items.join(' and ')}` : '';
  },
  /**
   * 字符串的Like语句，采用前后模糊查询
   * @param colName 列名
   * @param values 值
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  LIKE_SQL: (colName: string, value: string, not: boolean = false): string => {
    if (!colName || value.length == 0 || !value || value.length == 0) return '';
    if (not) return ` ${colName} not like CONCAT('%','${value}','%')`;
    else return ` ${colName} like CONCAT('%','${value}','%')`;
  },
  /**
   * 字符串列表的Like语句，采用OR/AND连接和前后模糊查询
   * @param colName 列名
   * @param values 值列表
   * @param andOr 采用and或or连接多个条件，默认为OR语句
   * @return {string}
   */
  LIKE_LIST_SQL: (colName: string, values: string[], andOr?: string): string => {
    if (!colName || !values || values.length == 0) return '';
    if (!andOr) andOr = 'OR';
    let items = values.map((value) => SysMenuSQL.LIKE_SQL(colName, value));
    return `( ${items.join(' ' + andOr + ' ')} )`;
  },
  /**
   * 排序查询SQL语句
   * @param dto
   * @return {string}
   */
  ORDER_BY_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    if (dto['order']?.length > 0) {
      return `order by ${dto['order']}`;
    }
    return '';
  },
  /**
   * 分组查询SQL语句
   * @param dto
   * @return {string}
   */
  GROUP_BY_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    if (dto['group']?.length > 0) {
      return `group by ${dto['group']}`;
    }
    return '';
  },
  /**
   * 分页查询条件SQL语句
   * @param dto
   * @return {string}
   */
  LIMIT_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    if (typeof dto['offset'] == 'number' && typeof dto['pageSize'] == 'number') {
      return `limit ${dto['offset']},${dto['pageSize']}`;
    }
    return '';
  },
  /**
   * IN语句，会自动判定是否为数字还是字符串，以便构建正确语法的SQL
   * @param colName 列名，如:SAM_NAME
   * @param values 数组
   * @param not true/false--不包含于/包含于
   * @return {string}
   */
  IN_SQL: (colName: string, values: number[] | string[], not: boolean = false) => {
    if (!values || values.length == 0) return '';
    let items = [];
    Array.isArray(values) ? values.forEach((value) => items.push(toSqlValue(value))) : items.push(toSqlValue(values));
    return ` ${colName} ${not ? 'NOT' : ''} IN(${items.join(',')})`;
  },
  /**
   * 插入记录SQL语句
   * @param dto
   * @return {string}
   */
  INSERT_SQL: (dto: SysMenuCDto | Partial<SysMenuCDto>): string => {
    if (!dto) return '';
    if (!dto[primerKey] || dto[primerKey] == undefined) return '';
    let cols = [];
    let values = [];
    cols.push(MENU_ID);
    cols.push(MENU_NAME);
    cols.push(MENU_NAME_EN);
    cols.push(MENU_URL);
    cols.push(MENU_PARENT_ID);
    cols.push(MENU_ORDER);
    cols.push(MENU_REMARK);
    cols.push(MENU_STATE);
    cols.push(MENU_TYPE);
    cols.push(MENU_ICON_CLASS);
    values.push(toSqlValue(dto[menuId]));
    values.push(toSqlValue(dto[menuName]));
    values.push(toSqlValue(dto[menuNameEn]));
    values.push(toSqlValue(dto[menuUrl]));
    values.push(toSqlValue(dto[menuParentId]));
    values.push(toSqlValue(dto[menuOrder]));
    values.push(toSqlValue(dto[menuRemark]));
    values.push(toSqlValue(dto[menuState]));
    values.push(toSqlValue(dto[menuType]));
    values.push(toSqlValue(dto[menuIconClass]));
    let sql = `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES( ${values.join(',')} )`;
    return sql;
  },
  /**
   * 批量插入SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_INSERT_SQL: (dtos: SysMenuCDto[] | Partial<SysMenuCDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let cols = [];
    cols.push(MENU_ID);
    cols.push(MENU_NAME);
    cols.push(MENU_NAME_EN);
    cols.push(MENU_URL);
    cols.push(MENU_PARENT_ID);
    cols.push(MENU_ORDER);
    cols.push(MENU_REMARK);
    cols.push(MENU_STATE);
    cols.push(MENU_TYPE);
    cols.push(MENU_ICON_CLASS);
    let items = [];
    dtos.forEach((dto) => {
      let values = [];
      values.push(toSqlValue(dto[menuId]));
      values.push(toSqlValue(dto[menuName]));
      values.push(toSqlValue(dto[menuNameEn]));
      values.push(toSqlValue(dto[menuUrl]));
      values.push(toSqlValue(dto[menuParentId]));
      values.push(toSqlValue(dto[menuOrder]));
      values.push(toSqlValue(dto[menuRemark]));
      values.push(toSqlValue(dto[menuState]));
      values.push(toSqlValue(dto[menuType]));
      values.push(toSqlValue(dto[menuIconClass]));
      items.push(`(${values.join(',')})`);
    });
    return `INSERT INTO ${TABLE}( ${cols.join(',')} ) VALUES ${items.join(',')}`;
  },
  /**
   * 根据ID更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SQL: (dto: SysMenuUDto | Partial<SysMenuUDto>): string => {
    if (!dto) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysMenuColNames, SysMenuColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${MENU_NAME} = ${toSqlValue(dto[menuName])}`);
    items.push(`${MENU_NAME_EN} = ${toSqlValue(dto[menuNameEn])}`);
    items.push(`${MENU_URL} = ${toSqlValue(dto[menuUrl])}`);
    items.push(`${MENU_PARENT_ID} = ${toSqlValue(dto[menuParentId])}`);
    items.push(`${MENU_ORDER} = ${toSqlValue(dto[menuOrder])}`);
    items.push(`${MENU_REMARK} = ${toSqlValue(dto[menuRemark])}`);
    items.push(`${MENU_STATE} = ${toSqlValue(dto[menuState])}`);
    items.push(`${MENU_TYPE} = ${toSqlValue(dto[menuType])}`);
    items.push(`${MENU_ICON_CLASS} = ${toSqlValue(dto[menuIconClass])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID列表批量更新所有属性值的更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_IDS_SQL: (dto: SysMenuUDto | Partial<SysMenuUDto>): string => {
    if (!dto) return '';
    //获取ID列表
    let ids = <string[]>dto[`${primerKey}List`];
    if (!ids || ids.length == 0) return '';
    //校正列名
    let cols = StringUtils.toColName(dto.cols, SysMenuColNames, SysMenuColProps);
    //移除日期列名
    cols = StringUtils.delDateCols(cols);
    //更新指定的列数据
    if (cols?.length > 0) {
      let items = cols.map((col) => `${col} = ${toSqlValue(dto[StringUtils.camelCase(col)])}`);
      return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysMenuSQL.IN_SQL(PRIMER_KEY, ids)}`;
    }
    //更新所有列数据
    let items = [];
    items.push(`${MENU_NAME} = ${toSqlValue(dto[menuName])}`);
    items.push(`${MENU_NAME_EN} = ${toSqlValue(dto[menuNameEn])}`);
    items.push(`${MENU_URL} = ${toSqlValue(dto[menuUrl])}`);
    items.push(`${MENU_PARENT_ID} = ${toSqlValue(dto[menuParentId])}`);
    items.push(`${MENU_ORDER} = ${toSqlValue(dto[menuOrder])}`);
    items.push(`${MENU_REMARK} = ${toSqlValue(dto[menuRemark])}`);
    items.push(`${MENU_STATE} = ${toSqlValue(dto[menuState])}`);
    items.push(`${MENU_TYPE} = ${toSqlValue(dto[menuType])}`);
    items.push(`${MENU_ICON_CLASS} = ${toSqlValue(dto[menuIconClass])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${SysMenuSQL.IN_SQL(PRIMER_KEY, ids)}`;
  },
  /**
   * 根据ID更新所有属性值的批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SQL: (dtos: SysMenuUDto[] | Partial<SysMenuUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let sqls = [];
    dtos.forEach((value) => {
      sqls.push(SysMenuSQL.UPDATE_BY_ID_SQL(value));
    });
    return sqls.join(';');
  },
  /**
   * 根据ID更新有值属性值的条件更新SQL语句
   * @param dto
   * @return {string}
   */
  UPDATE_BY_ID_SELECTIVE_SQL: (dto: SysMenuUDto | Partial<SysMenuUDto>): string => {
    if (!dto) return '';
    let items = [];
    if (dto[menuName]?.length > 0) items.push(`${MENU_NAME} = ${toSqlValue(dto[menuName])}`);
    if (dto[menuNameEn]?.length > 0) items.push(`${MENU_NAME_EN} = ${toSqlValue(dto[menuNameEn])}`);
    if (dto[menuUrl]?.length > 0) items.push(`${MENU_URL} = ${toSqlValue(dto[menuUrl])}`);
    if (dto[menuParentId]?.length > 0) items.push(`${MENU_PARENT_ID} = ${toSqlValue(dto[menuParentId])}`);
    if (dto[menuOrder]) items.push(`${MENU_ORDER} = ${toSqlValue(dto[menuOrder])}`);
    if (dto[menuRemark]?.length > 0) items.push(`${MENU_REMARK} = ${toSqlValue(dto[menuRemark])}`);
    if (dto[menuState]?.length > 0) items.push(`${MENU_STATE} = ${toSqlValue(dto[menuState])}`);
    if (dto[menuType]?.length > 0) items.push(`${MENU_TYPE} = ${toSqlValue(dto[menuType])}`);
    if (dto[menuIconClass]?.length > 0) items.push(`${MENU_ICON_CLASS} = ${toSqlValue(dto[menuIconClass])}`);
    return `UPDATE ${TABLE} SET ${items.join(',')} WHERE ${PRIMER_KEY} = '${dto[primerKey]}'`;
  },
  /**
   * 根据ID更新有值属性值的条件批量更新SQL语句
   * @param entitys
   * @return {string}
   */
  BATCH_UPDATE_BY_ID_SELECTIVE_SQL: (dtos: SysMenuUDto[] | Partial<SysMenuUDto>[]): string => {
    if (!dtos || dtos.length == 0) return '';
    let items = [];
    dtos.forEach((value) => items.push(SysMenuSQL.UPDATE_BY_ID_SELECTIVE_SQL(value)));
    return items.join(';');
  },
  /**
   * 条件删除SQL语句
   * @param dtos
   * @return {string}
   */
  DELETE_SELECTIVE_SQL: (dto: SysMenuQDto | Partial<SysMenuQDto>): string => {
    if (!dto) return '';
    let where = SysMenuSQL.WHERE_SQL(dto);
    return `DELETE FROM ${TABLE} ${where}`;
  },
  /**
   * 删除
   * @param dto
   * @return {string}
   */
  DELETE_BY_ID_SQL: (id: string): string => {
    if (!id) return '';
    return `DELETE FROM ${TABLE} WHERE ${PRIMER_KEY} = ${id}`;
  },
  /**
   *
   * @param dto
   * @return {string}
   */
  DELETE_BY_IDS_SQL: (idList: string[]): string => {
    if (!idList || idList.length == 0) return '';
    let inSql = SysMenuSQL.IN_SQL(PRIMER_KEY, idList);
    return `DELETE FROM ${TABLE} WHERE ${inSql}`;
  },
  /**
   * 删除所有记录的SQL语句
   */
  DELETE_ALL_SQL: (): string => {
    return `DELETE FROM ${TABLE}`;
  },
};
/**
 * 根据值的类型返回对格式化后的字符串，主要方便构建SQL语句
 * @param value
 */
const toSqlValue = StringUtils.toSqlValue;
/**
 * 表信息
 */
const TABLE = 'SYS_MENU';
/**
 * 列名常量字段
 */
const MENU_ID = SysMenuColNameEnum.MENU_ID;
const MENU_NAME = SysMenuColNameEnum.MENU_NAME;
const MENU_NAME_EN = SysMenuColNameEnum.MENU_NAME_EN;
const MENU_URL = SysMenuColNameEnum.MENU_URL;
const MENU_PARENT_ID = SysMenuColNameEnum.MENU_PARENT_ID;
const MENU_ORDER = SysMenuColNameEnum.MENU_ORDER;
const MENU_REMARK = SysMenuColNameEnum.MENU_REMARK;
const MENU_STATE = SysMenuColNameEnum.MENU_STATE;
const MENU_TYPE = SysMenuColNameEnum.MENU_TYPE;
const MENU_ICON_CLASS = SysMenuColNameEnum.MENU_ICON_CLASS;
/**
 * 实体类属性名
 */
const menuId = SysMenuColPropEnum.menuId;
const menuName = SysMenuColPropEnum.menuName;
const menuNameEn = SysMenuColPropEnum.menuNameEn;
const menuUrl = SysMenuColPropEnum.menuUrl;
const menuParentId = SysMenuColPropEnum.menuParentId;
const menuOrder = SysMenuColPropEnum.menuOrder;
const menuRemark = SysMenuColPropEnum.menuRemark;
const menuState = SysMenuColPropEnum.menuState;
const menuType = SysMenuColPropEnum.menuType;
const menuIconClass = SysMenuColPropEnum.menuIconClass;
/**
 * 主键信息
 */
const PRIMER_KEY = SysMenuTable.PRIMER_KEY;
const primerKey = SysMenuTable.primerKey;
