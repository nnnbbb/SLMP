import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_CONFIG
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @export
 * @class SysConfig
 */
@Entity({
  name: 'SYS_CONFIG',
})
export class SysConfig {
  /**
   * ID-主键
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【ID】不能为空' })
  @Max(64, { message: '【ID】长度不能超过64' })
  conId: string;
  /**
   * 参数名称
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_NAME', type: 'varchar', length: '128' })
  @IsNotEmpty({ message: '【参数名称】不能为空' })
  @Max(128, { message: '【参数名称】长度不能超过128' })
  conName: string;
  /**
   * 参数名称
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_NAME_EN', type: 'varchar', length: '128' })
  @Max(128, { message: '【参数名称】长度不能超过128' })
  conNameEn: string;
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_VALUE', type: 'varchar', length: '128' })
  @Max(128, { message: '【参数值】长度不能超过128' })
  conValue: string;
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_GROUP', type: 'varchar', length: '128' })
  @Max(128, { message: '【所属组】长度不能超过128' })
  conGroup: string;
  /**
   * 参数类型
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column('enum', { name: 'CON_PARAM_TYPE', enum: ['SYSTEM', 'USER', 'SPECIES'] })
  conParamType: string;
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_PARENT_ID', type: 'varchar', length: '64' })
  @Max(64, { message: '【父级ID】长度不能超过64' })
  conParentId: string;
  /**
   * 参数类型
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column('enum', { name: 'CON_TYPE', enum: ['DIR', 'DATA'] })
  @IsNotEmpty({ message: '【参数类型】不能为空' })
  conType: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysConfig
   */
  @Type(() => Date)
  @Column({ name: 'CON_CREATE_DATE', type: 'datetime' })
  @IsNotEmpty({ message: '【创建日期】不能为空' })
  conCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysConfig
   */
  @Type(() => Date)
  @Column({ name: 'CON_UPDATE_DATE', type: 'datetime' })
  conUpdateDate: Date;
  /**
   * 用户
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_MANAGER', type: 'varchar', length: '64' })
  @Max(64, { message: '【用户】长度不能超过64' })
  conManager: string;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_SPECIES', type: 'varchar', length: '64' })
  @Max(64, { message: '【种属】长度不能超过64' })
  conSpecies: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysConfig
   */
  @Type(() => Number)
  @Column({ name: 'CON_ORDER', type: 'int' })
  conOrder: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column('enum', { name: 'CON_STATE', enum: ['OFF', 'ON'] })
  @IsNotEmpty({ message: '【状态】不能为空' })
  conState: string;
  /**
   * 描述
   *
   * @type { string }
   * @memberof SysConfig
   */
  @Column({ name: 'CON_REMARK', type: 'varchar', length: '128' })
  @Max(128, { message: '【描述】长度不能超过128' })
  conRemark: string;
}
