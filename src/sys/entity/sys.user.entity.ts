import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Exclude } from 'class-transformer';
import { Type } from 'class-transformer';
/**
 * SYS_USER
 * @date 1/7/2021, 11:49:04 AM
 * @author jiangbin
 * @export
 * @class SysUser
 */
@Entity({
  name: 'SYS_USER',
})
export class SysUser {
  /**
   * 用户ID-主键
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【用户ID】不能为空' })
  @Max(64, { message: '【用户ID】长度不能超过64' })
  userId: string;
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_LOGIN_NAME', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【用户帐号】不能为空' })
  @Max(64, { message: '【用户帐号】长度不能超过64' })
  userLoginName: string;
  /**
   * 用户密码
   *
   * @type { string }
   * @memberof SysUser
   */
  @Exclude({ toPlainOnly: true })
  @Column({ name: 'USER_PASSWORD', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【用户密码】不能为空' })
  @Max(64, { message: '【用户密码】长度不能超过64' })
  userPassword: string;
  /**
   * userPasswordPrivate
   *
   * @type { string }
   * @memberof SysUser
   */
  @Exclude({ toPlainOnly: true })
  @Column({ name: 'USER_PASSWORD_PRIVATE', type: 'varchar', length: '64' })
  @Max(64, { message: '【userPasswordPrivate】长度不能超过64' })
  userPasswordPrivate: string;
  /**
   * 姓名缩写
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_NAME_ABBR', type: 'varchar', length: '64' })
  @Max(64, { message: '【姓名缩写】长度不能超过64' })
  userNameAbbr: string;
  /**
   * 用户名称
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_NAME', type: 'varchar', length: '64' })
  @Max(64, { message: '【用户名称】长度不能超过64' })
  userName: string;
  /**
   * 英文名
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_NAME_EN', type: 'varchar', length: '64' })
  @Max(64, { message: '【英文名】长度不能超过64' })
  userNameEn: string;
  /**
   * 性别
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_SEX', type: 'varchar', length: '64' })
  @Max(64, { message: '【性别】长度不能超过64' })
  userSex: string;
  /**
   * 用户手机号
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_PHONE', type: 'varchar', length: '64' })
  @Max(64, { message: '【用户手机号】长度不能超过64' })
  userPhone: string;
  /**
   * 用户的邮箱
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_EMAIL', type: 'varchar', length: '64' })
  @Max(64, { message: '【用户的邮箱】长度不能超过64' })
  userEmail: string;
  /**
   * 用户头像路径
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_IMAGE_URL', type: 'varchar', length: '500' })
  @Max(500, { message: '【用户头像路径】长度不能超过500' })
  userImageUrl: string;
  /**
   * 用户排序
   *
   * @type { number }
   * @memberof SysUser
   */
  @Type(() => Number)
  @Column({ name: 'USER_ORDER', type: 'int' })
  userOrder: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_STATE', type: 'varchar', length: '10' })
  @IsNotEmpty({ message: '【状态】不能为空' })
  @Max(10, { message: '【状态】长度不能超过10' })
  userState: string;
  /**
   * 用户代码
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_CODE', type: 'varchar', length: '30' })
  @IsNotEmpty({ message: '【用户代码】不能为空' })
  @Max(30, { message: '【用户代码】长度不能超过30' })
  userCode: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUser
   */
  @Type(() => Date)
  @Column({ name: 'USER_CREATE_DATE', type: 'datetime' })
  @IsNotEmpty({ message: '【创建日期】不能为空' })
  userCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUser
   */
  @Type(() => Date)
  @Column({ name: 'USER_UPDATE_DATE', type: 'datetime' })
  userUpdateDate: Date;
  /**
   * 用户最后登录时间
   *
   * @type { Date }
   * @memberof SysUser
   */
  @Type(() => Date)
  @Column({ name: 'USER_LAST_LOGIN_TIME', type: 'datetime' })
  userLastLoginTime: Date;
  /**
   * 用户最后登录Ip
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_LAST_LOGIN_IP', type: 'varchar', length: '40' })
  @Max(40, { message: '【用户最后登录Ip】长度不能超过40' })
  userLastLoginIp: string;
  /**
   * 用户所属单位
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_UNIT', type: 'varchar', length: '64' })
  @Max(64, { message: '【用户所属单位】长度不能超过64' })
  userUnit: string;
  /**
   * 用户职务/职称
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_JOB_TITLE', type: 'varchar', length: '128' })
  @Max(128, { message: '【用户职务/职称】长度不能超过128' })
  userJobTitle: string;
  /**
   * 用户通讯地址
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_ADDR', type: 'varchar', length: '256' })
  @Max(256, { message: '【用户通讯地址】长度不能超过256' })
  userAddr: string;
  /**
   * 邮编
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_POSTCODE', type: 'varchar', length: '32' })
  @Max(32, { message: '【邮编】长度不能超过32' })
  userPostcode: string;
  /**
   * 用户绑定种属
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_SAM_SPECIES', type: 'longtext' })
  userSamSpecies: string;
  /**
   * 当前种属
   *
   * @type { string }
   * @memberof SysUser
   */
  @Column({ name: 'USER_CURR_SPECIES', type: 'varchar', length: '32' })
  @Max(32, { message: '【当前种属】长度不能超过32' })
  userCurrSpecies: string;
}
