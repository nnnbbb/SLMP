import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
/**
 * SYS_ROLE_DICTIONARY
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysRoleDictionary
 */
@Entity({
  name: 'SYS_ROLE_DICTIONARY',
})
export class SysRoleDictionary {
  /**
   * 角色字典ID-主键
   *
   * @type { string }
   * @memberof SysRoleDictionary
   */
  @Column({ name: 'RD_ID', type: 'varchar', length: '64', primary: true })
  @IsNotEmpty({ message: '【角色字典ID】不能为空' })
  @Max(64, { message: '【角色字典ID】长度不能超过64' })
  rdId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleDictionary
   */
  @Column({ name: 'RD_ROLE_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【角色ID】不能为空' })
  @Max(64, { message: '【角色ID】长度不能超过64' })
  rdRoleId: string;
  /**
   * 字典ID
   *
   * @type { string }
   * @memberof SysRoleDictionary
   */
  @Column({ name: 'RD_DICTIONARY_ID', type: 'varchar', length: '64' })
  @IsNotEmpty({ message: '【字典ID】不能为空' })
  @Max(64, { message: '【字典ID】长度不能超过64' })
  rdDictionaryId: string;
}
