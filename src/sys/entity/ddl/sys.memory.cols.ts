/**
 * SYS_MEMORY-系统JVM内存表表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMemoryColNames
 */
export interface SysMemoryColNames {
  /**
   * @description
   * @field MEM_ID
   */
  MEM_ID: string; //
  /**
   * @description 总内存
   * @field MEM_TOTAL
   */
  MEM_TOTAL: number; //
  /**
   * @description 可用内存
   * @field MEM_FREE
   */
  MEM_FREE: number; //
  /**
   * @description 已使用内存
   * @field MEM_USED
   */
  MEM_USED: number; //
  /**
   * @description 内存用量是否预警
   * @field MEM_IS_WARNING
   */
  MEM_IS_WARNING: string; //
  /**
   * @description 最大内存
   * @field MEM_MAX
   */
  MEM_MAX: number; //
  /**
   * @description 创建日期
   * @field MEM_CREATE_DATE
   */
  MEM_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field MEM_UPDATE_DATE
   */
  MEM_UPDATE_DATE: Date; //
}

/**
 * SYS_MEMORY-系统JVM内存表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMemoryColProps
 */
export interface SysMemoryColProps {
  /**
   * @description memId
   * @field memId
   */
  memId: string; //
  /**
   * @description memTotal
   * @field memTotal
   */
  memTotal: number; //
  /**
   * @description memFree
   * @field memFree
   */
  memFree: number; //
  /**
   * @description memUsed
   * @field memUsed
   */
  memUsed: number; //
  /**
   * @description memIsWarning
   * @field memIsWarning
   */
  memIsWarning: string; //
  /**
   * @description memMax
   * @field memMax
   */
  memMax: number; //
  /**
   * @description memCreateDate
   * @field memCreateDate
   */
  memCreateDate: Date; //
  /**
   * @description memUpdateDate
   * @field memUpdateDate
   */
  memUpdateDate: Date; //
}

/**
 * SYS_MEMORY-系统JVM内存表表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMemoryCols
 */
export enum SysMemoryColNameEnum {
  /**
   * @description
   * @field MEM_ID
   */
  MEM_ID = 'MEM_ID', //
  /**
   * @description 总内存
   * @field MEM_TOTAL
   */
  MEM_TOTAL = 'MEM_TOTAL', //
  /**
   * @description 可用内存
   * @field MEM_FREE
   */
  MEM_FREE = 'MEM_FREE', //
  /**
   * @description 已使用内存
   * @field MEM_USED
   */
  MEM_USED = 'MEM_USED', //
  /**
   * @description 内存用量是否预警
   * @field MEM_IS_WARNING
   */
  MEM_IS_WARNING = 'MEM_IS_WARNING', //
  /**
   * @description 最大内存
   * @field MEM_MAX
   */
  MEM_MAX = 'MEM_MAX', //
  /**
   * @description 创建日期
   * @field MEM_CREATE_DATE
   */
  MEM_CREATE_DATE = 'MEM_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field MEM_UPDATE_DATE
   */
  MEM_UPDATE_DATE = 'MEM_UPDATE_DATE', //
}

/**
 * SYS_MEMORY-系统JVM内存表表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysMemoryCols
 */
export enum SysMemoryColPropEnum {
  /**
   * @description memId
   * @field memId
   */
  memId = 'memId', //
  /**
   * @description memTotal
   * @field memTotal
   */
  memTotal = 'memTotal', //
  /**
   * @description memFree
   * @field memFree
   */
  memFree = 'memFree', //
  /**
   * @description memUsed
   * @field memUsed
   */
  memUsed = 'memUsed', //
  /**
   * @description memIsWarning
   * @field memIsWarning
   */
  memIsWarning = 'memIsWarning', //
  /**
   * @description memMax
   * @field memMax
   */
  memMax = 'memMax', //
  /**
   * @description memCreateDate
   * @field memCreateDate
   */
  memCreateDate = 'memCreateDate', //
  /**
   * @description memUpdateDate
   * @field memUpdateDate
   */
  memUpdateDate = 'memUpdateDate', //
}

/**
 * SYS_MEMORY-系统JVM内存表表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysMemoryTable
 */
export enum SysMemoryTable {
  /**
   * @description MEM_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'MEM_ID',
  /**
   * @description memId
   * @field primerKey
   */
  primerKey = 'memId',
  /**
   * @description SYS_MEMORY
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_MEMORY',
  /**
   * @description SysMemory
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysMemory',
}

/**
 * SYS_MEMORY-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysMemoryColNames = Object.keys(SysMemoryColNameEnum);
/**
 * SYS_MEMORY-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysMemoryColProps = Object.keys(SysMemoryColPropEnum);
/**
 * SYS_MEMORY-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysMemoryColMap = { names: SysMemoryColNames, props: SysMemoryColProps };
