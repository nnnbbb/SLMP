/**
 * SYS_CMS_FILES-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsFilesColNames
 */
export interface SysCmsFilesColNames {
  /**
   * @description 文件编号
   * @field SC_FILE_ID
   */
  SC_FILE_ID: string; //
  /**
   * @description 文件名称
   * @field SC_FILE_NAME
   */
  SC_FILE_NAME: string; //
  /**
   * @description
   * @field SC_FILE_NAME_CN
   */
  SC_FILE_NAME_CN: string; //
  /**
   * @description 文件类型
   * @field SC_FILE_TYPE
   */
  SC_FILE_TYPE: string; //
  /**
   * @description 文件路径
   * @field SC_FILE_PATH
   */
  SC_FILE_PATH: string; //
  /**
   * @description 是否是最新
   * @field SC_FILE_IS_NEW
   */
  SC_FILE_IS_NEW: string; //
  /**
   * @description 文件上传者
   * @field SC_FILE_MANAGER
   */
  SC_FILE_MANAGER: string; //
  /**
   * @description 创建日期
   * @field SC_FILE_CREATE_DATE
   */
  SC_FILE_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field SC_FIEL_UPDATE_DATE
   */
  SC_FIEL_UPDATE_DATE: Date; //
  /**
   * @description 种属
   * @field SC_FILE_SPECIES
   */
  SC_FILE_SPECIES: string; //
}

/**
 * SYS_CMS_FILES-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsFilesColProps
 */
export interface SysCmsFilesColProps {
  /**
   * @description scFileId
   * @field scFileId
   */
  scFileId: string; //
  /**
   * @description scFileName
   * @field scFileName
   */
  scFileName: string; //
  /**
   * @description scFileNameCn
   * @field scFileNameCn
   */
  scFileNameCn: string; //
  /**
   * @description scFileType
   * @field scFileType
   */
  scFileType: string; //
  /**
   * @description scFilePath
   * @field scFilePath
   */
  scFilePath: string; //
  /**
   * @description scFileIsNew
   * @field scFileIsNew
   */
  scFileIsNew: string; //
  /**
   * @description scFileManager
   * @field scFileManager
   */
  scFileManager: string; //
  /**
   * @description scFileCreateDate
   * @field scFileCreateDate
   */
  scFileCreateDate: Date; //
  /**
   * @description scFielUpdateDate
   * @field scFielUpdateDate
   */
  scFielUpdateDate: Date; //
  /**
   * @description scFileSpecies
   * @field scFileSpecies
   */
  scFileSpecies: string; //
}

/**
 * SYS_CMS_FILES-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsFilesCols
 */
export enum SysCmsFilesColNameEnum {
  /**
   * @description 文件编号
   * @field SC_FILE_ID
   */
  SC_FILE_ID = 'SC_FILE_ID', //
  /**
   * @description 文件名称
   * @field SC_FILE_NAME
   */
  SC_FILE_NAME = 'SC_FILE_NAME', //
  /**
   * @description
   * @field SC_FILE_NAME_CN
   */
  SC_FILE_NAME_CN = 'SC_FILE_NAME_CN', //
  /**
   * @description 文件类型
   * @field SC_FILE_TYPE
   */
  SC_FILE_TYPE = 'SC_FILE_TYPE', //
  /**
   * @description 文件路径
   * @field SC_FILE_PATH
   */
  SC_FILE_PATH = 'SC_FILE_PATH', //
  /**
   * @description 是否是最新
   * @field SC_FILE_IS_NEW
   */
  SC_FILE_IS_NEW = 'SC_FILE_IS_NEW', //
  /**
   * @description 文件上传者
   * @field SC_FILE_MANAGER
   */
  SC_FILE_MANAGER = 'SC_FILE_MANAGER', //
  /**
   * @description 创建日期
   * @field SC_FILE_CREATE_DATE
   */
  SC_FILE_CREATE_DATE = 'SC_FILE_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field SC_FIEL_UPDATE_DATE
   */
  SC_FIEL_UPDATE_DATE = 'SC_FIEL_UPDATE_DATE', //
  /**
   * @description 种属
   * @field SC_FILE_SPECIES
   */
  SC_FILE_SPECIES = 'SC_FILE_SPECIES', //
}

/**
 * SYS_CMS_FILES-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysCmsFilesCols
 */
export enum SysCmsFilesColPropEnum {
  /**
   * @description scFileId
   * @field scFileId
   */
  scFileId = 'scFileId', //
  /**
   * @description scFileName
   * @field scFileName
   */
  scFileName = 'scFileName', //
  /**
   * @description scFileNameCn
   * @field scFileNameCn
   */
  scFileNameCn = 'scFileNameCn', //
  /**
   * @description scFileType
   * @field scFileType
   */
  scFileType = 'scFileType', //
  /**
   * @description scFilePath
   * @field scFilePath
   */
  scFilePath = 'scFilePath', //
  /**
   * @description scFileIsNew
   * @field scFileIsNew
   */
  scFileIsNew = 'scFileIsNew', //
  /**
   * @description scFileManager
   * @field scFileManager
   */
  scFileManager = 'scFileManager', //
  /**
   * @description scFileCreateDate
   * @field scFileCreateDate
   */
  scFileCreateDate = 'scFileCreateDate', //
  /**
   * @description scFielUpdateDate
   * @field scFielUpdateDate
   */
  scFielUpdateDate = 'scFielUpdateDate', //
  /**
   * @description scFileSpecies
   * @field scFileSpecies
   */
  scFileSpecies = 'scFileSpecies', //
}

/**
 * SYS_CMS_FILES-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysCmsFilesTable
 */
export enum SysCmsFilesTable {
  /**
   * @description SC_FILE_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'SC_FILE_ID',
  /**
   * @description scFileId
   * @field primerKey
   */
  primerKey = 'scFileId',
  /**
   * @description SYS_CMS_FILES
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_CMS_FILES',
  /**
   * @description SysCmsFiles
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysCmsFiles',
}

/**
 * SYS_CMS_FILES-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCmsFilesColNames = Object.keys(SysCmsFilesColNameEnum);
/**
 * SYS_CMS_FILES-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCmsFilesColProps = Object.keys(SysCmsFilesColPropEnum);
/**
 * SYS_CMS_FILES-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysCmsFilesColMap = { names: SysCmsFilesColNames, props: SysCmsFilesColProps };
