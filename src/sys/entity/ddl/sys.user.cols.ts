/**
 * SYS_USER-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/7/2021, 11:49:04 AM
 * @author jiangbin
 * @class SysUserColNames
 */
export interface SysUserColNames {
  /**
   * @description 用户ID
   * @field USER_ID
   */
  USER_ID: string; //
  /**
   * @description 用户帐号
   * @field USER_LOGIN_NAME
   */
  USER_LOGIN_NAME: string; //
  /**
   * @description 用户密码
   * @field USER_PASSWORD
   */
  USER_PASSWORD: string; //
  /**
   * @description
   * @field USER_PASSWORD_PRIVATE
   */
  USER_PASSWORD_PRIVATE: string; //
  /**
   * @description 姓名缩写
   * @field USER_NAME_ABBR
   */
  USER_NAME_ABBR: string; //
  /**
   * @description 用户名称
   * @field USER_NAME
   */
  USER_NAME: string; //
  /**
   * @description 英文名
   * @field USER_NAME_EN
   */
  USER_NAME_EN: string; //
  /**
   * @description 性别
   * @field USER_SEX
   */
  USER_SEX: string; //
  /**
   * @description 用户手机号
   * @field USER_PHONE
   */
  USER_PHONE: string; //
  /**
   * @description 用户的邮箱
   * @field USER_EMAIL
   */
  USER_EMAIL: string; //
  /**
   * @description 用户头像路径
   * @field USER_IMAGE_URL
   */
  USER_IMAGE_URL: string; //
  /**
   * @description 用户排序
   * @field USER_ORDER
   */
  USER_ORDER: number; //
  /**
   * @description 状态
   * @field USER_STATE
   */
  USER_STATE: string; //
  /**
   * @description 用户代码
   * @field USER_CODE
   */
  USER_CODE: string; //
  /**
   * @description 创建日期
   * @field USER_CREATE_DATE
   */
  USER_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field USER_UPDATE_DATE
   */
  USER_UPDATE_DATE: Date; //
  /**
   * @description 用户最后登录时间
   * @field USER_LAST_LOGIN_TIME
   */
  USER_LAST_LOGIN_TIME: Date; //
  /**
   * @description 用户最后登录Ip
   * @field USER_LAST_LOGIN_IP
   */
  USER_LAST_LOGIN_IP: string; //
  /**
   * @description 用户所属单位
   * @field USER_UNIT
   */
  USER_UNIT: string; //
  /**
   * @description 用户职务/职称
   * @field USER_JOB_TITLE
   */
  USER_JOB_TITLE: string; //
  /**
   * @description 用户通讯地址
   * @field USER_ADDR
   */
  USER_ADDR: string; //
  /**
   * @description 邮编
   * @field USER_POSTCODE
   */
  USER_POSTCODE: string; //
  /**
   * @description 用户绑定种属
   * @field USER_SAM_SPECIES
   */
  USER_SAM_SPECIES: string; //
  /**
   * @description 当前种属
   * @field USER_CURR_SPECIES
   */
  USER_CURR_SPECIES: string; //
}

/**
 * SYS_USER-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/7/2021, 11:49:04 AM
 * @author jiangbin
 * @class SysUserColProps
 */
export interface SysUserColProps {
  /**
   * @description userId
   * @field userId
   */
  userId: string; //
  /**
   * @description userLoginName
   * @field userLoginName
   */
  userLoginName: string; //
  /**
   * @description userPassword
   * @field userPassword
   */
  userPassword: string; //
  /**
   * @description userPasswordPrivate
   * @field userPasswordPrivate
   */
  userPasswordPrivate: string; //
  /**
   * @description userNameAbbr
   * @field userNameAbbr
   */
  userNameAbbr: string; //
  /**
   * @description userName
   * @field userName
   */
  userName: string; //
  /**
   * @description userNameEn
   * @field userNameEn
   */
  userNameEn: string; //
  /**
   * @description userSex
   * @field userSex
   */
  userSex: string; //
  /**
   * @description userPhone
   * @field userPhone
   */
  userPhone: string; //
  /**
   * @description userEmail
   * @field userEmail
   */
  userEmail: string; //
  /**
   * @description userImageUrl
   * @field userImageUrl
   */
  userImageUrl: string; //
  /**
   * @description userOrder
   * @field userOrder
   */
  userOrder: number; //
  /**
   * @description userState
   * @field userState
   */
  userState: string; //
  /**
   * @description userCode
   * @field userCode
   */
  userCode: string; //
  /**
   * @description userCreateDate
   * @field userCreateDate
   */
  userCreateDate: Date; //
  /**
   * @description userUpdateDate
   * @field userUpdateDate
   */
  userUpdateDate: Date; //
  /**
   * @description userLastLoginTime
   * @field userLastLoginTime
   */
  userLastLoginTime: Date; //
  /**
   * @description userLastLoginIp
   * @field userLastLoginIp
   */
  userLastLoginIp: string; //
  /**
   * @description userUnit
   * @field userUnit
   */
  userUnit: string; //
  /**
   * @description userJobTitle
   * @field userJobTitle
   */
  userJobTitle: string; //
  /**
   * @description userAddr
   * @field userAddr
   */
  userAddr: string; //
  /**
   * @description userPostcode
   * @field userPostcode
   */
  userPostcode: string; //
  /**
   * @description userSamSpecies
   * @field userSamSpecies
   */
  userSamSpecies: string; //
  /**
   * @description userCurrSpecies
   * @field userCurrSpecies
   */
  userCurrSpecies: string; //
}

/**
 * SYS_USER-表列名
 * @date 1/7/2021, 11:49:04 AM
 * @author jiangbin
 * @class SysUserCols
 */
export enum SysUserColNameEnum {
  /**
   * @description 用户ID
   * @field USER_ID
   */
  USER_ID = 'USER_ID', //
  /**
   * @description 用户帐号
   * @field USER_LOGIN_NAME
   */
  USER_LOGIN_NAME = 'USER_LOGIN_NAME', //
  /**
   * @description 用户密码
   * @field USER_PASSWORD
   */
  USER_PASSWORD = 'USER_PASSWORD', //
  /**
   * @description
   * @field USER_PASSWORD_PRIVATE
   */
  USER_PASSWORD_PRIVATE = 'USER_PASSWORD_PRIVATE', //
  /**
   * @description 姓名缩写
   * @field USER_NAME_ABBR
   */
  USER_NAME_ABBR = 'USER_NAME_ABBR', //
  /**
   * @description 用户名称
   * @field USER_NAME
   */
  USER_NAME = 'USER_NAME', //
  /**
   * @description 英文名
   * @field USER_NAME_EN
   */
  USER_NAME_EN = 'USER_NAME_EN', //
  /**
   * @description 性别
   * @field USER_SEX
   */
  USER_SEX = 'USER_SEX', //
  /**
   * @description 用户手机号
   * @field USER_PHONE
   */
  USER_PHONE = 'USER_PHONE', //
  /**
   * @description 用户的邮箱
   * @field USER_EMAIL
   */
  USER_EMAIL = 'USER_EMAIL', //
  /**
   * @description 用户头像路径
   * @field USER_IMAGE_URL
   */
  USER_IMAGE_URL = 'USER_IMAGE_URL', //
  /**
   * @description 用户排序
   * @field USER_ORDER
   */
  USER_ORDER = 'USER_ORDER', //
  /**
   * @description 状态
   * @field USER_STATE
   */
  USER_STATE = 'USER_STATE', //
  /**
   * @description 用户代码
   * @field USER_CODE
   */
  USER_CODE = 'USER_CODE', //
  /**
   * @description 创建日期
   * @field USER_CREATE_DATE
   */
  USER_CREATE_DATE = 'USER_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field USER_UPDATE_DATE
   */
  USER_UPDATE_DATE = 'USER_UPDATE_DATE', //
  /**
   * @description 用户最后登录时间
   * @field USER_LAST_LOGIN_TIME
   */
  USER_LAST_LOGIN_TIME = 'USER_LAST_LOGIN_TIME', //
  /**
   * @description 用户最后登录Ip
   * @field USER_LAST_LOGIN_IP
   */
  USER_LAST_LOGIN_IP = 'USER_LAST_LOGIN_IP', //
  /**
   * @description 用户所属单位
   * @field USER_UNIT
   */
  USER_UNIT = 'USER_UNIT', //
  /**
   * @description 用户职务/职称
   * @field USER_JOB_TITLE
   */
  USER_JOB_TITLE = 'USER_JOB_TITLE', //
  /**
   * @description 用户通讯地址
   * @field USER_ADDR
   */
  USER_ADDR = 'USER_ADDR', //
  /**
   * @description 邮编
   * @field USER_POSTCODE
   */
  USER_POSTCODE = 'USER_POSTCODE', //
  /**
   * @description 用户绑定种属
   * @field USER_SAM_SPECIES
   */
  USER_SAM_SPECIES = 'USER_SAM_SPECIES', //
  /**
   * @description 当前种属
   * @field USER_CURR_SPECIES
   */
  USER_CURR_SPECIES = 'USER_CURR_SPECIES', //
}

/**
 * SYS_USER-表列属性名
 * @date 1/7/2021, 11:49:04 AM
 * @author jiangbin
 * @class SysUserCols
 */
export enum SysUserColPropEnum {
  /**
   * @description userId
   * @field userId
   */
  userId = 'userId', //
  /**
   * @description userLoginName
   * @field userLoginName
   */
  userLoginName = 'userLoginName', //
  /**
   * @description userPassword
   * @field userPassword
   */
  userPassword = 'userPassword', //
  /**
   * @description userPasswordPrivate
   * @field userPasswordPrivate
   */
  userPasswordPrivate = 'userPasswordPrivate', //
  /**
   * @description userNameAbbr
   * @field userNameAbbr
   */
  userNameAbbr = 'userNameAbbr', //
  /**
   * @description userName
   * @field userName
   */
  userName = 'userName', //
  /**
   * @description userNameEn
   * @field userNameEn
   */
  userNameEn = 'userNameEn', //
  /**
   * @description userSex
   * @field userSex
   */
  userSex = 'userSex', //
  /**
   * @description userPhone
   * @field userPhone
   */
  userPhone = 'userPhone', //
  /**
   * @description userEmail
   * @field userEmail
   */
  userEmail = 'userEmail', //
  /**
   * @description userImageUrl
   * @field userImageUrl
   */
  userImageUrl = 'userImageUrl', //
  /**
   * @description userOrder
   * @field userOrder
   */
  userOrder = 'userOrder', //
  /**
   * @description userState
   * @field userState
   */
  userState = 'userState', //
  /**
   * @description userCode
   * @field userCode
   */
  userCode = 'userCode', //
  /**
   * @description userCreateDate
   * @field userCreateDate
   */
  userCreateDate = 'userCreateDate', //
  /**
   * @description userUpdateDate
   * @field userUpdateDate
   */
  userUpdateDate = 'userUpdateDate', //
  /**
   * @description userLastLoginTime
   * @field userLastLoginTime
   */
  userLastLoginTime = 'userLastLoginTime', //
  /**
   * @description userLastLoginIp
   * @field userLastLoginIp
   */
  userLastLoginIp = 'userLastLoginIp', //
  /**
   * @description userUnit
   * @field userUnit
   */
  userUnit = 'userUnit', //
  /**
   * @description userJobTitle
   * @field userJobTitle
   */
  userJobTitle = 'userJobTitle', //
  /**
   * @description userAddr
   * @field userAddr
   */
  userAddr = 'userAddr', //
  /**
   * @description userPostcode
   * @field userPostcode
   */
  userPostcode = 'userPostcode', //
  /**
   * @description userSamSpecies
   * @field userSamSpecies
   */
  userSamSpecies = 'userSamSpecies', //
  /**
   * @description userCurrSpecies
   * @field userCurrSpecies
   */
  userCurrSpecies = 'userCurrSpecies', //
}

/**
 * SYS_USER-表信息
 * @date 1/7/2021, 11:49:04 AM
 * @author jiangbin
 * @export
 * @class SysUserTable
 */
export enum SysUserTable {
  /**
   * @description USER_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'USER_ID',
  /**
   * @description userId
   * @field primerKey
   */
  primerKey = 'userId',
  /**
   * @description SYS_USER
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_USER',
  /**
   * @description SysUser
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysUser',
}

/**
 * SYS_USER-列名数组
 * @author jiangbin
 * @date 1/7/2021, 11:49:04 AM
 **/
export const SysUserColNames = Object.keys(SysUserColNameEnum);
/**
 * SYS_USER-列属性名数组
 * @author jiangbin
 * @date 1/7/2021, 11:49:04 AM
 **/
export const SysUserColProps = Object.keys(SysUserColPropEnum);
/**
 * SYS_USER-列名和列属性名映射表
 * @author jiangbin
 * @date 1/7/2021, 11:49:04 AM
 **/
export const SysUserColMap = { names: SysUserColNames, props: SysUserColProps };
