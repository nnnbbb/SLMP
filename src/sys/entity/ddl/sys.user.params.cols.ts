/**
 * SYS_USER_PARAMS-用户参数表表列名接口定义，用于为某些类提供参数名限制
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @class SysUserParamsColNames
 */
export interface SysUserParamsColNames {
  /**
   * @description 主键
   * @field UP_ID
   */
  UP_ID: string; //
  /**
   * @description 帐号
   * @field UP_LOGIN_NAME
   */
  UP_LOGIN_NAME: string; //
  /**
   * @description 参数名
   * @field UP_PARAM_NAME
   */
  UP_PARAM_NAME: string; //
  /**
   * @description 参数值
   * @field UP_PARAM_VALUE
   */
  UP_PARAM_VALUE: string; //
  /**
   * @description 创建日期
   * @field UP_CREATE_DATE
   */
  UP_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field UP_UPDATE_DATE
   */
  UP_UPDATE_DATE: Date; //
}
/**
 * SYS_USER_PARAMS-用户参数表表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @class SysUserParamsColProps
 */
export interface SysUserParamsColProps {
  /**
   * @description upId
   * @field upId
   */
  upId: string; //
  /**
   * @description upLoginName
   * @field upLoginName
   */
  upLoginName: string; //
  /**
   * @description upParamName
   * @field upParamName
   */
  upParamName: string; //
  /**
   * @description upParamValue
   * @field upParamValue
   */
  upParamValue: string; //
  /**
   * @description upCreateDate
   * @field upCreateDate
   */
  upCreateDate: Date; //
  /**
   * @description upUpdateDate
   * @field upUpdateDate
   */
  upUpdateDate: Date; //
}
/**
 * SYS_USER_PARAMS-用户参数表表列名
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @class SysUserParamsCols
 */
export enum SysUserParamsColNameEnum {
  /**
   * @description 主键
   * @field UP_ID
   */
  UP_ID = 'UP_ID', //
  /**
   * @description 帐号
   * @field UP_LOGIN_NAME
   */
  UP_LOGIN_NAME = 'UP_LOGIN_NAME', //
  /**
   * @description 参数名
   * @field UP_PARAM_NAME
   */
  UP_PARAM_NAME = 'UP_PARAM_NAME', //
  /**
   * @description 参数值
   * @field UP_PARAM_VALUE
   */
  UP_PARAM_VALUE = 'UP_PARAM_VALUE', //
  /**
   * @description 创建日期
   * @field UP_CREATE_DATE
   */
  UP_CREATE_DATE = 'UP_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field UP_UPDATE_DATE
   */
  UP_UPDATE_DATE = 'UP_UPDATE_DATE', //
}
/**
 * SYS_USER_PARAMS-用户参数表表列属性名
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @class SysUserParamsCols
 */
export enum SysUserParamsColPropEnum {
  /**
   * @description upId
   * @field upId
   */
  upId = 'upId', //
  /**
   * @description upLoginName
   * @field upLoginName
   */
  upLoginName = 'upLoginName', //
  /**
   * @description upParamName
   * @field upParamName
   */
  upParamName = 'upParamName', //
  /**
   * @description upParamValue
   * @field upParamValue
   */
  upParamValue = 'upParamValue', //
  /**
   * @description upCreateDate
   * @field upCreateDate
   */
  upCreateDate = 'upCreateDate', //
  /**
   * @description upUpdateDate
   * @field upUpdateDate
   */
  upUpdateDate = 'upUpdateDate', //
}
/**
 * SYS_USER_PARAMS-用户参数表表信息
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export
 * @class SysUserParamsTable
 */
export enum SysUserParamsTable {
  /**
   * @description UP_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'UP_ID',
  /**
   * @description upId
   * @field primerKey
   */
  primerKey = 'upId',
  /**
   * @description SYS_USER_PARAMS
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_USER_PARAMS',
  /**
   * @description SysUserParams
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysUserParams',
}
/**
 * SYS_USER_PARAMS-列名数组
 * @author jiangbin
 * @date 4/3/2021, 11:06:45 AM
 **/
export const SysUserParamsColNames = Object.keys(SysUserParamsColNameEnum);
/**
 * SYS_USER_PARAMS-列属性名数组
 * @author jiangbin
 * @date 4/3/2021, 11:06:45 AM
 **/
export const SysUserParamsColProps = Object.keys(SysUserParamsColPropEnum);
/**
 * SYS_USER_PARAMS-列名和列属性名映射表
 * @author jiangbin
 * @date 4/3/2021, 11:06:45 AM
 **/
export const SysUserParamsColMap = { names: SysUserParamsColNames, props: SysUserParamsColProps };
