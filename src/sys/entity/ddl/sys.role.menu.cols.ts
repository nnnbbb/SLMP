/**
 * SYS_ROLE_MENU-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleMenuColNames
 */
export interface SysRoleMenuColNames {
  /**
   * @description 角色菜单ID
   * @field RM_ID
   */
  RM_ID: string; //
  /**
   * @description 角色ID
   * @field RM_ROLE_ID
   */
  RM_ROLE_ID: string; //
  /**
   * @description 菜单ID
   * @field RM_MENU_ID
   */
  RM_MENU_ID: string; //
}

/**
 * SYS_ROLE_MENU-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleMenuColProps
 */
export interface SysRoleMenuColProps {
  /**
   * @description rmId
   * @field rmId
   */
  rmId: string; //
  /**
   * @description rmRoleId
   * @field rmRoleId
   */
  rmRoleId: string; //
  /**
   * @description rmMenuId
   * @field rmMenuId
   */
  rmMenuId: string; //
}

/**
 * SYS_ROLE_MENU-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleMenuCols
 */
export enum SysRoleMenuColNameEnum {
  /**
   * @description 角色菜单ID
   * @field RM_ID
   */
  RM_ID = 'RM_ID', //
  /**
   * @description 角色ID
   * @field RM_ROLE_ID
   */
  RM_ROLE_ID = 'RM_ROLE_ID', //
  /**
   * @description 菜单ID
   * @field RM_MENU_ID
   */
  RM_MENU_ID = 'RM_MENU_ID', //
}

/**
 * SYS_ROLE_MENU-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysRoleMenuCols
 */
export enum SysRoleMenuColPropEnum {
  /**
   * @description rmId
   * @field rmId
   */
  rmId = 'rmId', //
  /**
   * @description rmRoleId
   * @field rmRoleId
   */
  rmRoleId = 'rmRoleId', //
  /**
   * @description rmMenuId
   * @field rmMenuId
   */
  rmMenuId = 'rmMenuId', //
}

/**
 * SYS_ROLE_MENU-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysRoleMenuTable
 */
export enum SysRoleMenuTable {
  /**
   * @description RM_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'RM_ID',
  /**
   * @description rmId
   * @field primerKey
   */
  primerKey = 'rmId',
  /**
   * @description SYS_ROLE_MENU
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_ROLE_MENU',
  /**
   * @description SysRoleMenu
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysRoleMenu',
}

/**
 * SYS_ROLE_MENU-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleMenuColNames = Object.keys(SysRoleMenuColNameEnum);
/**
 * SYS_ROLE_MENU-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleMenuColProps = Object.keys(SysRoleMenuColPropEnum);
/**
 * SYS_ROLE_MENU-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysRoleMenuColMap = { names: SysRoleMenuColNames, props: SysRoleMenuColProps };
