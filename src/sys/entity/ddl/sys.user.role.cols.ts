/**
 * SYS_USER_ROLE-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserRoleColNames
 */
export interface SysUserRoleColNames {
  /**
   * @description 用户角色ID
   * @field UR_ID
   */
  UR_ID: string; //
  /**
   * @description 角色ID
   * @field UR_ROLE_ID
   */
  UR_ROLE_ID: string; //
  /**
   * @description 用户帐号
   * @field UR_USER_LOGIN_NAME
   */
  UR_USER_LOGIN_NAME: string; //
}

/**
 * SYS_USER_ROLE-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserRoleColProps
 */
export interface SysUserRoleColProps {
  /**
   * @description urId
   * @field urId
   */
  urId: string; //
  /**
   * @description urRoleId
   * @field urRoleId
   */
  urRoleId: string; //
  /**
   * @description urUserLoginName
   * @field urUserLoginName
   */
  urUserLoginName: string; //
}

/**
 * SYS_USER_ROLE-表列名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserRoleCols
 */
export enum SysUserRoleColNameEnum {
  /**
   * @description 用户角色ID
   * @field UR_ID
   */
  UR_ID = 'UR_ID', //
  /**
   * @description 角色ID
   * @field UR_ROLE_ID
   */
  UR_ROLE_ID = 'UR_ROLE_ID', //
  /**
   * @description 用户帐号
   * @field UR_USER_LOGIN_NAME
   */
  UR_USER_LOGIN_NAME = 'UR_USER_LOGIN_NAME', //
}

/**
 * SYS_USER_ROLE-表列属性名
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @class SysUserRoleCols
 */
export enum SysUserRoleColPropEnum {
  /**
   * @description urId
   * @field urId
   */
  urId = 'urId', //
  /**
   * @description urRoleId
   * @field urRoleId
   */
  urRoleId = 'urRoleId', //
  /**
   * @description urUserLoginName
   * @field urUserLoginName
   */
  urUserLoginName = 'urUserLoginName', //
}

/**
 * SYS_USER_ROLE-表信息
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export
 * @class SysUserRoleTable
 */
export enum SysUserRoleTable {
  /**
   * @description UR_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'UR_ID',
  /**
   * @description urId
   * @field primerKey
   */
  primerKey = 'urId',
  /**
   * @description SYS_USER_ROLE
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_USER_ROLE',
  /**
   * @description SysUserRole
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysUserRole',
}

/**
 * SYS_USER_ROLE-列名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUserRoleColNames = Object.keys(SysUserRoleColNameEnum);
/**
 * SYS_USER_ROLE-列属性名数组
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUserRoleColProps = Object.keys(SysUserRoleColPropEnum);
/**
 * SYS_USER_ROLE-列名和列属性名映射表
 * @author jiangbin
 * @date 1/2/2021, 10:41:20 AM
 **/
export const SysUserRoleColMap = { names: SysUserRoleColNames, props: SysUserRoleColProps };
