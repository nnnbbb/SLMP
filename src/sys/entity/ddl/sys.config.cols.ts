/**
 * SYS_CONFIG-表列名接口定义，用于为某些类提供参数名限制
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @class SysConfigColNames
 */
export interface SysConfigColNames {
  /**
   * @description ID
   * @field CON_ID
   */
  CON_ID: string; //
  /**
   * @description 参数名称
   * @field CON_NAME
   */
  CON_NAME: string; //
  /**
   * @description 参数名称
   * @field CON_NAME_EN
   */
  CON_NAME_EN: string; //
  /**
   * @description 参数值
   * @field CON_VALUE
   */
  CON_VALUE: string; //
  /**
   * @description 所属组
   * @field CON_GROUP
   */
  CON_GROUP: string; //
  /**
   * @description 参数类型
   * @field CON_PARAM_TYPE
   */
  CON_PARAM_TYPE: string; //
  /**
   * @description 父级ID
   * @field CON_PARENT_ID
   */
  CON_PARENT_ID: string; //
  /**
   * @description 参数类型
   * @field CON_TYPE
   */
  CON_TYPE: string; //
  /**
   * @description 创建日期
   * @field CON_CREATE_DATE
   */
  CON_CREATE_DATE: Date; //
  /**
   * @description 更新日期
   * @field CON_UPDATE_DATE
   */
  CON_UPDATE_DATE: Date; //
  /**
   * @description 用户
   * @field CON_MANAGER
   */
  CON_MANAGER: string; //
  /**
   * @description 种属
   * @field CON_SPECIES
   */
  CON_SPECIES: string; //
  /**
   * @description 排序
   * @field CON_ORDER
   */
  CON_ORDER: number; //
  /**
   * @description 状态
   * @field CON_STATE
   */
  CON_STATE: string; //
  /**
   * @description 描述
   * @field CON_REMARK
   */
  CON_REMARK: string; //
}

/**
 * SYS_CONFIG-表列名的属性名接口定义，用于为某些类提供参数名限制
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @class SysConfigColProps
 */
export interface SysConfigColProps {
  /**
   * @description conId
   * @field conId
   */
  conId: string; //
  /**
   * @description conName
   * @field conName
   */
  conName: string; //
  /**
   * @description conNameEn
   * @field conNameEn
   */
  conNameEn: string; //
  /**
   * @description conValue
   * @field conValue
   */
  conValue: string; //
  /**
   * @description conGroup
   * @field conGroup
   */
  conGroup: string; //
  /**
   * @description conParamType
   * @field conParamType
   */
  conParamType: string; //
  /**
   * @description conParentId
   * @field conParentId
   */
  conParentId: string; //
  /**
   * @description conType
   * @field conType
   */
  conType: string; //
  /**
   * @description conCreateDate
   * @field conCreateDate
   */
  conCreateDate: Date; //
  /**
   * @description conUpdateDate
   * @field conUpdateDate
   */
  conUpdateDate: Date; //
  /**
   * @description conManager
   * @field conManager
   */
  conManager: string; //
  /**
   * @description conSpecies
   * @field conSpecies
   */
  conSpecies: string; //
  /**
   * @description conOrder
   * @field conOrder
   */
  conOrder: number; //
  /**
   * @description conState
   * @field conState
   */
  conState: string; //
  /**
   * @description conRemark
   * @field conRemark
   */
  conRemark: string; //
}

/**
 * SYS_CONFIG-表列名
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @class SysConfigCols
 */
export enum SysConfigColNameEnum {
  /**
   * @description ID
   * @field CON_ID
   */
  CON_ID = 'CON_ID', //
  /**
   * @description 参数名称
   * @field CON_NAME
   */
  CON_NAME = 'CON_NAME', //
  /**
   * @description 参数名称
   * @field CON_NAME_EN
   */
  CON_NAME_EN = 'CON_NAME_EN', //
  /**
   * @description 参数值
   * @field CON_VALUE
   */
  CON_VALUE = 'CON_VALUE', //
  /**
   * @description 所属组
   * @field CON_GROUP
   */
  CON_GROUP = 'CON_GROUP', //
  /**
   * @description 参数类型
   * @field CON_PARAM_TYPE
   */
  CON_PARAM_TYPE = 'CON_PARAM_TYPE', //
  /**
   * @description 父级ID
   * @field CON_PARENT_ID
   */
  CON_PARENT_ID = 'CON_PARENT_ID', //
  /**
   * @description 参数类型
   * @field CON_TYPE
   */
  CON_TYPE = 'CON_TYPE', //
  /**
   * @description 创建日期
   * @field CON_CREATE_DATE
   */
  CON_CREATE_DATE = 'CON_CREATE_DATE', //
  /**
   * @description 更新日期
   * @field CON_UPDATE_DATE
   */
  CON_UPDATE_DATE = 'CON_UPDATE_DATE', //
  /**
   * @description 用户
   * @field CON_MANAGER
   */
  CON_MANAGER = 'CON_MANAGER', //
  /**
   * @description 种属
   * @field CON_SPECIES
   */
  CON_SPECIES = 'CON_SPECIES', //
  /**
   * @description 排序
   * @field CON_ORDER
   */
  CON_ORDER = 'CON_ORDER', //
  /**
   * @description 状态
   * @field CON_STATE
   */
  CON_STATE = 'CON_STATE', //
  /**
   * @description 描述
   * @field CON_REMARK
   */
  CON_REMARK = 'CON_REMARK', //
}

/**
 * SYS_CONFIG-表列属性名
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @class SysConfigCols
 */
export enum SysConfigColPropEnum {
  /**
   * @description conId
   * @field conId
   */
  conId = 'conId', //
  /**
   * @description conName
   * @field conName
   */
  conName = 'conName', //
  /**
   * @description conNameEn
   * @field conNameEn
   */
  conNameEn = 'conNameEn', //
  /**
   * @description conValue
   * @field conValue
   */
  conValue = 'conValue', //
  /**
   * @description conGroup
   * @field conGroup
   */
  conGroup = 'conGroup', //
  /**
   * @description conParamType
   * @field conParamType
   */
  conParamType = 'conParamType', //
  /**
   * @description conParentId
   * @field conParentId
   */
  conParentId = 'conParentId', //
  /**
   * @description conType
   * @field conType
   */
  conType = 'conType', //
  /**
   * @description conCreateDate
   * @field conCreateDate
   */
  conCreateDate = 'conCreateDate', //
  /**
   * @description conUpdateDate
   * @field conUpdateDate
   */
  conUpdateDate = 'conUpdateDate', //
  /**
   * @description conManager
   * @field conManager
   */
  conManager = 'conManager', //
  /**
   * @description conSpecies
   * @field conSpecies
   */
  conSpecies = 'conSpecies', //
  /**
   * @description conOrder
   * @field conOrder
   */
  conOrder = 'conOrder', //
  /**
   * @description conState
   * @field conState
   */
  conState = 'conState', //
  /**
   * @description conRemark
   * @field conRemark
   */
  conRemark = 'conRemark', //
}

/**
 * SYS_CONFIG-表信息
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @export
 * @class SysConfigTable
 */
export enum SysConfigTable {
  /**
   * @description CON_ID
   * @field PRIMER_KEY
   */
  PRIMER_KEY = 'CON_ID',
  /**
   * @description conId
   * @field primerKey
   */
  primerKey = 'conId',
  /**
   * @description SYS_CONFIG
   * @field TABLE_NAME
   */
  TABLE_NAME = 'SYS_CONFIG',
  /**
   * @description SysConfig
   * @field ENTITY_NAME
   */
  ENTITY_NAME = 'SysConfig',
}

/**
 * SYS_CONFIG-列名数组
 * @author jiangbin
 * @date 1/7/2021, 10:05:34 AM
 **/
export const SysConfigColNames = Object.keys(SysConfigColNameEnum);
/**
 * SYS_CONFIG-列属性名数组
 * @author jiangbin
 * @date 1/7/2021, 10:05:34 AM
 **/
export const SysConfigColProps = Object.keys(SysConfigColPropEnum);
/**
 * SYS_CONFIG-列名和列属性名映射表
 * @author jiangbin
 * @date 1/7/2021, 10:05:34 AM
 **/
export const SysConfigColMap = { names: SysConfigColNames, props: SysConfigColProps };
