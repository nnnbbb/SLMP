import { Entity, Column } from 'typeorm';
import { Max, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
/**
 * SYS_UPDATE
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export
 * @class SysUpdate
 */
@Entity({
  name: 'SYS_UPDATE',
})
export class SysUpdate {
  /**
   * 系统升级日志主键ID-主键
   *
   * @type { string }
   * @memberof SysUpdate
   */
  @Column({ name: 'UPDATE_ID', type: 'varchar', length: '36', primary: true })
  @IsNotEmpty({ message: '【系统升级日志主键ID】不能为空' })
  @Max(36, { message: '【系统升级日志主键ID】长度不能超过36' })
  updateId: string;
  /**
   * 升级版本号
   *
   * @type { string }
   * @memberof SysUpdate
   */
  @Column({ name: 'UPDATE_VERSION', type: 'varchar', length: '36' })
  @Max(36, { message: '【升级版本号】长度不能超过36' })
  updateVersion: string;
  /**
   * 升级内容
   *
   * @type { string }
   * @memberof SysUpdate
   */
  @Column({ name: 'UPDATE_CONTENT', type: 'varchar', length: '4986' })
  @Max(4986, { message: '【升级内容】长度不能超过4986' })
  updateContent: string;
  /**
   * 升级负责人
   *
   * @type { string }
   * @memberof SysUpdate
   */
  @Column({ name: 'UPDATE_MANAGER', type: 'varchar', length: '36' })
  @Max(36, { message: '【升级负责人】长度不能超过36' })
  updateManager: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUpdate
   */
  @Type(() => Date)
  @Column({ name: 'UPDATE_CREATE_TIME', type: 'datetime' })
  updateCreateTime: Date;
}
