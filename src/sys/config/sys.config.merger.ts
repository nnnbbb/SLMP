import { SysConfig } from '../entity/sys.config.entity';
import { DATA_NODE, DIR_NODE, ROOT_NODE_ID } from '../../common/tree/tree.node.type';
import { SysConfigCDto } from '../dto/create/sys.config.cdto';
import { ArrayUtils } from '../../common/utils/array.utils';
import { Mapper } from '../../common/mapper/mapper';

export declare type SysConfigMergerDto = SysConfig | Partial<SysConfig> | SysConfigCDto | Partial<SysConfigCDto>;

export declare type SysConfigMergerDtos = SysConfigMergerDto[];

/**
 * <pre>合并新参数到老参数列表中，用于提供参数的增量更新，即已有参数不动，
 * 只对照新老参数表将新参数进行初始化，合并逻辑如下：
 * 1、若新参数表和老参数表均不存在，则直接返回null
 * 2、若新参数表不存在，则直接返回老参数表
 * 3、若老参数表不存在，则直接返回新参数表
 * 4、从根结点开始合并参数：
 *    a、若新参数表中存在且老参数表中不存在则添加到老参数表中并构建关联关系
 *    b、若新参数表中存在且老参数表中也存在，则跳过节点
 * </pre>
 * @author: jiangbin
 * @date: 2021-03-16 14:42:06
 **/
export const SysConfigMerger = {
  /**
   * 合并新数据到已有参数中
   * @param news 原参数列表
   * @param olds 已有参数列表
   * @return 合并后的参数列表
   * @author: jiangbin
   * @date: 2021-03-16 15:14:03
   **/
  merger(news: SysConfigMergerDtos, olds: SysConfigMergerDtos) {
    if ((!olds || olds.length == 0) && (!news || news.length == 0)) {
      return null;
    }
    if (!olds || olds.length == 0) {
      return this.addRoot(news);
    }
    if (!news || news.length == 0) {
      return this.addRoot(olds);
    }

    let targets = [];

    //处理根节点
    let root = this.mergerRoot(news, olds);
    targets.push(root);

    //处理目录节点
    let dirs = this.mergerDir(news, olds);
    dirs?.length > 0 && (targets = targets.concat(dirs));

    //合并数据节点
    return targets;
  },
  /**
   * 合并ROOT节点
   * @param news 原参数列表
   * @param olds 已有参数列表
   * @return 合并后的参数列表
   * @author: jiangbin
   * @date: 2021-03-16 15:52:51
   **/
  mergerRoot(news: SysConfigMergerDtos, olds: SysConfigMergerDtos) {
    let oroot = olds.filter((item) => item.conId === ROOT_NODE_ID);
    if (oroot?.length > 0 && oroot[0]['conId']) return oroot[0];
    let nroot = news.filter((item) => item.conId === ROOT_NODE_ID);
    return nroot?.length > 0 && oroot[0]['conId'] ? nroot[0] : this.createRoot();
  },
  /**
   * 合并DIR节点
   * @param news 原参数列表
   * @param olds 已有参数列表
   * @return 合并后的参数列表
   * @author: jiangbin
   * @date: 2021-03-16 15:53:12
   **/
  mergerDir(news: SysConfigMergerDtos, olds: SysConfigMergerDtos) {
    let odirs = olds.filter((item) => item.conParentId === ROOT_NODE_ID);
    let ndirs = news.filter((item) => item.conParentId === ROOT_NODE_ID);
    if (ArrayUtils.isEmpty(odirs) && ArrayUtils.isEmpty(ndirs)) {
      return null;
    }
    if (ArrayUtils.isEmpty(odirs)) {
      return ndirs;
    }
    if (ArrayUtils.isEmpty(ndirs)) {
      return odirs;
    }

    //构建分组映射表
    let odmap = new Mapper<string, SysConfigMergerDto>();
    odmap.addList(odirs, (record) => record.conGroup);

    let ndmap = new Mapper<string, SysConfigMergerDto>();
    ndmap.addList(ndirs, (record) => record.conGroup);

    //移除已有目录节点列表
    ndmap.deleteList(odmap.getKeys());

    //得到新插入的目录节点列表
    ndirs = ndmap.getAllValues();

    let targets = [];

    //合并目录节点列表
    if (odirs?.length > 0) {
      targets = targets.concat(odirs);
    }
    if (ndirs?.length > 0) {
      targets = targets.concat(ndirs);
    }

    //目录节点排序
    targets = this.sortNodes(targets);

    //设置目录节点序号
    this.fillNodeOrder(targets);

    //添加新插入目录节点列表
    let nleafs = this.mergerNewDirLeafs(ndirs, news);
    if (nleafs?.length > 0) {
      targets = targets.concat(nleafs);
    }

    //老字典不作变动
    let oleafs = this.mergerOldDirLeafs(odirs, olds, news);
    if (oleafs?.length > 0) {
      targets = targets.concat(oleafs);
    }

    return targets;
  },

  /**
   * 查找指定分组的叶子节点
   * @param items 参数列表
   * @param conGroup 参数分组
   * @return
   * @author: jiangbin
   * @date: 2021-03-16 16:47:25
   **/
  findLeafByGroup(items: SysConfigMergerDtos, conGroup: string) {
    return items.filter((item) => item.conGroup === conGroup && item.conType === DATA_NODE);
  },

  /**
   * 合并新目录节点的所有子节点列表
   *
   * @param ndirs 新目录节点列表
   * @param news 新节点列表
   * @return
   * @author jiang
   * @date 2021-03-27 17:04:46
   **/
  mergerNewDirLeafs(ndirs: SysConfigMergerDtos, news: SysConfigMergerDtos) {
    let targets = [];
    ndirs?.length > 0 &&
      ndirs.forEach((ndir) => {
        let leafs = this.findLeafByGroup(news, ndir.conGroup);
        if (leafs?.length > 0) {
          leafs = this.sortNodes(leafs);
          this.fillNodeOrder(leafs);
          targets = targets.concat(leafs);
        }
      });
    return targets;
  },

  /**
   * 合并老目录节点下的叶子节点，若存在新的叶子节点则合并之
   *
   * @param odirs 已存在的老目录节点列表
   * @param olds 老字典节点列表
   * @param news 新字典节点列表
   * @return
   * @author jiang
   * @date 2021-03-27 17:08:02
   **/
  mergerOldDirLeafs(odirs: SysConfigMergerDtos, olds: SysConfigMergerDtos, news: SysConfigMergerDtos) {
    let targets = [];
    odirs?.length > 0 &&
      odirs.forEach((odir) => {
        //获取老叶子节点列表
        let oleafs = this.findLeafByGroup(olds, odir.conGroup);
        let omap = new Mapper<string, SysConfigMergerDto>();
        omap.addList(oleafs, (record) => record.conName);

        //获取新叶子节点列表
        let nleafs = this.findLeafByGroup(news, odir.conGroup);
        let nmap = new Mapper<string, SysConfigMergerDto>();
        nmap.addList(nleafs, (record) => record.conName);

        //得到新插入的叶子节点
        nmap.deleteList(omap.getKeys());
        nleafs = nmap.getAllValues();

        //设置父节点ID为老节点目录ID
        nleafs?.length > 0 && nleafs.forEach((leaf) => (leaf.conParentId = odir.conId));

        let nodes = [];

        //合并数据
        if (oleafs?.length > 0) {
          nodes = nodes.concat(oleafs);
        }
        if (nleafs?.length > 0) {
          nodes = nodes.concat(nleafs);
        }

        //节点排序
        nodes = this.sortNodes(nodes);

        //设置节点序号
        this.fillNodeOrder(nodes);

        nodes?.length > 0 && (targets = targets.concat(nodes));
      });

    return targets;
  },

  /**
   * 添加根结点
   * @param dtos
   * @return
   * @author: jiangbin
   * @date: 2021-03-17 09:57:43
   **/
  addRoot(dtos: SysConfigMergerDtos) {
    if (dtos?.length > 0) {
      let sroot = dtos.filter((dto) => dto.conId === ROOT_NODE_ID);
      let targets = [];
      if (!sroot || sroot.length == 0) {
        targets.push(this.createRoot());
      }
      targets = targets.concat(dtos);
      return targets;
    }
    return dtos;
  },

  /**
   * 创建根结点
   * @author: jiangbin
   * @date: 2021-03-17 10:07:57
   **/
  createRoot() {
    const record = new SysConfig();
    record.conId = ROOT_NODE_ID;
    record.conName = 'ROOT';
    record.conNameEn = 'ROOT';
    record.conValue = null;
    record.conGroup = null;
    record.conParamType = null;
    record.conType = DIR_NODE;
    record.conManager = null;
    record.conSpecies = null;
    record.conState = 'ON';
    record.conRemark = null;
    return record;
  },

  /**
   * 节点列表排序
   *
   * @param nodes
   * @return
   * @author jiang
   * @date 2021-03-27 17:25:20
   **/
  sortNodes(nodes: SysConfigMergerDtos) {
    return nodes?.length > 0
      ? nodes.sort((source, target) => {
          if (source.conOrder > target.conOrder) {
            return 1;
          } else if (source.conOrder == target.conOrder) {
            return 0;
          } else {
            return -1;
          }
        })
      : [];
  },

  /**
   * 设置节点序号
   *
   * @param nodes
   * @return
   * @author jiang
   * @date 2021-03-27 17:29:27
   **/
  fillNodeOrder(nodes: SysConfigMergerDtos) {
    nodes?.length > 0 && nodes.forEach((value, index) => (value.conOrder = index + 1));
  },
};
