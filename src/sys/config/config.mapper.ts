import { SysConfig } from '../entity/sys.config.entity';
import { SYS_SYSTEM, SYS_USER } from './sys.config.defined';
import { CON_TYPE, ConfigItem } from './sys.config.info';
import { UserContext } from '../../common/context/user.context';
import { SysServices } from '../sys.services';
import { LogUtils } from '../../common/log4js/log4js.utils';

/**
 * 参数容器对象
 *
 * @author jiang
 * @date 2021-01-06 20:15:40
 **/
export class ConfigMapper {
  private static configMap: Map<CON_TYPE, SysConfig[]> = new Map<CON_TYPE, SysConfig[]>();
  private static configs: SysConfig[] = new Array<SysConfig>();

  /**
   * 获取用户当前种属
   *
   * @return
   * @author jiang
   * @date 2021-01-06 21:38:27
   **/
  static get userSpecies(): string {
    const species = ConfigMapper.getValue(SYS_USER.USER_SAM_SPECIES);
    return species ? species : ConfigMapper.defaultSpecies;
  }

  /**
   * 获取用户当前语言
   *
   * @return
   * @author jiang
   * @date 2021-01-06 21:38:27
   **/
  static get userLanguage(): string {
    return ConfigMapper.getValue(SYS_USER.USER_LANGUAGE);
  }

  /**
   * 获取系统默认语言，在打开首页时会使用的默认语言
   *
   * @return
   * @author jiang
   * @date 2021-01-06 21:38:27
   **/
  static get defaultLanguage(): string {
    return ConfigMapper.getValue(SYS_SYSTEM.SYS_LANGUAGE);
  }

  /**
   * 获取系统默认种属
   *
   * @return
   * @author jiang
   * @date 2021-01-06 21:38:27
   **/
  static get defaultSpecies(): string {
    return ConfigMapper.getValue(SYS_SYSTEM.SYS_SAM_SPECIES);
  }

  /**
   * 获取参数值
   *
   * @param conGroup 参数分组
   * @param conName 参数名
   * @param conSpecies 种属
   * @return string 字符串参数值，若不存在参数记录则返回参数定义的默认值
   * @author jiang
   * @date 2021-01-06 20:03:35
   **/
  static getValue(conItem: ConfigItem | Partial<ConfigItem>): string {
    //获取当前分组参数列表
    const config = ConfigMapper.getConfig(conItem);
    //判定参数
    return config ? config.conValue : conItem.value;
  }

  /**
   * 重新加载全部参数
   * @author: jiangbin
   * @date: 2021-01-07 08:46:52
   **/
  static reload() {
    LogUtils.info(ConfigMapper.name, '开始加载参数项...');
    SysServices.sysConfigService
      .findList({
        conType: 'DATA',
        conState: 'ON',
      })
      .then((configs: SysConfig[]) => {
        ConfigMapper.configMap = new Map<CON_TYPE, SysConfig[]>();
        ConfigMapper.configs = new Array<SysConfig>();
        if (!configs || configs.length == 0) return;
        ConfigMapper.configs = configs;
        ConfigMapper.configMap.set(
          CON_TYPE.SYSTEM,
          configs.filter((config) => config.conParamType == CON_TYPE.SYSTEM),
        );
        ConfigMapper.configMap.set(
          CON_TYPE.USER,
          configs.filter((config) => config.conParamType == CON_TYPE.USER),
        );
        ConfigMapper.configMap.set(
          CON_TYPE.SPECIES,
          configs.filter((config) => config.conParamType == CON_TYPE.SPECIES),
        );
        LogUtils.info(ConfigMapper.name, '成功加载全部参数项数据!');
      })
      .catch((err) => {
        LogUtils.error(ConfigMapper.name, '加载全部参数项数据失败!', err);
      });
  }

  /**
   * 获取指定参数的参数记录
   * @param conItem 参数对象
   * @return SysConfig
   * @author: jiangbin
   * @date: 2021-01-07 12:00:54
   **/
  public static getConfig(conItem: ConfigItem | Partial<ConfigItem>): SysConfig {
    if (!ConfigMapper.configMap || ConfigMapper.configMap.size == 0) {
      return undefined;
    }
    let configs = ConfigMapper.configMap.get(conItem.group.type)?.filter((config) => {
      return config.conGroup === conItem.group.group && config.conName === conItem.name;
    });
    if (conItem.group.type == CON_TYPE.SPECIES) {
      //过滤出种属匹配的参数列表
      configs = configs.filter((config) => {
        return !config.conSpecies || config.conSpecies === UserContext.species;
      });
    } else if (conItem.group.type == CON_TYPE.USER) {
      //过滤出种属匹配的参数列表
      configs = configs.filter((config) => {
        return !config.conManager || config.conManager === UserContext.loginName;
      });
    }
    return configs && configs.length > 0 ? configs[0] : null;
  }

  /**
   * 获取缓存的所有参数项记录
   *
   * @return
   * @author jiang
   * @date 2021-04-11 22:00:44
   **/
  public static getAll(): SysConfig[] {
    return ConfigMapper.configs;
  }
}
