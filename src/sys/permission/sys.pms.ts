import { PmsInfo } from '../../common/pms/pms.info';
import { SYS_CMS_FILES_PMS } from './sys.cms.files.pms';
import { SYS_CMS_NOTICE_PMS } from './sys.cms.notice.pms';
import { SYS_CONFIG_PMS } from './sys.config.pms';
import { SYS_CONTACT_PMS } from './sys.contact.pms';
import { SYS_DICTIONARY_PMS } from './sys.dictionary.pms';
import { SYS_FILES_PMS } from './sys.files.pms';
import { SYS_LOG_PMS } from './sys.log.pms';
import { SYS_MEMORY_PMS } from './sys.memory.pms';
import { SYS_MENU_PMS } from './sys.menu.pms';
import { SYS_NOTICE_PMS } from './sys.notice.pms';
import { SYS_PERMISSION_PMS } from './sys.permission.pms';
import { SYS_ROLE_PMS } from './sys.role.pms';
import { SYS_ROLE_DICTIONARY_PMS } from './sys.role.dictionary.pms';
import { SYS_ROLE_MENU_PMS } from './sys.role.menu.pms';
import { SYS_ROLE_PERMISSION_PMS } from './sys.role.permission.pms';
import { SYS_TIP_PMS } from './sys.tip.pms';
import { SYS_UPDATE_PMS } from './sys.update.pms';
import { SYS_USER_PMS } from './sys.user.pms';
import { SYS_USER_ROLE_PMS } from './sys.user.role.pms';

/**
 * 集中注册SYS模块的所有权限信息，用于自动化注册后端权限功能到数据库中，以便后续的功能授权
 * @date 1/25/2021, 2:48:12 PM
 * @author jiangbin
 **/
export const SYS_PMS: PmsInfo[] = [
  SYS_CMS_FILES_PMS,
  SYS_CMS_NOTICE_PMS,
  SYS_CONFIG_PMS,
  SYS_CONTACT_PMS,
  SYS_DICTIONARY_PMS,
  SYS_FILES_PMS,
  SYS_LOG_PMS,
  SYS_MEMORY_PMS,
  SYS_MENU_PMS,
  SYS_NOTICE_PMS,
  SYS_PERMISSION_PMS,
  SYS_ROLE_PMS,
  SYS_ROLE_DICTIONARY_PMS,
  SYS_ROLE_MENU_PMS,
  SYS_ROLE_PERMISSION_PMS,
  SYS_TIP_PMS,
  SYS_UPDATE_PMS,
  SYS_USER_PMS,
  SYS_USER_ROLE_PMS,
];
