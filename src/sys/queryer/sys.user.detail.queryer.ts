import { TreeCreator } from '../../common/tree/tree.creator';
import { Inject, Injectable } from '@nestjs/common';
import { SysUserService } from '../service/sys.user.service';
import { SysRoleService } from '../service/sys.role.service';
import { SysUserRoleService } from '../service/sys.user.role.service';
import { SysMenuService } from '../service/sys.menu.service';
import { SysRoleMenuService } from '../service/sys.role.menu.service';
import { SysPermissionService } from '../service/sys.permission.service';
import { SysRolePermissionService } from '../service/sys.role.permission.service';
import { ObjUtils } from '../../common/utils/obj.utils';
import { SysRole } from '../entity/sys.role.entity';

/**
 * 查询用户详细信息
 * @author: jiangbin
 * @date: 2020-12-07 17:20:56
 **/
@Injectable()
export class SysUserDetailQueryer {
  private treeCreator: TreeCreator;

  constructor(
    @Inject(SysUserService) private readonly userService: SysUserService,
    @Inject(SysRoleService) private readonly roleService: SysRoleService,
    @Inject(SysUserRoleService) private readonly userRoleService: SysUserRoleService,
    @Inject(SysMenuService) private readonly menuService: SysMenuService,
    @Inject(SysRoleMenuService) private readonly roleMenuService: SysRoleMenuService,
    @Inject(SysPermissionService) private readonly permissionService: SysPermissionService,
    @Inject(SysRolePermissionService) private readonly rolePermissionService: SysRolePermissionService,
  ) {
    this.treeCreator = new TreeCreator();
  }

  /**
   * 查询用户包含的角色列表
   * @param userLoginName 用户登录帐号
   * @author: jiangbin
   * @date: 2020-12-07 17:16:15
   **/
  async queryRole(userLoginName: string): Promise<SysRole[]> {
    if (!userLoginName || userLoginName.length == 0) return;
    const urList = await this.userRoleService.findList({ urUserLoginName: userLoginName });
    if (!urList || urList.length == 0) return [];
    const roleIdList = urList.map((value) => value.urRoleId);
    return await this.roleService.findByIdList(roleIdList);
  }

  /**
   * 过滤出角色ID列表
   * @param roles 角色列表
   * @author: jiangbin
   * @date: 2020-12-07 17:25:33
   **/
  filterRoleIds(roles: SysRole[]) {
    if (!roles || roles.length == 0) return;
    return roles.map((value) => value.roleId);
  }

  /**
   * 查询菜单列表
   * @param roleIdList 角色ID列表
   * @author: jiangbin
   * @date: 2020-12-07 17:15:25
   **/
  async queryMenu(roleIdList: string[]) {
    if (!roleIdList || roleIdList.length == 0) return;
    const rmList = await this.roleMenuService.findList({ rmRoleIdList: roleIdList });
    if (!rmList || rmList.length == 0) return;
    const menuIdList = rmList.map((value) => value.rmMenuId);
    const menus = await this.menuService.findList({ menuIdList: menuIdList, menuState: 'ON' });
    if (!menus || menus.length == 0) return;
    //转换数据格式
    const tree = menus.map((menu) => {
      return {
        id: menu.menuId,
        parentId: menu.menuParentId,
        order: menu.menuOrder,
        path: menu.menuUrl,
        icon: menu.menuIconClass,
        nameCn: menu.menuName,
        nameEn: menu.menuNameEn ? menu.menuNameEn : menu.menuName,
      };
    });

    //转换成树形嵌套结构
    const targets = this.treeCreator.tree(tree);
    return ObjUtils.classToPlain(targets);
  }

  /**
   * 查询权限列表
   * @param roleIdList 角色ID列表
   * @author: jiangbin
   * @date: 2020-12-07 17:17:45
   **/
  async queryPermission(roleIdList: string[]) {
    if (!roleIdList || roleIdList.length == 0) return;
    const rpmsList = await this.rolePermissionService.findList({ rpRoleIdList: roleIdList });
    if (!rpmsList || rpmsList.length == 0) return [];
    const pmsIdList = rpmsList.map((value) => value.rpPermissionId);
    let pmsList = await this.permissionService.findList({ perIdList: pmsIdList, perType: 'DATA' });
    if (!pmsList || pmsList.length == 0) return [];
    //过滤出启用状态的权限
    pmsList = pmsList.filter((value) => value.perState == 'ON');
    return pmsList;
  }
}
