import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysCmsNotice } from '../entity/sys.cms.notice.entity';
import { SysCmsNoticeUDto } from '../dto/update/sys.cms.notice.udto';
import { SysCmsNoticeCDto } from '../dto/create/sys.cms.notice.cdto';
import { SysCmsNoticeQDto } from '../dto/query/sys.cms.notice.qdto';
import { SysCmsNoticeService } from '../service/sys.cms.notice.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_CMS_NOTICE_PMS } from '../permission/sys.cms.notice.pms';

/**
 * SYS_CMS_NOTICE表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysCmsNoticeController
 */
@Controller('sys/cms/notice')
@ApiTags('访问SYS_CMS_NOTICE表的控制器类')
export class SysCmsNoticeController {
  constructor(@Inject(SysCmsNoticeService) private readonly sysCmsNoticeService: SysCmsNoticeService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_CMS_NOTICE_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysCmsNotice[]> {
    return this.sysCmsNoticeService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_CMS_NOTICE_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysCmsNoticeQDto })
  async findList(@Query() dto: SysCmsNoticeQDto): Promise<SysCmsNotice[]> {
    return this.sysCmsNoticeService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_CMS_NOTICE_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysCmsNotice> {
    return this.sysCmsNoticeService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_CMS_NOTICE_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysCmsNoticeQDto })
  async findOne(@Query() dto: SysCmsNoticeQDto): Promise<SysCmsNotice> {
    return this.sysCmsNoticeService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_CMS_NOTICE_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysCmsNotice[]> {
    return this.sysCmsNoticeService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_CMS_NOTICE_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysCmsNoticeQDto })
  async findPage(@Query() query: SysCmsNoticeQDto): Promise<any> {
    return this.sysCmsNoticeService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_CMS_NOTICE_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysCmsNoticeQDto })
  async count(@Query() query: SysCmsNoticeQDto): Promise<number> {
    return this.sysCmsNoticeService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_CMS_NOTICE_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysCmsNoticeService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_CMS_NOTICE_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysCmsNoticeService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_CMS_NOTICE_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysCmsNoticeQDto })
  async delete(@Query() query: SysCmsNoticeQDto): Promise<any> {
    return this.sysCmsNoticeService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_CMS_NOTICE_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysCmsNoticeUDto })
  async update(@Body() dto: SysCmsNoticeUDto): Promise<any> {
    return this.sysCmsNoticeService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_CMS_NOTICE_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysCmsNoticeUDto })
  async updateByIds(@Body() dto: SysCmsNoticeUDto): Promise<any> {
    return this.sysCmsNoticeService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_CMS_NOTICE_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysCmsNoticeUDto] })
  async updateList(@Body() dtos: SysCmsNoticeUDto[]): Promise<any> {
    return this.sysCmsNoticeService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_CMS_NOTICE_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysCmsNoticeUDto })
  async updateSelective(@Body() dto: SysCmsNoticeUDto): Promise<any> {
    return this.sysCmsNoticeService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_CMS_NOTICE_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysCmsNoticeUDto] })
  async updateListSelective(@Body() dtos: SysCmsNoticeUDto[]): Promise<any> {
    return this.sysCmsNoticeService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_CMS_NOTICE_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysCmsNoticeCDto })
  async create(@Body() dto: SysCmsNoticeCDto): Promise<SysCmsNoticeCDto> {
    return this.sysCmsNoticeService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_CMS_NOTICE_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysCmsNoticeCDto] })
  async createList(@Body() dtos: SysCmsNoticeCDto[]): Promise<SysCmsNoticeCDto[]> {
    return this.sysCmsNoticeService.createList(dtos);
  }
}
