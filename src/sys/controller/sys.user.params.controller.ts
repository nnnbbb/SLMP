import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysUserParamsUDto } from '../dto/update/sys.user.params.udto';
import { SysUserParamsCDto } from '../dto/create/sys.user.params.cdto';
import { SysUserParamsQDto } from '../dto/query/sys.user.params.qdto';
import { SysUserParamsService } from '../service/sys.user.params.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_USER_PARAMS_PMS } from '../permission/sys.user.params.pms';
import { SysUserParams } from '../entity/sys.user.params.entity';

/**
 * SYS_USER_PARAMS表对应控制器类
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export
 * @class SysUserParamsController
 */
@Controller('sys/user/params')
@ApiTags('访问SYS_USER_PARAMS表的控制器类')
export class SysUserParamsController {
  constructor(@Inject(SysUserParamsService) private readonly sysUserParamsService: SysUserParamsService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_USER_PARAMS_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysUserParams[]> {
    return this.sysUserParamsService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_USER_PARAMS_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysUserParamsQDto })
  async findList(@Query() dto: SysUserParamsQDto): Promise<SysUserParams[]> {
    return this.sysUserParamsService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_USER_PARAMS_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysUserParams> {
    return this.sysUserParamsService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_USER_PARAMS_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysUserParamsQDto })
  async findOne(@Query() dto: SysUserParamsQDto): Promise<SysUserParams> {
    return this.sysUserParamsService.findOne(dto);
  }

  /**
   * 查询ID列表对应记录
   * @param ids
   * @param cols
   */
  @Permission(SYS_USER_PARAMS_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysUserParams[]> {
    return this.sysUserParamsService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_USER_PARAMS_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserParamsQDto })
  async findPage(@Query() query: SysUserParamsQDto): Promise<any> {
    return this.sysUserParamsService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_USER_PARAMS_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserParamsQDto })
  async count(@Query() query: SysUserParamsQDto): Promise<number> {
    return this.sysUserParamsService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_USER_PARAMS_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysUserParamsService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_USER_PARAMS_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Body('ids') ids: string[]): Promise<any> {
    return this.sysUserParamsService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_USER_PARAMS_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysUserParamsQDto })
  async delete(@Query() query: SysUserParamsQDto): Promise<any> {
    return this.sysUserParamsService.delete(query);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_PARAMS_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserParamsUDto })
  async updateByIds(@Body() dto: SysUserParamsUDto): Promise<any> {
    return this.sysUserParamsService.updateByIds(dto);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_PARAMS_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserParamsUDto })
  async update(@Body() dto: SysUserParamsUDto): Promise<any> {
    return this.sysUserParamsService.update(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_USER_PARAMS_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysUserParamsUDto] })
  async updateList(@Body() dtos: SysUserParamsUDto[]): Promise<any> {
    return this.sysUserParamsService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_USER_PARAMS_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysUserParamsUDto })
  async updateSelective(@Body() dto: SysUserParamsUDto): Promise<any> {
    return this.sysUserParamsService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_USER_PARAMS_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserParamsUDto] })
  async updateListSelective(@Body() dtos: SysUserParamsUDto[]): Promise<any> {
    return this.sysUserParamsService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_USER_PARAMS_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysUserParamsCDto })
  async create(@Body() dto: SysUserParamsCDto): Promise<SysUserParamsCDto> {
    return this.sysUserParamsService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_USER_PARAMS_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserParamsCDto] })
  async createList(@Body() dtos: SysUserParamsCDto[]): Promise<SysUserParamsCDto[]> {
    return this.sysUserParamsService.createList(dtos);
  }
}
