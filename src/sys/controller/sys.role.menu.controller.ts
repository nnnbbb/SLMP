import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysRoleMenu } from '../entity/sys.role.menu.entity';
import { SysRoleMenuUDto } from '../dto/update/sys.role.menu.udto';
import { SysRoleMenuCDto } from '../dto/create/sys.role.menu.cdto';
import { SysRoleMenuQDto } from '../dto/query/sys.role.menu.qdto';
import { SysRoleMenuService } from '../service/sys.role.menu.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_ROLE_MENU_PMS } from '../permission/sys.role.menu.pms';

/**
 * SYS_ROLE_MENU表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysRoleMenuController
 */
@Controller('sys/role/menu')
@ApiTags('访问SYS_ROLE_MENU表的控制器类')
export class SysRoleMenuController {
  constructor(@Inject(SysRoleMenuService) private readonly sysRoleMenuService: SysRoleMenuService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_ROLE_MENU_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysRoleMenu[]> {
    return this.sysRoleMenuService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_ROLE_MENU_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysRoleMenuQDto })
  async findList(@Query() dto: SysRoleMenuQDto): Promise<SysRoleMenu[]> {
    return this.sysRoleMenuService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_ROLE_MENU_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysRoleMenu> {
    return this.sysRoleMenuService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_ROLE_MENU_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysRoleMenuQDto })
  async findOne(@Query() dto: SysRoleMenuQDto): Promise<SysRoleMenu> {
    return this.sysRoleMenuService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_ROLE_MENU_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysRoleMenu[]> {
    return this.sysRoleMenuService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_ROLE_MENU_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRoleMenuQDto })
  async findPage(@Query() query: SysRoleMenuQDto): Promise<any> {
    return this.sysRoleMenuService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_ROLE_MENU_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRoleMenuQDto })
  async count(@Query() query: SysRoleMenuQDto): Promise<number> {
    return this.sysRoleMenuService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_ROLE_MENU_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysRoleMenuService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_ROLE_MENU_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysRoleMenuService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_ROLE_MENU_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysRoleMenuQDto })
  async delete(@Query() query: SysRoleMenuQDto): Promise<any> {
    return this.sysRoleMenuService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_MENU_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRoleMenuUDto })
  async update(@Body() dto: SysRoleMenuUDto): Promise<any> {
    return this.sysRoleMenuService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_MENU_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRoleMenuUDto })
  async updateByIds(@Body() dto: SysRoleMenuUDto): Promise<any> {
    return this.sysRoleMenuService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_ROLE_MENU_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysRoleMenuUDto] })
  async updateList(@Body() dtos: SysRoleMenuUDto[]): Promise<any> {
    return this.sysRoleMenuService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_ROLE_MENU_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysRoleMenuUDto })
  async updateSelective(@Body() dto: SysRoleMenuUDto): Promise<any> {
    return this.sysRoleMenuService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_ROLE_MENU_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysRoleMenuUDto] })
  async updateListSelective(@Body() dtos: SysRoleMenuUDto[]): Promise<any> {
    return this.sysRoleMenuService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_MENU_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysRoleMenuCDto })
  async create(@Body() dto: SysRoleMenuCDto): Promise<SysRoleMenuCDto> {
    return this.sysRoleMenuService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_MENU_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysRoleMenuCDto] })
  async createList(@Body() dtos: SysRoleMenuCDto[]): Promise<SysRoleMenuCDto[]> {
    return this.sysRoleMenuService.createList(dtos);
  }
}
