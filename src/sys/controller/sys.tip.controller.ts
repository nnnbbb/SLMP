import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysTip } from '../entity/sys.tip.entity';
import { SysTipUDto } from '../dto/update/sys.tip.udto';
import { SysTipCDto } from '../dto/create/sys.tip.cdto';
import { SysTipQDto } from '../dto/query/sys.tip.qdto';
import { SysTipService } from '../service/sys.tip.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_TIP_PMS } from '../permission/sys.tip.pms';

/**
 * SYS_TIP表对应控制器类
 * @date 12/30/2020, 2:44:22 PM
 * @author jiangbin
 * @export
 * @class SysTipController
 */
@Controller('sys/tip')
@ApiTags('访问SYS_TIP表的控制器类')
export class SysTipController {
  constructor(@Inject(SysTipService) private readonly sysTipService: SysTipService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_TIP_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysTip[]> {
    return this.sysTipService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_TIP_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysTipQDto })
  async findList(@Query() dto: SysTipQDto): Promise<SysTip[]> {
    return this.sysTipService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_TIP_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysTip> {
    return this.sysTipService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_TIP_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysTipQDto })
  async findOne(@Query() dto: SysTipQDto): Promise<SysTip> {
    return this.sysTipService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_TIP_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysTip[]> {
    return this.sysTipService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_TIP_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysTipQDto })
  async findPage(@Query() query: SysTipQDto): Promise<any> {
    return this.sysTipService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_TIP_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysTipQDto })
  async count(@Query() query: SysTipQDto): Promise<number> {
    return this.sysTipService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_TIP_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysTipService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_TIP_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysTipService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_TIP_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysTipQDto })
  async delete(@Query() query: SysTipQDto): Promise<any> {
    return this.sysTipService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_TIP_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysTipUDto })
  async update(@Body() dto: SysTipUDto): Promise<any> {
    return this.sysTipService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_TIP_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysTipUDto })
  async updateByIds(@Body() dto: SysTipUDto): Promise<any> {
    return this.sysTipService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_TIP_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysTipUDto] })
  async updateList(@Body() dtos: SysTipUDto[]): Promise<any> {
    return this.sysTipService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_TIP_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysTipUDto })
  async updateSelective(@Body() dto: SysTipUDto): Promise<any> {
    return this.sysTipService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_TIP_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysTipUDto] })
  async updateListSelective(@Body() dtos: SysTipUDto[]): Promise<any> {
    return this.sysTipService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_TIP_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysTipCDto })
  async create(@Body() dto: SysTipCDto): Promise<SysTipCDto> {
    return this.sysTipService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_TIP_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysTipCDto] })
  async createList(@Body() dtos: SysTipCDto[]): Promise<SysTipCDto[]> {
    return this.sysTipService.createList(dtos);
  }
}
