import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysCmsFiles } from '../entity/sys.cms.files.entity';
import { SysCmsFilesUDto } from '../dto/update/sys.cms.files.udto';
import { SysCmsFilesCDto } from '../dto/create/sys.cms.files.cdto';
import { SysCmsFilesQDto } from '../dto/query/sys.cms.files.qdto';
import { SysCmsFilesService } from '../service/sys.cms.files.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_CMS_FILES_PMS } from '../permission/sys.cms.files.pms';

/**
 * SYS_CMS_FILES表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysCmsFilesController
 */
@Controller('sys/cms/files')
@ApiTags('访问SYS_CMS_FILES表的控制器类')
export class SysCmsFilesController {
  constructor(@Inject(SysCmsFilesService) private readonly sysCmsFilesService: SysCmsFilesService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_CMS_FILES_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysCmsFiles[]> {
    return this.sysCmsFilesService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_CMS_FILES_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysCmsFilesQDto })
  async findList(@Query() dto: SysCmsFilesQDto): Promise<SysCmsFiles[]> {
    return this.sysCmsFilesService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_CMS_FILES_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysCmsFiles> {
    return this.sysCmsFilesService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_CMS_FILES_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysCmsFilesQDto })
  async findOne(@Query() dto: SysCmsFilesQDto): Promise<SysCmsFiles> {
    return this.sysCmsFilesService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_CMS_FILES_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysCmsFiles[]> {
    return this.sysCmsFilesService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_CMS_FILES_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysCmsFilesQDto })
  async findPage(@Query() query: SysCmsFilesQDto): Promise<any> {
    return this.sysCmsFilesService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_CMS_FILES_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysCmsFilesQDto })
  async count(@Query() query: SysCmsFilesQDto): Promise<number> {
    return this.sysCmsFilesService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_CMS_FILES_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysCmsFilesService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_CMS_FILES_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysCmsFilesService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_CMS_FILES_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysCmsFilesQDto })
  async delete(@Query() query: SysCmsFilesQDto): Promise<any> {
    return this.sysCmsFilesService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_CMS_FILES_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysCmsFilesUDto })
  async update(@Body() dto: SysCmsFilesUDto): Promise<any> {
    return this.sysCmsFilesService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_CMS_FILES_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysCmsFilesUDto })
  async updateByIds(@Body() dto: SysCmsFilesUDto): Promise<any> {
    return this.sysCmsFilesService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_CMS_FILES_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysCmsFilesUDto] })
  async updateList(@Body() dtos: SysCmsFilesUDto[]): Promise<any> {
    return this.sysCmsFilesService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_CMS_FILES_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysCmsFilesUDto })
  async updateSelective(@Body() dto: SysCmsFilesUDto): Promise<any> {
    return this.sysCmsFilesService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_CMS_FILES_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysCmsFilesUDto] })
  async updateListSelective(@Body() dtos: SysCmsFilesUDto[]): Promise<any> {
    return this.sysCmsFilesService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_CMS_FILES_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysCmsFilesCDto })
  async create(@Body() dto: SysCmsFilesCDto): Promise<SysCmsFilesCDto> {
    return this.sysCmsFilesService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_CMS_FILES_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysCmsFilesCDto] })
  async createList(@Body() dtos: SysCmsFilesCDto[]): Promise<SysCmsFilesCDto[]> {
    return this.sysCmsFilesService.createList(dtos);
  }
}
