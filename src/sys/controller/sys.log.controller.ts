import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysLog } from '../entity/sys.log.entity';
import { SysLogUDto } from '../dto/update/sys.log.udto';
import { SysLogCDto } from '../dto/create/sys.log.cdto';
import { SysLogQDto } from '../dto/query/sys.log.qdto';
import { SysLogService } from '../service/sys.log.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_LOG_PMS } from '../permission/sys.log.pms';

/**
 * SYS_LOG表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysLogController
 */
@Controller('sys/log')
@ApiTags('访问SYS_LOG表的控制器类')
export class SysLogController {
  constructor(@Inject(SysLogService) private readonly sysLogService: SysLogService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_LOG_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysLog[]> {
    return this.sysLogService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_LOG_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysLogQDto })
  async findList(@Query() dto: SysLogQDto): Promise<SysLog[]> {
    return this.sysLogService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_LOG_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysLog> {
    return this.sysLogService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_LOG_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysLogQDto })
  async findOne(@Query() dto: SysLogQDto): Promise<SysLog> {
    return this.sysLogService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_LOG_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysLog[]> {
    return this.sysLogService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_LOG_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysLogQDto })
  async findPage(@Query() query: SysLogQDto): Promise<any> {
    return this.sysLogService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_LOG_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysLogQDto })
  async count(@Query() query: SysLogQDto): Promise<number> {
    return this.sysLogService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_LOG_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysLogService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_LOG_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysLogService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_LOG_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysLogQDto })
  async delete(@Query() query: SysLogQDto): Promise<any> {
    return this.sysLogService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_LOG_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysLogUDto })
  async update(@Body() dto: SysLogUDto): Promise<any> {
    return this.sysLogService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_LOG_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysLogUDto })
  async updateByIds(@Body() dto: SysLogUDto): Promise<any> {
    return this.sysLogService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_LOG_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysLogUDto] })
  async updateList(@Body() dtos: SysLogUDto[]): Promise<any> {
    return this.sysLogService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_LOG_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysLogUDto })
  async updateSelective(@Body() dto: SysLogUDto): Promise<any> {
    return this.sysLogService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_LOG_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysLogUDto] })
  async updateListSelective(@Body() dtos: SysLogUDto[]): Promise<any> {
    return this.sysLogService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_LOG_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysLogCDto })
  async create(@Body() dto: SysLogCDto): Promise<SysLogCDto> {
    return this.sysLogService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_LOG_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysLogCDto] })
  async createList(@Body() dtos: SysLogCDto[]): Promise<SysLogCDto[]> {
    return this.sysLogService.createList(dtos);
  }
}
