import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysRole } from '../entity/sys.role.entity';
import { SysRoleUDto } from '../dto/update/sys.role.udto';
import { SysRoleCDto } from '../dto/create/sys.role.cdto';
import { SysRoleQDto } from '../dto/query/sys.role.qdto';
import { SysRoleService } from '../service/sys.role.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_ROLE_PMS } from '../permission/sys.role.pms';

/**
 * SYS_ROLE表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysRoleController
 */
@Controller('sys/role')
@ApiTags('访问SYS_ROLE表的控制器类')
export class SysRoleController {
  constructor(@Inject(SysRoleService) private readonly sysRoleService: SysRoleService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_ROLE_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysRole[]> {
    return this.sysRoleService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_ROLE_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysRoleQDto })
  async findList(@Query() dto: SysRoleQDto): Promise<SysRole[]> {
    return this.sysRoleService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_ROLE_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysRole> {
    return this.sysRoleService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_ROLE_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysRoleQDto })
  async findOne(@Query() dto: SysRoleQDto): Promise<SysRole> {
    return this.sysRoleService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_ROLE_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysRole[]> {
    return this.sysRoleService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_ROLE_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRoleQDto })
  async findPage(@Query() query: SysRoleQDto): Promise<any> {
    return this.sysRoleService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_ROLE_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysRoleQDto })
  async count(@Query() query: SysRoleQDto): Promise<number> {
    return this.sysRoleService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_ROLE_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysRoleService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_ROLE_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysRoleService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_ROLE_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysRoleQDto })
  async delete(@Query() query: SysRoleQDto): Promise<any> {
    return this.sysRoleService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRoleUDto })
  async update(@Body() dto: SysRoleUDto): Promise<any> {
    return this.sysRoleService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_ROLE_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysRoleUDto })
  async updateByIds(@Body() dto: SysRoleUDto): Promise<any> {
    return this.sysRoleService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_ROLE_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysRoleUDto] })
  async updateList(@Body() dtos: SysRoleUDto[]): Promise<any> {
    return this.sysRoleService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_ROLE_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysRoleUDto })
  async updateSelective(@Body() dto: SysRoleUDto): Promise<any> {
    return this.sysRoleService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_ROLE_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysRoleUDto] })
  async updateListSelective(@Body() dtos: SysRoleUDto[]): Promise<any> {
    return this.sysRoleService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysRoleCDto })
  async create(@Body() dto: SysRoleCDto): Promise<SysRoleCDto> {
    return this.sysRoleService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_ROLE_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysRoleCDto] })
  async createList(@Body() dtos: SysRoleCDto[]): Promise<SysRoleCDto[]> {
    return this.sysRoleService.createList(dtos);
  }
}
