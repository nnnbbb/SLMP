import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysContact } from '../entity/sys.contact.entity';
import { SysContactUDto } from '../dto/update/sys.contact.udto';
import { SysContactCDto } from '../dto/create/sys.contact.cdto';
import { SysContactQDto } from '../dto/query/sys.contact.qdto';
import { SysContactService } from '../service/sys.contact.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_CONTACT_PMS } from '../permission/sys.contact.pms';

/**
 * SYS_CONTACT表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysContactController
 */
@Controller('sys/contact')
@ApiTags('访问SYS_CONTACT表的控制器类')
export class SysContactController {
  constructor(@Inject(SysContactService) private readonly sysContactService: SysContactService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_CONTACT_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysContact[]> {
    return this.sysContactService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_CONTACT_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysContactQDto })
  async findList(@Query() dto: SysContactQDto): Promise<SysContact[]> {
    return this.sysContactService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_CONTACT_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysContact> {
    return this.sysContactService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_CONTACT_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysContactQDto })
  async findOne(@Query() dto: SysContactQDto): Promise<SysContact> {
    return this.sysContactService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_CONTACT_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysContact[]> {
    return this.sysContactService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_CONTACT_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysContactQDto })
  async findPage(@Query() query: SysContactQDto): Promise<any> {
    return this.sysContactService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_CONTACT_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysContactQDto })
  async count(@Query() query: SysContactQDto): Promise<number> {
    return this.sysContactService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_CONTACT_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysContactService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_CONTACT_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysContactService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_CONTACT_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysContactQDto })
  async delete(@Query() query: SysContactQDto): Promise<any> {
    return this.sysContactService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_CONTACT_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysContactUDto })
  async update(@Body() dto: SysContactUDto): Promise<any> {
    return this.sysContactService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_CONTACT_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysContactUDto })
  async updateByIds(@Body() dto: SysContactUDto): Promise<any> {
    return this.sysContactService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_CONTACT_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysContactUDto] })
  async updateList(@Body() dtos: SysContactUDto[]): Promise<any> {
    return this.sysContactService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_CONTACT_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysContactUDto })
  async updateSelective(@Body() dto: SysContactUDto): Promise<any> {
    return this.sysContactService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_CONTACT_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysContactUDto] })
  async updateListSelective(@Body() dtos: SysContactUDto[]): Promise<any> {
    return this.sysContactService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_CONTACT_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysContactCDto })
  async create(@Body() dto: SysContactCDto): Promise<SysContactCDto> {
    return this.sysContactService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_CONTACT_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysContactCDto] })
  async createList(@Body() dtos: SysContactCDto[]): Promise<SysContactCDto[]> {
    return this.sysContactService.createList(dtos);
  }
}
