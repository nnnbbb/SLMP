import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysUpdate } from '../entity/sys.update.entity';
import { SysUpdateUDto } from '../dto/update/sys.update.udto';
import { SysUpdateCDto } from '../dto/create/sys.update.cdto';
import { SysUpdateQDto } from '../dto/query/sys.update.qdto';
import { SysUpdateService } from '../service/sys.update.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_UPDATE_PMS } from '../permission/sys.update.pms';

/**
 * SYS_UPDATE表对应控制器类
 * @date 12/30/2020, 2:44:22 PM
 * @author jiangbin
 * @export
 * @class SysUpdateController
 */
@Controller('sys/update')
@ApiTags('访问SYS_UPDATE表的控制器类')
export class SysUpdateController {
  constructor(@Inject(SysUpdateService) private readonly sysUpdateService: SysUpdateService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_UPDATE_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysUpdate[]> {
    return this.sysUpdateService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_UPDATE_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysUpdateQDto })
  async findList(@Query() dto: SysUpdateQDto): Promise<SysUpdate[]> {
    return this.sysUpdateService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_UPDATE_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysUpdate> {
    return this.sysUpdateService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_UPDATE_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysUpdateQDto })
  async findOne(@Query() dto: SysUpdateQDto): Promise<SysUpdate> {
    return this.sysUpdateService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_UPDATE_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysUpdate[]> {
    return this.sysUpdateService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_UPDATE_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUpdateQDto })
  async findPage(@Query() query: SysUpdateQDto): Promise<any> {
    return this.sysUpdateService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_UPDATE_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUpdateQDto })
  async count(@Query() query: SysUpdateQDto): Promise<number> {
    return this.sysUpdateService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_UPDATE_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysUpdateService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_UPDATE_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysUpdateService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_UPDATE_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysUpdateQDto })
  async delete(@Query() query: SysUpdateQDto): Promise<any> {
    return this.sysUpdateService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_UPDATE_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUpdateUDto })
  async update(@Body() dto: SysUpdateUDto): Promise<any> {
    return this.sysUpdateService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_UPDATE_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUpdateUDto })
  async updateByIds(@Body() dto: SysUpdateUDto): Promise<any> {
    return this.sysUpdateService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_UPDATE_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysUpdateUDto] })
  async updateList(@Body() dtos: SysUpdateUDto[]): Promise<any> {
    return this.sysUpdateService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_UPDATE_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysUpdateUDto })
  async updateSelective(@Body() dto: SysUpdateUDto): Promise<any> {
    return this.sysUpdateService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_UPDATE_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysUpdateUDto] })
  async updateListSelective(@Body() dtos: SysUpdateUDto[]): Promise<any> {
    return this.sysUpdateService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_UPDATE_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysUpdateCDto })
  async create(@Body() dto: SysUpdateCDto): Promise<SysUpdateCDto> {
    return this.sysUpdateService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_UPDATE_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysUpdateCDto] })
  async createList(@Body() dtos: SysUpdateCDto[]): Promise<SysUpdateCDto[]> {
    return this.sysUpdateService.createList(dtos);
  }
}
