import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysFiles } from '../entity/sys.files.entity';
import { SysFilesUDto } from '../dto/update/sys.files.udto';
import { SysFilesCDto } from '../dto/create/sys.files.cdto';
import { SysFilesQDto } from '../dto/query/sys.files.qdto';
import { SysFilesService } from '../service/sys.files.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_FILES_PMS } from '../permission/sys.files.pms';

/**
 * SYS_FILES表对应控制器类
 * @date 12/30/2020, 2:44:21 PM
 * @author jiangbin
 * @export
 * @class SysFilesController
 */
@Controller('sys/files')
@ApiTags('访问SYS_FILES表的控制器类')
export class SysFilesController {
  constructor(@Inject(SysFilesService) private readonly sysFilesService: SysFilesService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_FILES_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysFiles[]> {
    return this.sysFilesService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_FILES_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysFilesQDto })
  async findList(@Query() dto: SysFilesQDto): Promise<SysFiles[]> {
    return this.sysFilesService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_FILES_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysFiles> {
    return this.sysFilesService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_FILES_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysFilesQDto })
  async findOne(@Query() dto: SysFilesQDto): Promise<SysFiles> {
    return this.sysFilesService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_FILES_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysFiles[]> {
    return this.sysFilesService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_FILES_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysFilesQDto })
  async findPage(@Query() query: SysFilesQDto): Promise<any> {
    return this.sysFilesService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_FILES_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysFilesQDto })
  async count(@Query() query: SysFilesQDto): Promise<number> {
    return this.sysFilesService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_FILES_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysFilesService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_FILES_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysFilesService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_FILES_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysFilesQDto })
  async delete(@Query() query: SysFilesQDto): Promise<any> {
    return this.sysFilesService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_FILES_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysFilesUDto })
  async update(@Body() dto: SysFilesUDto): Promise<any> {
    return this.sysFilesService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_FILES_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysFilesUDto })
  async updateByIds(@Body() dto: SysFilesUDto): Promise<any> {
    return this.sysFilesService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_FILES_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysFilesUDto] })
  async updateList(@Body() dtos: SysFilesUDto[]): Promise<any> {
    return this.sysFilesService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_FILES_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysFilesUDto })
  async updateSelective(@Body() dto: SysFilesUDto): Promise<any> {
    return this.sysFilesService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_FILES_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysFilesUDto] })
  async updateListSelective(@Body() dtos: SysFilesUDto[]): Promise<any> {
    return this.sysFilesService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_FILES_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysFilesCDto })
  async create(@Body() dto: SysFilesCDto): Promise<SysFilesCDto> {
    return this.sysFilesService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_FILES_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysFilesCDto] })
  async createList(@Body() dtos: SysFilesCDto[]): Promise<SysFilesCDto[]> {
    return this.sysFilesService.createList(dtos);
  }
}
