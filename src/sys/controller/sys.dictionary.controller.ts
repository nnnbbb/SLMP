import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysDictionary } from '../entity/sys.dictionary.entity';
import { SysDictionaryUDto } from '../dto/update/sys.dictionary.udto';
import { SysDictionaryCDto } from '../dto/create/sys.dictionary.cdto';
import { SysDictionaryQDto } from '../dto/query/sys.dictionary.qdto';
import { SysDictionaryService } from '../service/sys.dictionary.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_DICTIONARY_PMS } from '../permission/sys.dictionary.pms';
import { AopDictLoader } from '../../common/aop/aop.dict.loader';
import { DictMapper } from '../../common/dictionary/dict.mapper';

/**
 * SYS_DICTIONARY表对应控制器类
 * @date 12/28/2020, 9:22:29 PM
 * @author jiangbin
 * @export
 * @class SysDictionaryController
 */
@Controller('sys/dictionary')
@ApiTags('访问SYS_DICTIONARY表的控制器类')
export class SysDictionaryController {
  constructor(@Inject(SysDictionaryService) private readonly sysDictionaryService: SysDictionaryService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_DICTIONARY_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysDictionary[]> {
    let items = DictMapper.getAll();
    if (items?.length > 0) {
      return items;
    }
    return this.sysDictionaryService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysDictionaryQDto })
  async findList(@Query() dto: SysDictionaryQDto): Promise<SysDictionary[]> {
    return this.sysDictionaryService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_DICTIONARY_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysDictionary> {
    return this.sysDictionaryService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_DICTIONARY_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysDictionaryQDto })
  async findOne(@Query() dto: SysDictionaryQDto): Promise<SysDictionary> {
    return this.sysDictionaryService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   * @param cols
   */
  @Permission(SYS_DICTIONARY_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysDictionary[]> {
    return this.sysDictionaryService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_DICTIONARY_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysDictionaryQDto })
  async findPage(@Query() query: SysDictionaryQDto): Promise<any> {
    return this.sysDictionaryService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_DICTIONARY_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysDictionaryQDto })
  async count(@Query() query: SysDictionaryQDto): Promise<number> {
    return this.sysDictionaryService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_DICTIONARY_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  @AopDictLoader(false)
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysDictionaryService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_DICTIONARY_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @AopDictLoader(false)
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysDictionaryService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_DICTIONARY_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysDictionaryQDto })
  @AopDictLoader(false)
  async delete(@Query() query: SysDictionaryQDto): Promise<any> {
    return this.sysDictionaryService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysDictionaryUDto })
  @AopDictLoader(false)
  async update(@Body() dto: SysDictionaryUDto): Promise<any> {
    return this.sysDictionaryService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysDictionaryUDto })
  @AopDictLoader(false)
  async updateByIds(@Body() dto: SysDictionaryUDto): Promise<any> {
    return this.sysDictionaryService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_DICTIONARY_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysDictionaryUDto] })
  @AopDictLoader(false)
  async updateList(@Body() dtos: SysDictionaryUDto[]): Promise<any> {
    return this.sysDictionaryService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysDictionaryUDto })
  @AopDictLoader(false)
  async updateSelective(@Body() dto: SysDictionaryUDto): Promise<any> {
    return this.sysDictionaryService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_DICTIONARY_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysDictionaryUDto] })
  @AopDictLoader(false)
  async updateListSelective(@Body() dtos: SysDictionaryUDto[]): Promise<any> {
    return this.sysDictionaryService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysDictionaryCDto })
  @AopDictLoader(false)
  async create(@Body() dto: SysDictionaryCDto): Promise<SysDictionaryCDto> {
    return this.sysDictionaryService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysDictionaryCDto] })
  @AopDictLoader(false)
  async createList(@Body() dtos: SysDictionaryCDto[]): Promise<SysDictionaryCDto[]> {
    return this.sysDictionaryService.createList(dtos);
  }

  /*********以下为新增方法*********/
  @Permission(SYS_DICTIONARY_PMS.FIND_PAGE_DETAIL)
  @Get('find/page/detail')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysDictionaryQDto })
  async findPageDetail(@Query() dto: SysDictionaryQDto): Promise<any> {
    return this.sysDictionaryService.findPageDetail(dto);
  }

  /**
   * 初始化字典
   * @param dto
   */
  @Permission(SYS_DICTIONARY_PMS.INIT)
  @Post('init')
  @ApiOperation({ summary: '初始化字典' })
  @ApiQuery({ name: 'moduleName', type: [String] })
  @ApiQuery({ name: 'dtos', type: [SysDictionaryCDto] })
  @AopDictLoader(false)
  async init(): Promise<boolean> {
    return this.sysDictionaryService.init();
  }
}
