import { Body, Controller, Delete, Get, Inject, Post, Put, Query } from '@nestjs/common';
import { SysUser } from '../entity/sys.user.entity';
import { SysUserUDto } from '../dto/update/sys.user.udto';
import { SysUserCDto } from '../dto/create/sys.user.cdto';
import { SysUserQDto } from '../dto/query/sys.user.qdto';
import { SysUserService } from '../service/sys.user.service';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Permission } from '../../common/auth/guard/pms.auth';
import { SYS_USER_PMS } from '../permission/sys.user.pms';
import { AopUserLoader } from '../../common/aop/aop.user.loader';
import { UserMapper } from '../user/user.mapper';
import { Pager } from '../../common/decorators/pager.decorator';

/**
 * SYS_USER表对应控制器类
 * @date 12/30/2020, 2:44:22 PM
 * @author jiangbin
 * @export
 * @class SysUserController
 */
@Controller('sys/user')
@ApiTags('访问SYS_USER表的控制器类')
export class SysUserController {
  constructor(@Inject(SysUserService) private readonly sysUserService: SysUserService) {}

  /**
   * 查询所有记录
   */
  @Permission(SYS_USER_PMS.FIND_ALL)
  @Get('find/all')
  @ApiOperation({ summary: '查询所有记录' })
  async findAll(): Promise<SysUser[]> {
    let users = UserMapper.getAll();
    if (users?.length > 0) {
      return users;
    }
    return this.sysUserService.findAll();
  }

  /**
   * 条件查询记录
   * @param dto
   */
  @Permission(SYS_USER_PMS.FIND_LIST)
  @Get('find/list')
  @ApiOperation({ summary: '条件查询记录' })
  @ApiQuery({ name: 'dto', type: SysUserQDto })
  async findList(@Query() dto: SysUserQDto): Promise<SysUser[]> {
    return this.sysUserService.findList(dto);
  }

  /**
   * 查询ID对应记录
   * @param query
   */
  @Permission(SYS_USER_PMS.FIND_BY_ID)
  @Get('find/by/id')
  @ApiOperation({ summary: '查询指定ID的记录' })
  @ApiQuery({ name: 'id', type: String })
  async findById(@Query('id') id: string): Promise<SysUser> {
    return this.sysUserService.findById(id);
  }

  /**
   * 查询单条记录
   * @param query
   */
  @Permission(SYS_USER_PMS.FIND_ONE)
  @Get('find/one')
  @ApiOperation({ summary: '查询单条记录' })
  @ApiQuery({ name: 'dto', type: SysUserQDto })
  async findOne(@Query() dto: SysUserQDto): Promise<SysUser> {
    return this.sysUserService.findOne(dto);
  }

  /**
   * 查询ID对应记录
   * @param ids
   */
  @Permission(SYS_USER_PMS.FIND_BY_ID_LIST)
  @Get('find/by/ids')
  @ApiOperation({ summary: '查询指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @ApiQuery({ name: 'cols', type: [String] })
  async findByIdList(@Query('ids') ids: string[], @Query('cols') cols: string[]): Promise<SysUser[]> {
    return this.sysUserService.findByIdList(ids, cols);
  }

  /**
   * 分页查询记录
   * @param query
   */
  @Permission(SYS_USER_PMS.FIND_PAGE)
  @Get('find/page')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserQDto })
  async findPage(@Pager() query: SysUserQDto): Promise<any> {
    return this.sysUserService.findPage(query);
  }

  /**
   * 查询记录数
   * @param query
   */
  @Permission(SYS_USER_PMS.COUNT)
  @Get('count')
  @ApiOperation({ summary: '分页查询记录' })
  @ApiQuery({ name: 'query', type: SysUserQDto })
  async count(@Query() query: SysUserQDto): Promise<number> {
    return this.sysUserService.count(query);
  }

  /**
   * 删除指定ID记录
   * @param id
   */
  @Permission(SYS_USER_PMS.DELETE_BY_ID)
  @Delete('delete/by/id')
  @ApiOperation({ summary: '删除指定ID记录' })
  @ApiQuery({ name: 'id', type: String })
  @AopUserLoader(false)
  async deleteById(@Query('id') id: string): Promise<any> {
    return this.sysUserService.deleteById(id);
  }

  /**
   * 删除指定ID记录列表
   * @param ids
   */
  @Permission(SYS_USER_PMS.DELETE_BY_ID_LIST)
  @Delete('delete/by/ids')
  @ApiOperation({ summary: '删除指定ID列表的记录' })
  @ApiQuery({ name: 'ids', type: [String] })
  @AopUserLoader(false)
  async deleteByIdList(@Query('ids') ids: string[]): Promise<any> {
    return this.sysUserService.deleteByIdList(ids);
  }

  /**
   * 条件删除记录
   * @param query
   */
  @Permission(SYS_USER_PMS.DELETE)
  @Delete('delete')
  @ApiOperation({ summary: '条件删除记录' })
  @ApiQuery({ name: 'query', type: SysUserQDto })
  @AopUserLoader(false)
  async delete(@Query() query: SysUserQDto): Promise<any> {
    return this.sysUserService.delete(query);
  }

  /**
   * 更新指定ID记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_PMS.UPDATE)
  @Put('update/by/id')
  @ApiOperation({ summary: '更新指定ID的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserUDto })
  @AopUserLoader(false)
  async update(@Body() dto: SysUserUDto): Promise<any> {
    return this.sysUserService.update(dto);
  }

  /**
   * 更新指定ID列表记录全部字段
   * @param dto
   */
  @Permission(SYS_USER_PMS.UPDATE_BY_IDS)
  @Put('update/by/ids')
  @ApiOperation({ summary: '更新指定ID列表的记录全部字段' })
  @ApiQuery({ name: 'dto', type: SysUserUDto })
  @AopUserLoader(false)
  async updateByIds(@Body() dto: SysUserUDto): Promise<any> {
    return this.sysUserService.updateByIds(dto);
  }

  /**
   * 批量更新ID对应的记录全部字段
   * @param dtos
   */
  @Permission(SYS_USER_PMS.UPDATE_LIST)
  @Put('update/list')
  @ApiOperation({ summary: '批量更新ID对应的记录全部字段' })
  @ApiQuery({ name: 'dtos', type: [SysUserUDto] })
  @AopUserLoader(false)
  async updateList(@Body() dtos: SysUserUDto[]): Promise<any> {
    return this.sysUserService.updateList(dtos);
  }

  /**
   * 更新有值字段到指定ID记录
   * @param dto
   */
  @Permission(SYS_USER_PMS.UPDATE_SELECTIVE)
  @Put('update/by/id/selective')
  @ApiOperation({ summary: '更新有值字段到指定ID的记录' })
  @ApiQuery({ name: 'dto', type: SysUserUDto })
  @AopUserLoader(false)
  async updateSelective(@Body() dto: SysUserUDto): Promise<any> {
    return this.sysUserService.updateSelective(dto);
  }

  /**
   * 批量更新有值字段到ID对应的记录
   * @param dtos
   */
  @Permission(SYS_USER_PMS.UPDATE_LIST_SELECTIVE)
  @Put('update/list/selective')
  @ApiOperation({ summary: '批量更新有值字段到ID对应的记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserUDto] })
  @AopUserLoader(false)
  async updateListSelective(@Body() dtos: SysUserUDto[]): Promise<any> {
    return this.sysUserService.updateListSelective(dtos);
  }

  /**
   * 创建新记录
   * @param dto
   */
  @Permission(SYS_USER_PMS.CREATE)
  @Post('create')
  @ApiOperation({ summary: '创建新记录' })
  @ApiQuery({ name: 'dto', type: SysUserCDto })
  @AopUserLoader(false)
  async create(@Body() dto: SysUserCDto): Promise<SysUserCDto> {
    return this.sysUserService.create(dto);
  }

  /**
   * 批量创建新记录
   * @param dto
   */
  @Permission(SYS_USER_PMS.CREATE_LIST)
  @Post('create/list')
  @ApiOperation({ summary: '批量创建新记录' })
  @ApiQuery({ name: 'dtos', type: [SysUserCDto] })
  @AopUserLoader(false)
  async createList(@Body() dtos: SysUserCDto[]): Promise<SysUserCDto[]> {
    return this.sysUserService.createList(dtos);
  }
}
