import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysRoleMenuColNames, SysRoleMenuColProps } from '../../entity/ddl/sys.role.menu.cols';
/**
 * SYS_ROLE_MENU表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysRoleMenuQDto
 * @class SysRoleMenuQDto
 */
@ApiTags('SYS_ROLE_MENU表的QDTO对象')
export class SysRoleMenuQDto {
  /**
   * 角色菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'rmId', type: 'string', description: '角色菜单ID' })
  rmId?: string;
  @ApiProperty({ name: 'nrmId', type: 'string', description: '角色菜单ID' })
  nrmId?: string;
  @ApiPropertyOptional({ name: 'rmIdList', type: 'string[]', description: '角色菜单ID-列表条件' })
  rmIdList?: string[];
  @ApiPropertyOptional({ name: 'nrmIdList', type: 'string[]', description: '角色菜单ID-列表条件' })
  nrmIdList?: string[];
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'rmRoleId', type: 'string', description: '角色ID' })
  rmRoleId?: string;
  @ApiProperty({ name: 'nrmRoleId', type: 'string', description: '角色ID' })
  nrmRoleId?: string;
  @ApiPropertyOptional({ name: 'rmRoleIdLike', type: 'string', description: '角色ID-模糊条件' })
  rmRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'rmRoleIdList', type: 'string[]', description: '角色ID-列表条件' })
  rmRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'nrmRoleIdLike', type: 'string', description: '角色ID-模糊条件' })
  nrmRoleIdLike?: string;
  @ApiPropertyOptional({ name: 'nrmRoleIdList', type: 'string[]', description: '角色ID-列表条件' })
  nrmRoleIdList?: string[];
  @ApiPropertyOptional({ name: 'rmRoleIdLikeList', type: 'string[]', description: '角色ID-列表模糊条件' })
  rmRoleIdLikeList?: string[];
  /**
   * 菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'rmMenuId', type: 'string', description: '菜单ID' })
  rmMenuId?: string;
  @ApiProperty({ name: 'nrmMenuId', type: 'string', description: '菜单ID' })
  nrmMenuId?: string;
  @ApiPropertyOptional({ name: 'rmMenuIdLike', type: 'string', description: '菜单ID-模糊条件' })
  rmMenuIdLike?: string;
  @ApiPropertyOptional({ name: 'rmMenuIdList', type: 'string[]', description: '菜单ID-列表条件' })
  rmMenuIdList?: string[];
  @ApiPropertyOptional({ name: 'nrmMenuIdLike', type: 'string', description: '菜单ID-模糊条件' })
  nrmMenuIdLike?: string;
  @ApiPropertyOptional({ name: 'nrmMenuIdList', type: 'string[]', description: '菜单ID-列表条件' })
  nrmMenuIdList?: string[];
  @ApiPropertyOptional({ name: 'rmMenuIdLikeList', type: 'string[]', description: '菜单ID-列表模糊条件' })
  rmMenuIdLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysRoleMenuColNames)[]|(keyof SysRoleMenuColProps)[] }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysRoleMenuColNames)[] | (keyof SysRoleMenuColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysRoleMenuQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
