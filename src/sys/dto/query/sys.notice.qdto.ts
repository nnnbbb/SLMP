import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysNoticeColNames, SysNoticeColProps } from '../../entity/ddl/sys.notice.cols';
/**
 * 用户提示消息表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysNoticeQDto
 * @class SysNoticeQDto
 */
@ApiTags('SYS_NOTICE表的QDTO对象')
export class SysNoticeQDto {
  /**
   * 消息编号
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'noticeId', type: 'string', description: '消息编号' })
  noticeId?: string;
  @ApiProperty({ name: 'nnoticeId', type: 'string', description: '消息编号' })
  nnoticeId?: string;
  @ApiPropertyOptional({ name: 'noticeIdList', type: 'string[]', description: '消息编号-列表条件' })
  noticeIdList?: string[];
  @ApiPropertyOptional({ name: 'nnoticeIdList', type: 'string[]', description: '消息编号-列表条件' })
  nnoticeIdList?: string[];
  /**
   * 消息标题
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeTitle', type: 'string', description: '消息标题' })
  noticeTitle?: string;
  @ApiPropertyOptional({ name: 'nnoticeTitle', type: 'string', description: '消息标题' })
  nnoticeTitle?: string;
  @ApiPropertyOptional({ name: 'noticeTitleLike', type: 'string', description: '消息标题-模糊条件' })
  noticeTitleLike?: string;
  @ApiPropertyOptional({ name: 'noticeTitleList', type: 'string[]', description: '消息标题-列表条件' })
  noticeTitleList?: string[];
  @ApiPropertyOptional({ name: 'nnoticeTitleLike', type: 'string', description: '消息标题-模糊条件' })
  nnoticeTitleLike?: string;
  @ApiPropertyOptional({ name: 'nnoticeTitleList', type: 'string[]', description: '消息标题-列表条件' })
  nnoticeTitleList?: string[];
  @ApiPropertyOptional({ name: 'noticeTitleLikeList', type: 'string[]', description: '消息标题-列表模糊条件' })
  noticeTitleLikeList?: string[];
  /**
   * 消息内容
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeContent', type: 'string', description: '消息内容' })
  noticeContent?: string;
  @ApiPropertyOptional({ name: 'nnoticeContent', type: 'string', description: '消息内容' })
  nnoticeContent?: string;
  @ApiPropertyOptional({ name: 'noticeContentLike', type: 'string', description: '消息内容-模糊条件' })
  noticeContentLike?: string;
  @ApiPropertyOptional({ name: 'noticeContentList', type: 'string[]', description: '消息内容-列表条件' })
  noticeContentList?: string[];
  @ApiPropertyOptional({ name: 'nnoticeContentLike', type: 'string', description: '消息内容-模糊条件' })
  nnoticeContentLike?: string;
  @ApiPropertyOptional({ name: 'nnoticeContentList', type: 'string[]', description: '消息内容-列表条件' })
  nnoticeContentList?: string[];
  @ApiPropertyOptional({ name: 'noticeContentLikeList', type: 'string[]', description: '消息内容-列表模糊条件' })
  noticeContentLikeList?: string[];
  /**
   * 消息发布者
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeManager', type: 'string', description: '消息发布者' })
  noticeManager?: string;
  @ApiPropertyOptional({ name: 'nnoticeManager', type: 'string', description: '消息发布者' })
  nnoticeManager?: string;
  @ApiPropertyOptional({ name: 'noticeManagerLike', type: 'string', description: '消息发布者-模糊条件' })
  noticeManagerLike?: string;
  @ApiPropertyOptional({ name: 'noticeManagerList', type: 'string[]', description: '消息发布者-列表条件' })
  noticeManagerList?: string[];
  @ApiPropertyOptional({ name: 'nnoticeManagerLike', type: 'string', description: '消息发布者-模糊条件' })
  nnoticeManagerLike?: string;
  @ApiPropertyOptional({ name: 'nnoticeManagerList', type: 'string[]', description: '消息发布者-列表条件' })
  nnoticeManagerList?: string[];
  @ApiPropertyOptional({ name: 'noticeManagerLikeList', type: 'string[]', description: '消息发布者-列表模糊条件' })
  noticeManagerLikeList?: string[];
  /**
   * 是否是新消息
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeIsNew', type: 'string', description: '是否是新消息' })
  noticeIsNew?: string;
  @ApiPropertyOptional({ name: 'nnoticeIsNew', type: 'string', description: '是否是新消息' })
  nnoticeIsNew?: string;
  @ApiPropertyOptional({ name: 'noticeIsNewLike', type: 'string', description: '是否是新消息-模糊条件' })
  noticeIsNewLike?: string;
  @ApiPropertyOptional({ name: 'noticeIsNewList', type: 'string[]', description: '是否是新消息-列表条件' })
  noticeIsNewList?: string[];
  @ApiPropertyOptional({ name: 'nnoticeIsNewLike', type: 'string', description: '是否是新消息-模糊条件' })
  nnoticeIsNewLike?: string;
  @ApiPropertyOptional({ name: 'nnoticeIsNewList', type: 'string[]', description: '是否是新消息-列表条件' })
  nnoticeIsNewList?: string[];
  @ApiPropertyOptional({ name: 'noticeIsNewLikeList', type: 'string[]', description: '是否是新消息-列表模糊条件' })
  noticeIsNewLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  noticeCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nnoticeCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nnoticeCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sNoticeCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sNoticeCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eNoticeCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eNoticeCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  noticeUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nnoticeUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nnoticeUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sNoticeUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sNoticeUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eNoticeUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eNoticeUpdateDate?: Date;
  /**
   * 消息接收者
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiPropertyOptional({ name: 'noticeReceiver', type: 'string', description: '消息接收者' })
  noticeReceiver?: string;
  @ApiPropertyOptional({ name: 'nnoticeReceiver', type: 'string', description: '消息接收者' })
  nnoticeReceiver?: string;
  @ApiPropertyOptional({ name: 'noticeReceiverLike', type: 'string', description: '消息接收者-模糊条件' })
  noticeReceiverLike?: string;
  @ApiPropertyOptional({ name: 'noticeReceiverList', type: 'string[]', description: '消息接收者-列表条件' })
  noticeReceiverList?: string[];
  @ApiPropertyOptional({ name: 'nnoticeReceiverLike', type: 'string', description: '消息接收者-模糊条件' })
  nnoticeReceiverLike?: string;
  @ApiPropertyOptional({ name: 'nnoticeReceiverList', type: 'string[]', description: '消息接收者-列表条件' })
  nnoticeReceiverList?: string[];
  @ApiPropertyOptional({ name: 'noticeReceiverLikeList', type: 'string[]', description: '消息接收者-列表模糊条件' })
  noticeReceiverLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysNoticeColNames)[]|(keyof SysNoticeColProps)[] }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysNoticeColNames)[] | (keyof SysNoticeColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysNoticeQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
