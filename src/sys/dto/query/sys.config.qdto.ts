import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysConfigColNames, SysConfigColProps } from '../../entity/ddl/sys.config.cols';
/**
 * SYS_CONFIG表的QDTO对象
 * @date 1/7/2021, 10:05:34 AM
 * @author jiangbin
 * @export SysConfigQDto
 * @class SysConfigQDto
 */
@ApiTags('SYS_CONFIG表的QDTO对象')
export class SysConfigQDto {
  /**
   * ID
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'conId', type: 'string', description: 'ID' })
  conId?: string;
  @ApiProperty({ name: 'nconId', type: 'string', description: 'ID' })
  nconId?: string;
  @ApiPropertyOptional({ name: 'conIdList', type: 'string[]', description: 'ID-列表条件' })
  conIdList?: string[];
  @ApiPropertyOptional({ name: 'nconIdList', type: 'string[]', description: 'ID-列表条件' })
  nconIdList?: string[];
  /**
   * 参数名称
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'conName', type: 'string', description: '参数名称' })
  conName?: string;
  @ApiProperty({ name: 'nconName', type: 'string', description: '参数名称' })
  nconName?: string;
  @ApiPropertyOptional({ name: 'conNameLike', type: 'string', description: '参数名称-模糊条件' })
  conNameLike?: string;
  @ApiPropertyOptional({ name: 'conNameList', type: 'string[]', description: '参数名称-列表条件' })
  conNameList?: string[];
  @ApiPropertyOptional({ name: 'nconNameLike', type: 'string', description: '参数名称-模糊条件' })
  nconNameLike?: string;
  @ApiPropertyOptional({ name: 'nconNameList', type: 'string[]', description: '参数名称-列表条件' })
  nconNameList?: string[];
  @ApiPropertyOptional({ name: 'conNameLikeList', type: 'string[]', description: '参数名称-列表模糊条件' })
  conNameLikeList?: string[];
  /**
   * 参数名称
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conNameEn', type: 'string', description: '参数名称' })
  conNameEn?: string;
  @ApiPropertyOptional({ name: 'nconNameEn', type: 'string', description: '参数名称' })
  nconNameEn?: string;
  @ApiPropertyOptional({ name: 'conNameEnLike', type: 'string', description: '参数名称-模糊条件' })
  conNameEnLike?: string;
  @ApiPropertyOptional({ name: 'conNameEnList', type: 'string[]', description: '参数名称-列表条件' })
  conNameEnList?: string[];
  @ApiPropertyOptional({ name: 'nconNameEnLike', type: 'string', description: '参数名称-模糊条件' })
  nconNameEnLike?: string;
  @ApiPropertyOptional({ name: 'nconNameEnList', type: 'string[]', description: '参数名称-列表条件' })
  nconNameEnList?: string[];
  @ApiPropertyOptional({ name: 'conNameEnLikeList', type: 'string[]', description: '参数名称-列表模糊条件' })
  conNameEnLikeList?: string[];
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conValue', type: 'string', description: '参数值' })
  conValue?: string;
  @ApiPropertyOptional({ name: 'nconValue', type: 'string', description: '参数值' })
  nconValue?: string;
  @ApiPropertyOptional({ name: 'conValueLike', type: 'string', description: '参数值-模糊条件' })
  conValueLike?: string;
  @ApiPropertyOptional({ name: 'conValueList', type: 'string[]', description: '参数值-列表条件' })
  conValueList?: string[];
  @ApiPropertyOptional({ name: 'nconValueLike', type: 'string', description: '参数值-模糊条件' })
  nconValueLike?: string;
  @ApiPropertyOptional({ name: 'nconValueList', type: 'string[]', description: '参数值-列表条件' })
  nconValueList?: string[];
  @ApiPropertyOptional({ name: 'conValueLikeList', type: 'string[]', description: '参数值-列表模糊条件' })
  conValueLikeList?: string[];
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conGroup', type: 'string', description: '所属组' })
  conGroup?: string;
  @ApiPropertyOptional({ name: 'nconGroup', type: 'string', description: '所属组' })
  nconGroup?: string;
  @ApiPropertyOptional({ name: 'conGroupLike', type: 'string', description: '所属组-模糊条件' })
  conGroupLike?: string;
  @ApiPropertyOptional({ name: 'conGroupList', type: 'string[]', description: '所属组-列表条件' })
  conGroupList?: string[];
  @ApiPropertyOptional({ name: 'nconGroupLike', type: 'string', description: '所属组-模糊条件' })
  nconGroupLike?: string;
  @ApiPropertyOptional({ name: 'nconGroupList', type: 'string[]', description: '所属组-列表条件' })
  nconGroupList?: string[];
  @ApiPropertyOptional({ name: 'conGroupLikeList', type: 'string[]', description: '所属组-列表模糊条件' })
  conGroupLikeList?: string[];
  /**
   * 参数类型
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conParamType', type: 'string', description: '参数类型' })
  conParamType?: string;
  @ApiPropertyOptional({ name: 'nconParamType', type: 'string', description: '参数类型' })
  nconParamType?: string;
  @ApiPropertyOptional({ name: 'conParamTypeLike', type: 'string', description: '参数类型-模糊条件' })
  conParamTypeLike?: string;
  @ApiPropertyOptional({ name: 'conParamTypeList', type: 'string[]', description: '参数类型-列表条件' })
  conParamTypeList?: string[];
  @ApiPropertyOptional({ name: 'nconParamTypeLike', type: 'string', description: '参数类型-模糊条件' })
  nconParamTypeLike?: string;
  @ApiPropertyOptional({ name: 'nconParamTypeList', type: 'string[]', description: '参数类型-列表条件' })
  nconParamTypeList?: string[];
  @ApiPropertyOptional({ name: 'conParamTypeLikeList', type: 'string[]', description: '参数类型-列表模糊条件' })
  conParamTypeLikeList?: string[];
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conParentId', type: 'string', description: '父级ID' })
  conParentId?: string;
  @ApiPropertyOptional({ name: 'nconParentId', type: 'string', description: '父级ID' })
  nconParentId?: string;
  @ApiPropertyOptional({ name: 'conParentIdLike', type: 'string', description: '父级ID-模糊条件' })
  conParentIdLike?: string;
  @ApiPropertyOptional({ name: 'conParentIdList', type: 'string[]', description: '父级ID-列表条件' })
  conParentIdList?: string[];
  @ApiPropertyOptional({ name: 'nconParentIdLike', type: 'string', description: '父级ID-模糊条件' })
  nconParentIdLike?: string;
  @ApiPropertyOptional({ name: 'nconParentIdList', type: 'string[]', description: '父级ID-列表条件' })
  nconParentIdList?: string[];
  @ApiPropertyOptional({ name: 'conParentIdLikeList', type: 'string[]', description: '父级ID-列表模糊条件' })
  conParentIdLikeList?: string[];
  /**
   * 参数类型
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'conType', type: 'string', description: '参数类型' })
  conType?: string;
  @ApiProperty({ name: 'nconType', type: 'string', description: '参数类型' })
  nconType?: string;
  @ApiPropertyOptional({ name: 'conTypeLike', type: 'string', description: '参数类型-模糊条件' })
  conTypeLike?: string;
  @ApiPropertyOptional({ name: 'conTypeList', type: 'string[]', description: '参数类型-列表条件' })
  conTypeList?: string[];
  @ApiPropertyOptional({ name: 'nconTypeLike', type: 'string', description: '参数类型-模糊条件' })
  nconTypeLike?: string;
  @ApiPropertyOptional({ name: 'nconTypeList', type: 'string[]', description: '参数类型-列表条件' })
  nconTypeList?: string[];
  @ApiPropertyOptional({ name: 'conTypeLikeList', type: 'string[]', description: '参数类型-列表模糊条件' })
  conTypeLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'conCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  conCreateDate?: Date;
  @ApiProperty({ name: 'nconCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nconCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sConCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sConCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eConCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eConCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  conUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nconUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nconUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sConUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sConUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eConUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eConUpdateDate?: Date;
  /**
   * 用户
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conManager', type: 'string', description: '用户' })
  conManager?: string;
  @ApiPropertyOptional({ name: 'nconManager', type: 'string', description: '用户' })
  nconManager?: string;
  @ApiPropertyOptional({ name: 'conManagerLike', type: 'string', description: '用户-模糊条件' })
  conManagerLike?: string;
  @ApiPropertyOptional({ name: 'conManagerList', type: 'string[]', description: '用户-列表条件' })
  conManagerList?: string[];
  @ApiPropertyOptional({ name: 'nconManagerLike', type: 'string', description: '用户-模糊条件' })
  nconManagerLike?: string;
  @ApiPropertyOptional({ name: 'nconManagerList', type: 'string[]', description: '用户-列表条件' })
  nconManagerList?: string[];
  @ApiPropertyOptional({ name: 'conManagerLikeList', type: 'string[]', description: '用户-列表模糊条件' })
  conManagerLikeList?: string[];
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conSpecies', type: 'string', description: '种属' })
  conSpecies?: string;
  @ApiPropertyOptional({ name: 'nconSpecies', type: 'string', description: '种属' })
  nconSpecies?: string;
  @ApiPropertyOptional({ name: 'conSpeciesLike', type: 'string', description: '种属-模糊条件' })
  conSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'conSpeciesList', type: 'string[]', description: '种属-列表条件' })
  conSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'nconSpeciesLike', type: 'string', description: '种属-模糊条件' })
  nconSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'nconSpeciesList', type: 'string[]', description: '种属-列表条件' })
  nconSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'conSpeciesLikeList', type: 'string[]', description: '种属-列表模糊条件' })
  conSpeciesLikeList?: string[];
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conOrder', type: 'number', description: '排序' })
  conOrder?: number;
  @ApiPropertyOptional({ name: 'nconOrder', type: 'number', description: '排序' })
  nconOrder?: number;
  @ApiPropertyOptional({ name: 'minConOrder', type: 'number', description: '排序-最小值' })
  minConOrder?: number;
  @ApiPropertyOptional({ name: 'maxConOrder', type: 'number', description: '排序-最大值' })
  maxConOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'conState', type: 'string', description: '状态' })
  conState?: string;
  @ApiProperty({ name: 'nconState', type: 'string', description: '状态' })
  nconState?: string;
  @ApiPropertyOptional({ name: 'conStateLike', type: 'string', description: '状态-模糊条件' })
  conStateLike?: string;
  @ApiPropertyOptional({ name: 'conStateList', type: 'string[]', description: '状态-列表条件' })
  conStateList?: string[];
  @ApiPropertyOptional({ name: 'nconStateLike', type: 'string', description: '状态-模糊条件' })
  nconStateLike?: string;
  @ApiPropertyOptional({ name: 'nconStateList', type: 'string[]', description: '状态-列表条件' })
  nconStateList?: string[];
  @ApiPropertyOptional({ name: 'conStateLikeList', type: 'string[]', description: '状态-列表模糊条件' })
  conStateLikeList?: string[];
  /**
   * 描述
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiPropertyOptional({ name: 'conRemark', type: 'string', description: '描述' })
  conRemark?: string;
  @ApiPropertyOptional({ name: 'nconRemark', type: 'string', description: '描述' })
  nconRemark?: string;
  @ApiPropertyOptional({ name: 'conRemarkLike', type: 'string', description: '描述-模糊条件' })
  conRemarkLike?: string;
  @ApiPropertyOptional({ name: 'conRemarkList', type: 'string[]', description: '描述-列表条件' })
  conRemarkList?: string[];
  @ApiPropertyOptional({ name: 'nconRemarkLike', type: 'string', description: '描述-模糊条件' })
  nconRemarkLike?: string;
  @ApiPropertyOptional({ name: 'nconRemarkList', type: 'string[]', description: '描述-列表条件' })
  nconRemarkList?: string[];
  @ApiPropertyOptional({ name: 'conRemarkLikeList', type: 'string[]', description: '描述-列表模糊条件' })
  conRemarkLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysConfigColNames)[]|(keyof SysConfigColProps)[] }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysConfigColNames)[] | (keyof SysConfigColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysConfigQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
