import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysCmsFilesColNames, SysCmsFilesColProps } from '../../entity/ddl/sys.cms.files.cols';
/**
 * SYS_CMS_FILES表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysCmsFilesQDto
 * @class SysCmsFilesQDto
 */
@ApiTags('SYS_CMS_FILES表的QDTO对象')
export class SysCmsFilesQDto {
  /**
   * 文件编号
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'scFileId', type: 'string', description: '文件编号' })
  scFileId?: string;
  @ApiProperty({ name: 'nscFileId', type: 'string', description: '文件编号' })
  nscFileId?: string;
  @ApiPropertyOptional({ name: 'scFileIdList', type: 'string[]', description: '文件编号-列表条件' })
  scFileIdList?: string[];
  @ApiPropertyOptional({ name: 'nscFileIdList', type: 'string[]', description: '文件编号-列表条件' })
  nscFileIdList?: string[];
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFileName', type: 'string', description: '文件名称' })
  scFileName?: string;
  @ApiPropertyOptional({ name: 'nscFileName', type: 'string', description: '文件名称' })
  nscFileName?: string;
  @ApiPropertyOptional({ name: 'scFileNameLike', type: 'string', description: '文件名称-模糊条件' })
  scFileNameLike?: string;
  @ApiPropertyOptional({ name: 'scFileNameList', type: 'string[]', description: '文件名称-列表条件' })
  scFileNameList?: string[];
  @ApiPropertyOptional({ name: 'nscFileNameLike', type: 'string', description: '文件名称-模糊条件' })
  nscFileNameLike?: string;
  @ApiPropertyOptional({ name: 'nscFileNameList', type: 'string[]', description: '文件名称-列表条件' })
  nscFileNameList?: string[];
  @ApiPropertyOptional({ name: 'scFileNameLikeList', type: 'string[]', description: '文件名称-列表模糊条件' })
  scFileNameLikeList?: string[];
  /**
   * scFileNameCn
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFileNameCn', type: 'string', description: 'scFileNameCn' })
  scFileNameCn?: string;
  @ApiPropertyOptional({ name: 'nscFileNameCn', type: 'string', description: 'scFileNameCn' })
  nscFileNameCn?: string;
  @ApiPropertyOptional({ name: 'scFileNameCnLike', type: 'string', description: 'scFileNameCn-模糊条件' })
  scFileNameCnLike?: string;
  @ApiPropertyOptional({ name: 'scFileNameCnList', type: 'string[]', description: 'scFileNameCn-列表条件' })
  scFileNameCnList?: string[];
  @ApiPropertyOptional({ name: 'nscFileNameCnLike', type: 'string', description: 'scFileNameCn-模糊条件' })
  nscFileNameCnLike?: string;
  @ApiPropertyOptional({ name: 'nscFileNameCnList', type: 'string[]', description: 'scFileNameCn-列表条件' })
  nscFileNameCnList?: string[];
  @ApiPropertyOptional({ name: 'scFileNameCnLikeList', type: 'string[]', description: 'scFileNameCn-列表模糊条件' })
  scFileNameCnLikeList?: string[];
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'scFileType', type: 'string', description: '文件类型' })
  scFileType?: string;
  @ApiProperty({ name: 'nscFileType', type: 'string', description: '文件类型' })
  nscFileType?: string;
  @ApiPropertyOptional({ name: 'scFileTypeLike', type: 'string', description: '文件类型-模糊条件' })
  scFileTypeLike?: string;
  @ApiPropertyOptional({ name: 'scFileTypeList', type: 'string[]', description: '文件类型-列表条件' })
  scFileTypeList?: string[];
  @ApiPropertyOptional({ name: 'nscFileTypeLike', type: 'string', description: '文件类型-模糊条件' })
  nscFileTypeLike?: string;
  @ApiPropertyOptional({ name: 'nscFileTypeList', type: 'string[]', description: '文件类型-列表条件' })
  nscFileTypeList?: string[];
  @ApiPropertyOptional({ name: 'scFileTypeLikeList', type: 'string[]', description: '文件类型-列表模糊条件' })
  scFileTypeLikeList?: string[];
  /**
   * 文件路径
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFilePath', type: 'string', description: '文件路径' })
  scFilePath?: string;
  @ApiPropertyOptional({ name: 'nscFilePath', type: 'string', description: '文件路径' })
  nscFilePath?: string;
  @ApiPropertyOptional({ name: 'scFilePathLike', type: 'string', description: '文件路径-模糊条件' })
  scFilePathLike?: string;
  @ApiPropertyOptional({ name: 'scFilePathList', type: 'string[]', description: '文件路径-列表条件' })
  scFilePathList?: string[];
  @ApiPropertyOptional({ name: 'nscFilePathLike', type: 'string', description: '文件路径-模糊条件' })
  nscFilePathLike?: string;
  @ApiPropertyOptional({ name: 'nscFilePathList', type: 'string[]', description: '文件路径-列表条件' })
  nscFilePathList?: string[];
  @ApiPropertyOptional({ name: 'scFilePathLikeList', type: 'string[]', description: '文件路径-列表模糊条件' })
  scFilePathLikeList?: string[];
  /**
   * 是否是最新
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFileIsNew', type: 'string', description: '是否是最新' })
  scFileIsNew?: string;
  @ApiPropertyOptional({ name: 'nscFileIsNew', type: 'string', description: '是否是最新' })
  nscFileIsNew?: string;
  @ApiPropertyOptional({ name: 'scFileIsNewLike', type: 'string', description: '是否是最新-模糊条件' })
  scFileIsNewLike?: string;
  @ApiPropertyOptional({ name: 'scFileIsNewList', type: 'string[]', description: '是否是最新-列表条件' })
  scFileIsNewList?: string[];
  @ApiPropertyOptional({ name: 'nscFileIsNewLike', type: 'string', description: '是否是最新-模糊条件' })
  nscFileIsNewLike?: string;
  @ApiPropertyOptional({ name: 'nscFileIsNewList', type: 'string[]', description: '是否是最新-列表条件' })
  nscFileIsNewList?: string[];
  @ApiPropertyOptional({ name: 'scFileIsNewLikeList', type: 'string[]', description: '是否是最新-列表模糊条件' })
  scFileIsNewLikeList?: string[];
  /**
   * 文件上传者
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFileManager', type: 'string', description: '文件上传者' })
  scFileManager?: string;
  @ApiPropertyOptional({ name: 'nscFileManager', type: 'string', description: '文件上传者' })
  nscFileManager?: string;
  @ApiPropertyOptional({ name: 'scFileManagerLike', type: 'string', description: '文件上传者-模糊条件' })
  scFileManagerLike?: string;
  @ApiPropertyOptional({ name: 'scFileManagerList', type: 'string[]', description: '文件上传者-列表条件' })
  scFileManagerList?: string[];
  @ApiPropertyOptional({ name: 'nscFileManagerLike', type: 'string', description: '文件上传者-模糊条件' })
  nscFileManagerLike?: string;
  @ApiPropertyOptional({ name: 'nscFileManagerList', type: 'string[]', description: '文件上传者-列表条件' })
  nscFileManagerList?: string[];
  @ApiPropertyOptional({ name: 'scFileManagerLikeList', type: 'string[]', description: '文件上传者-列表模糊条件' })
  scFileManagerLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFileCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  scFileCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nscFileCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nscFileCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sScFileCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sScFileCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eScFileCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eScFileCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFielUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  scFielUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nscFielUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nscFielUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sScFielUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sScFielUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eScFielUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eScFielUpdateDate?: Date;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiPropertyOptional({ name: 'scFileSpecies', type: 'string', description: '种属' })
  scFileSpecies?: string;
  @ApiPropertyOptional({ name: 'nscFileSpecies', type: 'string', description: '种属' })
  nscFileSpecies?: string;
  @ApiPropertyOptional({ name: 'scFileSpeciesLike', type: 'string', description: '种属-模糊条件' })
  scFileSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'scFileSpeciesList', type: 'string[]', description: '种属-列表条件' })
  scFileSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'nscFileSpeciesLike', type: 'string', description: '种属-模糊条件' })
  nscFileSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'nscFileSpeciesList', type: 'string[]', description: '种属-列表条件' })
  nscFileSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'scFileSpeciesLikeList', type: 'string[]', description: '种属-列表模糊条件' })
  scFileSpeciesLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysCmsFilesColNames)[]|(keyof SysCmsFilesColProps)[] }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysCmsFilesColNames)[] | (keyof SysCmsFilesColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysCmsFilesQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
