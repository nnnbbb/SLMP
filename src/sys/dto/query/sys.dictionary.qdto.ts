import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysDictionaryColNames, SysDictionaryColProps } from '../../entity/ddl/sys.dictionary.cols';
/**
 * SYS_DICTIONARY表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysDictionaryQDto
 * @class SysDictionaryQDto
 */
@ApiTags('SYS_DICTIONARY表的QDTO对象')
export class SysDictionaryQDto {
  /**
   * ID
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'dicId', type: 'string', description: 'ID' })
  dicId?: string;
  @ApiProperty({ name: 'ndicId', type: 'string', description: 'ID' })
  ndicId?: string;
  @ApiPropertyOptional({ name: 'dicIdList', type: 'string[]', description: 'ID-列表条件' })
  dicIdList?: string[];
  @ApiPropertyOptional({ name: 'ndicIdList', type: 'string[]', description: 'ID-列表条件' })
  ndicIdList?: string[];
  /**
   * 字典名称
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'dicName', type: 'string', description: '字典名称' })
  dicName?: string;
  @ApiProperty({ name: 'ndicName', type: 'string', description: '字典名称' })
  ndicName?: string;
  @ApiPropertyOptional({ name: 'dicNameLike', type: 'string', description: '字典名称-模糊条件' })
  dicNameLike?: string;
  @ApiPropertyOptional({ name: 'dicNameList', type: 'string[]', description: '字典名称-列表条件' })
  dicNameList?: string[];
  @ApiPropertyOptional({ name: 'ndicNameLike', type: 'string', description: '字典名称-模糊条件' })
  ndicNameLike?: string;
  @ApiPropertyOptional({ name: 'ndicNameList', type: 'string[]', description: '字典名称-列表条件' })
  ndicNameList?: string[];
  @ApiPropertyOptional({ name: 'dicNameLikeList', type: 'string[]', description: '字典名称-列表模糊条件' })
  dicNameLikeList?: string[];
  /**
   * dicNameEn
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicNameEn', type: 'string', description: 'dicNameEn' })
  dicNameEn?: string;
  @ApiPropertyOptional({ name: 'ndicNameEn', type: 'string', description: 'dicNameEn' })
  ndicNameEn?: string;
  @ApiPropertyOptional({ name: 'dicNameEnLike', type: 'string', description: 'dicNameEn-模糊条件' })
  dicNameEnLike?: string;
  @ApiPropertyOptional({ name: 'dicNameEnList', type: 'string[]', description: 'dicNameEn-列表条件' })
  dicNameEnList?: string[];
  @ApiPropertyOptional({ name: 'ndicNameEnLike', type: 'string', description: 'dicNameEn-模糊条件' })
  ndicNameEnLike?: string;
  @ApiPropertyOptional({ name: 'ndicNameEnList', type: 'string[]', description: 'dicNameEn-列表条件' })
  ndicNameEnList?: string[];
  @ApiPropertyOptional({ name: 'dicNameEnLikeList', type: 'string[]', description: 'dicNameEn-列表模糊条件' })
  dicNameEnLikeList?: string[];
  /**
   * 字典值
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicValue', type: 'string', description: '字典值' })
  dicValue?: string;
  @ApiPropertyOptional({ name: 'ndicValue', type: 'string', description: '字典值' })
  ndicValue?: string;
  @ApiPropertyOptional({ name: 'dicValueLike', type: 'string', description: '字典值-模糊条件' })
  dicValueLike?: string;
  @ApiPropertyOptional({ name: 'dicValueList', type: 'string[]', description: '字典值-列表条件' })
  dicValueList?: string[];
  @ApiPropertyOptional({ name: 'ndicValueLike', type: 'string', description: '字典值-模糊条件' })
  ndicValueLike?: string;
  @ApiPropertyOptional({ name: 'ndicValueList', type: 'string[]', description: '字典值-列表条件' })
  ndicValueList?: string[];
  @ApiPropertyOptional({ name: 'dicValueLikeList', type: 'string[]', description: '字典值-列表模糊条件' })
  dicValueLikeList?: string[];
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicGroup', type: 'string', description: '所属组' })
  dicGroup?: string;
  @ApiPropertyOptional({ name: 'ndicGroup', type: 'string', description: '所属组' })
  ndicGroup?: string;
  @ApiPropertyOptional({ name: 'dicGroupLike', type: 'string', description: '所属组-模糊条件' })
  dicGroupLike?: string;
  @ApiPropertyOptional({ name: 'dicGroupList', type: 'string[]', description: '所属组-列表条件' })
  dicGroupList?: string[];
  @ApiPropertyOptional({ name: 'ndicGroupLike', type: 'string', description: '所属组-模糊条件' })
  ndicGroupLike?: string;
  @ApiPropertyOptional({ name: 'ndicGroupList', type: 'string[]', description: '所属组-列表条件' })
  ndicGroupList?: string[];
  @ApiPropertyOptional({ name: 'dicGroupLikeList', type: 'string[]', description: '所属组-列表模糊条件' })
  dicGroupLikeList?: string[];
  /**
   * 模块名
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicModule', type: 'string', description: '模块名' })
  dicModule?: string;
  @ApiPropertyOptional({ name: 'ndicModule', type: 'string', description: '模块名' })
  ndicModule?: string;
  @ApiPropertyOptional({ name: 'dicModuleLike', type: 'string', description: '模块名-模糊条件' })
  dicModuleLike?: string;
  @ApiPropertyOptional({ name: 'dicModuleList', type: 'string[]', description: '模块名-列表条件' })
  dicModuleList?: string[];
  @ApiPropertyOptional({ name: 'ndicModuleLike', type: 'string', description: '模块名-模糊条件' })
  ndicModuleLike?: string;
  @ApiPropertyOptional({ name: 'ndicModuleList', type: 'string[]', description: '模块名-列表条件' })
  ndicModuleList?: string[];
  @ApiPropertyOptional({ name: 'dicModuleLikeList', type: 'string[]', description: '模块名-列表模糊条件' })
  dicModuleLikeList?: string[];
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicParentId', type: 'string', description: '父级ID' })
  dicParentId?: string;
  @ApiPropertyOptional({ name: 'ndicParentId', type: 'string', description: '父级ID' })
  ndicParentId?: string;
  @ApiPropertyOptional({ name: 'dicParentIdLike', type: 'string', description: '父级ID-模糊条件' })
  dicParentIdLike?: string;
  @ApiPropertyOptional({ name: 'dicParentIdList', type: 'string[]', description: '父级ID-列表条件' })
  dicParentIdList?: string[];
  @ApiPropertyOptional({ name: 'ndicParentIdLike', type: 'string', description: '父级ID-模糊条件' })
  ndicParentIdLike?: string;
  @ApiPropertyOptional({ name: 'ndicParentIdList', type: 'string[]', description: '父级ID-列表条件' })
  ndicParentIdList?: string[];
  @ApiPropertyOptional({ name: 'dicParentIdLikeList', type: 'string[]', description: '父级ID-列表模糊条件' })
  dicParentIdLikeList?: string[];
  /**
   * 字典类型
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'dicType', type: 'string', description: '字典类型' })
  dicType?: string;
  @ApiProperty({ name: 'ndicType', type: 'string', description: '字典类型' })
  ndicType?: string;
  @ApiPropertyOptional({ name: 'dicTypeLike', type: 'string', description: '字典类型-模糊条件' })
  dicTypeLike?: string;
  @ApiPropertyOptional({ name: 'dicTypeList', type: 'string[]', description: '字典类型-列表条件' })
  dicTypeList?: string[];
  @ApiPropertyOptional({ name: 'ndicTypeLike', type: 'string', description: '字典类型-模糊条件' })
  ndicTypeLike?: string;
  @ApiPropertyOptional({ name: 'ndicTypeList', type: 'string[]', description: '字典类型-列表条件' })
  ndicTypeList?: string[];
  @ApiPropertyOptional({ name: 'dicTypeLikeList', type: 'string[]', description: '字典类型-列表模糊条件' })
  dicTypeLikeList?: string[];
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicOrder', type: 'number', description: '排序' })
  dicOrder?: number;
  @ApiPropertyOptional({ name: 'ndicOrder', type: 'number', description: '排序' })
  ndicOrder?: number;
  @ApiPropertyOptional({ name: 'minDicOrder', type: 'number', description: '排序-最小值' })
  minDicOrder?: number;
  @ApiPropertyOptional({ name: 'maxDicOrder', type: 'number', description: '排序-最大值' })
  maxDicOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'dicState', type: 'string', description: '状态' })
  dicState?: string;
  @ApiProperty({ name: 'ndicState', type: 'string', description: '状态' })
  ndicState?: string;
  @ApiPropertyOptional({ name: 'dicStateLike', type: 'string', description: '状态-模糊条件' })
  dicStateLike?: string;
  @ApiPropertyOptional({ name: 'dicStateList', type: 'string[]', description: '状态-列表条件' })
  dicStateList?: string[];
  @ApiPropertyOptional({ name: 'ndicStateLike', type: 'string', description: '状态-模糊条件' })
  ndicStateLike?: string;
  @ApiPropertyOptional({ name: 'ndicStateList', type: 'string[]', description: '状态-列表条件' })
  ndicStateList?: string[];
  @ApiPropertyOptional({ name: 'dicStateLikeList', type: 'string[]', description: '状态-列表模糊条件' })
  dicStateLikeList?: string[];
  /**
   * dicParams
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicParams', type: 'string', description: 'dicParams' })
  dicParams?: string;
  @ApiPropertyOptional({ name: 'ndicParams', type: 'string', description: 'dicParams' })
  ndicParams?: string;
  @ApiPropertyOptional({ name: 'dicParamsLike', type: 'string', description: 'dicParams-模糊条件' })
  dicParamsLike?: string;
  @ApiPropertyOptional({ name: 'dicParamsList', type: 'string[]', description: 'dicParams-列表条件' })
  dicParamsList?: string[];
  @ApiPropertyOptional({ name: 'ndicParamsLike', type: 'string', description: 'dicParams-模糊条件' })
  ndicParamsLike?: string;
  @ApiPropertyOptional({ name: 'ndicParamsList', type: 'string[]', description: 'dicParams-列表条件' })
  ndicParamsList?: string[];
  @ApiPropertyOptional({ name: 'dicParamsLikeList', type: 'string[]', description: 'dicParams-列表模糊条件' })
  dicParamsLikeList?: string[];
  /**
   * 关联种属
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicSpecies', type: 'string', description: '关联种属' })
  dicSpecies?: string;
  @ApiPropertyOptional({ name: 'ndicSpecies', type: 'string', description: '关联种属' })
  ndicSpecies?: string;
  @ApiPropertyOptional({ name: 'dicSpeciesLike', type: 'string', description: '关联种属-模糊条件' })
  dicSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'dicSpeciesList', type: 'string[]', description: '关联种属-列表条件' })
  dicSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'ndicSpeciesLike', type: 'string', description: '关联种属-模糊条件' })
  ndicSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'ndicSpeciesList', type: 'string[]', description: '关联种属-列表条件' })
  ndicSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'dicSpeciesLikeList', type: 'string[]', description: '关联种属-列表模糊条件' })
  dicSpeciesLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  dicCreateDate?: Date;
  @ApiPropertyOptional({ name: 'ndicCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  ndicCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sDicCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sDicCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eDicCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eDicCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysDictionaryQDto
   */
  @ApiPropertyOptional({ name: 'dicUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  dicUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'ndicUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  ndicUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sDicUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sDicUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eDicUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eDicUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysDictionaryColNames)[]|(keyof SysDictionaryColProps)[] }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysDictionaryColNames)[] | (keyof SysDictionaryColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysDictionaryQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
