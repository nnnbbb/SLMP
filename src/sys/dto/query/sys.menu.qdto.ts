import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysMenuColNames, SysMenuColProps } from '../../entity/ddl/sys.menu.cols';
/**
 * SYS_MENU表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysMenuQDto
 * @class SysMenuQDto
 */
@ApiTags('SYS_MENU表的QDTO对象')
export class SysMenuQDto {
  /**
   * 菜单ID号
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'menuId', type: 'string', description: '菜单ID号' })
  menuId?: string;
  @ApiProperty({ name: 'nmenuId', type: 'string', description: '菜单ID号' })
  nmenuId?: string;
  @ApiPropertyOptional({ name: 'menuIdList', type: 'string[]', description: '菜单ID号-列表条件' })
  menuIdList?: string[];
  @ApiPropertyOptional({ name: 'nmenuIdList', type: 'string[]', description: '菜单ID号-列表条件' })
  nmenuIdList?: string[];
  /**
   * 菜单名称
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'menuName', type: 'string', description: '菜单名称' })
  menuName?: string;
  @ApiProperty({ name: 'nmenuName', type: 'string', description: '菜单名称' })
  nmenuName?: string;
  @ApiPropertyOptional({ name: 'menuNameLike', type: 'string', description: '菜单名称-模糊条件' })
  menuNameLike?: string;
  @ApiPropertyOptional({ name: 'menuNameList', type: 'string[]', description: '菜单名称-列表条件' })
  menuNameList?: string[];
  @ApiPropertyOptional({ name: 'nmenuNameLike', type: 'string', description: '菜单名称-模糊条件' })
  nmenuNameLike?: string;
  @ApiPropertyOptional({ name: 'nmenuNameList', type: 'string[]', description: '菜单名称-列表条件' })
  nmenuNameList?: string[];
  @ApiPropertyOptional({ name: 'menuNameLikeList', type: 'string[]', description: '菜单名称-列表模糊条件' })
  menuNameLikeList?: string[];
  /**
   * 英文菜单名
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiPropertyOptional({ name: 'menuNameEn', type: 'string', description: '英文菜单名' })
  menuNameEn?: string;
  @ApiPropertyOptional({ name: 'nmenuNameEn', type: 'string', description: '英文菜单名' })
  nmenuNameEn?: string;
  @ApiPropertyOptional({ name: 'menuNameEnLike', type: 'string', description: '英文菜单名-模糊条件' })
  menuNameEnLike?: string;
  @ApiPropertyOptional({ name: 'menuNameEnList', type: 'string[]', description: '英文菜单名-列表条件' })
  menuNameEnList?: string[];
  @ApiPropertyOptional({ name: 'nmenuNameEnLike', type: 'string', description: '英文菜单名-模糊条件' })
  nmenuNameEnLike?: string;
  @ApiPropertyOptional({ name: 'nmenuNameEnList', type: 'string[]', description: '英文菜单名-列表条件' })
  nmenuNameEnList?: string[];
  @ApiPropertyOptional({ name: 'menuNameEnLikeList', type: 'string[]', description: '英文菜单名-列表模糊条件' })
  menuNameEnLikeList?: string[];
  /**
   * 菜单连接地址
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiPropertyOptional({ name: 'menuUrl', type: 'string', description: '菜单连接地址' })
  menuUrl?: string;
  @ApiPropertyOptional({ name: 'nmenuUrl', type: 'string', description: '菜单连接地址' })
  nmenuUrl?: string;
  @ApiPropertyOptional({ name: 'menuUrlLike', type: 'string', description: '菜单连接地址-模糊条件' })
  menuUrlLike?: string;
  @ApiPropertyOptional({ name: 'menuUrlList', type: 'string[]', description: '菜单连接地址-列表条件' })
  menuUrlList?: string[];
  @ApiPropertyOptional({ name: 'nmenuUrlLike', type: 'string', description: '菜单连接地址-模糊条件' })
  nmenuUrlLike?: string;
  @ApiPropertyOptional({ name: 'nmenuUrlList', type: 'string[]', description: '菜单连接地址-列表条件' })
  nmenuUrlList?: string[];
  @ApiPropertyOptional({ name: 'menuUrlLikeList', type: 'string[]', description: '菜单连接地址-列表模糊条件' })
  menuUrlLikeList?: string[];
  /**
   * 父菜单ID
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiPropertyOptional({ name: 'menuParentId', type: 'string', description: '父菜单ID' })
  menuParentId?: string;
  @ApiPropertyOptional({ name: 'nmenuParentId', type: 'string', description: '父菜单ID' })
  nmenuParentId?: string;
  @ApiPropertyOptional({ name: 'menuParentIdLike', type: 'string', description: '父菜单ID-模糊条件' })
  menuParentIdLike?: string;
  @ApiPropertyOptional({ name: 'menuParentIdList', type: 'string[]', description: '父菜单ID-列表条件' })
  menuParentIdList?: string[];
  @ApiPropertyOptional({ name: 'nmenuParentIdLike', type: 'string', description: '父菜单ID-模糊条件' })
  nmenuParentIdLike?: string;
  @ApiPropertyOptional({ name: 'nmenuParentIdList', type: 'string[]', description: '父菜单ID-列表条件' })
  nmenuParentIdList?: string[];
  @ApiPropertyOptional({ name: 'menuParentIdLikeList', type: 'string[]', description: '父菜单ID-列表模糊条件' })
  menuParentIdLikeList?: string[];
  /**
   * 菜单顺序
   *
   * @type { number }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'menuOrder', type: 'number', description: '菜单顺序' })
  menuOrder?: number;
  @ApiProperty({ name: 'nmenuOrder', type: 'number', description: '菜单顺序' })
  nmenuOrder?: number;
  @ApiPropertyOptional({ name: 'minMenuOrder', type: 'number', description: '菜单顺序-最小值' })
  minMenuOrder?: number;
  @ApiPropertyOptional({ name: 'maxMenuOrder', type: 'number', description: '菜单顺序-最大值' })
  maxMenuOrder?: number;
  /**
   * 菜单备注
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiPropertyOptional({ name: 'menuRemark', type: 'string', description: '菜单备注' })
  menuRemark?: string;
  @ApiPropertyOptional({ name: 'nmenuRemark', type: 'string', description: '菜单备注' })
  nmenuRemark?: string;
  @ApiPropertyOptional({ name: 'menuRemarkLike', type: 'string', description: '菜单备注-模糊条件' })
  menuRemarkLike?: string;
  @ApiPropertyOptional({ name: 'menuRemarkList', type: 'string[]', description: '菜单备注-列表条件' })
  menuRemarkList?: string[];
  @ApiPropertyOptional({ name: 'nmenuRemarkLike', type: 'string', description: '菜单备注-模糊条件' })
  nmenuRemarkLike?: string;
  @ApiPropertyOptional({ name: 'nmenuRemarkList', type: 'string[]', description: '菜单备注-列表条件' })
  nmenuRemarkList?: string[];
  @ApiPropertyOptional({ name: 'menuRemarkLikeList', type: 'string[]', description: '菜单备注-列表模糊条件' })
  menuRemarkLikeList?: string[];
  /**
   * 菜单状态
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'menuState', type: 'string', description: '菜单状态' })
  menuState?: string;
  @ApiProperty({ name: 'nmenuState', type: 'string', description: '菜单状态' })
  nmenuState?: string;
  @ApiPropertyOptional({ name: 'menuStateLike', type: 'string', description: '菜单状态-模糊条件' })
  menuStateLike?: string;
  @ApiPropertyOptional({ name: 'menuStateList', type: 'string[]', description: '菜单状态-列表条件' })
  menuStateList?: string[];
  @ApiPropertyOptional({ name: 'nmenuStateLike', type: 'string', description: '菜单状态-模糊条件' })
  nmenuStateLike?: string;
  @ApiPropertyOptional({ name: 'nmenuStateList', type: 'string[]', description: '菜单状态-列表条件' })
  nmenuStateList?: string[];
  @ApiPropertyOptional({ name: 'menuStateLikeList', type: 'string[]', description: '菜单状态-列表模糊条件' })
  menuStateLikeList?: string[];
  /**
   * 菜单类型
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiPropertyOptional({ name: 'menuType', type: 'string', description: '菜单类型' })
  menuType?: string;
  @ApiPropertyOptional({ name: 'nmenuType', type: 'string', description: '菜单类型' })
  nmenuType?: string;
  @ApiPropertyOptional({ name: 'menuTypeLike', type: 'string', description: '菜单类型-模糊条件' })
  menuTypeLike?: string;
  @ApiPropertyOptional({ name: 'menuTypeList', type: 'string[]', description: '菜单类型-列表条件' })
  menuTypeList?: string[];
  @ApiPropertyOptional({ name: 'nmenuTypeLike', type: 'string', description: '菜单类型-模糊条件' })
  nmenuTypeLike?: string;
  @ApiPropertyOptional({ name: 'nmenuTypeList', type: 'string[]', description: '菜单类型-列表条件' })
  nmenuTypeList?: string[];
  @ApiPropertyOptional({ name: 'menuTypeLikeList', type: 'string[]', description: '菜单类型-列表模糊条件' })
  menuTypeLikeList?: string[];
  /**
   * 菜单图标CSS名称
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiPropertyOptional({ name: 'menuIconClass', type: 'string', description: '菜单图标CSS名称' })
  menuIconClass?: string;
  @ApiPropertyOptional({ name: 'nmenuIconClass', type: 'string', description: '菜单图标CSS名称' })
  nmenuIconClass?: string;
  @ApiPropertyOptional({ name: 'menuIconClassLike', type: 'string', description: '菜单图标CSS名称-模糊条件' })
  menuIconClassLike?: string;
  @ApiPropertyOptional({ name: 'menuIconClassList', type: 'string[]', description: '菜单图标CSS名称-列表条件' })
  menuIconClassList?: string[];
  @ApiPropertyOptional({ name: 'nmenuIconClassLike', type: 'string', description: '菜单图标CSS名称-模糊条件' })
  nmenuIconClassLike?: string;
  @ApiPropertyOptional({ name: 'nmenuIconClassList', type: 'string[]', description: '菜单图标CSS名称-列表条件' })
  nmenuIconClassList?: string[];
  @ApiPropertyOptional({ name: 'menuIconClassLikeList', type: 'string[]', description: '菜单图标CSS名称-列表模糊条件' })
  menuIconClassLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysMenuColNames)[]|(keyof SysMenuColProps)[] }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysMenuColNames)[] | (keyof SysMenuColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysMenuQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
