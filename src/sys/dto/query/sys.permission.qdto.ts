import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysPermissionColNames, SysPermissionColProps } from '../../entity/ddl/sys.permission.cols';
/**
 * SYS_PERMISSION表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysPermissionQDto
 * @class SysPermissionQDto
 */
@ApiTags('SYS_PERMISSION表的QDTO对象')
export class SysPermissionQDto {
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'perId', type: 'string', description: '权限ID' })
  perId?: string;
  @ApiProperty({ name: 'nperId', type: 'string', description: '权限ID' })
  nperId?: string;
  @ApiPropertyOptional({ name: 'perIdList', type: 'string[]', description: '权限ID-列表条件' })
  perIdList?: string[];
  @ApiPropertyOptional({ name: 'nperIdList', type: 'string[]', description: '权限ID-列表条件' })
  nperIdList?: string[];
  /**
   * 权限名称
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'perName', type: 'string', description: '权限名称' })
  perName?: string;
  @ApiProperty({ name: 'nperName', type: 'string', description: '权限名称' })
  nperName?: string;
  @ApiPropertyOptional({ name: 'perNameLike', type: 'string', description: '权限名称-模糊条件' })
  perNameLike?: string;
  @ApiPropertyOptional({ name: 'perNameList', type: 'string[]', description: '权限名称-列表条件' })
  perNameList?: string[];
  @ApiPropertyOptional({ name: 'nperNameLike', type: 'string', description: '权限名称-模糊条件' })
  nperNameLike?: string;
  @ApiPropertyOptional({ name: 'nperNameList', type: 'string[]', description: '权限名称-列表条件' })
  nperNameList?: string[];
  @ApiPropertyOptional({ name: 'perNameLikeList', type: 'string[]', description: '权限名称-列表模糊条件' })
  perNameLikeList?: string[];
  /**
   * 权限英文名
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perNameEn', type: 'string', description: '权限英文名' })
  perNameEn?: string;
  @ApiPropertyOptional({ name: 'nperNameEn', type: 'string', description: '权限英文名' })
  nperNameEn?: string;
  @ApiPropertyOptional({ name: 'perNameEnLike', type: 'string', description: '权限英文名-模糊条件' })
  perNameEnLike?: string;
  @ApiPropertyOptional({ name: 'perNameEnList', type: 'string[]', description: '权限英文名-列表条件' })
  perNameEnList?: string[];
  @ApiPropertyOptional({ name: 'nperNameEnLike', type: 'string', description: '权限英文名-模糊条件' })
  nperNameEnLike?: string;
  @ApiPropertyOptional({ name: 'nperNameEnList', type: 'string[]', description: '权限英文名-列表条件' })
  nperNameEnList?: string[];
  @ApiPropertyOptional({ name: 'perNameEnLikeList', type: 'string[]', description: '权限英文名-列表模糊条件' })
  perNameEnLikeList?: string[];
  /**
   * 权限类型
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'perType', type: 'string', description: '权限类型' })
  perType?: string;
  @ApiProperty({ name: 'nperType', type: 'string', description: '权限类型' })
  nperType?: string;
  @ApiPropertyOptional({ name: 'perTypeLike', type: 'string', description: '权限类型-模糊条件' })
  perTypeLike?: string;
  @ApiPropertyOptional({ name: 'perTypeList', type: 'string[]', description: '权限类型-列表条件' })
  perTypeList?: string[];
  @ApiPropertyOptional({ name: 'nperTypeLike', type: 'string', description: '权限类型-模糊条件' })
  nperTypeLike?: string;
  @ApiPropertyOptional({ name: 'nperTypeList', type: 'string[]', description: '权限类型-列表条件' })
  nperTypeList?: string[];
  @ApiPropertyOptional({ name: 'perTypeLikeList', type: 'string[]', description: '权限类型-列表模糊条件' })
  perTypeLikeList?: string[];
  /**
   * 权限URL
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perUrl', type: 'string', description: '权限URL' })
  perUrl?: string;
  @ApiPropertyOptional({ name: 'nperUrl', type: 'string', description: '权限URL' })
  nperUrl?: string;
  @ApiPropertyOptional({ name: 'perUrlLike', type: 'string', description: '权限URL-模糊条件' })
  perUrlLike?: string;
  @ApiPropertyOptional({ name: 'perUrlList', type: 'string[]', description: '权限URL-列表条件' })
  perUrlList?: string[];
  @ApiPropertyOptional({ name: 'nperUrlLike', type: 'string', description: '权限URL-模糊条件' })
  nperUrlLike?: string;
  @ApiPropertyOptional({ name: 'nperUrlList', type: 'string[]', description: '权限URL-列表条件' })
  nperUrlList?: string[];
  @ApiPropertyOptional({ name: 'perUrlLikeList', type: 'string[]', description: '权限URL-列表模糊条件' })
  perUrlLikeList?: string[];
  /**
   * 权限访问方法
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perMethod', type: 'string', description: '权限访问方法' })
  perMethod?: string;
  @ApiPropertyOptional({ name: 'nperMethod', type: 'string', description: '权限访问方法' })
  nperMethod?: string;
  @ApiPropertyOptional({ name: 'perMethodLike', type: 'string', description: '权限访问方法-模糊条件' })
  perMethodLike?: string;
  @ApiPropertyOptional({ name: 'perMethodList', type: 'string[]', description: '权限访问方法-列表条件' })
  perMethodList?: string[];
  @ApiPropertyOptional({ name: 'nperMethodLike', type: 'string', description: '权限访问方法-模糊条件' })
  nperMethodLike?: string;
  @ApiPropertyOptional({ name: 'nperMethodList', type: 'string[]', description: '权限访问方法-列表条件' })
  nperMethodList?: string[];
  @ApiPropertyOptional({ name: 'perMethodLikeList', type: 'string[]', description: '权限访问方法-列表模糊条件' })
  perMethodLikeList?: string[];
  /**
   * 父节点ID
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perParentId', type: 'string', description: '父节点ID' })
  perParentId?: string;
  @ApiPropertyOptional({ name: 'nperParentId', type: 'string', description: '父节点ID' })
  nperParentId?: string;
  @ApiPropertyOptional({ name: 'perParentIdLike', type: 'string', description: '父节点ID-模糊条件' })
  perParentIdLike?: string;
  @ApiPropertyOptional({ name: 'perParentIdList', type: 'string[]', description: '父节点ID-列表条件' })
  perParentIdList?: string[];
  @ApiPropertyOptional({ name: 'nperParentIdLike', type: 'string', description: '父节点ID-模糊条件' })
  nperParentIdLike?: string;
  @ApiPropertyOptional({ name: 'nperParentIdList', type: 'string[]', description: '父节点ID-列表条件' })
  nperParentIdList?: string[];
  @ApiPropertyOptional({ name: 'perParentIdLikeList', type: 'string[]', description: '父节点ID-列表模糊条件' })
  perParentIdLikeList?: string[];
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'perOrder', type: 'number', description: '排序' })
  perOrder?: number;
  @ApiProperty({ name: 'nperOrder', type: 'number', description: '排序' })
  nperOrder?: number;
  @ApiPropertyOptional({ name: 'minPerOrder', type: 'number', description: '排序-最小值' })
  minPerOrder?: number;
  @ApiPropertyOptional({ name: 'maxPerOrder', type: 'number', description: '排序-最大值' })
  maxPerOrder?: number;
  /**
   * 备注信息
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perRemark', type: 'string', description: '备注信息' })
  perRemark?: string;
  @ApiPropertyOptional({ name: 'nperRemark', type: 'string', description: '备注信息' })
  nperRemark?: string;
  @ApiPropertyOptional({ name: 'perRemarkLike', type: 'string', description: '备注信息-模糊条件' })
  perRemarkLike?: string;
  @ApiPropertyOptional({ name: 'perRemarkList', type: 'string[]', description: '备注信息-列表条件' })
  perRemarkList?: string[];
  @ApiPropertyOptional({ name: 'nperRemarkLike', type: 'string', description: '备注信息-模糊条件' })
  nperRemarkLike?: string;
  @ApiPropertyOptional({ name: 'nperRemarkList', type: 'string[]', description: '备注信息-列表条件' })
  nperRemarkList?: string[];
  @ApiPropertyOptional({ name: 'perRemarkLikeList', type: 'string[]', description: '备注信息-列表模糊条件' })
  perRemarkLikeList?: string[];
  /**
   * 状态信息
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'perState', type: 'string', description: '状态信息' })
  perState?: string;
  @ApiProperty({ name: 'nperState', type: 'string', description: '状态信息' })
  nperState?: string;
  @ApiPropertyOptional({ name: 'perStateLike', type: 'string', description: '状态信息-模糊条件' })
  perStateLike?: string;
  @ApiPropertyOptional({ name: 'perStateList', type: 'string[]', description: '状态信息-列表条件' })
  perStateList?: string[];
  @ApiPropertyOptional({ name: 'nperStateLike', type: 'string', description: '状态信息-模糊条件' })
  nperStateLike?: string;
  @ApiPropertyOptional({ name: 'nperStateList', type: 'string[]', description: '状态信息-列表条件' })
  nperStateList?: string[];
  @ApiPropertyOptional({ name: 'perStateLikeList', type: 'string[]', description: '状态信息-列表模糊条件' })
  perStateLikeList?: string[];
  /**
   * 所属系统：前端
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perSystem', type: 'string', description: '所属系统：前端' })
  perSystem?: string;
  @ApiPropertyOptional({ name: 'nperSystem', type: 'string', description: '所属系统：前端' })
  nperSystem?: string;
  @ApiPropertyOptional({ name: 'perSystemLike', type: 'string', description: '所属系统：前端-模糊条件' })
  perSystemLike?: string;
  @ApiPropertyOptional({ name: 'perSystemList', type: 'string[]', description: '所属系统：前端-列表条件' })
  perSystemList?: string[];
  @ApiPropertyOptional({ name: 'nperSystemLike', type: 'string', description: '所属系统：前端-模糊条件' })
  nperSystemLike?: string;
  @ApiPropertyOptional({ name: 'nperSystemList', type: 'string[]', description: '所属系统：前端-列表条件' })
  nperSystemList?: string[];
  @ApiPropertyOptional({ name: 'perSystemLikeList', type: 'string[]', description: '所属系统：前端-列表模糊条件' })
  perSystemLikeList?: string[];
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiPropertyOptional({ name: 'perModule', type: 'string', description: '模块' })
  perModule?: string;
  @ApiPropertyOptional({ name: 'nperModule', type: 'string', description: '模块' })
  nperModule?: string;
  @ApiPropertyOptional({ name: 'perModuleLike', type: 'string', description: '模块-模糊条件' })
  perModuleLike?: string;
  @ApiPropertyOptional({ name: 'perModuleList', type: 'string[]', description: '模块-列表条件' })
  perModuleList?: string[];
  @ApiPropertyOptional({ name: 'nperModuleLike', type: 'string', description: '模块-模糊条件' })
  nperModuleLike?: string;
  @ApiPropertyOptional({ name: 'nperModuleList', type: 'string[]', description: '模块-列表条件' })
  nperModuleList?: string[];
  @ApiPropertyOptional({ name: 'perModuleLikeList', type: 'string[]', description: '模块-列表模糊条件' })
  perModuleLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysPermissionColNames)[]|(keyof SysPermissionColProps)[] }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysPermissionColNames)[] | (keyof SysPermissionColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysPermissionQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
