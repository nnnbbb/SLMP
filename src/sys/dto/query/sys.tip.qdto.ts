import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysTipColNames, SysTipColProps } from '../../entity/ddl/sys.tip.cols';
/**
 * SYS_TIP表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysTipQDto
 * @class SysTipQDto
 */
@ApiTags('SYS_TIP表的QDTO对象')
export class SysTipQDto {
  /**
   * 提示信息ID
   *
   * @type { string }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'tipId', type: 'string', description: '提示信息ID' })
  tipId?: string;
  @ApiProperty({ name: 'ntipId', type: 'string', description: '提示信息ID' })
  ntipId?: string;
  @ApiPropertyOptional({ name: 'tipIdList', type: 'string[]', description: '提示信息ID-列表条件' })
  tipIdList?: string[];
  @ApiPropertyOptional({ name: 'ntipIdList', type: 'string[]', description: '提示信息ID-列表条件' })
  ntipIdList?: string[];
  /**
   * 提示次数
   *
   * @type { number }
   * @memberof SysTipQDto
   */
  @ApiPropertyOptional({ name: 'tipCount', type: 'number', description: '提示次数' })
  tipCount?: number;
  @ApiPropertyOptional({ name: 'ntipCount', type: 'number', description: '提示次数' })
  ntipCount?: number;
  @ApiPropertyOptional({ name: 'minTipCount', type: 'number', description: '提示次数-最小值' })
  minTipCount?: number;
  @ApiPropertyOptional({ name: 'maxTipCount', type: 'number', description: '提示次数-最大值' })
  maxTipCount?: number;
  /**
   * 提示信息内容
   *
   * @type { string }
   * @memberof SysTipQDto
   */
  @ApiPropertyOptional({ name: 'tipContents', type: 'string', description: '提示信息内容' })
  tipContents?: string;
  @ApiPropertyOptional({ name: 'ntipContents', type: 'string', description: '提示信息内容' })
  ntipContents?: string;
  @ApiPropertyOptional({ name: 'tipContentsLike', type: 'string', description: '提示信息内容-模糊条件' })
  tipContentsLike?: string;
  @ApiPropertyOptional({ name: 'tipContentsList', type: 'string[]', description: '提示信息内容-列表条件' })
  tipContentsList?: string[];
  @ApiPropertyOptional({ name: 'ntipContentsLike', type: 'string', description: '提示信息内容-模糊条件' })
  ntipContentsLike?: string;
  @ApiPropertyOptional({ name: 'ntipContentsList', type: 'string[]', description: '提示信息内容-列表条件' })
  ntipContentsList?: string[];
  @ApiPropertyOptional({ name: 'tipContentsLikeList', type: 'string[]', description: '提示信息内容-列表模糊条件' })
  tipContentsLikeList?: string[];
  /**
   * 所属者
   *
   * @type { string }
   * @memberof SysTipQDto
   */
  @ApiPropertyOptional({ name: 'tipManager', type: 'string', description: '所属者' })
  tipManager?: string;
  @ApiPropertyOptional({ name: 'ntipManager', type: 'string', description: '所属者' })
  ntipManager?: string;
  @ApiPropertyOptional({ name: 'tipManagerLike', type: 'string', description: '所属者-模糊条件' })
  tipManagerLike?: string;
  @ApiPropertyOptional({ name: 'tipManagerList', type: 'string[]', description: '所属者-列表条件' })
  tipManagerList?: string[];
  @ApiPropertyOptional({ name: 'ntipManagerLike', type: 'string', description: '所属者-模糊条件' })
  ntipManagerLike?: string;
  @ApiPropertyOptional({ name: 'ntipManagerList', type: 'string[]', description: '所属者-列表条件' })
  ntipManagerList?: string[];
  @ApiPropertyOptional({ name: 'tipManagerLikeList', type: 'string[]', description: '所属者-列表模糊条件' })
  tipManagerLikeList?: string[];
  /**
   * 消息受众
   *
   * @type { string }
   * @memberof SysTipQDto
   */
  @ApiPropertyOptional({ name: 'tipAudience', type: 'string', description: '消息受众' })
  tipAudience?: string;
  @ApiPropertyOptional({ name: 'ntipAudience', type: 'string', description: '消息受众' })
  ntipAudience?: string;
  @ApiPropertyOptional({ name: 'tipAudienceLike', type: 'string', description: '消息受众-模糊条件' })
  tipAudienceLike?: string;
  @ApiPropertyOptional({ name: 'tipAudienceList', type: 'string[]', description: '消息受众-列表条件' })
  tipAudienceList?: string[];
  @ApiPropertyOptional({ name: 'ntipAudienceLike', type: 'string', description: '消息受众-模糊条件' })
  ntipAudienceLike?: string;
  @ApiPropertyOptional({ name: 'ntipAudienceList', type: 'string[]', description: '消息受众-列表条件' })
  ntipAudienceList?: string[];
  @ApiPropertyOptional({ name: 'tipAudienceLikeList', type: 'string[]', description: '消息受众-列表模糊条件' })
  tipAudienceLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysTipQDto
   */
  @ApiPropertyOptional({ name: 'tipCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  tipCreateDate?: Date;
  @ApiPropertyOptional({ name: 'ntipCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  ntipCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sTipCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sTipCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eTipCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eTipCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysTipQDto
   */
  @ApiPropertyOptional({ name: 'tipUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  tipUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'ntipUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  ntipUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sTipUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sTipUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eTipUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eTipUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysTipColNames)[]|(keyof SysTipColProps)[] }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysTipColNames)[] | (keyof SysTipColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysTipQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
