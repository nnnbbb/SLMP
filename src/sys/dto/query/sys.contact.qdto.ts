import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysContactColNames, SysContactColProps } from '../../entity/ddl/sys.contact.cols';
/**
 * SYS_CONTACT表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysContactQDto
 * @class SysContactQDto
 */
@ApiTags('SYS_CONTACT表的QDTO对象')
export class SysContactQDto {
  /**
   * 反馈编号
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'contactId', type: 'string', description: '反馈编号' })
  contactId?: string;
  @ApiProperty({ name: 'ncontactId', type: 'string', description: '反馈编号' })
  ncontactId?: string;
  @ApiPropertyOptional({ name: 'contactIdList', type: 'string[]', description: '反馈编号-列表条件' })
  contactIdList?: string[];
  @ApiPropertyOptional({ name: 'ncontactIdList', type: 'string[]', description: '反馈编号-列表条件' })
  ncontactIdList?: string[];
  /**
   * 反馈用户
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiPropertyOptional({ name: 'contactAuthor', type: 'string', description: '反馈用户' })
  contactAuthor?: string;
  @ApiPropertyOptional({ name: 'ncontactAuthor', type: 'string', description: '反馈用户' })
  ncontactAuthor?: string;
  @ApiPropertyOptional({ name: 'contactAuthorLike', type: 'string', description: '反馈用户-模糊条件' })
  contactAuthorLike?: string;
  @ApiPropertyOptional({ name: 'contactAuthorList', type: 'string[]', description: '反馈用户-列表条件' })
  contactAuthorList?: string[];
  @ApiPropertyOptional({ name: 'ncontactAuthorLike', type: 'string', description: '反馈用户-模糊条件' })
  ncontactAuthorLike?: string;
  @ApiPropertyOptional({ name: 'ncontactAuthorList', type: 'string[]', description: '反馈用户-列表条件' })
  ncontactAuthorList?: string[];
  @ApiPropertyOptional({ name: 'contactAuthorLikeList', type: 'string[]', description: '反馈用户-列表模糊条件' })
  contactAuthorLikeList?: string[];
  /**
   * 反馈邮箱
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiPropertyOptional({ name: 'contactEmail', type: 'string', description: '反馈邮箱' })
  contactEmail?: string;
  @ApiPropertyOptional({ name: 'ncontactEmail', type: 'string', description: '反馈邮箱' })
  ncontactEmail?: string;
  @ApiPropertyOptional({ name: 'contactEmailLike', type: 'string', description: '反馈邮箱-模糊条件' })
  contactEmailLike?: string;
  @ApiPropertyOptional({ name: 'contactEmailList', type: 'string[]', description: '反馈邮箱-列表条件' })
  contactEmailList?: string[];
  @ApiPropertyOptional({ name: 'ncontactEmailLike', type: 'string', description: '反馈邮箱-模糊条件' })
  ncontactEmailLike?: string;
  @ApiPropertyOptional({ name: 'ncontactEmailList', type: 'string[]', description: '反馈邮箱-列表条件' })
  ncontactEmailList?: string[];
  @ApiPropertyOptional({ name: 'contactEmailLikeList', type: 'string[]', description: '反馈邮箱-列表模糊条件' })
  contactEmailLikeList?: string[];
  /**
   * 反馈主题
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiPropertyOptional({ name: 'contactSubject', type: 'string', description: '反馈主题' })
  contactSubject?: string;
  @ApiPropertyOptional({ name: 'ncontactSubject', type: 'string', description: '反馈主题' })
  ncontactSubject?: string;
  @ApiPropertyOptional({ name: 'contactSubjectLike', type: 'string', description: '反馈主题-模糊条件' })
  contactSubjectLike?: string;
  @ApiPropertyOptional({ name: 'contactSubjectList', type: 'string[]', description: '反馈主题-列表条件' })
  contactSubjectList?: string[];
  @ApiPropertyOptional({ name: 'ncontactSubjectLike', type: 'string', description: '反馈主题-模糊条件' })
  ncontactSubjectLike?: string;
  @ApiPropertyOptional({ name: 'ncontactSubjectList', type: 'string[]', description: '反馈主题-列表条件' })
  ncontactSubjectList?: string[];
  @ApiPropertyOptional({ name: 'contactSubjectLikeList', type: 'string[]', description: '反馈主题-列表模糊条件' })
  contactSubjectLikeList?: string[];
  /**
   * 反馈内容
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'contactText', type: 'string', description: '反馈内容' })
  contactText?: string;
  @ApiProperty({ name: 'ncontactText', type: 'string', description: '反馈内容' })
  ncontactText?: string;
  @ApiPropertyOptional({ name: 'contactTextLike', type: 'string', description: '反馈内容-模糊条件' })
  contactTextLike?: string;
  @ApiPropertyOptional({ name: 'contactTextList', type: 'string[]', description: '反馈内容-列表条件' })
  contactTextList?: string[];
  @ApiPropertyOptional({ name: 'ncontactTextLike', type: 'string', description: '反馈内容-模糊条件' })
  ncontactTextLike?: string;
  @ApiPropertyOptional({ name: 'ncontactTextList', type: 'string[]', description: '反馈内容-列表条件' })
  ncontactTextList?: string[];
  @ApiPropertyOptional({ name: 'contactTextLikeList', type: 'string[]', description: '反馈内容-列表模糊条件' })
  contactTextLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'contactCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  contactCreateDate?: Date;
  @ApiProperty({ name: 'ncontactCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  ncontactCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sContactCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sContactCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eContactCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eContactCreateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysContactColNames)[]|(keyof SysContactColProps)[] }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysContactColNames)[] | (keyof SysContactColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysContactQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
