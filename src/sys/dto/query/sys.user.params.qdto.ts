import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysUserParamsColNames, SysUserParamsColProps } from '../../entity/ddl/sys.user.params.cols';

/**
 * 用户参数表表的QDTO对象
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export SysUserParamsQDto
 * @class SysUserParamsQDto
 */
@ApiTags('SYS_USER_PARAMS表的QDTO对象')
export class SysUserParamsQDto {
  /**
   * 主键
   *
   * @type { string }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'upId', type: 'string', description: '主键' })
  upId?: string;
  @ApiProperty({ name: 'nupId', type: 'string', description: '主键' })
  nupId?: string;
  @ApiPropertyOptional({ name: 'upIdList', type: 'string[]', description: '主键-列表条件' })
  upIdList?: string[];
  @ApiPropertyOptional({ name: 'nupIdList', type: 'string[]', description: '主键-列表条件' })
  nupIdList?: string[];
  /**
   * 帐号
   *
   * @type { string }
   * @memberof SysUserParamsQDto
   */
  @ApiPropertyOptional({ name: 'upLoginName', type: 'string', description: '帐号' })
  upLoginName?: string;
  @ApiPropertyOptional({ name: 'nupLoginName', type: 'string', description: '帐号' })
  nupLoginName?: string;
  @ApiPropertyOptional({ name: 'upLoginNameLike', type: 'string', description: '帐号-模糊条件' })
  upLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'upLoginNameList', type: 'string[]', description: '帐号-列表条件' })
  upLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'nupLoginNameLike', type: 'string', description: '帐号-模糊条件' })
  nupLoginNameLike?: string;
  @ApiPropertyOptional({ name: 'nupLoginNameList', type: 'string[]', description: '帐号-列表条件' })
  nupLoginNameList?: string[];
  @ApiPropertyOptional({ name: 'upLoginNameLikeList', type: 'string[]', description: '帐号-列表模糊条件' })
  upLoginNameLikeList?: string[];
  /**
   * 参数名
   *
   * @type { string }
   * @memberof SysUserParamsQDto
   */
  @ApiPropertyOptional({ name: 'upParamName', type: 'string', description: '参数名' })
  upParamName?: string;
  @ApiPropertyOptional({ name: 'nupParamName', type: 'string', description: '参数名' })
  nupParamName?: string;
  @ApiPropertyOptional({ name: 'upParamNameLike', type: 'string', description: '参数名-模糊条件' })
  upParamNameLike?: string;
  @ApiPropertyOptional({ name: 'upParamNameList', type: 'string[]', description: '参数名-列表条件' })
  upParamNameList?: string[];
  @ApiPropertyOptional({ name: 'nupParamNameLike', type: 'string', description: '参数名-模糊条件' })
  nupParamNameLike?: string;
  @ApiPropertyOptional({ name: 'nupParamNameList', type: 'string[]', description: '参数名-列表条件' })
  nupParamNameList?: string[];
  @ApiPropertyOptional({ name: 'upParamNameLikeList', type: 'string[]', description: '参数名-列表模糊条件' })
  upParamNameLikeList?: string[];
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysUserParamsQDto
   */
  @ApiPropertyOptional({ name: 'upParamValue', type: 'string', description: '参数值' })
  upParamValue?: string;
  @ApiPropertyOptional({ name: 'nupParamValue', type: 'string', description: '参数值' })
  nupParamValue?: string;
  @ApiPropertyOptional({ name: 'upParamValueLike', type: 'string', description: '参数值-模糊条件' })
  upParamValueLike?: string;
  @ApiPropertyOptional({ name: 'upParamValueList', type: 'string[]', description: '参数值-列表条件' })
  upParamValueList?: string[];
  @ApiPropertyOptional({ name: 'nupParamValueLike', type: 'string', description: '参数值-模糊条件' })
  nupParamValueLike?: string;
  @ApiPropertyOptional({ name: 'nupParamValueList', type: 'string[]', description: '参数值-列表条件' })
  nupParamValueList?: string[];
  @ApiPropertyOptional({ name: 'upParamValueLikeList', type: 'string[]', description: '参数值-列表模糊条件' })
  upParamValueLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserParamsQDto
   */
  @ApiPropertyOptional({ name: 'upCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  upCreateDate?: Date;
  @ApiPropertyOptional({ name: 'nupCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nupCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sUpCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sUpCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eUpCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eUpCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUserParamsQDto
   */
  @ApiPropertyOptional({ name: 'upUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  upUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'nupUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nupUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sUpUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sUpUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eUpUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eUpUpdateDate?: Date;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserParamsColNames)[]|(keyof SysUserParamsColProps)[] }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserParamsColNames)[] | (keyof SysUserParamsColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysUserParamsQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
