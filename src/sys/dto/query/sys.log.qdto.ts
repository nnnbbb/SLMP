import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysLogColNames, SysLogColProps } from '../../entity/ddl/sys.log.cols';
/**
 * SYS_LOG表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysLogQDto
 * @class SysLogQDto
 */
@ApiTags('SYS_LOG表的QDTO对象')
export class SysLogQDto {
  /**
   * 日志ID号
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logId', type: 'string', description: '日志ID号' })
  logId?: string;
  @ApiProperty({ name: 'nlogId', type: 'string', description: '日志ID号' })
  nlogId?: string;
  @ApiPropertyOptional({ name: 'logIdList', type: 'string[]', description: '日志ID号-列表条件' })
  logIdList?: string[];
  @ApiPropertyOptional({ name: 'nlogIdList', type: 'string[]', description: '日志ID号-列表条件' })
  nlogIdList?: string[];
  /**
   * 操作人员名称
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiPropertyOptional({ name: 'logUser', type: 'string', description: '操作人员名称' })
  logUser?: string;
  @ApiPropertyOptional({ name: 'nlogUser', type: 'string', description: '操作人员名称' })
  nlogUser?: string;
  @ApiPropertyOptional({ name: 'logUserLike', type: 'string', description: '操作人员名称-模糊条件' })
  logUserLike?: string;
  @ApiPropertyOptional({ name: 'logUserList', type: 'string[]', description: '操作人员名称-列表条件' })
  logUserList?: string[];
  @ApiPropertyOptional({ name: 'nlogUserLike', type: 'string', description: '操作人员名称-模糊条件' })
  nlogUserLike?: string;
  @ApiPropertyOptional({ name: 'nlogUserList', type: 'string[]', description: '操作人员名称-列表条件' })
  nlogUserList?: string[];
  @ApiPropertyOptional({ name: 'logUserLikeList', type: 'string[]', description: '操作人员名称-列表模糊条件' })
  logUserLikeList?: string[];
  /**
   * 操作时间
   *
   * @type { Date }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logTime', type: 'Date', description: '操作时间' })
  @Type(() => Date)
  logTime?: Date;
  @ApiProperty({ name: 'nlogTime', type: 'Date', description: '操作时间' })
  @Type(() => Date)
  nlogTime?: Date;
  @ApiPropertyOptional({ name: 'sLogTime', type: 'Date', description: '操作时间-起始日期' })
  @Type(() => Date)
  sLogTime?: Date;
  @ApiPropertyOptional({ name: 'eLogTime', type: 'Date', description: '操作时间-终止日期' })
  @Type(() => Date)
  eLogTime?: Date;
  /**
   * IP地址
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logIp', type: 'string', description: 'IP地址' })
  logIp?: string;
  @ApiProperty({ name: 'nlogIp', type: 'string', description: 'IP地址' })
  nlogIp?: string;
  @ApiPropertyOptional({ name: 'logIpLike', type: 'string', description: 'IP地址-模糊条件' })
  logIpLike?: string;
  @ApiPropertyOptional({ name: 'logIpList', type: 'string[]', description: 'IP地址-列表条件' })
  logIpList?: string[];
  @ApiPropertyOptional({ name: 'nlogIpLike', type: 'string', description: 'IP地址-模糊条件' })
  nlogIpLike?: string;
  @ApiPropertyOptional({ name: 'nlogIpList', type: 'string[]', description: 'IP地址-列表条件' })
  nlogIpList?: string[];
  @ApiPropertyOptional({ name: 'logIpLikeList', type: 'string[]', description: 'IP地址-列表模糊条件' })
  logIpLikeList?: string[];
  /**
   * 操作URL
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logUrl', type: 'string', description: '操作URL' })
  logUrl?: string;
  @ApiProperty({ name: 'nlogUrl', type: 'string', description: '操作URL' })
  nlogUrl?: string;
  @ApiPropertyOptional({ name: 'logUrlLike', type: 'string', description: '操作URL-模糊条件' })
  logUrlLike?: string;
  @ApiPropertyOptional({ name: 'logUrlList', type: 'string[]', description: '操作URL-列表条件' })
  logUrlList?: string[];
  @ApiPropertyOptional({ name: 'nlogUrlLike', type: 'string', description: '操作URL-模糊条件' })
  nlogUrlLike?: string;
  @ApiPropertyOptional({ name: 'nlogUrlList', type: 'string[]', description: '操作URL-列表条件' })
  nlogUrlList?: string[];
  @ApiPropertyOptional({ name: 'logUrlLikeList', type: 'string[]', description: '操作URL-列表模糊条件' })
  logUrlLikeList?: string[];
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logTitle', type: 'string', description: '模块' })
  logTitle?: string;
  @ApiProperty({ name: 'nlogTitle', type: 'string', description: '模块' })
  nlogTitle?: string;
  @ApiPropertyOptional({ name: 'logTitleLike', type: 'string', description: '模块-模糊条件' })
  logTitleLike?: string;
  @ApiPropertyOptional({ name: 'logTitleList', type: 'string[]', description: '模块-列表条件' })
  logTitleList?: string[];
  @ApiPropertyOptional({ name: 'nlogTitleLike', type: 'string', description: '模块-模糊条件' })
  nlogTitleLike?: string;
  @ApiPropertyOptional({ name: 'nlogTitleList', type: 'string[]', description: '模块-列表条件' })
  nlogTitleList?: string[];
  @ApiPropertyOptional({ name: 'logTitleLikeList', type: 'string[]', description: '模块-列表模糊条件' })
  logTitleLikeList?: string[];
  /**
   * 内容
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logContent', type: 'string', description: '内容' })
  logContent?: string;
  @ApiProperty({ name: 'nlogContent', type: 'string', description: '内容' })
  nlogContent?: string;
  @ApiPropertyOptional({ name: 'logContentLike', type: 'string', description: '内容-模糊条件' })
  logContentLike?: string;
  @ApiPropertyOptional({ name: 'logContentList', type: 'string[]', description: '内容-列表条件' })
  logContentList?: string[];
  @ApiPropertyOptional({ name: 'nlogContentLike', type: 'string', description: '内容-模糊条件' })
  nlogContentLike?: string;
  @ApiPropertyOptional({ name: 'nlogContentList', type: 'string[]', description: '内容-列表条件' })
  nlogContentList?: string[];
  @ApiPropertyOptional({ name: 'logContentLikeList', type: 'string[]', description: '内容-列表模糊条件' })
  logContentLikeList?: string[];
  /**
   * 操作类型
   *
   * @type { number }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'logType', type: 'number', description: '操作类型' })
  logType?: number;
  @ApiProperty({ name: 'nlogType', type: 'number', description: '操作类型' })
  nlogType?: number;
  @ApiPropertyOptional({ name: 'minLogType', type: 'number', description: '操作类型-最小值' })
  minLogType?: number;
  @ApiPropertyOptional({ name: 'maxLogType', type: 'number', description: '操作类型-最大值' })
  maxLogType?: number;
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysLogColNames)[]|(keyof SysLogColProps)[] }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysLogColNames)[] | (keyof SysLogColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysLogQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
