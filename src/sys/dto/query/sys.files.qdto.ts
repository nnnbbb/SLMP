import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { SysFilesColNames, SysFilesColProps } from '../../entity/ddl/sys.files.cols';
/**
 * 文件管理表的QDTO对象
 * @date 1/2/2021, 10:41:20 AM
 * @author jiangbin
 * @export SysFilesQDto
 * @class SysFilesQDto
 */
@ApiTags('SYS_FILES表的QDTO对象')
export class SysFilesQDto {
  /**
   * 记录ID
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileId', type: 'string', description: '记录ID' })
  fileId?: string;
  @ApiProperty({ name: 'nfileId', type: 'string', description: '记录ID' })
  nfileId?: string;
  @ApiPropertyOptional({ name: 'fileIdList', type: 'string[]', description: '记录ID-列表条件' })
  fileIdList?: string[];
  @ApiPropertyOptional({ name: 'nfileIdList', type: 'string[]', description: '记录ID-列表条件' })
  nfileIdList?: string[];
  /**
   * 文件条码号
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileBarcode', type: 'string', description: '文件条码号' })
  fileBarcode?: string;
  @ApiPropertyOptional({ name: 'nfileBarcode', type: 'string', description: '文件条码号' })
  nfileBarcode?: string;
  @ApiPropertyOptional({ name: 'fileBarcodeLike', type: 'string', description: '文件条码号-模糊条件' })
  fileBarcodeLike?: string;
  @ApiPropertyOptional({ name: 'fileBarcodeList', type: 'string[]', description: '文件条码号-列表条件' })
  fileBarcodeList?: string[];
  @ApiPropertyOptional({ name: 'nfileBarcodeLike', type: 'string', description: '文件条码号-模糊条件' })
  nfileBarcodeLike?: string;
  @ApiPropertyOptional({ name: 'nfileBarcodeList', type: 'string[]', description: '文件条码号-列表条件' })
  nfileBarcodeList?: string[];
  @ApiPropertyOptional({ name: 'fileBarcodeLikeList', type: 'string[]', description: '文件条码号-列表模糊条件' })
  fileBarcodeLikeList?: string[];
  /**
   * 关联的其它记录信息ID号
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileRelateId', type: 'string', description: '关联的其它记录信息ID号' })
  fileRelateId?: string;
  @ApiPropertyOptional({ name: 'nfileRelateId', type: 'string', description: '关联的其它记录信息ID号' })
  nfileRelateId?: string;
  @ApiPropertyOptional({ name: 'fileRelateIdLike', type: 'string', description: '关联的其它记录信息ID号-模糊条件' })
  fileRelateIdLike?: string;
  @ApiPropertyOptional({ name: 'fileRelateIdList', type: 'string[]', description: '关联的其它记录信息ID号-列表条件' })
  fileRelateIdList?: string[];
  @ApiPropertyOptional({ name: 'nfileRelateIdLike', type: 'string', description: '关联的其它记录信息ID号-模糊条件' })
  nfileRelateIdLike?: string;
  @ApiPropertyOptional({ name: 'nfileRelateIdList', type: 'string[]', description: '关联的其它记录信息ID号-列表条件' })
  nfileRelateIdList?: string[];
  @ApiPropertyOptional({ name: 'fileRelateIdLikeList', type: 'string[]', description: '关联的其它记录信息ID号-列表模糊条件' })
  fileRelateIdLikeList?: string[];
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileName', type: 'string', description: '文件名称' })
  fileName?: string;
  @ApiProperty({ name: 'nfileName', type: 'string', description: '文件名称' })
  nfileName?: string;
  @ApiPropertyOptional({ name: 'fileNameLike', type: 'string', description: '文件名称-模糊条件' })
  fileNameLike?: string;
  @ApiPropertyOptional({ name: 'fileNameList', type: 'string[]', description: '文件名称-列表条件' })
  fileNameList?: string[];
  @ApiPropertyOptional({ name: 'nfileNameLike', type: 'string', description: '文件名称-模糊条件' })
  nfileNameLike?: string;
  @ApiPropertyOptional({ name: 'nfileNameList', type: 'string[]', description: '文件名称-列表条件' })
  nfileNameList?: string[];
  @ApiPropertyOptional({ name: 'fileNameLikeList', type: 'string[]', description: '文件名称-列表模糊条件' })
  fileNameLikeList?: string[];
  /**
   * 文件名
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileNameEn', type: 'string', description: '文件名' })
  fileNameEn?: string;
  @ApiPropertyOptional({ name: 'nfileNameEn', type: 'string', description: '文件名' })
  nfileNameEn?: string;
  @ApiPropertyOptional({ name: 'fileNameEnLike', type: 'string', description: '文件名-模糊条件' })
  fileNameEnLike?: string;
  @ApiPropertyOptional({ name: 'fileNameEnList', type: 'string[]', description: '文件名-列表条件' })
  fileNameEnList?: string[];
  @ApiPropertyOptional({ name: 'nfileNameEnLike', type: 'string', description: '文件名-模糊条件' })
  nfileNameEnLike?: string;
  @ApiPropertyOptional({ name: 'nfileNameEnList', type: 'string[]', description: '文件名-列表条件' })
  nfileNameEnList?: string[];
  @ApiPropertyOptional({ name: 'fileNameEnLikeList', type: 'string[]', description: '文件名-列表模糊条件' })
  fileNameEnLikeList?: string[];
  /**
   * 文件存储路径
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'filePath', type: 'string', description: '文件存储路径' })
  filePath?: string;
  @ApiProperty({ name: 'nfilePath', type: 'string', description: '文件存储路径' })
  nfilePath?: string;
  @ApiPropertyOptional({ name: 'filePathLike', type: 'string', description: '文件存储路径-模糊条件' })
  filePathLike?: string;
  @ApiPropertyOptional({ name: 'filePathList', type: 'string[]', description: '文件存储路径-列表条件' })
  filePathList?: string[];
  @ApiPropertyOptional({ name: 'nfilePathLike', type: 'string', description: '文件存储路径-模糊条件' })
  nfilePathLike?: string;
  @ApiPropertyOptional({ name: 'nfilePathList', type: 'string[]', description: '文件存储路径-列表条件' })
  nfilePathList?: string[];
  @ApiPropertyOptional({ name: 'filePathLikeList', type: 'string[]', description: '文件存储路径-列表模糊条件' })
  filePathLikeList?: string[];
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  fileCreateDate?: Date;
  @ApiProperty({ name: 'nfileCreateDate', type: 'Date', description: '创建日期' })
  @Type(() => Date)
  nfileCreateDate?: Date;
  @ApiPropertyOptional({ name: 'sFileCreateDate', type: 'Date', description: '创建日期-起始日期' })
  @Type(() => Date)
  sFileCreateDate?: Date;
  @ApiPropertyOptional({ name: 'eFileCreateDate', type: 'Date', description: '创建日期-终止日期' })
  @Type(() => Date)
  eFileCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  fileUpdateDate?: Date;
  @ApiProperty({ name: 'nfileUpdateDate', type: 'Date', description: '更新日期' })
  @Type(() => Date)
  nfileUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'sFileUpdateDate', type: 'Date', description: '更新日期-起始日期' })
  @Type(() => Date)
  sFileUpdateDate?: Date;
  @ApiPropertyOptional({ name: 'eFileUpdateDate', type: 'Date', description: '更新日期-终止日期' })
  @Type(() => Date)
  eFileUpdateDate?: Date;
  /**
   * 是否被启用
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileIsUsed', type: 'string', description: '是否被启用' })
  fileIsUsed?: string;
  @ApiProperty({ name: 'nfileIsUsed', type: 'string', description: '是否被启用' })
  nfileIsUsed?: string;
  @ApiPropertyOptional({ name: 'fileIsUsedLike', type: 'string', description: '是否被启用-模糊条件' })
  fileIsUsedLike?: string;
  @ApiPropertyOptional({ name: 'fileIsUsedList', type: 'string[]', description: '是否被启用-列表条件' })
  fileIsUsedList?: string[];
  @ApiPropertyOptional({ name: 'nfileIsUsedLike', type: 'string', description: '是否被启用-模糊条件' })
  nfileIsUsedLike?: string;
  @ApiPropertyOptional({ name: 'nfileIsUsedList', type: 'string[]', description: '是否被启用-列表条件' })
  nfileIsUsedList?: string[];
  @ApiPropertyOptional({ name: 'fileIsUsedLikeList', type: 'string[]', description: '是否被启用-列表模糊条件' })
  fileIsUsedLikeList?: string[];
  /**
   * 所属用户
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileOwner', type: 'string', description: '所属用户' })
  fileOwner?: string;
  @ApiProperty({ name: 'nfileOwner', type: 'string', description: '所属用户' })
  nfileOwner?: string;
  @ApiPropertyOptional({ name: 'fileOwnerLike', type: 'string', description: '所属用户-模糊条件' })
  fileOwnerLike?: string;
  @ApiPropertyOptional({ name: 'fileOwnerList', type: 'string[]', description: '所属用户-列表条件' })
  fileOwnerList?: string[];
  @ApiPropertyOptional({ name: 'nfileOwnerLike', type: 'string', description: '所属用户-模糊条件' })
  nfileOwnerLike?: string;
  @ApiPropertyOptional({ name: 'nfileOwnerList', type: 'string[]', description: '所属用户-列表条件' })
  nfileOwnerList?: string[];
  @ApiPropertyOptional({ name: 'fileOwnerLikeList', type: 'string[]', description: '所属用户-列表模糊条件' })
  fileOwnerLikeList?: string[];
  /**
   * 文件大小
   *
   * @type { number }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'fileSize', type: 'number', description: '文件大小' })
  fileSize?: number;
  @ApiProperty({ name: 'nfileSize', type: 'number', description: '文件大小' })
  nfileSize?: number;
  @ApiPropertyOptional({ name: 'minFileSize', type: 'number', description: '文件大小-最小值' })
  minFileSize?: number;
  @ApiPropertyOptional({ name: 'maxFileSize', type: 'number', description: '文件大小-最大值' })
  maxFileSize?: number;
  /**
   * 用户权限表
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileGrants', type: 'string', description: '用户权限表' })
  fileGrants?: string;
  @ApiPropertyOptional({ name: 'nfileGrants', type: 'string', description: '用户权限表' })
  nfileGrants?: string;
  @ApiPropertyOptional({ name: 'fileGrantsLike', type: 'string', description: '用户权限表-模糊条件' })
  fileGrantsLike?: string;
  @ApiPropertyOptional({ name: 'fileGrantsList', type: 'string[]', description: '用户权限表-列表条件' })
  fileGrantsList?: string[];
  @ApiPropertyOptional({ name: 'nfileGrantsLike', type: 'string', description: '用户权限表-模糊条件' })
  nfileGrantsLike?: string;
  @ApiPropertyOptional({ name: 'nfileGrantsList', type: 'string[]', description: '用户权限表-列表条件' })
  nfileGrantsList?: string[];
  @ApiPropertyOptional({ name: 'fileGrantsLikeList', type: 'string[]', description: '用户权限表-列表模糊条件' })
  fileGrantsLikeList?: string[];
  /**
   * 文件分类
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileClasses', type: 'string', description: '文件分类' })
  fileClasses?: string;
  @ApiPropertyOptional({ name: 'nfileClasses', type: 'string', description: '文件分类' })
  nfileClasses?: string;
  @ApiPropertyOptional({ name: 'fileClassesLike', type: 'string', description: '文件分类-模糊条件' })
  fileClassesLike?: string;
  @ApiPropertyOptional({ name: 'fileClassesList', type: 'string[]', description: '文件分类-列表条件' })
  fileClassesList?: string[];
  @ApiPropertyOptional({ name: 'nfileClassesLike', type: 'string', description: '文件分类-模糊条件' })
  nfileClassesLike?: string;
  @ApiPropertyOptional({ name: 'nfileClassesList', type: 'string[]', description: '文件分类-列表条件' })
  nfileClassesList?: string[];
  @ApiPropertyOptional({ name: 'fileClassesLikeList', type: 'string[]', description: '文件分类-列表模糊条件' })
  fileClassesLikeList?: string[];
  /**
   * 文件状态信息
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileState', type: 'string', description: '文件状态信息' })
  fileState?: string;
  @ApiPropertyOptional({ name: 'nfileState', type: 'string', description: '文件状态信息' })
  nfileState?: string;
  @ApiPropertyOptional({ name: 'fileStateLike', type: 'string', description: '文件状态信息-模糊条件' })
  fileStateLike?: string;
  @ApiPropertyOptional({ name: 'fileStateList', type: 'string[]', description: '文件状态信息-列表条件' })
  fileStateList?: string[];
  @ApiPropertyOptional({ name: 'nfileStateLike', type: 'string', description: '文件状态信息-模糊条件' })
  nfileStateLike?: string;
  @ApiPropertyOptional({ name: 'nfileStateList', type: 'string[]', description: '文件状态信息-列表条件' })
  nfileStateList?: string[];
  @ApiPropertyOptional({ name: 'fileStateLikeList', type: 'string[]', description: '文件状态信息-列表模糊条件' })
  fileStateLikeList?: string[];
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileType', type: 'string', description: '文件类型' })
  fileType?: string;
  @ApiPropertyOptional({ name: 'nfileType', type: 'string', description: '文件类型' })
  nfileType?: string;
  @ApiPropertyOptional({ name: 'fileTypeLike', type: 'string', description: '文件类型-模糊条件' })
  fileTypeLike?: string;
  @ApiPropertyOptional({ name: 'fileTypeList', type: 'string[]', description: '文件类型-列表条件' })
  fileTypeList?: string[];
  @ApiPropertyOptional({ name: 'nfileTypeLike', type: 'string', description: '文件类型-模糊条件' })
  nfileTypeLike?: string;
  @ApiPropertyOptional({ name: 'nfileTypeList', type: 'string[]', description: '文件类型-列表条件' })
  nfileTypeList?: string[];
  @ApiPropertyOptional({ name: 'fileTypeLikeList', type: 'string[]', description: '文件类型-列表模糊条件' })
  fileTypeLikeList?: string[];
  /**
   * 文件备注信息
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileComments', type: 'string', description: '文件备注信息' })
  fileComments?: string;
  @ApiPropertyOptional({ name: 'nfileComments', type: 'string', description: '文件备注信息' })
  nfileComments?: string;
  @ApiPropertyOptional({ name: 'fileCommentsLike', type: 'string', description: '文件备注信息-模糊条件' })
  fileCommentsLike?: string;
  @ApiPropertyOptional({ name: 'fileCommentsList', type: 'string[]', description: '文件备注信息-列表条件' })
  fileCommentsList?: string[];
  @ApiPropertyOptional({ name: 'nfileCommentsLike', type: 'string', description: '文件备注信息-模糊条件' })
  nfileCommentsLike?: string;
  @ApiPropertyOptional({ name: 'nfileCommentsList', type: 'string[]', description: '文件备注信息-列表条件' })
  nfileCommentsList?: string[];
  @ApiPropertyOptional({ name: 'fileCommentsLikeList', type: 'string[]', description: '文件备注信息-列表模糊条件' })
  fileCommentsLikeList?: string[];
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiPropertyOptional({ name: 'fileSpecies', type: 'string', description: '种属' })
  fileSpecies?: string;
  @ApiPropertyOptional({ name: 'nfileSpecies', type: 'string', description: '种属' })
  nfileSpecies?: string;
  @ApiPropertyOptional({ name: 'fileSpeciesLike', type: 'string', description: '种属-模糊条件' })
  fileSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'fileSpeciesList', type: 'string[]', description: '种属-列表条件' })
  fileSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'nfileSpeciesLike', type: 'string', description: '种属-模糊条件' })
  nfileSpeciesLike?: string;
  @ApiPropertyOptional({ name: 'nfileSpeciesList', type: 'string[]', description: '种属-列表条件' })
  nfileSpeciesList?: string[];
  @ApiPropertyOptional({ name: 'fileSpeciesLikeList', type: 'string[]', description: '种属-列表模糊条件' })
  fileSpeciesLikeList?: string[];
  /**
   * 分页查询记录数
   *
   * @type { number }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'pageSize', type: 'number', description: '分页记录数' })
  pageSize?: number;
  /**
   * 记录偏移量
   *
   * @type { number }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'offset', type: 'number', description: '记录偏移量' })
  offset?: number;
  /**
   * 记录排序，示例：SAM_KIND DESC,SAM_NAME ASC
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'order', type: 'string', description: '排序' })
  order?: string;
  /**
   * 分组查询条件,例如：SAM_KIND
   *
   * @type { string }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'group', type: 'string', description: '排序' })
  group?: string;
  /**
   * 结果列限定条件,在只想查询部分列数据时可以给定本参数
   *
   * @type { (keyof SysFilesColNames)[]|(keyof SysFilesColProps)[] }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysFilesColNames)[] | (keyof SysFilesColProps)[];
  /**
   * 对{cols}给定的列数据进行去重
   *
   * @type { boolean }
   * @memberof SysFilesQDto
   */
  @ApiProperty({ name: 'distinct', type: 'boolean', description: '列去重' })
  distinct?: boolean;
}
