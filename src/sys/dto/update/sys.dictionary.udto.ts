import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysDictionaryColNames } from '../../entity/ddl/sys.dictionary.cols';
/**
 * SYS_DICTIONARY表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysDictionaryUDto
 * @class SysDictionaryUDto
 */
@ApiTags('SYS_DICTIONARY表的UDTO对象')
export class SysDictionaryUDto {
  /**
   * ID
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiProperty({ name: 'dicId', type: 'string', description: 'ID' })
  dicId: string;
  /**
   * 字典名称
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiProperty({ name: 'dicName', type: 'string', description: '字典名称' })
  dicName: string;
  /**
   * dicNameEn
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicNameEn', type: 'string', description: 'dicNameEn' })
  dicNameEn?: string;
  /**
   * 字典值
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicValue', type: 'string', description: '字典值' })
  dicValue?: string;
  /**
   * 所属组
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicGroup', type: 'string', description: '所属组' })
  dicGroup?: string;
  /**
   * 模块名
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicModule', type: 'string', description: '模块名' })
  dicModule?: string;
  /**
   * 父级ID
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicParentId', type: 'string', description: '父级ID' })
  dicParentId?: string;
  /**
   * 字典类型
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiProperty({ name: 'dicType', type: 'string', description: '字典类型' })
  dicType: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicOrder', type: 'number', description: '排序' })
  dicOrder?: number;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiProperty({ name: 'dicState', type: 'string', description: '状态' })
  dicState: string;
  /**
   * dicParams
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicParams', type: 'string', description: 'dicParams' })
  dicParams?: string;
  /**
   * 关联种属
   *
   * @type { string }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicSpecies', type: 'string', description: '关联种属' })
  dicSpecies?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicCreateDate', type: 'Date', description: '创建日期' })
  dicCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysDictionaryUDto
   */
  @ApiPropertyOptional({ name: 'dicUpdateDate', type: 'Date', description: '更新日期' })
  dicUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysDictionaryColNames)[] }
   * @memberof SysDictionaryUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysDictionaryColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysDictionaryUDto
   */
  @ApiProperty({ name: 'dicIdList', type: 'string[]', description: 'ID列表' })
  dicIdList?: string[];
}
