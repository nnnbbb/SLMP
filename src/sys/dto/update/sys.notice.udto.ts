import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysNoticeColNames } from '../../entity/ddl/sys.notice.cols';
/**
 * 用户提示消息表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysNoticeUDto
 * @class SysNoticeUDto
 */
@ApiTags('SYS_NOTICE表的UDTO对象')
export class SysNoticeUDto {
  /**
   * 消息编号
   *
   * @type { string }
   * @memberof SysNoticeUDto
   */
  @ApiProperty({ name: 'noticeId', type: 'string', description: '消息编号' })
  noticeId: string;
  /**
   * 消息标题
   *
   * @type { string }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeTitle', type: 'string', description: '消息标题' })
  noticeTitle?: string;
  /**
   * 消息内容
   *
   * @type { string }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeContent', type: 'string', description: '消息内容' })
  noticeContent?: string;
  /**
   * 消息发布者
   *
   * @type { string }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeManager', type: 'string', description: '消息发布者' })
  noticeManager?: string;
  /**
   * 是否是新消息
   *
   * @type { string }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeIsNew', type: 'string', description: '是否是新消息' })
  noticeIsNew?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeCreateDate', type: 'Date', description: '创建日期' })
  noticeCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeUpdateDate', type: 'Date', description: '更新日期' })
  noticeUpdateDate?: Date;
  /**
   * 消息接收者
   *
   * @type { string }
   * @memberof SysNoticeUDto
   */
  @ApiPropertyOptional({ name: 'noticeReceiver', type: 'string', description: '消息接收者' })
  noticeReceiver?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysNoticeColNames)[] }
   * @memberof SysNoticeUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysNoticeColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysNoticeUDto
   */
  @ApiProperty({ name: 'noticeIdList', type: 'string[]', description: 'ID列表' })
  noticeIdList?: string[];
}
