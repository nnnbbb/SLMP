import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysTipColNames } from '../../entity/ddl/sys.tip.cols';
/**
 * SYS_TIP表的UDTO对象
 * @date 12/30/2020, 3:09:40 PM
 * @author jiangbin
 * @export SysTipUDto
 * @class SysTipUDto
 */
@ApiTags('SYS_TIP表的UDTO对象')
export class SysTipUDto {
  /**
   * 提示信息ID
   *
   * @type { string }
   * @memberof SysTipUDto
   */
  @ApiProperty({ name: 'tipId', type: 'string', description: '提示信息ID' })
  tipId: string;
  /**
   * 提示次数
   *
   * @type { number }
   * @memberof SysTipUDto
   */
  @ApiPropertyOptional({ name: 'tipCount', type: 'number', description: '提示次数' })
  tipCount?: number;
  /**
   * 提示信息内容
   *
   * @type { string }
   * @memberof SysTipUDto
   */
  @ApiPropertyOptional({ name: 'tipContents', type: 'string', description: '提示信息内容' })
  tipContents?: string;
  /**
   * 所属者
   *
   * @type { string }
   * @memberof SysTipUDto
   */
  @ApiPropertyOptional({ name: 'tipManager', type: 'string', description: '所属者' })
  tipManager?: string;
  /**
   * 消息受众
   *
   * @type { string }
   * @memberof SysTipUDto
   */
  @ApiPropertyOptional({ name: 'tipAudience', type: 'string', description: '消息受众' })
  tipAudience?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysTipUDto
   */
  @ApiPropertyOptional({ name: 'tipCreateDate', type: 'Date', description: '创建日期' })
  tipCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysTipUDto
   */
  @ApiPropertyOptional({ name: 'tipUpdateDate', type: 'Date', description: '更新日期' })
  tipUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysTipColNames)[] }
   * @memberof SysTipUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysTipColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysTipUDto
   */
  @ApiProperty({ name: 'tipIdList', type: 'string[]', description: 'ID列表' })
  tipIdList?: string[];
}
