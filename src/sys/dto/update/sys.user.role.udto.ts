import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysUserRoleColNames } from '../../entity/ddl/sys.user.role.cols';
/**
 * SYS_USER_ROLE表的UDTO对象
 * @date 12/30/2020, 3:09:40 PM
 * @author jiangbin
 * @export SysUserRoleUDto
 * @class SysUserRoleUDto
 */
@ApiTags('SYS_USER_ROLE表的UDTO对象')
export class SysUserRoleUDto {
  /**
   * 用户角色ID
   *
   * @type { string }
   * @memberof SysUserRoleUDto
   */
  @ApiProperty({ name: 'urId', type: 'string', description: '用户角色ID' })
  urId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysUserRoleUDto
   */
  @ApiProperty({ name: 'urRoleId', type: 'string', description: '角色ID' })
  urRoleId: string;
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUserRoleUDto
   */
  @ApiProperty({ name: 'urUserLoginName', type: 'string', description: '用户帐号' })
  urUserLoginName: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserRoleColNames)[] }
   * @memberof SysUserRoleUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserRoleColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysUserRoleUDto
   */
  @ApiProperty({ name: 'urIdList', type: 'string[]', description: 'ID列表' })
  urIdList?: string[];
}
