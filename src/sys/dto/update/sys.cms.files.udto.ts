import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysCmsFilesColNames } from '../../entity/ddl/sys.cms.files.cols';
/**
 * SYS_CMS_FILES表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysCmsFilesUDto
 * @class SysCmsFilesUDto
 */
@ApiTags('SYS_CMS_FILES表的UDTO对象')
export class SysCmsFilesUDto {
  /**
   * 文件编号
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiProperty({ name: 'scFileId', type: 'string', description: '文件编号' })
  scFileId: string;
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFileName', type: 'string', description: '文件名称' })
  scFileName?: string;
  /**
   * scFileNameCn
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFileNameCn', type: 'string', description: 'scFileNameCn' })
  scFileNameCn?: string;
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiProperty({ name: 'scFileType', type: 'string', description: '文件类型' })
  scFileType: string;
  /**
   * 文件路径
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFilePath', type: 'string', description: '文件路径' })
  scFilePath?: string;
  /**
   * 是否是最新
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFileIsNew', type: 'string', description: '是否是最新' })
  scFileIsNew?: string;
  /**
   * 文件上传者
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFileManager', type: 'string', description: '文件上传者' })
  scFileManager?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFileCreateDate', type: 'Date', description: '创建日期' })
  scFileCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFielUpdateDate', type: 'Date', description: '更新日期' })
  scFielUpdateDate?: Date;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysCmsFilesUDto
   */
  @ApiPropertyOptional({ name: 'scFileSpecies', type: 'string', description: '种属' })
  scFileSpecies?: string;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysCmsFilesColNames)[] }
   * @memberof SysCmsFilesUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysCmsFilesColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysCmsFilesUDto
   */
  @ApiProperty({ name: 'scFileIdList', type: 'string[]', description: 'ID列表' })
  scFileIdList?: string[];
}
