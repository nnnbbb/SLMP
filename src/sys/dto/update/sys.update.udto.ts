import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysUpdateColNames } from '../../entity/ddl/sys.update.cols';
/**
 * SYS_UPDATE表的UDTO对象
 * @date 12/30/2020, 3:09:40 PM
 * @author jiangbin
 * @export SysUpdateUDto
 * @class SysUpdateUDto
 */
@ApiTags('SYS_UPDATE表的UDTO对象')
export class SysUpdateUDto {
  /**
   * 系统升级日志主键ID
   *
   * @type { string }
   * @memberof SysUpdateUDto
   */
  @ApiProperty({ name: 'updateId', type: 'string', description: '系统升级日志主键ID' })
  updateId: string;
  /**
   * 升级版本号
   *
   * @type { string }
   * @memberof SysUpdateUDto
   */
  @ApiPropertyOptional({ name: 'updateVersion', type: 'string', description: '升级版本号' })
  updateVersion?: string;
  /**
   * 升级内容
   *
   * @type { string }
   * @memberof SysUpdateUDto
   */
  @ApiPropertyOptional({ name: 'updateContent', type: 'string', description: '升级内容' })
  updateContent?: string;
  /**
   * 升级负责人
   *
   * @type { string }
   * @memberof SysUpdateUDto
   */
  @ApiPropertyOptional({ name: 'updateManager', type: 'string', description: '升级负责人' })
  updateManager?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUpdateUDto
   */
  @ApiPropertyOptional({ name: 'updateCreateTime', type: 'Date', description: '创建日期' })
  updateCreateTime?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysUpdateColNames)[] }
   * @memberof SysUpdateUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUpdateColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysUpdateUDto
   */
  @ApiProperty({ name: 'updateIdList', type: 'string[]', description: 'ID列表' })
  updateIdList?: string[];
}
