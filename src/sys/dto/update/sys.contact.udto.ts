import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysContactColNames } from '../../entity/ddl/sys.contact.cols';
/**
 * SYS_CONTACT表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysContactUDto
 * @class SysContactUDto
 */
@ApiTags('SYS_CONTACT表的UDTO对象')
export class SysContactUDto {
  /**
   * 反馈编号
   *
   * @type { string }
   * @memberof SysContactUDto
   */
  @ApiProperty({ name: 'contactId', type: 'string', description: '反馈编号' })
  contactId: string;
  /**
   * 反馈用户
   *
   * @type { string }
   * @memberof SysContactUDto
   */
  @ApiPropertyOptional({ name: 'contactAuthor', type: 'string', description: '反馈用户' })
  contactAuthor?: string;
  /**
   * 反馈邮箱
   *
   * @type { string }
   * @memberof SysContactUDto
   */
  @ApiPropertyOptional({ name: 'contactEmail', type: 'string', description: '反馈邮箱' })
  contactEmail?: string;
  /**
   * 反馈主题
   *
   * @type { string }
   * @memberof SysContactUDto
   */
  @ApiPropertyOptional({ name: 'contactSubject', type: 'string', description: '反馈主题' })
  contactSubject?: string;
  /**
   * 反馈内容
   *
   * @type { string }
   * @memberof SysContactUDto
   */
  @ApiProperty({ name: 'contactText', type: 'string', description: '反馈内容' })
  contactText: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysContactUDto
   */
  @ApiProperty({ name: 'contactCreateDate', type: 'Date', description: '创建日期' })
  contactCreateDate: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysContactColNames)[] }
   * @memberof SysContactUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysContactColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysContactUDto
   */
  @ApiProperty({ name: 'contactIdList', type: 'string[]', description: 'ID列表' })
  contactIdList?: string[];
}
