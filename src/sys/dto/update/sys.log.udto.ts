import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysLogColNames } from '../../entity/ddl/sys.log.cols';
/**
 * SYS_LOG表的UDTO对象
 * @date 12/30/2020, 3:09:39 PM
 * @author jiangbin
 * @export SysLogUDto
 * @class SysLogUDto
 */
@ApiTags('SYS_LOG表的UDTO对象')
export class SysLogUDto {
  /**
   * 日志ID号
   *
   * @type { string }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logId', type: 'string', description: '日志ID号' })
  logId: string;
  /**
   * 操作人员名称
   *
   * @type { string }
   * @memberof SysLogUDto
   */
  @ApiPropertyOptional({ name: 'logUser', type: 'string', description: '操作人员名称' })
  logUser?: string;
  /**
   * 操作时间
   *
   * @type { Date }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logTime', type: 'Date', description: '操作时间' })
  logTime: Date;
  /**
   * IP地址
   *
   * @type { string }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logIp', type: 'string', description: 'IP地址' })
  logIp: string;
  /**
   * 操作URL
   *
   * @type { string }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logUrl', type: 'string', description: '操作URL' })
  logUrl: string;
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logTitle', type: 'string', description: '模块' })
  logTitle: string;
  /**
   * 内容
   *
   * @type { string }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logContent', type: 'string', description: '内容' })
  logContent: string;
  /**
   * 操作类型
   *
   * @type { number }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logType', type: 'number', description: '操作类型' })
  logType: number;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysLogColNames)[] }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysLogColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysLogUDto
   */
  @ApiProperty({ name: 'logIdList', type: 'string[]', description: 'ID列表' })
  logIdList?: string[];
}
