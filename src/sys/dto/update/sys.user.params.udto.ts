import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
import { SysUserParamsColNames } from '../../entity/ddl/sys.user.params.cols';

/**
 * 用户参数表表的UDTO对象
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export SysUserParamsUDto
 * @class SysUserParamsUDto
 */
@ApiTags('SYS_USER_PARAMS表的UDTO对象')
export class SysUserParamsUDto {
  /**
   * 主键
   *
   * @type { string }
   * @memberof SysUserParamsUDto
   */
  @ApiProperty({ name: 'upId', type: 'string', description: '主键' })
  upId: string;
  /**
   * 帐号
   *
   * @type { string }
   * @memberof SysUserParamsUDto
   */
  @ApiPropertyOptional({ name: 'upLoginName', type: 'string', description: '帐号' })
  upLoginName?: string;
  /**
   * 参数名
   *
   * @type { string }
   * @memberof SysUserParamsUDto
   */
  @ApiPropertyOptional({ name: 'upParamName', type: 'string', description: '参数名' })
  upParamName?: string;
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysUserParamsUDto
   */
  @ApiPropertyOptional({ name: 'upParamValue', type: 'string', description: '参数值' })
  upParamValue?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserParamsUDto
   */
  @ApiPropertyOptional({ name: 'upCreateDate', type: 'Date', description: '创建日期' })
  upCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUserParamsUDto
   */
  @ApiPropertyOptional({ name: 'upUpdateDate', type: 'Date', description: '更新日期' })
  upUpdateDate?: Date;
  /**
   * 更新列限定条件,在只想更新部分列数据时可以给定本参数
   *
   * @type { (keyof SysUserParamsColNames)[] }
   * @memberof SysUserParamsUDto
   */
  @ApiProperty({ name: 'cols', type: 'string[]', description: '列名列表' })
  cols?: (keyof SysUserParamsColNames)[];
  /**
   * ID列表
   *
   * @type { string[] }
   * @memberof SysUserParamsUDto
   */
  @ApiProperty({ name: 'upIdList', type: 'string[]', description: 'ID列表' })
  upIdList?: string[];
}
