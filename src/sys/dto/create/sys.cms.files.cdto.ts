import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_CMS_FILES CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysCmsFilesCDto
 * @class SysCmsFilesCDto
 */
@ApiTags('SYS_CMS_FILES表的CDTO对象')
export class SysCmsFilesCDto {
  /**
   * 文件编号
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiProperty({ name: 'scFileId', type: 'string', description: '文件编号' })
  scFileId: string;
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFileName', type: 'string', description: '文件名称' })
  scFileName?: string;
  /**
   * scFileNameCn
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFileNameCn', type: 'string', description: 'scFileNameCn' })
  scFileNameCn?: string;
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiProperty({ name: 'scFileType', type: 'string', description: '文件类型' })
  scFileType: string;
  /**
   * 文件路径
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFilePath', type: 'string', description: '文件路径' })
  scFilePath?: string;
  /**
   * 是否是最新
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFileIsNew', type: 'string', description: '是否是最新' })
  scFileIsNew?: string;
  /**
   * 文件上传者
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFileManager', type: 'string', description: '文件上传者' })
  scFileManager?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFileCreateDate', type: 'Date', description: '创建日期' })
  scFileCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFielUpdateDate', type: 'Date', description: '更新日期' })
  scFielUpdateDate?: Date;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysCmsFilesCDto
   */
  @ApiPropertyOptional({ name: 'scFileSpecies', type: 'string', description: '种属' })
  scFileSpecies?: string;
}
