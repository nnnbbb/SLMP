import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 文件管理 CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysFilesCDto
 * @class SysFilesCDto
 */
@ApiTags('SYS_FILES表的CDTO对象')
export class SysFilesCDto {
  /**
   * 记录ID
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileId', type: 'string', description: '记录ID' })
  fileId: string;
  /**
   * 文件条码号
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileBarcode', type: 'string', description: '文件条码号' })
  fileBarcode?: string;
  /**
   * 关联的其它记录信息ID号
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileRelateId', type: 'string', description: '关联的其它记录信息ID号' })
  fileRelateId?: string;
  /**
   * 文件名称
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileName', type: 'string', description: '文件名称' })
  fileName: string;
  /**
   * 文件名
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileNameEn', type: 'string', description: '文件名' })
  fileNameEn?: string;
  /**
   * 文件存储路径
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'filePath', type: 'string', description: '文件存储路径' })
  filePath: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileCreateDate', type: 'Date', description: '创建日期' })
  fileCreateDate: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileUpdateDate', type: 'Date', description: '更新日期' })
  fileUpdateDate: Date;
  /**
   * 是否被启用
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileIsUsed', type: 'string', description: '是否被启用' })
  fileIsUsed: string;
  /**
   * 所属用户
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileOwner', type: 'string', description: '所属用户' })
  fileOwner: string;
  /**
   * 文件大小
   *
   * @type { number }
   * @memberof SysFilesCDto
   */
  @ApiProperty({ name: 'fileSize', type: 'number', description: '文件大小' })
  fileSize: number;
  /**
   * 用户权限表
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileGrants', type: 'string', description: '用户权限表' })
  fileGrants?: string;
  /**
   * 文件分类
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileClasses', type: 'string', description: '文件分类' })
  fileClasses?: string;
  /**
   * 文件状态信息
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileState', type: 'string', description: '文件状态信息' })
  fileState?: string;
  /**
   * 文件类型
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileType', type: 'string', description: '文件类型' })
  fileType?: string;
  /**
   * 文件备注信息
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileComments', type: 'string', description: '文件备注信息' })
  fileComments?: string;
  /**
   * 种属
   *
   * @type { string }
   * @memberof SysFilesCDto
   */
  @ApiPropertyOptional({ name: 'fileSpecies', type: 'string', description: '种属' })
  fileSpecies?: string;
}
