import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_CONTACT CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysContactCDto
 * @class SysContactCDto
 */
@ApiTags('SYS_CONTACT表的CDTO对象')
export class SysContactCDto {
  /**
   * 反馈编号
   *
   * @type { string }
   * @memberof SysContactCDto
   */
  @ApiProperty({ name: 'contactId', type: 'string', description: '反馈编号' })
  contactId: string;
  /**
   * 反馈用户
   *
   * @type { string }
   * @memberof SysContactCDto
   */
  @ApiPropertyOptional({ name: 'contactAuthor', type: 'string', description: '反馈用户' })
  contactAuthor?: string;
  /**
   * 反馈邮箱
   *
   * @type { string }
   * @memberof SysContactCDto
   */
  @ApiPropertyOptional({ name: 'contactEmail', type: 'string', description: '反馈邮箱' })
  contactEmail?: string;
  /**
   * 反馈主题
   *
   * @type { string }
   * @memberof SysContactCDto
   */
  @ApiPropertyOptional({ name: 'contactSubject', type: 'string', description: '反馈主题' })
  contactSubject?: string;
  /**
   * 反馈内容
   *
   * @type { string }
   * @memberof SysContactCDto
   */
  @ApiProperty({ name: 'contactText', type: 'string', description: '反馈内容' })
  contactText: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysContactCDto
   */
  @ApiProperty({ name: 'contactCreateDate', type: 'Date', description: '创建日期' })
  contactCreateDate: Date;
}
