import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_ROLE CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysRoleCDto
 * @class SysRoleCDto
 */
@ApiTags('SYS_ROLE表的CDTO对象')
export class SysRoleCDto {
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleCDto
   */
  @ApiProperty({ name: 'roleId', type: 'string', description: '角色ID' })
  roleId: string;
  /**
   * 角色名称
   *
   * @type { string }
   * @memberof SysRoleCDto
   */
  @ApiPropertyOptional({ name: 'roleName', type: 'string', description: '角色名称' })
  roleName?: string;
  /**
   * roleNameEn
   *
   * @type { string }
   * @memberof SysRoleCDto
   */
  @ApiPropertyOptional({ name: 'roleNameEn', type: 'string', description: 'roleNameEn' })
  roleNameEn?: string;
  /**
   * 状态
   *
   * @type { string }
   * @memberof SysRoleCDto
   */
  @ApiPropertyOptional({ name: 'roleState', type: 'string', description: '状态' })
  roleState?: string;
  /**
   * 角色所属系统
   *
   * @type { string }
   * @memberof SysRoleCDto
   */
  @ApiPropertyOptional({ name: 'roleSys', type: 'string', description: '角色所属系统' })
  roleSys?: string;
}
