import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_CMS_NOTICE CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysCmsNoticeCDto
 * @class SysCmsNoticeCDto
 */
@ApiTags('SYS_CMS_NOTICE表的CDTO对象')
export class SysCmsNoticeCDto {
  /**
   * 公告编号
   *
   * @type { string }
   * @memberof SysCmsNoticeCDto
   */
  @ApiProperty({ name: 'scNoticeId', type: 'string', description: '公告编号' })
  scNoticeId: string;
  /**
   * 公告标题
   *
   * @type { string }
   * @memberof SysCmsNoticeCDto
   */
  @ApiPropertyOptional({ name: 'scNoticeTitle', type: 'string', description: '公告标题' })
  scNoticeTitle?: string;
  /**
   * 公告内容
   *
   * @type { string }
   * @memberof SysCmsNoticeCDto
   */
  @ApiPropertyOptional({ name: 'scNoticeContent', type: 'string', description: '公告内容' })
  scNoticeContent?: string;
  /**
   * 是否是新文件
   *
   * @type { string }
   * @memberof SysCmsNoticeCDto
   */
  @ApiPropertyOptional({ name: 'scNoticeIsNew', type: 'string', description: '是否是新文件' })
  scNoticeIsNew?: string;
  /**
   * 公告发布者
   *
   * @type { string }
   * @memberof SysCmsNoticeCDto
   */
  @ApiPropertyOptional({ name: 'scNoticeManager', type: 'string', description: '公告发布者' })
  scNoticeManager?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysCmsNoticeCDto
   */
  @ApiPropertyOptional({ name: 'scNoticeCreateDate', type: 'Date', description: '创建日期' })
  scNoticeCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysCmsNoticeCDto
   */
  @ApiPropertyOptional({ name: 'scNoticeUpdateDate', type: 'Date', description: '更新日期' })
  scNoticeUpdateDate?: Date;
}
