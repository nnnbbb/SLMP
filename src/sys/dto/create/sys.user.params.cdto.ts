import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 用户参数表 CDTO对象
 * @date 4/3/2021, 11:06:45 AM
 * @author jiangbin
 * @export SysUserParamsCDto
 * @class SysUserParamsCDto
 */
@ApiTags('SYS_USER_PARAMS表的CDTO对象')
export class SysUserParamsCDto {
  /**
   * 主键
   *
   * @type { string }
   * @memberof SysUserParamsCDto
   */
  @ApiProperty({ name: 'upId', type: 'string', description: '主键' })
  upId: string;
  /**
   * 帐号
   *
   * @type { string }
   * @memberof SysUserParamsCDto
   */
  @ApiPropertyOptional({ name: 'upLoginName', type: 'string', description: '帐号' })
  upLoginName?: string;
  /**
   * 参数名
   *
   * @type { string }
   * @memberof SysUserParamsCDto
   */
  @ApiPropertyOptional({ name: 'upParamName', type: 'string', description: '参数名' })
  upParamName?: string;
  /**
   * 参数值
   *
   * @type { string }
   * @memberof SysUserParamsCDto
   */
  @ApiPropertyOptional({ name: 'upParamValue', type: 'string', description: '参数值' })
  upParamValue?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysUserParamsCDto
   */
  @ApiPropertyOptional({ name: 'upCreateDate', type: 'Date', description: '创建日期' })
  upCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysUserParamsCDto
   */
  @ApiPropertyOptional({ name: 'upUpdateDate', type: 'Date', description: '更新日期' })
  upUpdateDate?: Date;
}
