import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_USER_ROLE CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysUserRoleCDto
 * @class SysUserRoleCDto
 */
@ApiTags('SYS_USER_ROLE表的CDTO对象')
export class SysUserRoleCDto {
  /**
   * 用户角色ID
   *
   * @type { string }
   * @memberof SysUserRoleCDto
   */
  @ApiProperty({ name: 'urId', type: 'string', description: '用户角色ID' })
  urId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysUserRoleCDto
   */
  @ApiProperty({ name: 'urRoleId', type: 'string', description: '角色ID' })
  urRoleId: string;
  /**
   * 用户帐号
   *
   * @type { string }
   * @memberof SysUserRoleCDto
   */
  @ApiProperty({ name: 'urUserLoginName', type: 'string', description: '用户帐号' })
  urUserLoginName: string;
}
