import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_PERMISSION CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysPermissionCDto
 * @class SysPermissionCDto
 */
@ApiTags('SYS_PERMISSION表的CDTO对象')
export class SysPermissionCDto {
  /**
   * 权限ID
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiProperty({ name: 'perId', type: 'string', description: '权限ID' })
  perId: string;
  /**
   * 权限名称
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiProperty({ name: 'perName', type: 'string', description: '权限名称' })
  perName: string;
  /**
   * 权限英文名
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perNameEn', type: 'string', description: '权限英文名' })
  perNameEn?: string;
  /**
   * 权限类型
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiProperty({ name: 'perType', type: 'string', description: '权限类型' })
  perType: string;
  /**
   * 权限URL
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perUrl', type: 'string', description: '权限URL' })
  perUrl?: string;
  /**
   * 权限访问方法
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perMethod', type: 'string', description: '权限访问方法' })
  perMethod?: string;
  /**
   * 父节点ID
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perParentId', type: 'string', description: '父节点ID' })
  perParentId?: string;
  /**
   * 排序
   *
   * @type { number }
   * @memberof SysPermissionCDto
   */
  @ApiProperty({ name: 'perOrder', type: 'number', description: '排序' })
  perOrder: number;
  /**
   * 备注信息
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perRemark', type: 'string', description: '备注信息' })
  perRemark?: string;
  /**
   * 状态信息
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiProperty({ name: 'perState', type: 'string', description: '状态信息' })
  perState: string;
  /**
   * 所属系统：前端
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perSystem', type: 'string', description: '所属系统：前端' })
  perSystem?: string;
  /**
   * 模块
   *
   * @type { string }
   * @memberof SysPermissionCDto
   */
  @ApiPropertyOptional({ name: 'perModule', type: 'string', description: '模块' })
  perModule?: string;
}
