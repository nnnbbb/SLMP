import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * SYS_ROLE_MENU CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysRoleMenuCDto
 * @class SysRoleMenuCDto
 */
@ApiTags('SYS_ROLE_MENU表的CDTO对象')
export class SysRoleMenuCDto {
  /**
   * 角色菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenuCDto
   */
  @ApiProperty({ name: 'rmId', type: 'string', description: '角色菜单ID' })
  rmId: string;
  /**
   * 角色ID
   *
   * @type { string }
   * @memberof SysRoleMenuCDto
   */
  @ApiProperty({ name: 'rmRoleId', type: 'string', description: '角色ID' })
  rmRoleId: string;
  /**
   * 菜单ID
   *
   * @type { string }
   * @memberof SysRoleMenuCDto
   */
  @ApiProperty({ name: 'rmMenuId', type: 'string', description: '菜单ID' })
  rmMenuId: string;
}
