import { ApiProperty, ApiPropertyOptional, ApiTags } from '@nestjs/swagger';
/**
 * 用户提示消息 CDTO对象
 * @date 12/28/2020, 4:11:32 PM
 * @author jiangbin
 * @export SysNoticeCDto
 * @class SysNoticeCDto
 */
@ApiTags('SYS_NOTICE表的CDTO对象')
export class SysNoticeCDto {
  /**
   * 消息编号
   *
   * @type { string }
   * @memberof SysNoticeCDto
   */
  @ApiProperty({ name: 'noticeId', type: 'string', description: '消息编号' })
  noticeId: string;
  /**
   * 消息标题
   *
   * @type { string }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeTitle', type: 'string', description: '消息标题' })
  noticeTitle?: string;
  /**
   * 消息内容
   *
   * @type { string }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeContent', type: 'string', description: '消息内容' })
  noticeContent?: string;
  /**
   * 消息发布者
   *
   * @type { string }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeManager', type: 'string', description: '消息发布者' })
  noticeManager?: string;
  /**
   * 是否是新消息
   *
   * @type { string }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeIsNew', type: 'string', description: '是否是新消息' })
  noticeIsNew?: string;
  /**
   * 创建日期
   *
   * @type { Date }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeCreateDate', type: 'Date', description: '创建日期' })
  noticeCreateDate?: Date;
  /**
   * 更新日期
   *
   * @type { Date }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeUpdateDate', type: 'Date', description: '更新日期' })
  noticeUpdateDate?: Date;
  /**
   * 消息接收者
   *
   * @type { string }
   * @memberof SysNoticeCDto
   */
  @ApiPropertyOptional({ name: 'noticeReceiver', type: 'string', description: '消息接收者' })
  noticeReceiver?: string;
}
