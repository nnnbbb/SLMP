import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { SysCmsFilesController } from './controller/sys.cms.files.controller';
import { SysCmsFilesService } from './service/sys.cms.files.service';
import { SysCmsFiles } from './entity/sys.cms.files.entity';
import { SysCmsNoticeController } from './controller/sys.cms.notice.controller';
import { SysCmsNoticeService } from './service/sys.cms.notice.service';
import { SysCmsNotice } from './entity/sys.cms.notice.entity';
import { SysConfigController } from './controller/sys.config.controller';
import { SysConfigService } from './service/sys.config.service';
import { SysConfig } from './entity/sys.config.entity';
import { SysContactController } from './controller/sys.contact.controller';
import { SysContactService } from './service/sys.contact.service';
import { SysContact } from './entity/sys.contact.entity';
import { SysDictionaryController } from './controller/sys.dictionary.controller';
import { SysDictionaryService } from './service/sys.dictionary.service';
import { SysDictionary } from './entity/sys.dictionary.entity';
import { SysFilesController } from './controller/sys.files.controller';
import { SysFilesService } from './service/sys.files.service';
import { SysFiles } from './entity/sys.files.entity';
import { SysLogController } from './controller/sys.log.controller';
import { SysLogService } from './service/sys.log.service';
import { SysLog } from './entity/sys.log.entity';
import { SysMemoryController } from './controller/sys.memory.controller';
import { SysMemoryService } from './service/sys.memory.service';
import { SysMemory } from './entity/sys.memory.entity';
import { SysMenuController } from './controller/sys.menu.controller';
import { SysMenuService } from './service/sys.menu.service';
import { SysMenu } from './entity/sys.menu.entity';
import { SysNoticeController } from './controller/sys.notice.controller';
import { SysNoticeService } from './service/sys.notice.service';
import { SysNotice } from './entity/sys.notice.entity';
import { SysPermissionController } from './controller/sys.permission.controller';
import { SysPermissionService } from './service/sys.permission.service';
import { SysPermission } from './entity/sys.permission.entity';
import { SysRoleController } from './controller/sys.role.controller';
import { SysRoleService } from './service/sys.role.service';
import { SysRole } from './entity/sys.role.entity';
import { SysRoleDictionaryController } from './controller/sys.role.dictionary.controller';
import { SysRoleDictionaryService } from './service/sys.role.dictionary.service';
import { SysRoleDictionary } from './entity/sys.role.dictionary.entity';
import { SysRoleMenuController } from './controller/sys.role.menu.controller';
import { SysRoleMenuService } from './service/sys.role.menu.service';
import { SysRoleMenu } from './entity/sys.role.menu.entity';
import { SysRolePermissionController } from './controller/sys.role.permission.controller';
import { SysRolePermissionService } from './service/sys.role.permission.service';
import { SysRolePermission } from './entity/sys.role.permission.entity';
import { SysTipController } from './controller/sys.tip.controller';
import { SysTipService } from './service/sys.tip.service';
import { SysTip } from './entity/sys.tip.entity';
import { SysUpdateController } from './controller/sys.update.controller';
import { SysUpdateService } from './service/sys.update.service';
import { SysUpdate } from './entity/sys.update.entity';
import { SysUserController } from './controller/sys.user.controller';
import { SysUserService } from './service/sys.user.service';
import { SysUser } from './entity/sys.user.entity';
import { SysUserRoleController } from './controller/sys.user.role.controller';
import { SysUserRoleService } from './service/sys.user.role.service';
import { SysUserRole } from './entity/sys.user.role.entity';
import { SysUserDetailQueryer } from './queryer/sys.user.detail.queryer';
import { SysUserParams } from './entity/sys.user.params.entity';
import { SysUserParamsController } from './controller/sys.user.params.controller';
import { SysUserParamsService } from './service/sys.user.params.service';

@Global()
@Module({
  imports: [
    ScheduleModule.forRoot(),
    EventEmitterModule.forRoot(),
    TypeOrmModule.forFeature([
      SysCmsFiles,
      SysCmsNotice,
      SysConfig,
      SysContact,
      SysDictionary,
      SysFiles,
      SysLog,
      SysMemory,
      SysMenu,
      SysNotice,
      SysPermission,
      SysRole,
      SysRoleDictionary,
      SysRoleMenu,
      SysRolePermission,
      SysTip,
      SysUpdate,
      SysUserParams,
      SysUser,
      SysUserRole,
    ]),
  ],
  controllers: [
    SysCmsFilesController,
    SysCmsNoticeController,
    SysConfigController,
    SysContactController,
    SysDictionaryController,
    SysFilesController,
    SysLogController,
    SysMemoryController,
    SysMenuController,
    SysNoticeController,
    SysPermissionController,
    SysRoleController,
    SysRoleDictionaryController,
    SysRoleMenuController,
    SysRolePermissionController,
    SysTipController,
    SysUpdateController,
    SysUserController,
    SysUserParamsController,
    SysUserRoleController,
  ],
  providers: [
    SysCmsFilesService,
    SysCmsNoticeService,
    SysConfigService,
    SysContactService,
    SysDictionaryService,
    SysFilesService,
    SysLogService,
    SysMemoryService,
    SysMenuService,
    SysNoticeService,
    SysPermissionService,
    SysRoleService,
    SysRoleDictionaryService,
    SysRoleMenuService,
    SysRolePermissionService,
    SysTipService,
    SysUpdateService,
    SysUserService,
    SysUserParamsService,
    SysUserRoleService,
    SysUserDetailQueryer,
  ],
  exports: [
    SysCmsFilesService,
    SysCmsNoticeService,
    SysConfigService,
    SysContactService,
    SysDictionaryService,
    SysFilesService,
    SysLogService,
    SysMemoryService,
    SysMenuService,
    SysNoticeService,
    SysPermissionService,
    SysRoleService,
    SysRoleDictionaryService,
    SysRoleMenuService,
    SysRolePermissionService,
    SysTipService,
    SysUpdateService,
    SysUserService,
    SysUserParamsService,
    SysUserRoleService,
    SysUserDetailQueryer,
  ],
})
export class SysModule {}
